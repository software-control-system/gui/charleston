package fr.soleil.bean.utils;

import fr.soleil.comete.swing.ImageViewer;

public class CharlestonImageViewer extends ImageViewer {

    private static final long serialVersionUID = 8515867151335114101L;

    public CharlestonImageViewer() {
        super();
        // setAlwaysFitMaxSize(true);
        setCleanOnDataSetting(false);
    }
}
