package fr.soleil.bean.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.scalar.INumberTarget;

public class ProfileViewer extends JPanel implements ActionListener {

    private static final long serialVersionUID = -7623083640402833779L;

    protected MyChartViewer profileViewer;
    protected TangoKey profile;
    protected TangoKey profileFitted;
    protected TangoKey profileError;
    protected ChartViewerBox chartbox;

    protected JPanel profileButtonsPanel;
    protected JScrollPane profileButtonsScrollPane;
    protected ButtonGroup profileGroup;
    protected JRadioButton profileRadio;
    protected JRadioButton profileFittedRadio;
    protected JRadioButton profileErrorRadio;

    protected String profileTitle = "profile";
    protected String profileFittedTitle = "profileFitted";
    protected String profileErrorTitle = "profileError";

    protected final static CometeColor profileColor = CometeColor.BLUE;
    protected final static CometeColor profileFittedColor = CometeColor.GREEN;
    protected final static CometeColor profileErrorColor = CometeColor.RED;

    protected final static int PROFILE_CASE = 0;
    protected final static int PROFILE_FITTED_CASE = 1;
    protected final static int PROFILE_ERROR_CASE = 2;
    private double pixelSize;

    private TangoKey profilePixelSizeKey;
    private final NumberScalarBox numberBox;
    private final INumberTarget localTarget;

    public ProfileViewer(String filePropertiesName) {
        super(new BorderLayout());
        pixelSize = 1;
        numberBox = new NumberScalarBox();
        localTarget = new INumberTarget() {
            @Override
            public Number getNumberValue() {
                return null;
            }

            @Override
            public void addMediator(Mediator<?> mediator) {
            }

            @Override
            public void removeMediator(Mediator<?> mediator) {
            }

            @Override
            public void setNumberValue(Number value) {
                setPixelSize(value.doubleValue());
            }
        };
        profilePixelSizeKey = null;
        connectToPixelSize();
        chartbox = new ChartViewerBox();

        profileViewer = new MyChartViewer();

        // JIRA CONTROLGUI-14
        String directory = ImgBeamAnalyzer.getPropertiesDirectoryName();
        File file = ImgBeamAnalyzer.getPropertiesFile(directory, filePropertiesName);
        if (file.exists()) {
            ChartProperties chartProperties = ImgBeamAnalyzer.getChartProperties(file);
            profileViewer.setChartProperties(chartProperties);
        }

        profileViewer.setAutoHighlightOnLegend(true);
        profileViewer.setHeaderVisible(true);
        profileViewer.setManagementPanelVisible(true);
        profileViewer.setFreezePanelVisible(true);
        profileViewer.setMinimumSize(new Dimension(0, 0));
        profileViewer.setPreferredSize(new Dimension(0, 0));

        profileButtonsPanel = new JPanel(new GridBagLayout());

        profileGroup = new ButtonGroup();

        profileRadio = new JRadioButton(profileTitle);
        profileRadio.setSelected(true);
        profileRadio.setHorizontalAlignment(JRadioButton.LEFT);
        profileRadio.addActionListener(this);
        profileGroup.add(profileRadio);

        profileFittedRadio = new JRadioButton(profileTitle + " + " + profileFittedTitle);
        profileFittedRadio.setSelected(false);
        profileFittedRadio.setHorizontalAlignment(JRadioButton.CENTER);
        profileFittedRadio.addActionListener(this);
        profileGroup.add(profileFittedRadio);

        profileErrorRadio = new JRadioButton(profileErrorTitle);
        profileErrorRadio.setSelected(false);
        profileErrorRadio.setHorizontalAlignment(JRadioButton.RIGHT);
        profileErrorRadio.addActionListener(this);
        profileGroup.add(profileErrorRadio);

        GridBagConstraints profileConstraints = new GridBagConstraints();
        profileConstraints.fill = GridBagConstraints.HORIZONTAL;
        profileConstraints.gridx = 0;
        profileConstraints.gridy = 0;
        profileConstraints.weightx = 1.0d / 3.0d;

        GridBagConstraints profileFittedConstraints = new GridBagConstraints();
        profileFittedConstraints.fill = GridBagConstraints.HORIZONTAL;
        profileFittedConstraints.gridx = 1;
        profileFittedConstraints.gridy = 0;
        profileFittedConstraints.weightx = 1.0d / 3.0d;

        GridBagConstraints profileErrorConstraints = new GridBagConstraints();
        profileErrorConstraints.fill = GridBagConstraints.HORIZONTAL;
        profileErrorConstraints.gridx = 2;
        profileErrorConstraints.gridy = 0;
        profileErrorConstraints.weightx = 1.0d / 3.0d;

        profileButtonsPanel.add(profileRadio, profileConstraints);
        profileButtonsPanel.add(profileFittedRadio, profileFittedConstraints);
        profileButtonsPanel.add(profileErrorRadio, profileErrorConstraints);

        // Ensure having buttons visible, even when horizontal scroll bar is displayed
        profileButtonsScrollPane = new JScrollPane(profileButtonsPanel);
        profileButtonsScrollPane.setMinimumSize(new Dimension(5, profileButtonsPanel.getPreferredSize().height + 5
                + profileButtonsScrollPane.getHorizontalScrollBar().getPreferredSize().height));
        profileButtonsScrollPane.setPreferredSize(new Dimension(profileButtonsPanel.getPreferredSize().width,
                profileButtonsScrollPane.getMinimumSize().height));

        this.add(profileViewer, BorderLayout.CENTER);
        this.add(profileButtonsScrollPane, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == profileRadio && profileRadio.isSelected()) {
            manageProfile(PROFILE_CASE);
        } else if (e.getSource() == profileFittedRadio && profileFittedRadio.isSelected()) {
            manageProfile(PROFILE_FITTED_CASE);
        } else if (e.getSource() == profileErrorRadio && profileErrorRadio.isSelected()) {
            manageProfile(PROFILE_ERROR_CASE);
        }
    }

    /**
     * @param profile
     *            the profile to set
     */
    public void setProfile(TangoKey profile) {
        if (this.profile != profile) {
            this.profile = profile;
            if (profileRadio.isSelected()) {
                manageProfile(PROFILE_CASE);
            } else if (profileFittedRadio.isSelected()) {
                manageProfile(PROFILE_FITTED_CASE);
            }
        }
    }

    /**
     * @param profileError
     *            the profileError to set
     */
    public void setProfileError(TangoKey profileError) {
        if (this.profileError != profileError) {
            this.profileError = profileError;
            if (profileErrorRadio.isSelected()) {
                manageProfile(PROFILE_ERROR_CASE);
            }
        }
    }

    /**
     * @param profileFitted
     *            the profileFitted to set
     */
    public void setProfileFitted(TangoKey profileFitted) {
        if (this.profileFitted != profileFitted) {
            this.profileFitted = profileFitted;
            if (profileFittedRadio.isSelected()) {
                manageProfile(PROFILE_FITTED_CASE);
            }
        }
    }

    /**
     * @return the profileErrorTitle
     */
    public String getProfileErrorTitle() {
        return profileErrorTitle;
    }

    /**
     * @param profileErrorTitle
     *            the profileErrorTitle to set
     */
    public void setProfileErrorTitle(String profileErrorTitle) {
        this.profileErrorTitle = profileErrorTitle;
        profileErrorRadio.setText(this.profileErrorTitle);
        if (profileErrorRadio.isSelected()) {
            profileViewer.setHeader(this.profileErrorTitle);
        }
    }

    /**
     * @return the profileFittedTitle
     */
    public String getProfileFittedTitle() {
        return profileFittedTitle;
    }

    /**
     * @param profileFittedTitle
     *            the profileFittedTitle to set
     */
    public void setProfileFittedTitle(String profileFittedTitle) {
        this.profileFittedTitle = profileFittedTitle;
        String computedTitle = this.profileTitle + " + " + this.profileFittedTitle;
        profileFittedRadio.setText(computedTitle);
        if (profileRadio.isSelected()) {
            profileViewer.setHeader(computedTitle);
        }
        computedTitle = null;
    }

    /**
     * @return the profileTitle
     */
    public String getProfileTitle() {
        return profileTitle;
    }

    /**
     * @param profileTitle
     *            the profileTitle to set
     */
    public void setProfileTitle(String profileTitle) {
        this.profileTitle = profileTitle;
        profileRadio.setText(this.profileTitle);
        String computedTitle = this.profileTitle + " + " + this.profileFittedTitle;
        profileFittedRadio.setText(computedTitle);
        computedTitle = null;
        if (profileRadio.isSelected()) {
            profileViewer.setHeader(this.profileTitle);
        }
    }

    /**
     * Sets the buttons panel visible or not
     * 
     * @param visible
     *            a boolean value
     */
    public void setButtonsVisible(boolean visible) {
        profileButtonsPanel.setVisible(visible);
    }

    public void setHeaderFont(Font font) {
        profileViewer.setHeaderCometeFont(FontTool.getCometeFont(font));
    }

    public void setGeneralBackground(Color bg) {
        super.setBackground(bg);
        profileViewer.setCometeBackground(ColorTool.getCometeColor(getBackground()));
        profileButtonsPanel.setBackground(getBackground());
    }

    protected void manageProfile(int toManage) {
        cleanProfileViewer();
        switch (toManage) {
            case PROFILE_CASE:
                cleanProfileFitted();
                cleanProfileError();
                if (profile != null) {
                    chartbox.connectWidget(profileViewer, profile);
                    profileViewer.setDataViewCometeColor(TangoKeyTool.getDeviceAndEntityLabel(profile), profileColor);
                }
                profileViewer.setHeader(profileTitle);

                break;
            case PROFILE_FITTED_CASE:
                cleanProfile();
                cleanProfileError();
                multiConnectChart(profile, profileFitted);
                profileViewer.setHeader(profileTitle + " + " + profileFittedTitle);
                break;
            case PROFILE_ERROR_CASE:
                cleanProfile();
                cleanProfileFitted();
                if (profileError != null) {
                    chartbox.connectWidget(profileViewer, profileError);
                    profileViewer.setDataViewCometeColor(TangoKeyTool.getDeviceAndEntityLabel(profileError),
                            profileErrorColor);
                }
                profileViewer.setHeader(profileErrorTitle);
                break;
            default:
                profileViewer.resetAll();
        }
        repaint();
    }

    protected void multiConnectChart(IKey... keys) {
        if (keys != null) {
            for (IKey key : keys) {
                if (key != null) {
                    chartbox.connectWidget(profileViewer, key);
                    String name = TangoKeyTool.getDeviceAndEntityLabel(key);
                    String subName = name;
                    int slashIndex = name.lastIndexOf("/");
                    if (slashIndex > -1) {
                        subName = name.substring(slashIndex + 1);
                    }
                    if (subName.indexOf("fit") > -1) {
                        profileViewer.setDataViewCometeColor(name, profileFittedColor);
                    } else {
                        profileViewer.setDataViewCometeColor(name, profileColor);
                    }
                }
            }
        }
    }

    protected void cleanProfile() {
        if ((profile != null) && (TangoKeyTool.isAnAttribute(profile))) {
            profileViewer.cleanDataViewProperties(TangoKeyTool.getDeviceAndEntityLabel(profile));
            profileViewer.resetAll();
        }
    }

    protected void cleanProfileFitted() {
        if (profileFitted != null) {
            String name = TangoKeyTool.getDeviceAndEntityLabel(profileFitted);
            profileViewer.cleanDataViewProperties(name.toLowerCase());
            profileViewer.resetAll();
        }
    }

    protected void cleanProfileError() {
        if ((profileError != null) && (TangoKeyTool.isAnAttribute(profileError))) {
            profileViewer.cleanDataViewProperties(TangoKeyTool.getDeviceAndEntityLabel(profileError));
            profileViewer.resetAll();
        }
    }

    protected void cleanProfileViewer() {
        chartbox.disconnectWidgetFromAll(profileViewer);
        profileViewer.setHeader(null);
    }

    private void connectToPixelSize() {
        numberBox.connectWidget(localTarget, profilePixelSizeKey);
    }

    private void setPixelSize(double value) {
        pixelSize = value;
    }

    public void setPixelSizeKey(TangoKey yProfilePixelSizeY) {
        this.profilePixelSizeKey = yProfilePixelSizeY;
        if (yProfilePixelSizeY == null) {
            disconnectFromPixelSize();
        } else {
            connectToPixelSize();
        }
    }

    public void disconnectFromPixelSize() {
        numberBox.disconnectWidgetFromAll(localTarget);
    }

    protected class MyChartViewer extends Chart {// JIRA CONTROLGUI-15

        private static final long serialVersionUID = 2557008085904878966L;

        private void modifyData(Map<String, Object> data) {
            if (data != null) {
                Object arrayObject = data.values().iterator().next();
                if (arrayObject != null) {
                    Object[] array = (Object[]) arrayObject;
                    double[] values = (double[]) array[0];

                    for (int i = 0; i < values.length; i++) {
                        values[i] = values[i] * pixelSize;
                    }
                }
            }
        }

        @Override
        public void addData(Map<String, Object> data) {
            modifyData(data);
            super.addData(data);
        }

        @Override
        public void setData(Map<String, Object> data) {
            modifyData(data);
            super.setData(data);
        }
    }

    public MyChartViewer getProfileViewer() {
        return profileViewer;
    }
}
