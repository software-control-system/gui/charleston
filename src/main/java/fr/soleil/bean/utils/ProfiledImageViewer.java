package fr.soleil.bean.utils;

import ij.gui.Line;

import java.awt.Rectangle;

import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.image.ijviewer.events.IIJViewerListener;
import fr.soleil.comete.swing.image.ijviewer.events.IJViewerEvent;

public class ProfiledImageViewer extends CharlestonImageComponent implements IIJViewerListener {

    private static final long serialVersionUID = -1215150481873794361L;

    protected ITextComponent xTextField = null;
    protected ITextComponent yTextField = null;
    protected ITextComponent widthTextField = null;
    protected ITextComponent heightTextField = null;

    protected ITextComponent profileX1TextField = null;
    protected ITextComponent profileX2TextField = null;
    protected ITextComponent profileY1TextField = null;
    protected ITextComponent profileY2TextField = null;

    public ProfiledImageViewer() {
        super();
        addViewerListener(this);
    }

    /**
     * This method transmits selection in NumberImageViewer to the x, y, width and height text
     * fields
     */
    protected void transmitToTextFields() {
        if (IJRoiManager.getRoiMode() == IJRoiManager.LINE_PROFILE_ROI_MODE) {
            Line profile = getLineRoi();
            if (profile != null) {
                if (profileX1TextField != null) {
                    profileX1TextField.setText(Integer.toString(profile.x1));
                }
                if (profileX2TextField != null) {
                    profileX2TextField.setText(Integer.toString(profile.x2));
                }
                if (profileY1TextField != null) {
                    profileY1TextField.setText(Integer.toString(profile.y1));
                }
                if (profileY2TextField != null) {
                    profileY2TextField.setText(Integer.toString(profile.y2));
                }
            }
        } else {
            Rectangle selection = getSelectionRectangle();
            if (selection != null) {
                if (xTextField != null) {
                    xTextField.setText(Integer.toString(selection.x));
                }
                if (yTextField != null) {
                    yTextField.setText(Integer.toString(selection.y));
                }
                if (widthTextField != null) {
                    widthTextField.setText(Integer.toString(selection.width));
                }
                if (heightTextField != null) {
                    heightTextField.setText(Integer.toString(selection.height));
                }
            }
        }
    }

    public ITextComponent getXTextField() {
        return xTextField;
    }

    public void setXTextField(ITextComponent xTextField) {
        this.xTextField = xTextField;
    }

    public ITextComponent getYTextField() {
        return yTextField;
    }

    public void setYTextField(ITextComponent yTextField) {
        this.yTextField = yTextField;
    }

    public ITextComponent getWidthTextField() {
        return widthTextField;
    }

    public void setWidthTextField(ITextComponent widthTextField) {
        this.widthTextField = widthTextField;
    }

    public ITextComponent getHeightTextField() {
        return heightTextField;
    }

    public void setHeightTextField(ITextComponent heightTextField) {
        this.heightTextField = heightTextField;
    }

    public ITextComponent getProfileX1TextField() {
        return profileX1TextField;
    }

    public void setProfileX1TextField(ITextComponent profileXStartTextField) {
        this.profileX1TextField = profileXStartTextField;
    }

    public ITextComponent getProfileX2TextField() {
        return profileX2TextField;
    }

    public void setProfileX2TextField(ITextComponent profileXEndTextField) {
        this.profileX2TextField = profileXEndTextField;
    }

    public ITextComponent getProfileY1TextField() {
        return profileY1TextField;
    }

    public void setProfileY1TextField(ITextComponent profileYStartTextField) {
        this.profileY1TextField = profileYStartTextField;
    }

    public ITextComponent getProfileY2TextField() {
        return profileY2TextField;
    }

    public void setProfileY2TextField(ITextComponent profileYEndTextField) {
        this.profileY2TextField = profileYEndTextField;
    }

    @Override
    public void mouseChanged(IJViewerEvent event) {
        transmitToTextFields();
    }

    @Override
    public void mouseClicked(IJViewerEvent event) {
        // nothing to do
    }

    @Override
    public void pixelSizeChanged(IJViewerEvent event) {
        // nothing to do
    }

    @Override
    public void valueChanged(IJViewerEvent event) {
        // nothing to do
    }

}
