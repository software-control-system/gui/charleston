package fr.soleil.bean.utils;

import java.awt.Color;
import java.awt.Font;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.border.Border;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.bean.imgbeamanalyzer.view.ByGroupChangeRefreshingPeriodAction;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.event.GroupEvent;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.util.DockingUtils;
import fr.soleil.docking.view.IView;
import fr.soleil.lib.project.math.MathConst;

public class ImageProfileViewManager {

    // ROI image viewer
    protected final ProfiledImageViewer imageViewer;
    // ROI image viewer axis convertors (to adapt coordinates to original image ones)
    protected final AxisConvertor xAxisConvertor, yAxisConvertor;
    // Auto ROI coordinates management
    protected final AutoRoiEnableTarget autoRoiEnableTarget;
    protected final AutoRoiXNumberTarget autoRoiXNumberTarget;
    protected final AutoRoiYNumberTarget autoRoiYNumberTarget;
    // User ROI coordinates management
    protected final UserRoiEnableTarget userRoiEnableTarget;
    protected final UserRoiXNumberTarget userRoiXNumberTarget;
    protected final UserRoiYNumberTarget userRoiYNumberTarget;
    // Refreshing group listener
    protected final IRefreshingGroupListener refreshingGroupListener;

    protected final ProfileViewer profileViewer1;
    protected final ProfileViewer profileViewer2;
    protected final ProfileViewer profileViewer3;
    protected final JComponent profileTabbedPane;
    protected boolean profile1Visible;
    protected boolean profile2Visible;
    protected boolean profile3Visible;
    protected boolean imageProfile1Visible;
    protected boolean imageProfile2Visible;

    protected static final String X_PROJECTION = "X Projection";
    protected static final String Y_PROJECTION = "Y Projection";
    protected static final String FIT_LINE_PROFILE = "Line Profile for Fit";
    protected static final String LINE_PROFILE = "Image Component Line Profile";
    protected static final String HISTOGRAM = "Image Component Histogram";
    protected static final String HORIZONTAL_PROFILE = "Image Component Horizontal Profile";
    protected static final String VETICAL_PROFILE = "Image Component Vertical Profile";

    private final NumberMatrixBox numberMatrixBox;
    private final AbstractAction xProjectionViewAction;
    private final AbstractAction yProjectionViewAction;
    private final AbstractAction lineProfileViewAction;

    /**
     * Change refreshing period action JIRA CONTROLGUI-12
     */
    private final ChangeRefreshingPeriodAction changeRefreshingPeriodAction = new ByGroupChangeRefreshingPeriodAction();

    public ImageProfileViewManager(ADockingManager dockingManager) {
        super();

        profile1Visible = false;
        profile2Visible = false;
        profile3Visible = false;
        imageProfile1Visible = false;
        imageProfile2Visible = false;

        numberMatrixBox = new NumberMatrixBox();

        imageViewer = new ProfiledImageViewer();
        // manage ROI coordinates in imageViewer
        xAxisConvertor = new AxisConvertor();
        xAxisConvertor.setAutoWarn(false);
        yAxisConvertor = new AxisConvertor();
        yAxisConvertor.setAutoWarn(false);
        imageViewer.setXAxisConvertor(xAxisConvertor);
        imageViewer.setYAxisConvertor(yAxisConvertor);
        // coordinates targets
        autoRoiEnableTarget = new AutoRoiEnableTarget();
        autoRoiXNumberTarget = new AutoRoiXNumberTarget();
        autoRoiYNumberTarget = new AutoRoiYNumberTarget();
        userRoiEnableTarget = new UserRoiEnableTarget();
        userRoiXNumberTarget = new UserRoiXNumberTarget();
        userRoiYNumberTarget = new UserRoiYNumberTarget();
        // refreshing group listener;
        refreshingGroupListener = new CoordinatesRefresher();

        // Add refreshing period action popup menu JIRA CONTROLGUI-12
        imageViewer.getPopupMenu().add(changeRefreshingPeriodAction);

        profileViewer1 = new ProfileViewer(ImgBeamAnalyzer.PROFILE1_CHART_PROPERTIES);
        profileViewer1.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        profileViewer1.setProfileTitle("xProjection");
        profileViewer1.setProfileFittedTitle("xProjectionFitted");
        profileViewer1.setProfileErrorTitle("xProjectionError");
        profileViewer2 = new ProfileViewer(ImgBeamAnalyzer.PROFILE2_CHART_PROPERTIES);
        profileViewer2.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        profileViewer2.setProfileTitle("yProjection");
        profileViewer2.setProfileFittedTitle("yProjectionFitted");
        profileViewer2.setProfileErrorTitle("yProjectionError");
        profileViewer3 = new ProfileViewer(ImgBeamAnalyzer.PROFILE3_CHART_PROPERTIES);
        profileViewer3.setHeaderFont(new Font("Dialog", Font.BOLD, 18));
        profileViewer3.setProfileTitle("lineProfile");
        profileViewer3.setProfileFittedTitle("lineProfileFitted");
        profileViewer3.setProfileErrorTitle("lineProfileError");

        profileTabbedPane = dockingManager.createNewDockingArea(Color.GRAY);

        IView xProjectionView = dockingManager.getViewFactory().addView(X_PROJECTION, null, profileViewer1,
                X_PROJECTION, profileTabbedPane);
        xProjectionViewAction = DockingUtils.generateShowViewAction(xProjectionView);

        IView yProjectionView = dockingManager.getViewFactory().addView(Y_PROJECTION, null, profileViewer2,
                Y_PROJECTION, profileTabbedPane);
        yProjectionViewAction = DockingUtils.generateShowViewAction(yProjectionView);

        IView lineProfileView = dockingManager.getViewFactory().addView(FIT_LINE_PROFILE, null, profileViewer3,
                FIT_LINE_PROFILE, profileTabbedPane);
        lineProfileViewAction = DockingUtils.generateShowViewAction(lineProfileView);

//        profileTabbedPane.addTab(xProjection, profile1ScrollPane);
//        profileTabbedPane.addTab(yProjection, profile2ScrollPane);
//        profileTabbedPane.addTab(lineProfile, profile3ScrollPane);

    }

    public void setImageModel(IKey key) {
        numberMatrixBox.disconnectWidgetFromAll(imageViewer);
        if (key != null) {
            numberMatrixBox.connectWidget(imageViewer, key);
        }
        // JIRA CONTROLGUI-12
        changeRefreshingPeriodAction.setKey(key);
    }

    public IBooleanTarget getAutoRoiEnableTarget() {
        return autoRoiEnableTarget;
    }

    public INumberTarget getAutoRoiXNumberTarget() {
        return autoRoiXNumberTarget;
    }

    public INumberTarget getAutoRoiYNumberTarget() {
        return autoRoiYNumberTarget;
    }

    public IBooleanTarget getUserRoiEnableTarget() {
        return userRoiEnableTarget;
    }

    public INumberTarget getUserRoiXNumberTarget() {
        return userRoiXNumberTarget;
    }

    public INumberTarget getUserRoiYNumberTarget() {
        return userRoiYNumberTarget;
    }

    public IRefreshingGroupListener getRefreshingGroupListener() {
        return refreshingGroupListener;
    }

//    public void setROIsModel(IKey autoRoiEnabledKey, IKey autoRoiXKey, IKey autoRoiYKey, IKey userRoiEnabledKey,
//            IKey userRoiXKey, IKey userRoiYKey) {
//        booleanBox.disconnectWidgetFromAll(autoRoiEnableTarget);
//        booleanBox.disconnectWidgetFromAll(userRoiEnableTarget);
//        numberBox.disconnectWidgetFromAll(autoRoiXNumberTarget);
//        numberBox.disconnectWidgetFromAll(autoRoiYNumberTarget);
//        numberBox.disconnectWidgetFromAll(userRoiXNumberTarget);
//        numberBox.disconnectWidgetFromAll(userRoiYNumberTarget);
//        if (autoRoiEnabledKey != null) {
//            booleanBox.connectWidget(autoRoiEnableTarget, autoRoiEnabledKey);
//        }
//        if (autoRoiXKey != null) {
//            numberBox.connectWidget(autoRoiXNumberTarget, autoRoiXKey);
//        }
//        if (autoRoiYKey != null) {
//            numberBox.connectWidget(autoRoiYNumberTarget, autoRoiYKey);
//        }
//        if (userRoiEnabledKey != null) {
//            booleanBox.connectWidget(userRoiEnableTarget, userRoiEnabledKey);
//        }
//        if (userRoiXKey != null) {
//            numberBox.connectWidget(userRoiXNumberTarget, userRoiXKey);
//        }
//        if (userRoiYKey != null) {
//            numberBox.connectWidget(userRoiYNumberTarget, userRoiYKey);
//        }
//    }

    public void setProfile1(TangoKey spectrum) {
        profileViewer1.setProfile(spectrum);
    }

    public void setProfileFitted1(TangoKey spectrum) {
        profileViewer1.setProfileFitted(spectrum);
    }

    public void setProfileError1(TangoKey spectrum) {
        profileViewer1.setProfileError(spectrum);
    }

    public void setProfile2(TangoKey spectrum) {
        profileViewer2.setProfile(spectrum);
    }

    public void setProfileFitted2(TangoKey spectrum) {
        profileViewer2.setProfileFitted(spectrum);
    }

    public void setProfileError2(TangoKey spectrum) {
        profileViewer2.setProfileError(spectrum);
    }

    public void setProfile3(TangoKey spectrum) {
        profileViewer3.setProfile(spectrum);
    }

    public void setProfileFitted3(TangoKey spectrum) {
        profileViewer3.setProfileFitted(spectrum);
    }

    public void setProfileError3(TangoKey spectrum) {
        profileViewer3.setProfileError(spectrum);
    }

    public void setImageBorder(Border imageBorder) {
        imageViewer.setBorder(imageBorder);
    }

    /**
     * @return the xTextField
     */
    public ITextComponent getXTextField() {
        if (imageViewer != null) {
            return imageViewer.getXTextField();
        }
        return null;
    }

    /**
     * @param textField the xTextField to set
     */
    public void setXTextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setXTextField(textField);
        }
    }

    /**
     * @return the yTextField
     */
    public ITextComponent getYTextField() {
        if (imageViewer != null) {
            return imageViewer.getYTextField();
        }
        return null;
    }

    /**
     * @param textField the yTextField to set
     */
    public void setYTextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setYTextField(textField);
        }
    }

    /**
     * @return the widthTextField
     */
    public ITextComponent getWidthTextField() {
        if (imageViewer != null) {
            return imageViewer.getWidthTextField();
        }
        return null;
    }

    /**
     * @param widthTextField the widthTextField to set
     */
    public void setWidthTextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setWidthTextField(textField);
        }
    }

    /**
     * @return the heightTextField
     */
    public ITextComponent getHeightTextField() {
        if (imageViewer != null) {
            return imageViewer.getHeightTextField();
        }
        return null;
    }

    /**
     * @param heightTextField the heightTextField to set
     */
    public void setHeightTextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setHeightTextField(textField);
        }
    }

    public ITextComponent getProfileX1TextField() {
        if (imageViewer != null) {
            return imageViewer.getProfileX1TextField();
        }
        return null;
    }

    public void setProfileX1TextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setProfileX1TextField(textField);
        }
    }

    public ITextComponent getProfileY1TextField() {
        if (imageViewer != null) {
            return imageViewer.getProfileY1TextField();
        }
        return null;
    }

    public void setProfileY1TextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setProfileY1TextField(textField);
        }
    }

    public ITextComponent getProfileX2TextField() {
        if (imageViewer != null) {
            return imageViewer.getProfileX2TextField();
        }
        return null;
    }

    public void setProfileX2TextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setProfileX2TextField(textField);
        }
    }

    public ITextComponent getProfileY2TextField() {
        if (imageViewer != null) {
            return imageViewer.getProfileY2TextField();
        }
        return null;
    }

    public void setProfileY2TextField(ITextComponent textField) {
        if (imageViewer != null) {
            imageViewer.setProfileY2TextField(textField);
        }
    }

    public void setPixelSizeX(TangoKey xProfilePixelSizeX) {
        profileViewer1.setPixelSizeKey(xProfilePixelSizeX);

    }

    public void setPixelSizeY(TangoKey yProfilePixelSizeY) {
        profileViewer2.setPixelSizeKey(yProfilePixelSizeY);
    }

    public ProfileViewer getProfileViewer1() {
        return profileViewer1;
    }

    public ProfileViewer getProfileViewer2() {
        return profileViewer2;
    }

    public ProfileViewer getProfileViewer3() {
        return profileViewer3;
    }

    public ProfiledImageViewer getImageViewer() {
        return imageViewer;
    }

    public JComponent getProfileTabbedPane() {
        return profileTabbedPane;
    }

    public AbstractAction getxProjectionViewAction() {
        return xProjectionViewAction;
    }

    public AbstractAction getyProjectionViewAction() {
        return yProjectionViewAction;
    }

    public AbstractAction getLineProfileViewAction() {
        return lineProfileViewAction;
    }

    protected static double numberTodouble(Number value) {
        return value == null ? MathConst.NAN_FOR_NULL : value.doubleValue();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class AutoRoiEnableTarget implements IBooleanTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public boolean isSelected() {
            return xAxisConvertor.isAutoRoiEnable();

        }

        @Override
        public void setSelected(boolean bool) {
            xAxisConvertor.setAutoRoiEnable(bool);
            yAxisConvertor.setAutoRoiEnable(bool);
        }

    }

    protected class AutoRoiXNumberTarget implements INumberTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public Number getNumberValue() {
            return Double.valueOf(xAxisConvertor.getAutoRoiStart());
        }

        @Override
        public void setNumberValue(Number value) {
            xAxisConvertor.setAutoRoiState(numberTodouble(value));
        }

    }

    protected class AutoRoiYNumberTarget implements INumberTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public Number getNumberValue() {
            return Double.valueOf(yAxisConvertor.getAutoRoiStart());
        }

        @Override
        public void setNumberValue(Number value) {
            yAxisConvertor.setAutoRoiState(numberTodouble(value));
        }

    }

    protected class UserRoiEnableTarget implements IBooleanTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public boolean isSelected() {
            return xAxisConvertor.isUserRoiEnable();
        }

        @Override
        public void setSelected(boolean bool) {
            xAxisConvertor.setUserRoiEnable(bool);
            yAxisConvertor.setUserRoiEnable(bool);
        }

    }

    protected class UserRoiXNumberTarget implements INumberTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public Number getNumberValue() {
            return Double.valueOf(xAxisConvertor.getUserRoiStart());
        }

        @Override
        public void setNumberValue(Number value) {
            xAxisConvertor.setUserRoiState(numberTodouble(value));
        }

    }

    protected class UserRoiYNumberTarget implements INumberTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public Number getNumberValue() {
            return Double.valueOf(yAxisConvertor.getUserRoiStart());
        }

        @Override
        public void setNumberValue(Number value) {
            yAxisConvertor.setUserRoiState(numberTodouble(value));
        }

    }

    protected class CoordinatesRefresher implements IRefreshingGroupListener {

        @Override
        public void groupRefreshed(GroupEvent event) {
            if ((event != null) && ImgBeamAnalyzer.ROI_IMAGE_GROUP.equals(event.getGroupName())) {
                xAxisConvertor.warnListeners();
                yAxisConvertor.warnListeners();
            }
        }

    }

}
