package fr.soleil.bean.utils;

import java.awt.Color;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JToggleButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.charleston.Charleston;
import fr.soleil.comete.definition.widget.util.AngularSector;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.exception.ApplicationIdException;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.mask.MaskController;
import ij.gui.Line;
import ij.gui.Roi;

public class CharlestonImageComponent extends ImageViewer {

    private static final long serialVersionUID = 5995505721922981522L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(CharlestonImageComponent.class.getName());

    private JToggleButton profileButton;

    public CharlestonImageComponent() {
        super();
        filterActions();
        setToolBarVisible(true);
        setShowRoiInformationTable(false);
        registerSectorClass(AngularSector.class);
        setMouseZoomMode(MouseZoomMode.PIXEL);
        setSingleRoiMode(true);
        setCleanOnDataSetting(false);
    }

    protected void initProfileButton() {
        profileButton = new JToggleButton("Use profiles");
        profileButton.setIcon(Charleston.PROFILE_ICON);
        profileButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setProfileVisible(profileButton.isSelected());
            }
        });
        profileButton.setSelected(false);
        profileButton.setMargin(CometeUtils.getzInset());
    }

    @Override
    protected void initToolBarScrollPane() {
        initProfileButton();
        super.initToolBarScrollPane();
        toolBarScrollPane.setRowHeaderView(profileButton);
    }

    public void setProfileVisible(boolean visible) {
        profileButton.setSelected(visible);
        profileButton.revalidate();
        toolBarScrollPane.revalidate();
        revalidate();
    }

    protected void filterActions() {
        filterContainer(super.getSelectionMenu());
        filterContainer(super.getActionMenu());
        filterContainer(super.getPopupMenu());
        filterContainer(toolBar);
    }

    private void filterContainer(Container container) {
        if (container != null) {
            ArrayList<AbstractButton> itemsToRemove = new ArrayList<AbstractButton>();
            for (int i = 0; i < container.getComponentCount(); i++) {
                if (container.getComponent(i) instanceof AbstractButton) {
                    AbstractButton item = (AbstractButton) container.getComponent(i);
                    if (item != null) {
                        if (super.getAction(ImageViewer.MODE_POINT_ACTION).equals(item.getAction())
                                || super.getAction(ImageViewer.UNDO_ACTION).equals(item.getAction())
                                || super.getAction(ImageViewer.REDO_ACTION).equals(item.getAction())) {
                            itemsToRemove.add(item);
                        }
                    }
                    item = null;
                }
            }
            for (AbstractButton item : itemsToRemove) {
                container.remove(item);
            }
            itemsToRemove.clear();
            itemsToRemove = null;
        }
    }

    public Rectangle getMaskBounds() {
        Rectangle result = null;
        Object data = super.getValue();
        if (data != null) {
            Mask mask = getMask();
            if (mask == null) {
                result = new Rectangle(0, 0, super.getDimX(), super.getDimY());
            } else {
                int minX = super.getDimX();
                int maxX = 0;
                int minY = super.getDimY();
                int maxY = 0;
                int dimY = super.getDimY();
                int dimX = super.getDimX();
                for (int y = 0; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        if (mask.getValue()[y * dimX + x]) {
                            if (x < minX) {
                                minX = x;
                            }
                            if (x > maxX) {
                                maxX = x;
                            }
                            if (y < minY) {
                                minY = y;
                            }
                            if (y > maxY) {
                                maxY = y;
                            }
                        }
                    }
                }
                if (maxX >= minX && maxY >= minY) {
                    // Usual case
                    result = new Rectangle(minX, minY, maxX + 1 - minX, maxY + 1 - minY);
                } else {
                    // Case where mask is, in fact, an all true boolean matrix
                    result = new Rectangle(0, 0, super.getDimX(), super.getDimY());
                }
            }
        }
        return result;
    }

    public double[][] getDataAsDoubleMatrix(Roi roi) {
        boolean innerRoi = false;
        Object value = super.getValue();
        if (value == null || super.getRoiManager() == null) {
            return null;
        }
        if (roi != null && (!super.getRoiManager().isInner(roi)) && (!super.getRoiManager().isOuter(roi))) {
            roi = null;
        }
        if (roi == null) {
            innerRoi = true;
        } else if (super.getRoiManager().isInner(roi)) {
            innerRoi = true;
        } else if (super.getRoiManager().isOuter(roi)) {
            innerRoi = false;
        }
        return getDataAsDoubleMatrix(roi, value, super.getDimX(), super.getDimY(), innerRoi, getMask(),
                getValidatedSectorMask());
    }

    public double[][] getDataAsDoubleMatrix() {
        double[][] result = null;
        Object data = super.getValue();
        if (data != null) {
            Mask mask = getMask();
            result = new double[super.getDimY()][super.getDimX()];
            if (mask == null) {
                for (int y = 0; y < super.getDimY(); y++) {
                    for (int x = 0; x < super.getDimX(); x++) {
                        result[y][x] = Array.getDouble(data, y * super.getDimX() + x);
                    }
                }
            } else {
                int dimY = super.getDimY();
                int dimX = super.getDimX();
                for (int y = 0; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        if (mask.getValue()[y * dimX + x]) {
                            result[y][x] = Array.getDouble(data, y * dimX + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
            }
        }
        return result;
    }

    public double[][] getDataAsDoubleMatrixNoMask(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi) {
        if (imageData == null) {
            return null;
        }
        Rectangle roiBounds = new Rectangle(0, 0, dimX, dimY);
        if (roi != null) {
            roiBounds = roi.getBounds();
        }
        int roiX = Math.max(roiBounds.x, 0);
        if (dimX > 0 && roiX > dimX - 1) {
            roiX = dimX - 1;
        }
        int roiY = Math.max(roiBounds.y, 0);
        if (dimY > 0 && roiY > dimY - 1) {
            roiY = dimY - 1;
        }
        int roiWidth = Math.min(dimX - roiX, roiBounds.x + roiBounds.width - roiX);
        int roiHeight = Math.min(dimY - roiY, roiBounds.y + roiBounds.height - roiY);
        roiBounds.x = roiX;
        roiBounds.y = roiY;
        roiBounds.width = roiWidth;
        roiBounds.height = roiHeight;
        double[][] result = new double[dimY][dimX];
        boolean returnNaN = false;
        if (innerRoi) {
            if (roiBounds.width <= 0 || roiBounds.height <= 0) {
                returnNaN = true;
            }
        } else {
            if (dimX <= 0 || dimY <= 0) {
                returnNaN = true;
            }
        }
        if (returnNaN) {
            for (int y = 0; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y][x] = Double.NaN;
                }
            }
            return result;
        }

        int minYtop = Math.max(roiBounds.y, 0);
        int maxYtop = Math.min(minYtop, dimY);
        int maxYroi = Math.min(roiBounds.y + roiBounds.height, dimY);
        int minXleft = Math.max(roiBounds.x, 0);
        int maxXleft = Math.min(minXleft, dimX);
        int maxXroi = Math.min(roiBounds.x + roiBounds.width, dimX);
        if (innerRoi) {
            // treat top rectangle
            for (int y = 0; y < maxYtop; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y][x] = Double.NaN;
                }
            }
            for (int y = maxYtop; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < maxXleft; x++) {
                    result[y][x] = Double.NaN;
                }
                // treat roi rectangle
                for (int x = maxXleft; x < maxXroi; x++) {
                    if ((roi == null) || roi.contains(x, y)) {
                        result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                    } else {
                        result[y][x] = Double.NaN;
                    }
                }
                // treat right rectangle
                for (int x = roiBounds.x + roiBounds.width; x < dimX; x++) {
                    result[y][x] = Double.NaN;
                }
            }
            // treat bottom rectangle
            for (int y = roiBounds.y + roiBounds.height; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y][x] = Double.NaN;
                }
            }
        } // end if (innerRoi)
        else {
            // treat top rectangle
            for (int y = 0; y < maxYtop; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                }
            }
            for (int y = maxYtop; y < maxYroi; y++) {
                // treat left rectangle
                for (int x = 0; x < maxXleft; x++) {
                    result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                }
                // treat roi rectangle
                for (int x = maxXleft; x < maxXroi; x++) {
                    if ((roi != null) && roi.contains(x, y)) {
                        result[y][x] = Double.NaN;
                    } else {
                        result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                    }
                }
                // treat right rectangle
                for (int x = roiBounds.x + roiBounds.width; x < dimX; x++) {
                    result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                }
            }
            // treat bottom rectangle
            for (int y = roiBounds.y + roiBounds.height; y < dimY; y++) {
                for (int x = 0; x < dimX; x++) {
                    result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                }
            }
        } // end if (innerRoi)...else
          // calculate roi
        return result;
    }

    public double[][] getDataAsDoubleMatrix(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi,
            Mask mainMask, Mask secondaryMask) {
        if (secondaryMask == null) {
            return getDataAsDoubleMatrix(roi, imageData, dimX, dimY, innerRoi, mainMask);
        } else if (mainMask == null) {
            return getDataAsDoubleMatrix(roi, imageData, dimX, dimY, innerRoi, secondaryMask);
        } else {
            if (imageData == null) {
                return null;
            }
            Rectangle roiBounds = new Rectangle(0, 0, dimX, dimY);
            if (roi != null) {
                roiBounds = roi.getBounds();
            }
            int roiX = Math.max(roiBounds.x, 0);
            if (dimX > 0 && roiX > dimX - 1) {
                roiX = dimX - 1;
            }
            int roiY = Math.max(roiBounds.y, 0);
            if (dimY > 0 && roiY > dimY - 1) {
                roiY = dimY - 1;
            }
            int roiWidth = Math.min(dimX - roiX, roiBounds.x + roiBounds.width - roiX);
            int roiHeight = Math.min(dimY - roiY, roiBounds.y + roiBounds.height - roiY);
            roiBounds.x = roiX;
            roiBounds.y = roiY;
            roiBounds.width = roiWidth;
            roiBounds.height = roiHeight;

            double[][] result = new double[dimY][dimX];
            boolean returnNaN = false;
            if (innerRoi) {
                if (roiBounds.width <= 0 || roiBounds.height <= 0) {
                    returnNaN = true;
                }
            } else {
                if (dimX <= 0 || dimY <= 0) {
                    returnNaN = true;
                }
            }
            if (returnNaN) {
                for (int y = 0; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
                return result;
            }

            int minYtop = Math.max(roiBounds.y, 0);
            int maxYtop = Math.min(minYtop, dimY);
            int maxYroi = Math.min(roiBounds.y + roiBounds.height, dimY);
            int minXleft = Math.max(roiBounds.x, 0);
            int maxXleft = Math.min(minXleft, dimX);
            int maxXroi = Math.min(roiBounds.x + roiBounds.width, dimX);
            if (innerRoi) {
                // treat top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // treat left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        result[y][x] = Double.NaN;
                    }
                    // treat roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (((roi == null) || roi.contains(x, y)) && isDisplayable(x, y, mainMask, secondaryMask)) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                    // treat right rectangle
                    for (int x = roiBounds.x + roiBounds.width; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
                // treat bottom rectangle
                for (int y = roiBounds.y + roiBounds.height; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
            } // end if (innerRoi)
            else {
                // treat top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        if (isDisplayable(x, y, mainMask, secondaryMask)) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // treat left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        if (isDisplayable(x, y, mainMask, secondaryMask)) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                    // treat roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if ((roi != null) && roi.contains(x, y)) {
                            result[y][x] = Double.NaN;
                        } else {
                            if (isDisplayable(x, y, mainMask, secondaryMask)) {
                                result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                            } else {
                                result[y][x] = Double.NaN;
                            }
                        }
                    }
                    // treat right rectangle
                    for (int x = roiBounds.x + roiBounds.width; x < dimX; x++) {
                        if (isDisplayable(x, y, mainMask, secondaryMask)) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
                // treat bottom rectangle
                for (int y = roiBounds.y + roiBounds.height; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        if (isDisplayable(x, y, mainMask, secondaryMask)) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
            } // end if (innerRoi)...else
              // calculate roi
            return result;
        }
    }

    protected boolean isDisplayable(int x, int y, Mask mainMask, Mask secondaryMask) {
        int dimX = super.getDimX();
        return (mainMask == null || mainMask.getValue()[y * dimX + x])
                && (secondaryMask == null || secondaryMask.getValue()[y * dimX + x]);
    }

    public double[][] getDataAsDoubleMatrix(Roi roi, Object imageData, int dimX, int dimY, boolean innerRoi,
            Mask mask) {
        if (mask == null) {
            return getDataAsDoubleMatrixNoMask(roi, imageData, dimX, dimY, innerRoi);
        } else {
            if (imageData == null) {
                return null;
            }
            Rectangle roiBounds = new Rectangle(0, 0, dimX, dimY);
            if (roi != null) {
                roiBounds = roi.getBounds();
            }
            int roiX = Math.max(roiBounds.x, 0);
            if (dimX > 0 && roiX > dimX - 1) {
                roiX = dimX - 1;
            }
            int roiY = Math.max(roiBounds.y, 0);
            if (dimY > 0 && roiY > dimY - 1) {
                roiY = dimY - 1;
            }
            int roiWidth = Math.min(dimX - roiX, roiBounds.x + roiBounds.width - roiX);
            int roiHeight = Math.min(dimY - roiY, roiBounds.y + roiBounds.height - roiY);
            roiBounds.x = roiX;
            roiBounds.y = roiY;
            roiBounds.width = roiWidth;
            roiBounds.height = roiHeight;

            double[][] result = new double[dimY][dimX];
            boolean returnNaN = false;
            if (innerRoi) {
                if (roiBounds.width <= 0 || roiBounds.height <= 0) {
                    returnNaN = true;
                }
            } else {
                if (dimX <= 0 || dimY <= 0) {
                    returnNaN = true;
                }
            }
            if (returnNaN) {
                for (int y = 0; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
                return result;
            }

            int minYtop = Math.max(roiBounds.y, 0);
            int maxYtop = Math.min(minYtop, dimY);
            int maxYroi = Math.min(roiBounds.y + roiBounds.height, dimY);
            int minXleft = Math.max(roiBounds.x, 0);
            int maxXleft = Math.min(minXleft, dimX);
            int maxXroi = Math.min(roiBounds.x + roiBounds.width, dimX);
            if (innerRoi) {
                // treat top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // treat left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        result[y][x] = Double.NaN;
                    }
                    // treat roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if (((roi == null) || roi.contains(x, y)) && (mask == null || mask.getValue()[y * dimX + x])) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                    // treat right rectangle
                    for (int x = roiBounds.x + roiBounds.width; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
                // treat bottom rectangle
                for (int y = roiBounds.y + roiBounds.height; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        result[y][x] = Double.NaN;
                    }
                }
            } // end if (innerRoi)
            else {
                // treat top rectangle
                for (int y = 0; y < maxYtop; y++) {
                    for (int x = 0; x < dimX; x++) {
                        if (mask == null || mask.getValue()[y * dimX + x]) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
                for (int y = maxYtop; y < maxYroi; y++) {
                    // treat left rectangle
                    for (int x = 0; x < maxXleft; x++) {
                        if (mask == null || mask.getValue()[y * dimX + x]) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                    // treat roi rectangle
                    for (int x = maxXleft; x < maxXroi; x++) {
                        if ((roi != null) && roi.contains(x, y)) {
                            result[y][x] = Double.NaN;
                        } else {
                            if (mask == null || mask.getValue()[y * dimX + x]) {
                                result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                            } else {
                                result[y][x] = Double.NaN;
                            }
                        }
                    }
                    // treat right rectangle
                    for (int x = roiBounds.x + roiBounds.width; x < dimX; x++) {
                        if (mask == null || mask.getValue()[y * dimX + x]) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
                // treat bottom rectangle
                for (int y = roiBounds.y + roiBounds.height; y < dimY; y++) {
                    for (int x = 0; x < dimX; x++) {
                        if (mask == null || mask.getValue()[y * dimX + x]) {
                            result[y][x] = Array.getDouble(imageData, (y * dimX) + x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
            } // end if (innerRoi)...else
              // calculate roi
            return result;
        }
    }

    public double[][] getAdaptedDataAsDoubleMatrix() {
        double[][] result = null;

        Object data = super.getValue();
        if (data != null) {
            Mask mask = getMask();
            Rectangle maskBounds = getMaskBounds();
            result = new double[maskBounds.height][maskBounds.width];
            if (mask == null) {
                for (int y = 0; y < super.getDimY(); y++) {
                    for (int x = 0; x < super.getDimX(); x++) {
                        result[y][x] = Array.getDouble(data, y * super.getDimX() + x);
                    }
                }
            } else {
                for (int y = 0; y < maskBounds.height; y++) {
                    for (int x = 0; x < maskBounds.width; x++) {
                        if (mask.getValue()[(y + maskBounds.y) * dimX + x + maskBounds.x]) {
                            result[y][x] = Array.getDouble(data,
                                    (y + maskBounds.y) * super.getDimX() + x + maskBounds.x);
                        } else {
                            result[y][x] = Double.NaN;
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void setAutoZoom(boolean auto) {
        super.setAutoZoom(auto);
    }

    @Override
    public MaskController getMaskController() {
        return super.getMaskController();
    }

    @Override
    public void setAsMask() {
        super.setAsMask();
    }

    @Override
    public void clearMask() {
        super.clearMask();
    }

    @Override
    public void setSector(MathematicSector sector) {
        super.setSector(sector);
    }

    @Override
    public void registerSectorClass(Class<?> sectorClass) {
        super.registerSectorClass(sectorClass);
    }

    @Override
    public JToggleButton createRectangleButton(JComponent container) {
        return super.createRectangleButton(container);
    }

    @Override
    public JToggleButton createEllipseButton(JComponent container) {
        return super.createEllipseButton(container);
    }

    @Override
    public JToggleButton createPolygonButton(JComponent container) {
        return super.createPolygonButton(container);
    }

    @Override
    public JToggleButton createFreeHandButton(JComponent container) {
        return super.createFreeHandButton(container);
    }

    @Override
    public JToggleButton createLineButton(JComponent container) {
        return super.createLineButton(container);
    }

    @Override
    public JToggleButton createWandButton(JComponent container) {
        return super.createWandButton(container);
    }

    @Override
    public JToggleButton createSelectionButton(JComponent container) {
        return super.createSelectionButton(container);
    }

    @Override
    public JToggleButton createPanButton(JComponent container) {
        return super.createPanButton(container);
    }

    @Override
    public JToggleButton createZoomButton(JComponent container) {
        return super.createZoomButton(container);
    }

    @Override
    public JButton createOrSelectedRoisButton(JComponent container) {
        return super.createOrSelectedRoisButton(container);
    }

    @Override
    public JButton createAndSelectedRoisButton(JComponent container) {
        return super.createAndSelectedRoisButton(container);
    }

    @Override
    public JButton createNotSelectedRoisButton(JComponent container) {
        return super.createNotSelectedRoisButton(container);
    }

    @Override
    public JButton createXorSelectedRoisButton(JComponent container) {
        return super.createXorSelectedRoisButton(container);
    }

    @Override
    public JButton createDeleteSelectedRoisButton(JComponent container) {
        return super.createDeleteSelectedRoisButton(container);
    }

    @Override
    public JButton createDeleteAllRoisButton(JComponent container) {
        return super.createDeleteAllRoisButton(container);
    }

    @Override
    public JButton createZoomInButton(JComponent container) {
        return super.createZoomInButton(container);
    }

    @Override
    public JButton createZoomOutButton(JComponent container) {
        return super.createZoomOutButton(container);
    }

    @Override
    public JButton createUndoButton(JComponent container) {
        return super.createUndoButton(container);
    }

    @Override
    public JButton createRedoButton(JComponent container) {
        return super.createRedoButton(container);
    }

    @Override
    public JButton createUnZoomButton(JComponent container) {
        return super.createUnZoomButton(container);
    }

    @Override
    public JButton createShowTableButton(JComponent container) {
        return super.createShowTableButton(container);
    }

    @Override
    public JButton createSetRoiColorsButton(JComponent container) {
        return super.createSetRoiColorsButton(container);
    }

    @Override
    public JButton createSetMaskColorButton(JComponent container) {
        return super.createSetMaskColorButton(container);
    }

    @Override
    public JButton createSetBeamPositionColorButton(JComponent container) {
        return super.createSetBeamPositionColorButton(container);
    }

    @Override
    public JButton createSetSectorColorsButton(JComponent container) {
        return super.createSetSectorColorsButton(container);
    }

    @Override
    public JButton createInnerRoiButton(JComponent container) {
        return super.createInnerRoiButton(container);
    }

    @Override
    public JButton createOuterRoiButton(JComponent container) {
        return super.createOuterRoiButton(container);
    }

    @Override
    public JButton createInvalidateRoiButton(JComponent container) {
        return super.createInvalidateRoiButton(container);
    }

    @Override
    public JButton createSetMaskButton(JComponent container) {
        return super.createSetMaskButton(container);
    }

    @Override
    public JButton createAddToMaskButton(JComponent container) {
        return super.createAddToMaskButton(container);
    }

    @Override
    public JButton createRemoveFromMaskButton(JComponent container) {
        return super.createRemoveFromMaskButton(container);
    }

    @Override
    public JButton createClearMaskButton(JComponent container) {
        return super.createClearMaskButton(container);
    }

    @Override
    public JButton createSaveMaskButton(JComponent container) {
        return super.createSaveMaskButton(container);
    }

    @Override
    public JButton createLoadMaskButton(JComponent container) {
        return super.createLoadMaskButton(container);
    }

    @Override
    public JButton createSnapshotButton(JComponent container) {
        return super.createSnapshotButton(container);
    }

    @Override
    public JMenuItem createFitMaxSizeItem(JComponent container) {
        return super.createFitMaxSizeItem(container);
    }

    @Override
    public IJRoiManager getRoiManager() {
        return super.getRoiManager();
    }

    @Override
    public int getDimX() {
        return super.getDimX();
    }

    @Override
    public int getDimY() {
        return super.getDimY();
    }

    @Override
    public void setMaskColor(Color maskColor) {
        super.setMaskColor(maskColor);
    }

    @Override
    public Color getMaskColor() {
        return super.getMaskColor();
    }

    public void setAllBackgrounds(Color bg) {
        setBackground(bg);
        super.setBackground(bg);
        imagePanel.setBackground(bg);
        imageScrollPane.setBackground(bg);
        imageScrollPane.getViewport().setBackground(bg);
        imageScrollPane.getHorizontalScrollBar().setBackground(bg);
        imageScrollPane.getVerticalScrollBar().setBackground(bg);
        infoPanel.setBackground(bg);
    }

    public void deleteAllRois() {
        super.deleteAllRois(true);
    }

    @Override
    public void cleanUndoManager() {
        super.cleanUndoManager();
    }

    public Mask getValidatedSectorMask() {
        MathematicSector sector = getSector();
        Mask sectorMask = super.getSectorMask();
        if ((sector == null) || (sector.isConstructing())) {
            sectorMask = null;
        }
        sector = null;
        return sectorMask;
    }

    public MathematicSector getSectorCopyIfValid() {
        MathematicSector sector = getSector();
        if ((sector != null) && (sector.isConstructing())) {
            sector = null;
        }
        return sector == null ? null : sector.clone(true);
    }

    public Rectangle getSelectionRectangle() {
        Roi roi = getRoiManager().getRoi();
        if (roi != null) {
            return roi.getBounds();
        }
        roi = null;
        return null;

    }

    public Line getLineRoi() {
        return (Line) getRoiManager()
                .getSpecialRoi(getRoiManager().getRoiGenerator(IJRoiManager.LINE_PROFILE_ROI_MODE));
    }

    private void myFitData() {
        super.setAlwaysFitMaxSize(true);
        fitMaxSize();
        doUpdateRefSize();
        super.setAlwaysFitMaxSize(false);
    }

    @Override
    public synchronized void setFlatNumberMatrix(Object value, int width, int height) throws ApplicationIdException {
        if (width > 0 && height > 0 && super.dimX != width && super.dimY != height) {
            super.setFlatNumberMatrix(value, width, height);
            myFitData();
        } else {
            super.setFlatNumberMatrix(value, width, height);
        }

    }

}
