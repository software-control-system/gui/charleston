package fr.soleil.bean.utils;

import fr.soleil.comete.definition.util.AbstractValueConvertor;

public class AxisConvertor extends AbstractValueConvertor {
    protected boolean autoRoiEnable;
    protected boolean userRoiEnable;
    protected double autoRoiStart;
    protected double userRoiStart;
    protected volatile boolean autoWarn;

    public AxisConvertor() {
        super();
        autoRoiEnable = false;
        userRoiEnable = false;
        autoRoiStart = Double.NaN;
        userRoiStart = Double.NaN;
        autoWarn = true;
    }

    public boolean isAutoWarn() {
        return autoWarn;
    }

    public void setAutoWarn(boolean autoWarn) {
        this.autoWarn = autoWarn;
    }

    public double getAutoRoiStart() {
        return autoRoiStart;
    }

    public void setAutoRoiState(double autoRoiState) {
        this.autoRoiStart = autoRoiState;
        if (isAutoWarn()) {
            warnListeners();
        }
    }

    public double getUserRoiStart() {
        return userRoiStart;
    }

    public void setUserRoiState(double userRoiState) {
        this.userRoiStart = userRoiState;
        if (isAutoWarn()) {
            warnListeners();
        }
    }

    public boolean isUserRoiEnable() {
        return userRoiEnable;
    }

    public void setUserRoiEnable(boolean userRoiEnable) {
        this.userRoiEnable = userRoiEnable;
        if (isAutoWarn()) {
            warnListeners();
        }
    }

    public boolean isAutoRoiEnable() {
        return autoRoiEnable;
    }

    public void setAutoRoiEnable(boolean autoRoiEnable) {
        this.autoRoiEnable = autoRoiEnable;
        if (isAutoWarn()) {
            warnListeners();
        }
    }

    public void setCoordinates(double autoRoiStart, double userRoiStart, boolean userRoiEnable, boolean autoRoiEnable) {
        this.autoRoiStart = autoRoiStart;
        this.userRoiStart = userRoiStart;
        this.userRoiEnable = userRoiEnable;
        this.autoRoiEnable = autoRoiEnable;
        if (isAutoWarn()) {
            warnListeners();
        }
    }

    @Override
    public double convertValue(double value) {
        double converted;
        if (autoRoiEnable) {
            converted = value + autoRoiStart;
        } else if (userRoiEnable) {
            converted = value + userRoiStart;
        } else {
            converted = value;
        }
        return converted;
    }

    @Override
    public boolean isValid() {
        boolean valid;
        if (autoRoiEnable) {
            valid = !Double.isNaN(autoRoiStart);
        } else if (userRoiEnable) {
            valid = !Double.isNaN(userRoiStart);
        } else {
            valid = false;
        }
        return valid;
    }

}
