package fr.soleil.bean.view;

import java.awt.Font;
import java.awt.Insets;

import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.TextFieldButton;
import fr.soleil.comete.swing.WheelSwitch;

public abstract class AbstractView {

    protected final static Insets SPACE = new Insets(0, 2, 2, 2);

    public AbstractView() {
        super();
    }

    protected WheelSwitch generateWheelSwitch() {
        WheelSwitch wheelSwitch = new WheelSwitch();
        wheelSwitch.setCometeFont(FontTool.getCometeFont(new Font(Font.DIALOG, Font.PLAIN, 14)));
        return wheelSwitch;
    }

    protected StringButton generateStringButton() {
        StringButton stringButton = new StringButton();
        stringButton.setButtonLook(true);
        return stringButton;
    }

    protected TextField generateNumberField() {
        TextField numberField = new TextField();
        numberField.setCometeForeground(CometeColor.BLACK);
        return numberField;
    }

    protected TextFieldButton generateNumberFieldButton() {
        TextFieldButton numberFieldButton = new TextFieldButton();
        numberFieldButton.setTextButton("Write");
        numberFieldButton.setCometeBackground(CometeColor.WHITE);
        numberFieldButton.setBackgroundTextField(CometeColor.WHITE);
        numberFieldButton.setCometeFont(CometeFont.DEFAULT_FONT);
        return numberFieldButton;
    }

    protected CheckBox generateCheckbox() {
        CheckBox checkbox = new CheckBox();
        return checkbox;
    }

    protected BooleanComboBox generateBooleanComboBox() {
        BooleanComboBox booleanComboBox = new BooleanComboBox();
        booleanComboBox.setTrueLabel("true");
        booleanComboBox.setFalseLabel("false");
        return booleanComboBox;
    }

    protected Label generateLabel() {
        Label label = new Label();
        label.setHorizontalAlignment(IComponent.CENTER);
        label.setCometeFont(CometeFont.DEFAULT_FONT);
        label.setOpaque(true);
        return label;
    }

}
