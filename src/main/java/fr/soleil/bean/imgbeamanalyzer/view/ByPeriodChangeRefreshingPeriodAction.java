package fr.soleil.bean.imgbeamanalyzer.view;

import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.comete.tango.data.service.TangoPolledRefreshingStrategy;
import fr.soleil.comete.tango.data.service.TangoPolledRefreshingStrategy.RefreshingGroup;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;

public class ByPeriodChangeRefreshingPeriodAction extends ChangeRefreshingPeriodAction {

    private static final long serialVersionUID = -5717396724558508852L;

    @Override
    protected PolledRefreshingStrategy buildRefreshingStrategy(IKey key, int period) {
        return new TangoPolledRefreshingStrategy(period, RefreshingGroup.GROUPED_BY_REFRESHING_PERIOD);
    }

}