package fr.soleil.bean.imgbeamanalyzer.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import fr.soleil.bean.view.AbstractView;
import fr.soleil.charleston.docking.DockingFactory;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.docking.ADockingManager;

public abstract class ImgBeamAnalyzerAbstractView extends AbstractView {

    protected final static Border ANALYZER_BORDER = new LineBorder(Color.BLUE, 2);

    // auto ROI
    protected JPanel autoROIPanel;
    protected JLabel enableAutoROILabel;
    protected CheckBox enableAutoROI;
    protected BooleanComboBox enableAutoROI2;
    protected JLabel autoROIFoundLabel;
    protected CheckBox autoROIFound;
    protected JLabel autoROIOriginXLabel;
    protected Label autoROIOriginX;
    protected JLabel autoROIOriginYLabel;
    protected Label autoROIOriginY;
    protected JLabel autoROIWidthLabel;
    protected Label autoROIWidth;
    protected JLabel autoROIHeightLabel;
    protected Label autoROIHeight;
    protected JLabel autoROIThresholdLabel;
    protected Label autoROIThreshold;
    protected WheelSwitch autoROIThreshold2;
    protected JLabel autoROIMagFactorXLabel;
    protected Label autoROIMagFactorX;
    protected WheelSwitch autoROIMagFactorX2;
    protected JLabel autoROIMagFactorYLabel;
    protected Label autoROIMagFactorY;
    protected WheelSwitch autoROIMagFactorY2;
    protected JLabel alarmZoneLabel;
    protected Label alarmZone;
    protected WheelSwitch alarmZone2;

    // user ROI
    protected JPanel userROIPanel;
    protected JLabel enableUserROILabel;
    protected CheckBox enableUserROI;
    protected BooleanComboBox enableUserROI2;
    protected JLabel userROIOriginXLabel;
    protected Label userROIOriginX;
    protected ITextComponent userROIOriginX2;
    protected JLabel userROIOriginYLabel;
    protected Label userROIOriginY;
    protected ITextComponent userROIOriginY2;
    protected JLabel userROIWidthLabel;
    protected Label userROIWidth;
    protected ITextComponent userROIWidth2;
    protected JLabel userROIHeightLabel;
    protected Label userROIHeight;
    protected ITextComponent userROIHeight2;

    // gamma
    protected JPanel gammaPanel;
    protected JLabel bitsPerPixelLabel;
    protected Label bitsPerPixel;
    protected WheelSwitch bitsPerPixel2;
    protected JLabel gammaCorrectionLabel;
    protected Label gammaCorrection;
    protected WheelSwitch gammaCorrection2;

    // histogram
    protected JPanel histogramPanel;
    protected JLabel enableHistogramLabel;
    protected CheckBox enableHistogram;
    protected BooleanComboBox enableHistogram2;
    protected JLabel histogramNbBinsLabel;
    protected Label histogramNbBins;
    protected WheelSwitch histogramNbBins2;
    protected JLabel histogramRangeMinLabel;
    protected Label histogramRangeMin;
    protected WheelSwitch histogramRangeMin2;
    protected JLabel histogramRangeMaxLabel;
    protected Label histogramRangeMax;
    protected WheelSwitch histogramRangeMax2;

    // moments
    protected JPanel momentsPanel;
    protected JLabel enableImageMomentsLabel;
    protected CheckBox enableImageMoments;
    protected BooleanComboBox enableImageMoments2;
    protected JLabel maxIntensityLabel;
    protected Label maxIntensity;
    protected JLabel meanIntensityLabel;
    protected Label meanIntensity;
    protected JLabel centroidXLabel;
    protected Label centroidX;
    protected JLabel centroidYLabel;
    protected Label centroidY;
    protected JLabel varianceXLabel;
    protected Label varianceX;
    protected JLabel varianceYLabel;
    protected Label varianceY;
    protected JLabel covarianceXYLabel;
    protected Label covarianceXY;
    protected JLabel correlationXYLabel;
    protected Label correlationXY;
    protected JLabel skewXLabel;
    protected Label skewX;
    protected JLabel skewYLabel;
    protected Label skewY;
    protected JLabel skewX2YLabel;
    protected Label skewX2Y;
    protected JLabel skewXY2Label;
    protected Label skewXY2;

    // profiles
    protected JPanel lineProfilePanel;
    protected JLabel lineProfileFitConvergedLabel;
    protected CheckBox lineProfileFitConverged;
    protected JLabel lineProfileOriginXLabel;
    protected Label lineProfileOriginX;
    protected ITextComponent lineProfileOriginX2;
    protected JLabel lineProfileOriginYLabel;
    protected Label lineProfileOriginY;
    protected ITextComponent lineProfileOriginY2;
    protected JLabel lineProfileEndXLabel;
    protected Label lineProfileEndX;
    protected ITextComponent lineProfileEndX2;
    protected JLabel lineProfileEndYLabel;
    protected Label lineProfileEndY;
    protected ITextComponent lineProfileEndY2;
    protected JLabel lineProfileThicknessLabel;
    protected Label lineProfileThickness;
    protected ITextComponent lineProfileThickness2;

    // gaussian 2d
    protected JPanel gaussian2dPanel;
    protected JLabel enable2DGaussianFitLabel;
    protected CheckBox enable2DGaussianFit;
    protected BooleanComboBox enable2DGaussianFit2;
    protected JLabel fit2DNbIterMaxLabel;
    protected Label fit2DNbIterMax;
    protected WheelSwitch fit2DNbIterMax2;
    protected JLabel fit2DMaxRelChangeLabel;
    protected Label fit2DMaxRelChange;
    protected WheelSwitch fit2DMaxRelChange2;
    protected JLabel gaussianFitConvergedLabel;
    protected CheckBox gaussianFitConverged;
    protected JLabel gaussianFitMagnitudeLabel;
    protected Label gaussianFitMagnitude;
    protected JLabel gaussianFitCenterXLabel;
    protected Label gaussianFitCenterX;
    protected JLabel gaussianFitCenterYLabel;
    protected Label gaussianFitCenterY;
    protected JLabel gaussianFitVarianceXLabel;
    protected Label gaussianFitVarianceX;
    protected JLabel gaussianFitVarianceYLabel;
    protected Label gaussianFitVarianceY;
    protected JLabel gaussianFitCovarianceXYLabel;
    protected Label gaussianFitCovarianceXY;
    protected JLabel gaussianFitMajorAxisFWHMLabel;
    protected Label gaussianFitMajorAxisFWHM;
    protected JLabel gaussianFitMinorAxisFWHMLabel;
    protected Label gaussianFitMinorAxisFWHM;
    protected JLabel gaussianFitTiltLabel;
    protected Label gaussianFitTilt;
    protected JLabel gaussianFitBGLabel;
    protected Label gaussianFitBG;
    protected JLabel gaussianFitChi2Label;
    protected Label gaussianFitChi2;
    protected JLabel gaussianFitNbIterLabel;
    protected Label gaussianFitNbIter;
    protected JLabel gaussianFitRelChangeLabel;
    protected Label gaussianFitRelChange;

    // pixel conversion
    protected JPanel pixelConversionPanel;
    protected JLabel pixelSizeXLabel;
    protected Label pixelSizeX;
    protected WheelSwitch pixelSizeX2;
    protected JLabel pixelSizeYLabel;
    protected Label pixelSizeY;
    protected WheelSwitch pixelSizeY2;
    protected JLabel opticalMagnificationLabel;
    protected Label opticalMagnification;
    protected WheelSwitch opticalMagnification2;

    // pre-processing
    protected JPanel preProcessingPanel;
    protected JLabel rotationLabel;
    protected Label rotation;
    protected WheelSwitch rotation2;
    protected JLabel horizontalFlipLabel;
    protected CheckBox horizontalFlip;
    protected BooleanComboBox horizontalFlip2;

    // general
    protected JPanel generalPanel;
    protected JLabel computationPeriodLabel;
    protected Label computationPeriod;
    protected WheelSwitch computationPeriod2;
    protected JLabel estimComputTimeLabel;
    protected Label estimComputTime;

    // look and feel
    protected JComponent mainPanel;

    protected ADockingManager dockingManager;

    protected DockingFactory dockingFactory;

    public ImgBeamAnalyzerAbstractView(DockingFactory dockingFactory, ADockingManager dockingManager) {
        super();
        this.dockingFactory = dockingFactory;
        this.dockingManager = dockingManager;
        initAutoRoiComponents();
        initUserRoiComponents();
        initGammaComponents();
        initHistogramComponents();
        initMomentsComponents();
        initProfileComponents();
        initGaussian2dComponents();
        initPixelConversionComponents();
        initPreProcessingComponents();
        initGeneralComponents();
    }

    protected void initAutoRoiComponents() {
        enableAutoROILabel = new JLabel("enable auto ROI");
        enableAutoROI = generateCheckbox();
        enableAutoROI2 = generateBooleanComboBox();
        autoROIFoundLabel = new JLabel("auto ROI found");
        autoROIFound = generateCheckbox();
        autoROIOriginXLabel = new JLabel("origin X");
        autoROIOriginX = generateLabel();
        autoROIOriginYLabel = new JLabel("origin Y");
        autoROIOriginY = generateLabel();
        autoROIWidthLabel = new JLabel("width");
        autoROIWidth = generateLabel();
        autoROIHeightLabel = new JLabel("height");
        autoROIHeight = generateLabel();
        autoROIThresholdLabel = new JLabel("auto ROI Threshold");
        autoROIThreshold = generateLabel();
        autoROIThreshold2 = generateWheelSwitch();
        autoROIMagFactorXLabel = new JLabel("auto ROI MagFactorX");
        autoROIMagFactorX = generateLabel();
        autoROIMagFactorX2 = generateWheelSwitch();
        autoROIMagFactorYLabel = new JLabel("auto ROI MagFactorY");
        autoROIMagFactorY = generateLabel();
        autoROIMagFactorY2 = generateWheelSwitch();
        alarmZoneLabel = new JLabel("alarm zone");
        alarmZone = generateLabel();
        alarmZone2 = generateWheelSwitch();
    }

    protected void initUserRoiComponents() {
        enableUserROILabel = new JLabel("enable user ROI");
        enableUserROI = generateCheckbox();
        enableUserROI2 = generateBooleanComboBox();
        userROIOriginXLabel = new JLabel("origin X");
        userROIOriginX = generateLabel();
        initUserROIOriginX2();
        userROIOriginYLabel = new JLabel("origin Y");
        userROIOriginY = generateLabel();
        initUserROIOriginY2();
        userROIWidthLabel = new JLabel("width");
        userROIWidth = generateLabel();
        initUserROIWidth2();
        userROIHeightLabel = new JLabel("height");
        userROIHeight = generateLabel();
        initUserROIHeight2();
    }

    protected abstract void initUserROIOriginX2();

    protected abstract void initUserROIOriginY2();

    protected abstract void initUserROIWidth2();

    protected abstract void initUserROIHeight2();

    protected void initMomentsComponents() {
        enableImageMomentsLabel = new JLabel("enable image moments");
        enableImageMoments = generateCheckbox();
        enableImageMoments2 = generateBooleanComboBox();
        maxIntensityLabel = new JLabel("max intensity");
        maxIntensity = generateLabel();
        meanIntensityLabel = new JLabel("mean intensity");
        meanIntensity = generateLabel();
        centroidXLabel = new JLabel("centroid X");
        centroidX = generateLabel();
        centroidYLabel = new JLabel("centroid Y");
        centroidY = generateLabel();
        varianceXLabel = new JLabel("variance X");
        varianceX = generateLabel();
        varianceYLabel = new JLabel("variance Y");
        varianceY = generateLabel();
        covarianceXYLabel = new JLabel("covariance XY");
        covarianceXY = generateLabel();
        correlationXYLabel = new JLabel("correlation XY");
        correlationXY = generateLabel();
        skewXLabel = new JLabel("skew X");
        skewX = generateLabel();
        skewYLabel = new JLabel("skew Y");
        skewY = generateLabel();
        skewX2YLabel = new JLabel("skew X2Y");
        skewX2Y = generateLabel();
        skewXY2Label = new JLabel("skew XY2");
        skewXY2 = generateLabel();
    }

    protected void initProfileComponents() {
        lineProfileFitConvergedLabel = new JLabel("fit converged");
        lineProfileFitConverged = generateCheckbox();
        lineProfileOriginXLabel = new JLabel("origin X");
        lineProfileOriginX = generateLabel();
        initLineProfileOriginX2();
        lineProfileOriginYLabel = new JLabel("origin Y");
        lineProfileOriginY = generateLabel();
        initLineProfileOriginY2();
        lineProfileEndXLabel = new JLabel("end X");
        lineProfileEndX = generateLabel();
        initLineProfileEndX2();
        lineProfileEndYLabel = new JLabel("end Y");
        lineProfileEndY = generateLabel();
        initLineProfileEndY2();
        lineProfileThicknessLabel = new JLabel("thickness");
        lineProfileThickness = generateLabel();
        lineProfileThickness2 = generateWheelSwitch();
    }

    protected abstract void initLineProfileOriginX2();

    protected abstract void initLineProfileOriginY2();

    protected abstract void initLineProfileEndX2();

    protected abstract void initLineProfileEndY2();

    protected void initGaussian2dComponents() {
        enable2DGaussianFitLabel = new JLabel("enable 2d gaussian fit");
        enable2DGaussianFit = generateCheckbox();
        enable2DGaussianFit2 = generateBooleanComboBox();
        fit2DNbIterMaxLabel = new JLabel("nb iterations max");
        fit2DNbIterMax = generateLabel();
        fit2DNbIterMax2 = generateWheelSwitch();
        fit2DMaxRelChangeLabel = new JLabel("relative change max");
        fit2DMaxRelChange = generateLabel();
        fit2DMaxRelChange2 = generateWheelSwitch();
        gaussianFitConvergedLabel = new JLabel("fit converged");
        gaussianFitConverged = generateCheckbox();
        gaussianFitMagnitudeLabel = new JLabel("magnitude");
        gaussianFitMagnitude = generateLabel();
        gaussianFitCenterXLabel = new JLabel("center X");
        gaussianFitCenterX = generateLabel();
        gaussianFitCenterYLabel = new JLabel("center Y");
        gaussianFitCenterY = generateLabel();
        gaussianFitVarianceXLabel = new JLabel("variance X");
        gaussianFitVarianceX = generateLabel();
        gaussianFitVarianceYLabel = new JLabel("variance Y");
        gaussianFitVarianceY = generateLabel();
        gaussianFitCovarianceXYLabel = new JLabel("covariance XY");
        gaussianFitCovarianceXY = generateLabel();
        gaussianFitMajorAxisFWHMLabel = new JLabel("major axis FWHM");
        gaussianFitMajorAxisFWHM = generateLabel();
        gaussianFitMinorAxisFWHMLabel = new JLabel("minor axis FWHM");
        gaussianFitMinorAxisFWHM = generateLabel();
        gaussianFitTiltLabel = new JLabel("tilt");
        gaussianFitTilt = generateLabel();
        gaussianFitBGLabel = new JLabel("background");
        gaussianFitBG = generateLabel();
        gaussianFitChi2Label = new JLabel("chi2");
        gaussianFitChi2 = generateLabel();
        gaussianFitNbIterLabel = new JLabel("nb iterations");
        gaussianFitNbIter = generateLabel();
        gaussianFitRelChangeLabel = new JLabel("relative change");
        gaussianFitRelChange = generateLabel();
    }

    protected void initPixelConversionComponents() {
        pixelSizeXLabel = new JLabel("pixel size X");
        pixelSizeX = generateLabel();
        pixelSizeX2 = generateWheelSwitch();
        pixelSizeYLabel = new JLabel("pixel size Y");
        pixelSizeY = generateLabel();
        pixelSizeY2 = generateWheelSwitch();
        opticalMagnificationLabel = new JLabel("optical magnification");
        opticalMagnification = generateLabel();
        opticalMagnification2 = generateWheelSwitch();
    }

    protected void initPreProcessingComponents() {
        rotationLabel = new JLabel("rotation");
        rotation = generateLabel();
        rotation2 = generateWheelSwitch();
        horizontalFlipLabel = new JLabel("horizontal flip");
        horizontalFlip = generateCheckbox();
        horizontalFlip2 = generateBooleanComboBox();
    }

    protected void initGeneralComponents() {
        computationPeriodLabel = new JLabel("computation period");
        computationPeriod = generateLabel();
        computationPeriod2 = generateWheelSwitch();
        estimComputTimeLabel = new JLabel("last image computation duration");
        estimComputTime = generateLabel();
    }

    protected void initGammaComponents() {
        bitsPerPixelLabel = new JLabel("bits per pixel");
        bitsPerPixel = generateLabel();
        bitsPerPixel2 = generateWheelSwitch();
        gammaCorrectionLabel = new JLabel("gamma correction");
        gammaCorrection = generateLabel();
        gammaCorrection2 = generateWheelSwitch();
    }

    protected void initHistogramComponents() {
        enableHistogramLabel = new JLabel("enable histogram");
        enableHistogram = generateCheckbox();
        enableHistogram2 = generateBooleanComboBox();
        histogramNbBinsLabel = new JLabel("nb bins");
        histogramNbBins = generateLabel();
        histogramNbBins2 = generateWheelSwitch();
        histogramRangeMinLabel = new JLabel("minimum intensity");
        histogramRangeMin = generateLabel();
        histogramRangeMin2 = generateWheelSwitch();
        histogramRangeMaxLabel = new JLabel("maximum intensity");
        histogramRangeMax = generateLabel();
        histogramRangeMax2 = generateWheelSwitch();
    }

    public JComponent getMainPanel() {
        if (mainPanel == null) {
//            mainPanel = dockingManager.createNewDockingArea(Color.gray);
//        mainPanel = new JPanel();
//            dockingManager.getPerspectiveFactory().setSelectedPerspective(
//                    CharlestonPerspectiveFactory.RIGHT_GRABBER_PERSPECTIVE);
            layoutMainPanel();

        }
        return mainPanel;
    }

    protected abstract void layoutMainPanel();

    /**
     * @return the line profilePanel
     */
    public JPanel getLineProfilePanel() {
        if (lineProfilePanel == null) {
            lineProfilePanel = new JPanel();
            lineProfilePanel.setLayout(new GridBagLayout());
            layoutLineProfilePanel();
        }
        return lineProfilePanel;
    }

    protected void layoutLineProfilePanel() {
        lineProfilePanel.setLayout(new GridBagLayout());

        GridBagConstraints fitConvergedLabelConstraints = new GridBagConstraints();
        fitConvergedLabelConstraints.fill = GridBagConstraints.NONE;
        fitConvergedLabelConstraints.gridx = 0;
        fitConvergedLabelConstraints.gridy = 0;
        fitConvergedLabelConstraints.weightx = 0;
        fitConvergedLabelConstraints.insets = SPACE;
        GridBagConstraints fitConvergedConstraints = new GridBagConstraints();
        fitConvergedConstraints.fill = GridBagConstraints.HORIZONTAL;
        fitConvergedConstraints.gridx = 1;
        fitConvergedConstraints.gridy = 0;
        fitConvergedConstraints.weightx = 1;
        fitConvergedConstraints.gridwidth = GridBagConstraints.REMAINDER;
        fitConvergedConstraints.insets = SPACE;

        GridBagConstraints originXLabelConstraints = new GridBagConstraints();
        originXLabelConstraints.fill = GridBagConstraints.NONE;
        originXLabelConstraints.gridx = 0;
        originXLabelConstraints.gridy = 1;
        originXLabelConstraints.weightx = 0;
        originXLabelConstraints.insets = SPACE;
        GridBagConstraints originXViewerConstraints = new GridBagConstraints();
        originXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        originXViewerConstraints.gridx = 1;
        originXViewerConstraints.gridy = 1;
        originXViewerConstraints.weightx = 0.9;
        originXViewerConstraints.insets = SPACE;
        GridBagConstraints originXSetterConstraints = new GridBagConstraints();
        originXSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        originXSetterConstraints.gridx = 2;
        originXSetterConstraints.gridy = 1;
        originXSetterConstraints.weightx = 0.1;
        originXSetterConstraints.insets = SPACE;

        GridBagConstraints originYLabelConstraints = new GridBagConstraints();
        originYLabelConstraints.fill = GridBagConstraints.NONE;
        originYLabelConstraints.gridx = 0;
        originYLabelConstraints.gridy = 2;
        originYLabelConstraints.weightx = 0;
        originYLabelConstraints.insets = SPACE;
        GridBagConstraints originYViewerConstraints = new GridBagConstraints();
        originYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        originYViewerConstraints.gridx = 1;
        originYViewerConstraints.gridy = 2;
        originYViewerConstraints.weightx = 0.9;
        originYViewerConstraints.insets = SPACE;
        GridBagConstraints originYSetterConstraints = new GridBagConstraints();
        originYSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        originYSetterConstraints.gridx = 2;
        originYSetterConstraints.gridy = 2;
        originYSetterConstraints.weightx = 0.1;
        originYSetterConstraints.insets = SPACE;

        GridBagConstraints endXLabelConstraints = new GridBagConstraints();
        endXLabelConstraints.fill = GridBagConstraints.NONE;
        endXLabelConstraints.gridx = 0;
        endXLabelConstraints.gridy = 3;
        endXLabelConstraints.weightx = 0;
        endXLabelConstraints.insets = SPACE;
        GridBagConstraints endXViewerConstraints = new GridBagConstraints();
        endXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        endXViewerConstraints.gridx = 1;
        endXViewerConstraints.gridy = 3;
        endXViewerConstraints.weightx = 0.9;
        endXViewerConstraints.insets = SPACE;
        GridBagConstraints endXSetterConstraints = new GridBagConstraints();
        endXSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        endXSetterConstraints.gridx = 2;
        endXSetterConstraints.gridy = 3;
        endXSetterConstraints.weightx = 0.1;
        endXSetterConstraints.insets = SPACE;

        GridBagConstraints endYLabelConstraints = new GridBagConstraints();
        endYLabelConstraints.fill = GridBagConstraints.NONE;
        endYLabelConstraints.gridx = 0;
        endYLabelConstraints.gridy = 4;
        endYLabelConstraints.weightx = 0;
        endYLabelConstraints.insets = SPACE;
        GridBagConstraints endYViewerConstraints = new GridBagConstraints();
        endYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        endYViewerConstraints.gridx = 1;
        endYViewerConstraints.gridy = 4;
        endYViewerConstraints.weightx = 0.9;
        endYViewerConstraints.insets = SPACE;
        GridBagConstraints endYSetterConstraints = new GridBagConstraints();
        endYSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        endYSetterConstraints.gridx = 2;
        endYSetterConstraints.gridy = 4;
        endYSetterConstraints.weightx = 0.1;
        endYSetterConstraints.insets = SPACE;

        GridBagConstraints thicknessLabelConstraints = new GridBagConstraints();
        thicknessLabelConstraints.fill = GridBagConstraints.NONE;
        thicknessLabelConstraints.gridx = 0;
        thicknessLabelConstraints.gridy = 5;
        thicknessLabelConstraints.weightx = 0;
        thicknessLabelConstraints.insets = SPACE;
        GridBagConstraints thicknessViewerConstraints = new GridBagConstraints();
        thicknessViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        thicknessViewerConstraints.gridx = 1;
        thicknessViewerConstraints.gridy = 5;
        thicknessViewerConstraints.weightx = 0.9;
        thicknessViewerConstraints.insets = SPACE;
        GridBagConstraints thicknessSetterConstraints = new GridBagConstraints();
        thicknessSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        thicknessSetterConstraints.gridx = 2;
        thicknessSetterConstraints.gridy = 5;
        thicknessSetterConstraints.weightx = 0.1;
        thicknessSetterConstraints.insets = SPACE;

        lineProfilePanel.add(lineProfileFitConvergedLabel, fitConvergedLabelConstraints);
        lineProfilePanel.add(getLineProfileFitConverged(), fitConvergedConstraints);

        lineProfilePanel.add(lineProfileOriginXLabel, originXLabelConstraints);
        lineProfilePanel.add(getLineProfileOriginX(), originXViewerConstraints);
        lineProfilePanel.add((JComponent) getLineProfileOriginX2(), originXSetterConstraints);

        lineProfilePanel.add(lineProfileOriginYLabel, originYLabelConstraints);
        lineProfilePanel.add(getLineProfileOriginY(), originYViewerConstraints);
        lineProfilePanel.add((JComponent) getLineProfileOriginY2(), originYSetterConstraints);

        lineProfilePanel.add(lineProfileEndXLabel, endXLabelConstraints);
        lineProfilePanel.add(getLineProfileEndX(), endXViewerConstraints);
        lineProfilePanel.add((JComponent) getLineProfileEndX2(), endXSetterConstraints);

        lineProfilePanel.add(lineProfileEndYLabel, endYLabelConstraints);
        lineProfilePanel.add(getLineProfileEndY(), endYViewerConstraints);
        lineProfilePanel.add((JComponent) getLineProfileEndY2(), endYSetterConstraints);

        lineProfilePanel.add(lineProfileThicknessLabel, thicknessLabelConstraints);
        lineProfilePanel.add(getLineProfileThickness(), thicknessViewerConstraints);
        lineProfilePanel.add((JComponent) getLineProfileThickness2(), thicknessSetterConstraints);
    }

    /**
     * @return the autoROIPanel
     */
    public JPanel getAutoROIPanel() {
        if (autoROIPanel == null) {
            autoROIPanel = new JPanel();
            layoutAutoRoiPanel();
        }
        return autoROIPanel;
    }

    protected void layoutAutoRoiPanel() {
        autoROIPanel.setLayout(new GridBagLayout());

        GridBagConstraints enableLabelConstraints = new GridBagConstraints();
        enableLabelConstraints.fill = GridBagConstraints.NONE;
        enableLabelConstraints.gridx = 0;
        enableLabelConstraints.gridy = 0;
        enableLabelConstraints.weightx = 0;
        enableLabelConstraints.insets = SPACE;
        GridBagConstraints enableViewerConstraints = new GridBagConstraints();
        enableViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableViewerConstraints.gridx = 1;
        enableViewerConstraints.gridy = 0;
        enableViewerConstraints.weightx = 0.9;
        enableViewerConstraints.insets = SPACE;
        GridBagConstraints enableSetterConstraints = new GridBagConstraints();
        enableSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableSetterConstraints.gridx = 2;
        enableSetterConstraints.gridy = 0;
        enableSetterConstraints.weightx = 0.1;
        enableSetterConstraints.insets = SPACE;

        GridBagConstraints xLabelConstraints = new GridBagConstraints();
        xLabelConstraints.fill = GridBagConstraints.NONE;
        xLabelConstraints.gridx = 0;
        xLabelConstraints.gridy = 1;
        xLabelConstraints.weightx = 0;
        xLabelConstraints.insets = SPACE;
        GridBagConstraints xViewerConstraints = new GridBagConstraints();
        xViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        xViewerConstraints.gridx = 1;
        xViewerConstraints.gridy = 1;
        xViewerConstraints.weightx = 1;
        xViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        xViewerConstraints.insets = SPACE;

        GridBagConstraints yLabelConstraints = new GridBagConstraints();
        yLabelConstraints.fill = GridBagConstraints.NONE;
        yLabelConstraints.gridx = 0;
        yLabelConstraints.gridy = 2;
        yLabelConstraints.weightx = 0;
        yLabelConstraints.insets = SPACE;
        GridBagConstraints yViewerConstraints = new GridBagConstraints();
        yViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        yViewerConstraints.gridx = 1;
        yViewerConstraints.gridy = 2;
        yViewerConstraints.weightx = 1;
        yViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        yViewerConstraints.insets = SPACE;

        GridBagConstraints widthLabelConstraints = new GridBagConstraints();
        widthLabelConstraints.fill = GridBagConstraints.NONE;
        widthLabelConstraints.gridx = 0;
        widthLabelConstraints.gridy = 3;
        widthLabelConstraints.weightx = 0;
        widthLabelConstraints.insets = SPACE;
        GridBagConstraints widthViewerConstraints = new GridBagConstraints();
        widthViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        widthViewerConstraints.gridx = 1;
        widthViewerConstraints.gridy = 3;
        widthViewerConstraints.weightx = 1;
        widthViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        widthViewerConstraints.insets = SPACE;

        GridBagConstraints heightLabelConstraints = new GridBagConstraints();
        heightLabelConstraints.fill = GridBagConstraints.NONE;
        heightLabelConstraints.gridx = 0;
        heightLabelConstraints.gridy = 4;
        heightLabelConstraints.weightx = 0;
        heightLabelConstraints.insets = SPACE;
        GridBagConstraints heightViewerConstraints = new GridBagConstraints();
        heightViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        heightViewerConstraints.gridx = 1;
        heightViewerConstraints.gridy = 4;
        heightViewerConstraints.weightx = 1;
        heightViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        heightViewerConstraints.insets = SPACE;

        GridBagConstraints foundLabelConstraints = new GridBagConstraints();
        foundLabelConstraints.fill = GridBagConstraints.NONE;
        foundLabelConstraints.gridx = 0;
        foundLabelConstraints.gridy = 5;
        foundLabelConstraints.weightx = 0;
        foundLabelConstraints.insets = SPACE;
        GridBagConstraints foundViewerConstraints = new GridBagConstraints();
        foundViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        foundViewerConstraints.gridx = 1;
        foundViewerConstraints.gridy = 5;
        foundViewerConstraints.weightx = 1;
        foundViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        foundViewerConstraints.insets = SPACE;

        GridBagConstraints alarmLabelConstraints = new GridBagConstraints();
        alarmLabelConstraints.fill = GridBagConstraints.NONE;
        alarmLabelConstraints.gridx = 0;
        alarmLabelConstraints.gridy = 6;
        alarmLabelConstraints.weightx = 0;
        alarmLabelConstraints.insets = SPACE;
        GridBagConstraints alarmViewerConstraints = new GridBagConstraints();
        alarmViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        alarmViewerConstraints.gridx = 1;
        alarmViewerConstraints.gridy = 6;
        alarmViewerConstraints.weightx = 0.9;
        alarmViewerConstraints.insets = SPACE;
        GridBagConstraints alarmSetterConstraints = new GridBagConstraints();
        alarmSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        alarmSetterConstraints.gridx = 2;
        alarmSetterConstraints.gridy = 6;
        alarmSetterConstraints.weightx = 0.1;
        alarmSetterConstraints.insets = SPACE;

        GridBagConstraints thresholdLabelConstraints = new GridBagConstraints();
        thresholdLabelConstraints.fill = GridBagConstraints.NONE;
        thresholdLabelConstraints.gridx = 0;
        thresholdLabelConstraints.gridy = 7;
        thresholdLabelConstraints.weightx = 0;
        thresholdLabelConstraints.insets = SPACE;
        GridBagConstraints thresholdViewerConstraints = new GridBagConstraints();
        thresholdViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        thresholdViewerConstraints.gridx = 1;
        thresholdViewerConstraints.gridy = 7;
        thresholdViewerConstraints.weightx = 0.9;
        thresholdViewerConstraints.insets = SPACE;
        GridBagConstraints thresholdSetterConstraints = new GridBagConstraints();
        thresholdSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        thresholdSetterConstraints.gridx = 2;
        thresholdSetterConstraints.gridy = 7;
        thresholdSetterConstraints.weightx = 0.1;
        thresholdSetterConstraints.insets = SPACE;

        GridBagConstraints factorXLabelConstraints = new GridBagConstraints();
        factorXLabelConstraints.fill = GridBagConstraints.NONE;
        factorXLabelConstraints.gridx = 0;
        factorXLabelConstraints.gridy = 8;
        factorXLabelConstraints.weightx = 0;
        factorXLabelConstraints.insets = SPACE;
        GridBagConstraints factorXViewerConstraints = new GridBagConstraints();
        factorXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        factorXViewerConstraints.gridx = 1;
        factorXViewerConstraints.gridy = 8;
        factorXViewerConstraints.weightx = 0.9;
        factorXViewerConstraints.insets = SPACE;
        GridBagConstraints factorXSetterConstraints = new GridBagConstraints();
        factorXSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        factorXSetterConstraints.gridx = 2;
        factorXSetterConstraints.gridy = 8;
        factorXSetterConstraints.weightx = 0.1;
        factorXSetterConstraints.insets = SPACE;

        GridBagConstraints factorYLabelConstraints = new GridBagConstraints();
        factorYLabelConstraints.fill = GridBagConstraints.NONE;
        factorYLabelConstraints.gridx = 0;
        factorYLabelConstraints.gridy = 9;
        factorYLabelConstraints.weightx = 0;
        factorYLabelConstraints.insets = SPACE;
        GridBagConstraints factorYViewerConstraints = new GridBagConstraints();
        factorYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        factorYViewerConstraints.gridx = 1;
        factorYViewerConstraints.gridy = 9;
        factorYViewerConstraints.weightx = 0.9;
        factorYViewerConstraints.insets = SPACE;
        GridBagConstraints factorYSetterConstraints = new GridBagConstraints();
        factorYSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        factorYSetterConstraints.gridx = 2;
        factorYSetterConstraints.gridy = 9;
        factorYSetterConstraints.weightx = 0.1;
        factorYSetterConstraints.insets = SPACE;

        autoROIPanel.add(enableAutoROILabel, enableLabelConstraints);
        autoROIPanel.add(getEnableAutoROI(), enableViewerConstraints);
        autoROIPanel.add(getEnableAutoROI2(), enableSetterConstraints);

        autoROIPanel.add(autoROIOriginXLabel, xLabelConstraints);
        autoROIPanel.add(getAutoROIOriginX(), xViewerConstraints);

        autoROIPanel.add(autoROIOriginYLabel, yLabelConstraints);
        autoROIPanel.add(getAutoROIOriginY(), yViewerConstraints);

        autoROIPanel.add(autoROIWidthLabel, widthLabelConstraints);
        autoROIPanel.add(getAutoROIWidth(), widthViewerConstraints);

        autoROIPanel.add(autoROIHeightLabel, heightLabelConstraints);
        autoROIPanel.add(getAutoROIHeight(), heightViewerConstraints);

        autoROIPanel.add(autoROIFoundLabel, foundLabelConstraints);
        autoROIPanel.add(getAutoROIFound(), foundViewerConstraints);

        autoROIPanel.add(alarmZoneLabel, alarmLabelConstraints);
        autoROIPanel.add(getAlarmZone(), alarmViewerConstraints);
        autoROIPanel.add(getAlarmZone2(), alarmSetterConstraints);

        autoROIPanel.add(autoROIThresholdLabel, thresholdLabelConstraints);
        autoROIPanel.add(getAutoROIThreshold(), thresholdViewerConstraints);
        autoROIPanel.add(getAutoROIThreshold2(), thresholdSetterConstraints);

        autoROIPanel.add(autoROIMagFactorXLabel, factorXLabelConstraints);
        autoROIPanel.add(getAutoROIMagFactorX(), factorXViewerConstraints);
        autoROIPanel.add(getAutoROIMagFactorX2(), factorXSetterConstraints);

        autoROIPanel.add(autoROIMagFactorYLabel, factorYLabelConstraints);
        autoROIPanel.add(getAutoROIMagFactorY(), factorYViewerConstraints);
        autoROIPanel.add(getAutoROIMagFactorY2(), factorYSetterConstraints);

        // autoROIPanel.setBorder(new TitledBorder("Auto ROI"));
    }

    /**
     * @return the userROIPanel
     */
    public JPanel getUserROIPanel() {
        if (userROIPanel == null) {
            userROIPanel = new JPanel();
            layoutUserRoiPanel();
        }
        return userROIPanel;
    }

    protected void layoutUserRoiPanel() {
        userROIPanel.setLayout(new GridBagLayout());
        GridBagConstraints enableLabelConstraints = new GridBagConstraints();
        enableLabelConstraints.fill = GridBagConstraints.NONE;
        enableLabelConstraints.gridx = 0;
        enableLabelConstraints.gridy = 0;
        enableLabelConstraints.weightx = 0;
        enableLabelConstraints.insets = SPACE;
        GridBagConstraints enableViewerConstraints = new GridBagConstraints();
        enableViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableViewerConstraints.gridx = 1;
        enableViewerConstraints.gridy = 0;
        enableViewerConstraints.weightx = 0.9;
        enableViewerConstraints.insets = SPACE;
        GridBagConstraints enableSetterConstraints = new GridBagConstraints();
        enableSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableSetterConstraints.gridx = 2;
        enableSetterConstraints.gridy = 0;
        enableSetterConstraints.weightx = 0.1;
        enableSetterConstraints.insets = SPACE;

        GridBagConstraints xLabelConstraints = new GridBagConstraints();
        xLabelConstraints.fill = GridBagConstraints.NONE;
        xLabelConstraints.gridx = 0;
        xLabelConstraints.gridy = 1;
        xLabelConstraints.weightx = 0;
        xLabelConstraints.insets = SPACE;
        GridBagConstraints xViewerConstraints = new GridBagConstraints();
        xViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        xViewerConstraints.gridx = 1;
        xViewerConstraints.gridy = 1;
        xViewerConstraints.weightx = 0.9;
        xViewerConstraints.insets = SPACE;
        GridBagConstraints xSetterConstraints = new GridBagConstraints();
        xSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        xSetterConstraints.gridx = 2;
        xSetterConstraints.gridy = 1;
        xSetterConstraints.weightx = 0.1;
        xSetterConstraints.insets = SPACE;

        GridBagConstraints yLabelConstraints = new GridBagConstraints();
        yLabelConstraints.fill = GridBagConstraints.NONE;
        yLabelConstraints.gridx = 0;
        yLabelConstraints.gridy = 2;
        yLabelConstraints.weightx = 0;
        yLabelConstraints.insets = SPACE;
        GridBagConstraints yViewerConstraints = new GridBagConstraints();
        yViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        yViewerConstraints.gridx = 1;
        yViewerConstraints.gridy = 2;
        yViewerConstraints.weightx = 0.9;
        yViewerConstraints.insets = SPACE;
        GridBagConstraints ySetterConstraints = new GridBagConstraints();
        ySetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        ySetterConstraints.gridx = 2;
        ySetterConstraints.gridy = 2;
        ySetterConstraints.weightx = 0.1;
        ySetterConstraints.insets = SPACE;

        GridBagConstraints widthLabelConstraints = new GridBagConstraints();
        widthLabelConstraints.fill = GridBagConstraints.NONE;
        widthLabelConstraints.gridx = 0;
        widthLabelConstraints.gridy = 3;
        widthLabelConstraints.weightx = 0;
        widthLabelConstraints.insets = SPACE;
        GridBagConstraints widthViewerConstraints = new GridBagConstraints();
        widthViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        widthViewerConstraints.gridx = 1;
        widthViewerConstraints.gridy = 3;
        widthViewerConstraints.weightx = 0.9;
        widthViewerConstraints.insets = SPACE;
        GridBagConstraints widthSetterConstraints = new GridBagConstraints();
        widthSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        widthSetterConstraints.gridx = 2;
        widthSetterConstraints.gridy = 3;
        widthSetterConstraints.weightx = 0.1;
        widthSetterConstraints.insets = SPACE;

        GridBagConstraints heightLabelConstraints = new GridBagConstraints();
        heightLabelConstraints.fill = GridBagConstraints.NONE;
        heightLabelConstraints.gridx = 0;
        heightLabelConstraints.gridy = 4;
        heightLabelConstraints.weightx = 0;
        heightLabelConstraints.insets = SPACE;
        GridBagConstraints heightViewerConstraints = new GridBagConstraints();
        heightViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        heightViewerConstraints.gridx = 1;
        heightViewerConstraints.gridy = 4;
        heightViewerConstraints.weightx = 0.9;
        heightViewerConstraints.insets = SPACE;
        GridBagConstraints heightSetterConstraints = new GridBagConstraints();
        heightSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        heightSetterConstraints.gridx = 2;
        heightSetterConstraints.gridy = 4;
        heightSetterConstraints.weightx = 0.1;
        heightSetterConstraints.insets = SPACE;

        userROIPanel.add(enableUserROILabel, enableLabelConstraints);
        userROIPanel.add(enableUserROI, enableViewerConstraints);
        userROIPanel.add(enableUserROI2, enableSetterConstraints);

        userROIPanel.add(userROIOriginXLabel, xLabelConstraints);
        userROIPanel.add(userROIOriginX, xViewerConstraints);
        userROIPanel.add((JComponent) userROIOriginX2, xSetterConstraints);

        userROIPanel.add(userROIOriginYLabel, yLabelConstraints);
        userROIPanel.add(userROIOriginY, yViewerConstraints);
        userROIPanel.add((JComponent) userROIOriginY2, ySetterConstraints);

        userROIPanel.add(userROIWidthLabel, widthLabelConstraints);
        userROIPanel.add(userROIWidth, widthViewerConstraints);
        userROIPanel.add((JComponent) userROIWidth2, widthSetterConstraints);

        userROIPanel.add(userROIHeightLabel, heightLabelConstraints);
        userROIPanel.add(userROIHeight, heightViewerConstraints);
        userROIPanel.add((JComponent) userROIHeight2, heightSetterConstraints);
    }

    /**
     * @return the gammaPanel
     */
    public JPanel getGammaPanel() {
        if (gammaPanel == null) {
            gammaPanel = new JPanel();
            gammaPanel.setLayout(new GridBagLayout());

            GridBagConstraints correctionLabelConstraints = new GridBagConstraints();
            correctionLabelConstraints.fill = GridBagConstraints.NONE;
            correctionLabelConstraints.gridx = 0;
            correctionLabelConstraints.gridy = 0;
            correctionLabelConstraints.weightx = 0;
            correctionLabelConstraints.insets = SPACE;
            GridBagConstraints correctionViewerConstraints = new GridBagConstraints();
            correctionViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            correctionViewerConstraints.gridx = 1;
            correctionViewerConstraints.gridy = 0;
            correctionViewerConstraints.weightx = 0.9;
            correctionViewerConstraints.insets = SPACE;
            GridBagConstraints correctionSetterConstraints = new GridBagConstraints();
            correctionSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
            correctionSetterConstraints.gridx = 2;
            correctionSetterConstraints.gridy = 0;
            correctionSetterConstraints.weightx = 0.1;
            correctionSetterConstraints.insets = SPACE;

            GridBagConstraints bppLabelConstraints = new GridBagConstraints();
            bppLabelConstraints.fill = GridBagConstraints.NONE;
            bppLabelConstraints.gridx = 0;
            bppLabelConstraints.gridy = 1;
            bppLabelConstraints.weightx = 0;
            bppLabelConstraints.insets = SPACE;
            GridBagConstraints bppViewerConstraints = new GridBagConstraints();
            bppViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            bppViewerConstraints.gridx = 1;
            bppViewerConstraints.gridy = 1;
            bppViewerConstraints.weightx = 0.9;
            bppViewerConstraints.insets = SPACE;
            GridBagConstraints bppSetterConstraints = new GridBagConstraints();
            bppSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
            bppSetterConstraints.gridx = 2;
            bppSetterConstraints.gridy = 1;
            bppSetterConstraints.weightx = 0.1;
            bppSetterConstraints.insets = SPACE;

            gammaPanel.add(gammaCorrectionLabel, correctionLabelConstraints);
            gammaPanel.add(gammaCorrection, correctionViewerConstraints);
            gammaPanel.add(gammaCorrection2, correctionSetterConstraints);

            gammaPanel.add(bitsPerPixelLabel, bppLabelConstraints);
            gammaPanel.add(bitsPerPixel, bppViewerConstraints);
            gammaPanel.add(bitsPerPixel2, bppSetterConstraints);

            // gammaPanel.setBorder(new TitledBorder("Gamma"));
        }
        return gammaPanel;
    }

    /**
     * @return the histogramPanel
     */
    public JPanel getHistogramPanel() {
        if (histogramPanel == null) {
            histogramPanel = new JPanel();
            layoutHistogramPanel();
        }
        return histogramPanel;
    }

    protected void layoutHistogramPanel() {
        histogramPanel.setLayout(new GridBagLayout());

        GridBagConstraints enableLabelConstraints = new GridBagConstraints();
        enableLabelConstraints.fill = GridBagConstraints.NONE;
        enableLabelConstraints.gridx = 0;
        enableLabelConstraints.gridy = 0;
        enableLabelConstraints.weightx = 0;
        enableLabelConstraints.insets = SPACE;
        GridBagConstraints enableViewerConstraints = new GridBagConstraints();
        enableViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableViewerConstraints.gridx = 1;
        enableViewerConstraints.gridy = 0;
        enableViewerConstraints.weightx = 0.9;
        enableViewerConstraints.insets = SPACE;
        GridBagConstraints enableSetterConstraints = new GridBagConstraints();
        enableSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableSetterConstraints.gridx = 2;
        enableSetterConstraints.gridy = 0;
        enableSetterConstraints.weightx = 0.1;
        enableSetterConstraints.insets = SPACE;

        GridBagConstraints nbBinsLabelConstraints = new GridBagConstraints();
        nbBinsLabelConstraints.fill = GridBagConstraints.NONE;
        nbBinsLabelConstraints.gridx = 0;
        nbBinsLabelConstraints.gridy = 1;
        nbBinsLabelConstraints.weightx = 0;
        nbBinsLabelConstraints.insets = SPACE;
        GridBagConstraints nbBinsViewerConstraints = new GridBagConstraints();
        nbBinsViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        nbBinsViewerConstraints.gridx = 1;
        nbBinsViewerConstraints.gridy = 1;
        nbBinsViewerConstraints.weightx = 0.9;
        nbBinsViewerConstraints.insets = SPACE;
        GridBagConstraints nbBinsSetterConstraints = new GridBagConstraints();
        nbBinsSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        nbBinsSetterConstraints.gridx = 2;
        nbBinsSetterConstraints.gridy = 1;
        nbBinsSetterConstraints.weightx = 0.1;
        nbBinsSetterConstraints.insets = SPACE;

        GridBagConstraints rangeMinLabelConstraints = new GridBagConstraints();
        rangeMinLabelConstraints.fill = GridBagConstraints.NONE;
        rangeMinLabelConstraints.gridx = 0;
        rangeMinLabelConstraints.gridy = 2;
        rangeMinLabelConstraints.weightx = 0;
        rangeMinLabelConstraints.insets = SPACE;
        GridBagConstraints rangeMinViewerConstraints = new GridBagConstraints();
        rangeMinViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        rangeMinViewerConstraints.gridx = 1;
        rangeMinViewerConstraints.gridy = 2;
        rangeMinViewerConstraints.weightx = 0.9;
        rangeMinViewerConstraints.insets = SPACE;
        GridBagConstraints rangeMinSetterConstraints = new GridBagConstraints();
        rangeMinSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        rangeMinSetterConstraints.gridx = 2;
        rangeMinSetterConstraints.gridy = 2;
        rangeMinSetterConstraints.weightx = 0.1;
        rangeMinSetterConstraints.insets = SPACE;

        GridBagConstraints rangeMaxLabelConstraints = new GridBagConstraints();
        rangeMaxLabelConstraints.fill = GridBagConstraints.NONE;
        rangeMaxLabelConstraints.gridx = 0;
        rangeMaxLabelConstraints.gridy = 3;
        rangeMaxLabelConstraints.weightx = 0;
        rangeMaxLabelConstraints.insets = SPACE;
        GridBagConstraints rangeMaxViewerConstraints = new GridBagConstraints();
        rangeMaxViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        rangeMaxViewerConstraints.gridx = 1;
        rangeMaxViewerConstraints.gridy = 3;
        rangeMaxViewerConstraints.weightx = 0.9;
        rangeMaxViewerConstraints.insets = SPACE;
        GridBagConstraints rangeMaxSetterConstraints = new GridBagConstraints();
        rangeMaxSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        rangeMaxSetterConstraints.gridx = 2;
        rangeMaxSetterConstraints.gridy = 3;
        rangeMaxSetterConstraints.weightx = 0.1;
        rangeMaxSetterConstraints.insets = SPACE;

        histogramPanel.add(enableHistogramLabel, enableLabelConstraints);
        histogramPanel.add(enableHistogram, enableViewerConstraints);
        histogramPanel.add(enableHistogram2, enableSetterConstraints);

        histogramPanel.add(histogramNbBinsLabel, nbBinsLabelConstraints);
        histogramPanel.add(histogramNbBins, nbBinsViewerConstraints);
        histogramPanel.add(histogramNbBins2, nbBinsSetterConstraints);

        histogramPanel.add(histogramRangeMinLabel, rangeMinLabelConstraints);
        histogramPanel.add(histogramRangeMin, rangeMinViewerConstraints);
        histogramPanel.add(histogramRangeMin2, rangeMinSetterConstraints);

        histogramPanel.add(histogramRangeMaxLabel, rangeMaxLabelConstraints);
        histogramPanel.add(histogramRangeMax, rangeMaxViewerConstraints);
        histogramPanel.add(histogramRangeMax2, rangeMaxSetterConstraints);

        // histogramPanel.setBorder(new TitledBorder("Histogram setup"));
    }

    /**
     * @return the momentsPanel
     */
    public JPanel getMomentsPanel() {
        if (momentsPanel == null) {
            momentsPanel = new JPanel();
            layoutMomentsPanel();
        }
        return momentsPanel;
    }

    protected void layoutMomentsPanel() {
        momentsPanel.setLayout(new GridBagLayout());

        GridBagConstraints enableLabelConstraints = new GridBagConstraints();
        enableLabelConstraints.fill = GridBagConstraints.NONE;
        enableLabelConstraints.gridx = 0;
        enableLabelConstraints.gridy = 0;
        enableLabelConstraints.weightx = 0;
        enableLabelConstraints.insets = SPACE;
        GridBagConstraints enableViewerConstraints = new GridBagConstraints();
        enableViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableViewerConstraints.gridx = 1;
        enableViewerConstraints.gridy = 0;
        enableViewerConstraints.weightx = 0.9;
        enableViewerConstraints.insets = SPACE;
        GridBagConstraints enableSetterConstraints = new GridBagConstraints();
        enableSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableSetterConstraints.gridx = 2;
        enableSetterConstraints.gridy = 0;
        enableSetterConstraints.weightx = 0.1;
        enableSetterConstraints.insets = SPACE;

        GridBagConstraints maxLabelConstraints = new GridBagConstraints();
        maxLabelConstraints.fill = GridBagConstraints.NONE;
        maxLabelConstraints.gridx = 0;
        maxLabelConstraints.gridy = 1;
        maxLabelConstraints.weightx = 0;
        maxLabelConstraints.insets = SPACE;
        GridBagConstraints maxViewerConstraints = new GridBagConstraints();
        maxViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        maxViewerConstraints.gridx = 1;
        maxViewerConstraints.gridy = 1;
        maxViewerConstraints.weightx = 1;
        maxViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        maxViewerConstraints.insets = SPACE;

        GridBagConstraints meanLabelConstraints = new GridBagConstraints();
        meanLabelConstraints.fill = GridBagConstraints.NONE;
        meanLabelConstraints.gridx = 0;
        meanLabelConstraints.gridy = 2;
        meanLabelConstraints.weightx = 0;
        meanLabelConstraints.insets = SPACE;
        GridBagConstraints meanViewerConstraints = new GridBagConstraints();
        meanViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        meanViewerConstraints.gridx = 1;
        meanViewerConstraints.gridy = 2;
        meanViewerConstraints.weightx = 1;
        meanViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        meanViewerConstraints.insets = SPACE;

        GridBagConstraints centroidXLabelConstraints = new GridBagConstraints();
        centroidXLabelConstraints.fill = GridBagConstraints.NONE;
        centroidXLabelConstraints.gridx = 0;
        centroidXLabelConstraints.gridy = 3;
        centroidXLabelConstraints.weightx = 0;
        centroidXLabelConstraints.insets = SPACE;
        GridBagConstraints centroidXViewerConstraints = new GridBagConstraints();
        centroidXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        centroidXViewerConstraints.gridx = 1;
        centroidXViewerConstraints.gridy = 3;
        centroidXViewerConstraints.weightx = 1;
        centroidXViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        centroidXViewerConstraints.insets = SPACE;

        GridBagConstraints centroidYLabelConstraints = new GridBagConstraints();
        centroidYLabelConstraints.fill = GridBagConstraints.NONE;
        centroidYLabelConstraints.gridx = 0;
        centroidYLabelConstraints.gridy = 4;
        centroidYLabelConstraints.weightx = 0;
        centroidYLabelConstraints.insets = SPACE;
        GridBagConstraints centroidYViewerConstraints = new GridBagConstraints();
        centroidYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        centroidYViewerConstraints.gridx = 1;
        centroidYViewerConstraints.gridy = 4;
        centroidYViewerConstraints.weightx = 1;
        centroidYViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        centroidYViewerConstraints.insets = SPACE;

        GridBagConstraints varianceXLabelConstraints = new GridBagConstraints();
        varianceXLabelConstraints.fill = GridBagConstraints.NONE;
        varianceXLabelConstraints.gridx = 0;
        varianceXLabelConstraints.gridy = 5;
        varianceXLabelConstraints.weightx = 0;
        varianceXLabelConstraints.insets = SPACE;
        GridBagConstraints varianceXViewerConstraints = new GridBagConstraints();
        varianceXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        varianceXViewerConstraints.gridx = 1;
        varianceXViewerConstraints.gridy = 5;
        varianceXViewerConstraints.weightx = 1;
        varianceXViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        varianceXViewerConstraints.insets = SPACE;

        GridBagConstraints varianceYLabelConstraints = new GridBagConstraints();
        varianceYLabelConstraints.fill = GridBagConstraints.NONE;
        varianceYLabelConstraints.gridx = 0;
        varianceYLabelConstraints.gridy = 6;
        varianceYLabelConstraints.weightx = 0;
        varianceYLabelConstraints.insets = SPACE;
        GridBagConstraints varianceYViewerConstraints = new GridBagConstraints();
        varianceYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        varianceYViewerConstraints.gridx = 1;
        varianceYViewerConstraints.gridy = 6;
        varianceYViewerConstraints.weightx = 1;
        varianceYViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        varianceYViewerConstraints.insets = SPACE;

        GridBagConstraints covarianceLabelConstraints = new GridBagConstraints();
        covarianceLabelConstraints.fill = GridBagConstraints.NONE;
        covarianceLabelConstraints.gridx = 0;
        covarianceLabelConstraints.gridy = 7;
        covarianceLabelConstraints.weightx = 0;
        covarianceLabelConstraints.insets = SPACE;
        GridBagConstraints covarianceViewerConstraints = new GridBagConstraints();
        covarianceViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        covarianceViewerConstraints.gridx = 1;
        covarianceViewerConstraints.gridy = 7;
        covarianceViewerConstraints.weightx = 1;
        covarianceViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        covarianceViewerConstraints.insets = SPACE;

        GridBagConstraints correlationLabelConstraints = new GridBagConstraints();
        correlationLabelConstraints.fill = GridBagConstraints.NONE;
        correlationLabelConstraints.gridx = 0;
        correlationLabelConstraints.gridy = 8;
        correlationLabelConstraints.weightx = 0;
        correlationLabelConstraints.insets = SPACE;
        GridBagConstraints correlationViewerConstraints = new GridBagConstraints();
        correlationViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        correlationViewerConstraints.gridx = 1;
        correlationViewerConstraints.gridy = 8;
        correlationViewerConstraints.weightx = 1;
        correlationViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        correlationViewerConstraints.insets = SPACE;

        GridBagConstraints skewXLabelConstraints = new GridBagConstraints();
        skewXLabelConstraints.fill = GridBagConstraints.NONE;
        skewXLabelConstraints.gridx = 0;
        skewXLabelConstraints.gridy = 9;
        skewXLabelConstraints.weightx = 0;
        skewXLabelConstraints.insets = SPACE;
        GridBagConstraints skewXViewerConstraints = new GridBagConstraints();
        skewXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        skewXViewerConstraints.gridx = 1;
        skewXViewerConstraints.gridy = 9;
        skewXViewerConstraints.weightx = 1;
        skewXViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        skewXViewerConstraints.insets = SPACE;

        GridBagConstraints skewYLabelConstraints = new GridBagConstraints();
        skewYLabelConstraints.fill = GridBagConstraints.NONE;
        skewYLabelConstraints.gridx = 0;
        skewYLabelConstraints.gridy = 10;
        skewYLabelConstraints.weightx = 0;
        skewYLabelConstraints.insets = SPACE;
        GridBagConstraints skewYViewerConstraints = new GridBagConstraints();
        skewYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        skewYViewerConstraints.gridx = 1;
        skewYViewerConstraints.gridy = 10;
        skewYViewerConstraints.weightx = 1;
        skewYViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        skewYViewerConstraints.insets = SPACE;

        GridBagConstraints skewX2YLabelConstraints = new GridBagConstraints();
        skewX2YLabelConstraints.fill = GridBagConstraints.NONE;
        skewX2YLabelConstraints.gridx = 0;
        skewX2YLabelConstraints.gridy = 11;
        skewX2YLabelConstraints.weightx = 0;
        skewX2YLabelConstraints.insets = SPACE;
        GridBagConstraints skewX2YViewerConstraints = new GridBagConstraints();
        skewX2YViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        skewX2YViewerConstraints.gridx = 1;
        skewX2YViewerConstraints.gridy = 11;
        skewX2YViewerConstraints.weightx = 1;
        skewX2YViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        skewX2YViewerConstraints.insets = SPACE;

        GridBagConstraints skewXY2LabelConstraints = new GridBagConstraints();
        skewXY2LabelConstraints.fill = GridBagConstraints.NONE;
        skewXY2LabelConstraints.gridx = 0;
        skewXY2LabelConstraints.gridy = 12;
        skewXY2LabelConstraints.weightx = 0;
        skewXY2LabelConstraints.insets = SPACE;
        GridBagConstraints skewXY2ViewerConstraints = new GridBagConstraints();
        skewXY2ViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        skewXY2ViewerConstraints.gridx = 1;
        skewXY2ViewerConstraints.gridy = 12;
        skewXY2ViewerConstraints.weightx = 1;
        skewXY2ViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        skewXY2ViewerConstraints.insets = SPACE;

        momentsPanel.add(enableImageMomentsLabel, enableLabelConstraints);
        momentsPanel.add(enableImageMoments, enableViewerConstraints);
        momentsPanel.add(enableImageMoments2, enableSetterConstraints);

        momentsPanel.add(maxIntensityLabel, maxLabelConstraints);
        momentsPanel.add(maxIntensity, maxViewerConstraints);

        momentsPanel.add(meanIntensityLabel, meanLabelConstraints);
        momentsPanel.add(meanIntensity, meanViewerConstraints);

        momentsPanel.add(centroidXLabel, centroidXLabelConstraints);
        momentsPanel.add(centroidX, centroidXViewerConstraints);

        momentsPanel.add(centroidYLabel, centroidYLabelConstraints);
        momentsPanel.add(centroidY, centroidYViewerConstraints);

        momentsPanel.add(varianceXLabel, varianceXLabelConstraints);
        momentsPanel.add(varianceX, varianceXViewerConstraints);

        momentsPanel.add(varianceYLabel, varianceYLabelConstraints);
        momentsPanel.add(varianceY, varianceYViewerConstraints);

        momentsPanel.add(covarianceXYLabel, covarianceLabelConstraints);
        momentsPanel.add(covarianceXY, covarianceViewerConstraints);

        momentsPanel.add(correlationXYLabel, correlationLabelConstraints);
        momentsPanel.add(correlationXY, correlationViewerConstraints);

        momentsPanel.add(skewXLabel, skewXLabelConstraints);
        momentsPanel.add(skewX, skewXViewerConstraints);

        momentsPanel.add(skewYLabel, skewYLabelConstraints);
        momentsPanel.add(skewY, skewYViewerConstraints);

        momentsPanel.add(skewX2YLabel, skewX2YLabelConstraints);
        momentsPanel.add(skewX2Y, skewX2YViewerConstraints);

        momentsPanel.add(skewXY2Label, skewXY2LabelConstraints);
        momentsPanel.add(skewXY2, skewXY2ViewerConstraints);
    }

    /**
     * @return the gaussian2dPanel
     */
    public JPanel getGaussian2dPanel() {
        if (gaussian2dPanel == null) {
            gaussian2dPanel = new JPanel();
            layoutGaussian2dPanel();
        }
        return gaussian2dPanel;
    }

    protected void layoutGaussian2dPanel() {
        gaussian2dPanel.setLayout(new GridBagLayout());

        GridBagConstraints enableLabelConstraints = new GridBagConstraints();
        enableLabelConstraints.fill = GridBagConstraints.NONE;
        enableLabelConstraints.gridx = 0;
        enableLabelConstraints.gridy = 0;
        enableLabelConstraints.weightx = 0;
        enableLabelConstraints.insets = SPACE;
        GridBagConstraints enableViewerConstraints = new GridBagConstraints();
        enableViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableViewerConstraints.gridx = 1;
        enableViewerConstraints.gridy = 0;
        enableViewerConstraints.weightx = 0.9;
        enableViewerConstraints.insets = SPACE;
        GridBagConstraints enableSetterConstraints = new GridBagConstraints();
        enableSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        enableSetterConstraints.gridx = 2;
        enableSetterConstraints.gridy = 0;
        enableSetterConstraints.weightx = 0.1;
        enableSetterConstraints.insets = SPACE;

        GridBagConstraints iterMLabelConstraints = new GridBagConstraints();
        iterMLabelConstraints.fill = GridBagConstraints.NONE;
        iterMLabelConstraints.gridx = 0;
        iterMLabelConstraints.gridy = 1;
        iterMLabelConstraints.weightx = 0;
        iterMLabelConstraints.insets = SPACE;
        GridBagConstraints iterMViewerConstraints = new GridBagConstraints();
        iterMViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        iterMViewerConstraints.gridx = 1;
        iterMViewerConstraints.gridy = 1;
        iterMViewerConstraints.weightx = 0.9;
        iterMViewerConstraints.insets = SPACE;
        GridBagConstraints iterMSetterConstraints = new GridBagConstraints();
        iterMSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        iterMSetterConstraints.gridx = 2;
        iterMSetterConstraints.gridy = 1;
        iterMSetterConstraints.weightx = 0.1;
        iterMSetterConstraints.insets = SPACE;

        GridBagConstraints changeMLabelConstraints = new GridBagConstraints();
        changeMLabelConstraints.fill = GridBagConstraints.NONE;
        changeMLabelConstraints.gridx = 0;
        changeMLabelConstraints.gridy = 2;
        changeMLabelConstraints.weightx = 0;
        changeMLabelConstraints.insets = SPACE;
        GridBagConstraints changeMViewerConstraints = new GridBagConstraints();
        changeMViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        changeMViewerConstraints.gridx = 1;
        changeMViewerConstraints.gridy = 2;
        changeMViewerConstraints.weightx = 0.9;
        changeMViewerConstraints.insets = SPACE;
        GridBagConstraints changeMSetterConstraints = new GridBagConstraints();
        changeMSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        changeMSetterConstraints.gridx = 2;
        changeMSetterConstraints.gridy = 2;
        changeMSetterConstraints.weightx = 0.1;
        changeMSetterConstraints.insets = SPACE;

        GridBagConstraints convergedLabelConstraints = new GridBagConstraints();
        convergedLabelConstraints.fill = GridBagConstraints.NONE;
        convergedLabelConstraints.gridx = 0;
        convergedLabelConstraints.gridy = 3;
        convergedLabelConstraints.weightx = 0;
        convergedLabelConstraints.insets = SPACE;
        GridBagConstraints convergedViewerConstraints = new GridBagConstraints();
        convergedViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        convergedViewerConstraints.gridx = 1;
        convergedViewerConstraints.gridy = 3;
        convergedViewerConstraints.weightx = 1;
        convergedViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        convergedViewerConstraints.insets = SPACE;

        GridBagConstraints magLabelConstraints = new GridBagConstraints();
        magLabelConstraints.fill = GridBagConstraints.NONE;
        magLabelConstraints.gridx = 0;
        magLabelConstraints.gridy = 4;
        magLabelConstraints.weightx = 0;
        magLabelConstraints.insets = SPACE;
        GridBagConstraints magViewerConstraints = new GridBagConstraints();
        magViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        magViewerConstraints.gridx = 1;
        magViewerConstraints.gridy = 4;
        magViewerConstraints.weightx = 1;
        magViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        magViewerConstraints.insets = SPACE;

        GridBagConstraints centerXLabelConstraints = new GridBagConstraints();
        centerXLabelConstraints.fill = GridBagConstraints.NONE;
        centerXLabelConstraints.gridx = 0;
        centerXLabelConstraints.gridy = 5;
        centerXLabelConstraints.weightx = 0;
        centerXLabelConstraints.insets = SPACE;
        GridBagConstraints centerXViewerConstraints = new GridBagConstraints();
        centerXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        centerXViewerConstraints.gridx = 1;
        centerXViewerConstraints.gridy = 5;
        centerXViewerConstraints.weightx = 1;
        centerXViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        centerXViewerConstraints.insets = SPACE;

        GridBagConstraints centerYLabelConstraints = new GridBagConstraints();
        centerYLabelConstraints.fill = GridBagConstraints.NONE;
        centerYLabelConstraints.gridx = 0;
        centerYLabelConstraints.gridy = 6;
        centerYLabelConstraints.weightx = 0;
        centerYLabelConstraints.insets = SPACE;
        GridBagConstraints centerYViewerConstraints = new GridBagConstraints();
        centerYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        centerYViewerConstraints.gridx = 1;
        centerYViewerConstraints.gridy = 6;
        centerYViewerConstraints.weightx = 1;
        centerYViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        centerYViewerConstraints.insets = SPACE;

        GridBagConstraints varianceXLabelConstraints = new GridBagConstraints();
        varianceXLabelConstraints.fill = GridBagConstraints.NONE;
        varianceXLabelConstraints.gridx = 0;
        varianceXLabelConstraints.gridy = 7;
        varianceXLabelConstraints.weightx = 0;
        varianceXLabelConstraints.insets = SPACE;
        GridBagConstraints varianceXViewerConstraints = new GridBagConstraints();
        varianceXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        varianceXViewerConstraints.gridx = 1;
        varianceXViewerConstraints.gridy = 7;
        varianceXViewerConstraints.weightx = 1;
        varianceXViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        varianceXViewerConstraints.insets = SPACE;

        GridBagConstraints varianceYLabelConstraints = new GridBagConstraints();
        varianceYLabelConstraints.fill = GridBagConstraints.NONE;
        varianceYLabelConstraints.gridx = 0;
        varianceYLabelConstraints.gridy = 8;
        varianceYLabelConstraints.weightx = 0;
        varianceYLabelConstraints.insets = SPACE;
        GridBagConstraints varianceYViewerConstraints = new GridBagConstraints();
        varianceYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        varianceYViewerConstraints.gridx = 1;
        varianceYViewerConstraints.gridy = 8;
        varianceYViewerConstraints.weightx = 1;
        varianceYViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        varianceYViewerConstraints.insets = SPACE;

        GridBagConstraints covarianceLabelConstraints = new GridBagConstraints();
        covarianceLabelConstraints.fill = GridBagConstraints.NONE;
        covarianceLabelConstraints.gridx = 0;
        covarianceLabelConstraints.gridy = 9;
        covarianceLabelConstraints.weightx = 0;
        covarianceLabelConstraints.insets = SPACE;
        GridBagConstraints covarianceViewerConstraints = new GridBagConstraints();
        covarianceViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        covarianceViewerConstraints.gridx = 1;
        covarianceViewerConstraints.gridy = 9;
        covarianceViewerConstraints.weightx = 1;
        covarianceViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        covarianceViewerConstraints.insets = SPACE;

        GridBagConstraints MFWHMLabelConstraints = new GridBagConstraints();
        MFWHMLabelConstraints.fill = GridBagConstraints.NONE;
        MFWHMLabelConstraints.gridx = 0;
        MFWHMLabelConstraints.gridy = 10;
        MFWHMLabelConstraints.weightx = 0;
        MFWHMLabelConstraints.insets = SPACE;
        GridBagConstraints MFWHMViewerConstraints = new GridBagConstraints();
        MFWHMViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        MFWHMViewerConstraints.gridx = 1;
        MFWHMViewerConstraints.gridy = 10;
        MFWHMViewerConstraints.weightx = 1;
        MFWHMViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        MFWHMViewerConstraints.insets = SPACE;

        GridBagConstraints mFWHMLabelConstraints = new GridBagConstraints();
        mFWHMLabelConstraints.fill = GridBagConstraints.NONE;
        mFWHMLabelConstraints.gridx = 0;
        mFWHMLabelConstraints.gridy = 11;
        mFWHMLabelConstraints.weightx = 0;
        mFWHMLabelConstraints.insets = SPACE;
        GridBagConstraints mFWHMViewerConstraints = new GridBagConstraints();
        mFWHMViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        mFWHMViewerConstraints.gridx = 1;
        mFWHMViewerConstraints.gridy = 11;
        mFWHMViewerConstraints.weightx = 1;
        mFWHMViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        mFWHMViewerConstraints.insets = SPACE;

        GridBagConstraints tiltLabelConstraints = new GridBagConstraints();
        tiltLabelConstraints.fill = GridBagConstraints.NONE;
        tiltLabelConstraints.gridx = 0;
        tiltLabelConstraints.gridy = 12;
        tiltLabelConstraints.weightx = 0;
        tiltLabelConstraints.insets = SPACE;
        GridBagConstraints tiltViewerConstraints = new GridBagConstraints();
        tiltViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        tiltViewerConstraints.gridx = 1;
        tiltViewerConstraints.gridy = 12;
        tiltViewerConstraints.weightx = 1;
        tiltViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        tiltViewerConstraints.insets = SPACE;

        GridBagConstraints bgLabelConstraints = new GridBagConstraints();
        bgLabelConstraints.fill = GridBagConstraints.NONE;
        bgLabelConstraints.gridx = 0;
        bgLabelConstraints.gridy = 13;
        bgLabelConstraints.weightx = 0;
        bgLabelConstraints.insets = SPACE;
        GridBagConstraints bgViewerConstraints = new GridBagConstraints();
        bgViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        bgViewerConstraints.gridx = 1;
        bgViewerConstraints.gridy = 13;
        bgViewerConstraints.weightx = 1;
        bgViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        bgViewerConstraints.insets = SPACE;

        GridBagConstraints chi2LabelConstraints = new GridBagConstraints();
        chi2LabelConstraints.fill = GridBagConstraints.NONE;
        chi2LabelConstraints.gridx = 0;
        chi2LabelConstraints.gridy = 14;
        chi2LabelConstraints.weightx = 0;
        chi2LabelConstraints.insets = SPACE;
        GridBagConstraints chi2ViewerConstraints = new GridBagConstraints();
        chi2ViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        chi2ViewerConstraints.gridx = 1;
        chi2ViewerConstraints.gridy = 14;
        chi2ViewerConstraints.weightx = 1;
        chi2ViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        chi2ViewerConstraints.insets = SPACE;

        GridBagConstraints iterLabelConstraints = new GridBagConstraints();
        iterLabelConstraints.fill = GridBagConstraints.NONE;
        iterLabelConstraints.gridx = 0;
        iterLabelConstraints.gridy = 15;
        iterLabelConstraints.weightx = 0;
        iterLabelConstraints.insets = SPACE;
        GridBagConstraints iterViewerConstraints = new GridBagConstraints();
        iterViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        iterViewerConstraints.gridx = 1;
        iterViewerConstraints.gridy = 15;
        iterViewerConstraints.weightx = 1;
        iterViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        iterViewerConstraints.insets = SPACE;

        GridBagConstraints changeLabelConstraints = new GridBagConstraints();
        changeLabelConstraints.fill = GridBagConstraints.NONE;
        changeLabelConstraints.gridx = 0;
        changeLabelConstraints.gridy = 16;
        changeLabelConstraints.weightx = 0;
        changeLabelConstraints.insets = SPACE;
        GridBagConstraints changeViewerConstraints = new GridBagConstraints();
        changeViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        changeViewerConstraints.gridx = 1;
        changeViewerConstraints.gridy = 16;
        changeViewerConstraints.weightx = 1;
        changeViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        changeViewerConstraints.insets = SPACE;

        gaussian2dPanel.add(enable2DGaussianFitLabel, enableLabelConstraints);
        gaussian2dPanel.add(enable2DGaussianFit, enableViewerConstraints);
        gaussian2dPanel.add(enable2DGaussianFit2, enableSetterConstraints);

        gaussian2dPanel.add(fit2DNbIterMaxLabel, iterMLabelConstraints);
        gaussian2dPanel.add(fit2DNbIterMax, iterMViewerConstraints);
        gaussian2dPanel.add(fit2DNbIterMax2, iterMSetterConstraints);

        gaussian2dPanel.add(fit2DMaxRelChangeLabel, changeMLabelConstraints);
        gaussian2dPanel.add(fit2DMaxRelChange, changeMViewerConstraints);
        gaussian2dPanel.add(fit2DMaxRelChange2, changeMSetterConstraints);

        gaussian2dPanel.add(gaussianFitConvergedLabel, convergedLabelConstraints);
        gaussian2dPanel.add(gaussianFitConverged, convergedViewerConstraints);

        gaussian2dPanel.add(gaussianFitMagnitudeLabel, magLabelConstraints);
        gaussian2dPanel.add(gaussianFitMagnitude, magViewerConstraints);

        gaussian2dPanel.add(gaussianFitCenterXLabel, centerXLabelConstraints);
        gaussian2dPanel.add(gaussianFitCenterX, centerXViewerConstraints);

        gaussian2dPanel.add(gaussianFitCenterYLabel, centerYLabelConstraints);
        gaussian2dPanel.add(gaussianFitCenterY, centerYViewerConstraints);

        gaussian2dPanel.add(gaussianFitVarianceXLabel, varianceXLabelConstraints);
        gaussian2dPanel.add(gaussianFitVarianceX, varianceXViewerConstraints);

        gaussian2dPanel.add(gaussianFitVarianceYLabel, varianceYLabelConstraints);
        gaussian2dPanel.add(gaussianFitVarianceY, varianceYViewerConstraints);

        gaussian2dPanel.add(gaussianFitCovarianceXYLabel, covarianceLabelConstraints);
        gaussian2dPanel.add(gaussianFitCovarianceXY, covarianceViewerConstraints);

        gaussian2dPanel.add(gaussianFitMajorAxisFWHMLabel, MFWHMLabelConstraints);
        gaussian2dPanel.add(gaussianFitMajorAxisFWHM, MFWHMViewerConstraints);

        gaussian2dPanel.add(gaussianFitMinorAxisFWHMLabel, mFWHMLabelConstraints);
        gaussian2dPanel.add(gaussianFitMinorAxisFWHM, mFWHMViewerConstraints);

        gaussian2dPanel.add(gaussianFitTiltLabel, tiltLabelConstraints);
        gaussian2dPanel.add(gaussianFitTilt, tiltViewerConstraints);

        gaussian2dPanel.add(gaussianFitBGLabel, bgLabelConstraints);
        gaussian2dPanel.add(gaussianFitBG, bgViewerConstraints);

        gaussian2dPanel.add(gaussianFitChi2Label, chi2LabelConstraints);
        gaussian2dPanel.add(gaussianFitChi2, chi2ViewerConstraints);

        gaussian2dPanel.add(gaussianFitNbIterLabel, iterLabelConstraints);
        gaussian2dPanel.add(gaussianFitNbIter, iterViewerConstraints);

        gaussian2dPanel.add(gaussianFitRelChangeLabel, changeLabelConstraints);
        gaussian2dPanel.add(gaussianFitRelChange, changeViewerConstraints);

        // gaussian2dPanel.setBorder(new TitledBorder("2D Gaussian Fit"));
    }

    /**
     * @return the pixelConversionPanel
     */
    public JPanel getPixelConversionPanel() {
        if (pixelConversionPanel == null) {
            pixelConversionPanel = new JPanel();
            layoutPixelConversionPanel();
        }
        return pixelConversionPanel;
    }

    protected void layoutPixelConversionPanel() {
        pixelConversionPanel.setLayout(new GridBagLayout());

        GridBagConstraints sizeXLabelConstraints = new GridBagConstraints();
        sizeXLabelConstraints.fill = GridBagConstraints.NONE;
        sizeXLabelConstraints.gridx = 0;
        sizeXLabelConstraints.gridy = 0;
        sizeXLabelConstraints.weightx = 0;
        sizeXLabelConstraints.insets = SPACE;
        GridBagConstraints sizeXViewerConstraints = new GridBagConstraints();
        sizeXViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        sizeXViewerConstraints.gridx = 1;
        sizeXViewerConstraints.gridy = 0;
        sizeXViewerConstraints.weightx = 0.9;
        sizeXViewerConstraints.insets = SPACE;
        GridBagConstraints sizeXSetterConstraints = new GridBagConstraints();
        sizeXSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        sizeXSetterConstraints.gridx = 2;
        sizeXSetterConstraints.gridy = 0;
        sizeXSetterConstraints.weightx = 0.1;
        sizeXSetterConstraints.insets = SPACE;

        GridBagConstraints sizeYLabelConstraints = new GridBagConstraints();
        sizeYLabelConstraints.fill = GridBagConstraints.NONE;
        sizeYLabelConstraints.gridx = 0;
        sizeYLabelConstraints.gridy = 1;
        sizeYLabelConstraints.weightx = 0;
        sizeYLabelConstraints.insets = SPACE;
        GridBagConstraints sizeYViewerConstraints = new GridBagConstraints();
        sizeYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        sizeYViewerConstraints.gridx = 1;
        sizeYViewerConstraints.gridy = 1;
        sizeYViewerConstraints.weightx = 0.9;
        sizeYViewerConstraints.insets = SPACE;
        GridBagConstraints sizeYSetterConstraints = new GridBagConstraints();
        sizeYSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        sizeYSetterConstraints.gridx = 2;
        sizeYSetterConstraints.gridy = 1;
        sizeYSetterConstraints.weightx = 0.1;
        sizeYSetterConstraints.insets = SPACE;

        GridBagConstraints magLabelConstraints = new GridBagConstraints();
        magLabelConstraints.fill = GridBagConstraints.NONE;
        magLabelConstraints.gridx = 0;
        magLabelConstraints.gridy = 2;
        magLabelConstraints.weightx = 0;
        magLabelConstraints.insets = SPACE;
        GridBagConstraints magViewerConstraints = new GridBagConstraints();
        magViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        magViewerConstraints.gridx = 1;
        magViewerConstraints.gridy = 2;
        magViewerConstraints.weightx = 0.9;
        magViewerConstraints.insets = SPACE;
        GridBagConstraints magSetterConstraints = new GridBagConstraints();
        magSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        magSetterConstraints.gridx = 2;
        magSetterConstraints.gridy = 2;
        magSetterConstraints.weightx = 0.1;
        magSetterConstraints.insets = SPACE;

        pixelConversionPanel.add(pixelSizeXLabel, sizeXLabelConstraints);
        pixelConversionPanel.add(pixelSizeX, sizeXViewerConstraints);
        pixelConversionPanel.add(pixelSizeX2, sizeXSetterConstraints);

        pixelConversionPanel.add(pixelSizeYLabel, sizeYLabelConstraints);
        pixelConversionPanel.add(pixelSizeY, sizeYViewerConstraints);
        pixelConversionPanel.add(pixelSizeY2, sizeYSetterConstraints);

        pixelConversionPanel.add(opticalMagnificationLabel, magLabelConstraints);
        pixelConversionPanel.add(opticalMagnification, magViewerConstraints);
        pixelConversionPanel.add(opticalMagnification2, magSetterConstraints);

        // pixelConversionPanel.setBorder(new TitledBorder("Pixel to length unit conversion"));
    }

    /**
     * @return the preProcessingPanel
     */
    public JPanel getPreProcessingPanel() {
        if (preProcessingPanel == null) {
            preProcessingPanel = new JPanel();
            layoutPreProcessingPanel();
        }
        return preProcessingPanel;
    }

    protected void layoutPreProcessingPanel() {
        preProcessingPanel.setLayout(new GridBagLayout());

        GridBagConstraints rotationLabelConstraints = new GridBagConstraints();
        rotationLabelConstraints.fill = GridBagConstraints.NONE;
        rotationLabelConstraints.gridx = 0;
        rotationLabelConstraints.gridy = 0;
        rotationLabelConstraints.weightx = 0;
        rotationLabelConstraints.insets = SPACE;
        GridBagConstraints rotationViewerConstraints = new GridBagConstraints();
        rotationViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        rotationViewerConstraints.gridx = 1;
        rotationViewerConstraints.gridy = 0;
        rotationViewerConstraints.weightx = 0.9;
        rotationViewerConstraints.insets = SPACE;
        GridBagConstraints rotationSetterConstraints = new GridBagConstraints();
        rotationSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        rotationSetterConstraints.gridx = 2;
        rotationSetterConstraints.gridy = 0;
        rotationSetterConstraints.weightx = 0.1;
        rotationSetterConstraints.insets = SPACE;

        GridBagConstraints flipLabelConstraints = new GridBagConstraints();
        flipLabelConstraints.fill = GridBagConstraints.NONE;
        flipLabelConstraints.gridx = 0;
        flipLabelConstraints.gridy = 1;
        flipLabelConstraints.weightx = 0;
        flipLabelConstraints.insets = SPACE;
        GridBagConstraints flipViewerConstraints = new GridBagConstraints();
        flipViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        flipViewerConstraints.gridx = 1;
        flipViewerConstraints.gridy = 1;
        flipViewerConstraints.weightx = 0.9;
        flipViewerConstraints.insets = SPACE;
        GridBagConstraints flipSetterConstraints = new GridBagConstraints();
        flipSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        flipSetterConstraints.gridx = 2;
        flipSetterConstraints.gridy = 1;
        flipSetterConstraints.weightx = 0.1;
        flipSetterConstraints.insets = SPACE;

        preProcessingPanel.add(rotationLabel, rotationLabelConstraints);
        preProcessingPanel.add(rotation, rotationViewerConstraints);
        preProcessingPanel.add(rotation2, rotationSetterConstraints);

        preProcessingPanel.add(horizontalFlipLabel, flipLabelConstraints);
        preProcessingPanel.add(horizontalFlip, flipViewerConstraints);
        preProcessingPanel.add(horizontalFlip2, flipSetterConstraints);

    }

    /**
     * @return the imagePanel
     */
    public JPanel getGeneralPanel() {
        if (generalPanel == null) {
            generalPanel = new JPanel();
            layoutGeneralPanel();
        }
        return generalPanel;
    }

    protected void layoutGeneralPanel() {
        generalPanel.setLayout(new GridBagLayout());

        GridBagConstraints periodLabelConstraints = new GridBagConstraints();
        periodLabelConstraints.fill = GridBagConstraints.NONE;
        periodLabelConstraints.gridx = 0;
        periodLabelConstraints.gridy = 0;
        periodLabelConstraints.weightx = 0;
        periodLabelConstraints.insets = SPACE;
        GridBagConstraints periodViewerConstraints = new GridBagConstraints();
        periodViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        periodViewerConstraints.gridx = 1;
        periodViewerConstraints.gridy = 0;
        periodViewerConstraints.weightx = 0.9;
        periodViewerConstraints.insets = SPACE;
        GridBagConstraints periodSetterConstraints = new GridBagConstraints();
        periodSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
        periodSetterConstraints.gridx = 2;
        periodSetterConstraints.gridy = 0;
        periodSetterConstraints.weightx = 0.1;
        periodSetterConstraints.insets = SPACE;

        GridBagConstraints estimLabelConstraints = new GridBagConstraints();
        estimLabelConstraints.fill = GridBagConstraints.NONE;
        estimLabelConstraints.gridx = 0;
        estimLabelConstraints.gridy = 1;
        estimLabelConstraints.weightx = 0;
        estimLabelConstraints.insets = SPACE;
        GridBagConstraints estimViewerConstraints = new GridBagConstraints();
        estimViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        estimViewerConstraints.gridx = 1;
        estimViewerConstraints.gridy = 1;
        estimViewerConstraints.weightx = 1;
        estimViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        estimViewerConstraints.insets = SPACE;

        generalPanel.add(computationPeriodLabel, periodLabelConstraints);
        generalPanel.add(computationPeriod, periodViewerConstraints);
        generalPanel.add(computationPeriod2, periodSetterConstraints);

        generalPanel.add(estimComputTimeLabel, estimLabelConstraints);
        generalPanel.add(estimComputTime, estimViewerConstraints);

        // generalPanel.setBorder(new TitledBorder("General"));
    }

    public CheckBox getEnableAutoROI() {
        return enableAutoROI;
    }

    public BooleanComboBox getEnableAutoROI2() {
        return enableAutoROI2;
    }

    public CheckBox getAutoROIFound() {
        return autoROIFound;
    }

    public Label getAutoROIOriginX() {
        return autoROIOriginX;
    }

    public Label getAutoROIOriginY() {
        return autoROIOriginY;
    }

    public Label getAutoROIWidth() {
        return autoROIWidth;
    }

    public Label getAutoROIHeight() {
        return autoROIHeight;
    }

    public Label getAutoROIThreshold() {
        return autoROIThreshold;
    }

    public WheelSwitch getAutoROIThreshold2() {
        return autoROIThreshold2;
    }

    public Label getAutoROIMagFactorX() {
        return autoROIMagFactorX;
    }

    public WheelSwitch getAutoROIMagFactorX2() {
        return autoROIMagFactorX2;
    }

    public Label getAutoROIMagFactorY() {
        return autoROIMagFactorY;
    }

    public WheelSwitch getAutoROIMagFactorY2() {
        return autoROIMagFactorY2;
    }

    public Label getAlarmZone() {
        return alarmZone;
    }

    public WheelSwitch getAlarmZone2() {
        return alarmZone2;
    }

    public CheckBox getEnableUserROI() {
        return enableUserROI;
    }

    public BooleanComboBox getEnableUserROI2() {
        return enableUserROI2;
    }

    public Label getUserROIOriginX() {
        return userROIOriginX;
    }

    public ITextComponent getUserROIOriginX2() {
        return userROIOriginX2;
    }

    public Label getUserROIOriginY() {
        return userROIOriginY;
    }

    public ITextComponent getUserROIOriginY2() {
        return userROIOriginY2;
    }

    public Label getUserROIWidth() {
        return userROIWidth;
    }

    public ITextComponent getUserROIWidth2() {
        return userROIWidth2;
    }

    public Label getUserROIHeight() {
        return userROIHeight;
    }

    public ITextComponent getUserROIHeight2() {
        return userROIHeight2;
    }

    public Label getBitsPerPixel() {
        return bitsPerPixel;
    }

    public WheelSwitch getBitsPerPixel2() {
        return bitsPerPixel2;
    }

    public Label getGammaCorrection() {
        return gammaCorrection;
    }

    public WheelSwitch getGammaCorrection2() {
        return gammaCorrection2;
    }

    public CheckBox getEnableHistogram() {
        return enableHistogram;
    }

    public BooleanComboBox getEnableHistogram2() {
        return enableHistogram2;
    }

    public Label getHistogramNbBins() {
        return histogramNbBins;
    }

    public WheelSwitch getHistogramNbBins2() {
        return histogramNbBins2;
    }

    public Label getHistogramRangeMin() {
        return histogramRangeMin;
    }

    public WheelSwitch getHistogramRangeMin2() {
        return histogramRangeMin2;
    }

    public Label getHistogramRangeMax() {
        return histogramRangeMax;
    }

    public WheelSwitch getHistogramRangeMax2() {
        return histogramRangeMax2;
    }

    public CheckBox getEnableImageMoments() {
        return enableImageMoments;
    }

    public BooleanComboBox getEnableImageMoments2() {
        return enableImageMoments2;
    }

    public Label getMaxIntensity() {
        return maxIntensity;
    }

    public Label getMeanIntensity() {
        return meanIntensity;
    }

    public Label getCentroidX() {
        return centroidX;
    }

    public Label getCentroidY() {
        return centroidY;
    }

    public Label getVarianceX() {
        return varianceX;
    }

    public Label getVarianceY() {
        return varianceY;
    }

    public Label getCovarianceXY() {
        return covarianceXY;
    }

    public Label getCorrelationXY() {
        return correlationXY;
    }

    public Label getSkewX() {
        return skewX;
    }

    public Label getSkewY() {
        return skewY;
    }

    public Label getSkewX2Y() {
        return skewX2Y;
    }

    public Label getSkewXY2() {
        return skewXY2;
    }

    public CheckBox getLineProfileFitConverged() {
        return lineProfileFitConverged;
    }

    public Label getLineProfileOriginX() {
        return lineProfileOriginX;
    }

    public ITextComponent getLineProfileOriginX2() {
        return lineProfileOriginX2;
    }

    public Label getLineProfileOriginY() {
        return lineProfileOriginY;
    }

    public ITextComponent getLineProfileOriginY2() {
        return lineProfileOriginY2;
    }

    public Label getLineProfileEndX() {
        return lineProfileEndX;
    }

    public ITextComponent getLineProfileEndX2() {
        return lineProfileEndX2;
    }

    public Label getLineProfileEndY() {
        return lineProfileEndY;
    }

    public ITextComponent getLineProfileEndY2() {
        return lineProfileEndY2;
    }

    public Label getLineProfileThickness() {
        return lineProfileThickness;
    }

    public ITextComponent getLineProfileThickness2() {
        return lineProfileThickness2;
    }

    public CheckBox getEnable2DGaussianFit() {
        return enable2DGaussianFit;
    }

    public BooleanComboBox getEnable2DGaussianFit2() {
        return enable2DGaussianFit2;
    }

    public Label getFit2DNbIterMax() {
        return fit2DNbIterMax;
    }

    public WheelSwitch getFit2DNbIterMax2() {
        return fit2DNbIterMax2;
    }

    public Label getFit2DMaxRelChange() {
        return fit2DMaxRelChange;
    }

    public WheelSwitch getFit2DMaxRelChange2() {
        return fit2DMaxRelChange2;
    }

    public CheckBox getGaussianFitConverged() {
        return gaussianFitConverged;
    }

    public Label getGaussianFitMagnitude() {
        return gaussianFitMagnitude;
    }

    public Label getGaussianFitCenterX() {
        return gaussianFitCenterX;
    }

    public Label getGaussianFitCenterY() {
        return gaussianFitCenterY;
    }

    public Label getGaussianFitVarianceX() {
        return gaussianFitVarianceX;
    }

    public Label getGaussianFitVarianceY() {
        return gaussianFitVarianceY;
    }

    public Label getGaussianFitCovarianceXY() {
        return gaussianFitCovarianceXY;
    }

    public Label getGaussianFitMajorAxisFWHM() {
        return gaussianFitMajorAxisFWHM;
    }

    public Label getGaussianFitMinorAxisFWHM() {
        return gaussianFitMinorAxisFWHM;
    }

    public Label getGaussianFitTilt() {
        return gaussianFitTilt;
    }

    public Label getGaussianFitBG() {
        return gaussianFitBG;
    }

    public Label getGaussianFitChi2() {
        return gaussianFitChi2;
    }

    public Label getGaussianFitNbIter() {
        return gaussianFitNbIter;
    }

    public Label getGaussianFitRelChange() {
        return gaussianFitRelChange;
    }

    public Label getPixelSizeX() {
        return pixelSizeX;
    }

    public WheelSwitch getPixelSizeX2() {
        return pixelSizeX2;
    }

    public Label getPixelSizeY() {
        return pixelSizeY;
    }

    public WheelSwitch getPixelSizeY2() {
        return pixelSizeY2;
    }

    public Label getOpticalMagnification() {
        return opticalMagnification;
    }

    public WheelSwitch getOpticalMagnification2() {
        return opticalMagnification2;
    }

    public Label getRotation() {
        return rotation;
    }

    public WheelSwitch getRotation2() {
        return rotation2;
    }

    public CheckBox getHorizontalFlip() {
        return horizontalFlip;
    }

    public BooleanComboBox getHorizontalFlip2() {
        return horizontalFlip2;
    }

    public Label getComputationPeriod() {
        return computationPeriod;
    }

    public WheelSwitch getComputationPeriod2() {
        return computationPeriod2;
    }

    public Label getEstimComputTime() {
        return estimComputTime;
    }

}
