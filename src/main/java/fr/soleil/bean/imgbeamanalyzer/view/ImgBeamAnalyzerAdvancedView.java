package fr.soleil.bean.imgbeamanalyzer.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.soleil.charleston.docking.DockingFactory;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.util.DockingUtils;
import fr.soleil.docking.view.IView;

public class ImgBeamAnalyzerAdvancedView extends ImgBeamAnalyzerAbstractView {

    protected StringButton versionNumber;
    protected StringButton init;
    protected StringButton process;
    protected StringButton start;
    protected StringButton state;
    protected StringButton status;
    protected StringButton stop;
    protected JPanel commandPanel;
    // protected JComponent attributesPanel;

    protected JPanel commonProfilePanel;
    // protected JComponent globalProfilePanel;
    protected CheckBox enableProfiles;
    protected BooleanComboBox enableProfiles2;
    protected Label fit1DNbIterMax;
    protected WheelSwitch fit1DNbIterMax2;
    protected Label fit1DMaxRelChange;
    protected WheelSwitch fit1DMaxRelChange2;
    protected CheckBox profileFitFixedBg;
    protected BooleanComboBox profileFitFixedBg2;
    protected JPanel xProfilePanel;
    protected CheckBox xProfileFitConverged;
    protected Label xProfileCenter;
    protected Label xProfileMag;
    protected Label xProfileSigma;
    protected Label xProfileFWHM;
    protected Label xProfileBG;
    protected Label xProfileChi2;
    protected Label xProfileNbIter;
    protected Label xProfileFitRelChange;
    protected JPanel yProfilePanel;
    protected CheckBox yProfileFitConverged;
    protected Label yProfileCenter;
    protected Label yProfileMag;
    protected Label yProfileSigma;
    protected Label yProfileFWHM;
    protected Label yProfileBG;
    protected Label yProfileChi2;
    protected Label yProfileNbIter;
    protected Label yProfileFitRelChange;
    protected Label lineProfileCenter;
    protected Label lineProfileMag;
    protected Label lineProfileSigma;
    protected Label lineProfileFWHM;
    protected Label lineProfileBG;
    protected Label lineProfileChi2;
    protected Label lineProfileNbIter;
    protected Label lineProfileFitRelChange;
    // private ADockingManager scalarAttributesDockingManager;

    private AbstractAction preProcessingPanelViewAction;
    private AbstractAction generalViewAction;
    private AbstractAction userROIPanelViewAction;
    private AbstractAction autoRoiPanelViewAction;
//    private AbstractAction globalProfilePanelViewAction;
    private AbstractAction gaussian2dPanelViewAction;
    private AbstractAction attributesPanelViewAction;
    private AbstractAction pixelConversionPanelViewAction;
    private AbstractAction histogramPanelViewAction;
    private AbstractAction gammaPanelViewAction;
    private AbstractAction imgAnalyzerCommandsViewAction;
    // private final ADockingManager globalProfileDockingManager;
    private AbstractAction commonProfileViewAction;
    private AbstractAction xProfileViewAction;
    private AbstractAction yProfileViewAction;
    private AbstractAction lineProfileViewAction;

    public ImgBeamAnalyzerAdvancedView(DockingFactory dockingFactory, ADockingManager dockingManager) {
        super(dockingFactory, dockingManager);
        versionNumber = generateStringButton();
        versionNumber.setText("Version");
        init = generateStringButton();
        init.setText("Init");
        process = generateStringButton();
        process.setText("Process");
        start = generateStringButton();
        start.setText("Start");
        state = generateStringButton();
        state.setText("State");
        status = generateStringButton();
        status.setText("Status");
        stop = generateStringButton();
        stop.setText("Stop");

        enableProfiles = generateCheckbox();
        enableProfiles2 = generateBooleanComboBox();
        fit1DNbIterMax = generateLabel();
        fit1DNbIterMax2 = generateWheelSwitch();
        fit1DMaxRelChange = generateLabel();
        fit1DMaxRelChange2 = generateWheelSwitch();
        profileFitFixedBg = generateCheckbox();
        profileFitFixedBg2 = generateBooleanComboBox();
        xProfileFitConverged = generateCheckbox();
        xProfileCenter = generateLabel();
        xProfileMag = generateLabel();
        xProfileSigma = generateLabel();
        xProfileFWHM = generateLabel();
        xProfileBG = generateLabel();
        xProfileChi2 = generateLabel();
        xProfileNbIter = generateLabel();
        xProfileFitRelChange = generateLabel();
        yProfileFitConverged = generateCheckbox();
        yProfileCenter = generateLabel();
        yProfileMag = generateLabel();
        yProfileSigma = generateLabel();
        yProfileFWHM = generateLabel();
        yProfileBG = generateLabel();
        yProfileChi2 = generateLabel();
        yProfileNbIter = generateLabel();
        yProfileFitRelChange = generateLabel();
        lineProfileFitConverged = generateCheckbox();
        lineProfileCenter = generateLabel();
        lineProfileMag = generateLabel();
        lineProfileSigma = generateLabel();
        lineProfileFWHM = generateLabel();
        lineProfileBG = generateLabel();
        lineProfileChi2 = generateLabel();
        lineProfileNbIter = generateLabel();
        lineProfileFitRelChange = generateLabel();
    }

    @Override
    protected void initLineProfileEndX2() {
        lineProfileEndX2 = generateWheelSwitch();
    }

    @Override
    protected void initLineProfileEndY2() {
        lineProfileEndY2 = generateWheelSwitch();
    }

    @Override
    protected void initLineProfileOriginX2() {
        lineProfileOriginX2 = generateWheelSwitch();
    }

    @Override
    protected void initLineProfileOriginY2() {
        lineProfileOriginY2 = generateWheelSwitch();
    }

    @Override
    protected void initUserROIHeight2() {
        userROIHeight2 = generateWheelSwitch();
    }

    @Override
    protected void initUserROIOriginX2() {
        userROIOriginX2 = generateWheelSwitch();
    }

    @Override
    protected void initUserROIOriginY2() {
        userROIOriginY2 = generateWheelSwitch();
    }

    @Override
    protected void initUserROIWidth2() {
        userROIWidth2 = generateWheelSwitch();
    }

//    public void fillAdvancedPanel(JComponent advancedSplitPane) {
//
//        IView imgAnalyzerCommandsView = dockingManager.getViewFactory().addView("Analyzer Commands", null,
//                getCommandPanel(), "imgAnalyzerCommands", advancedSplitPane);
//        imgAnalyzerCommandsViewAction = DockingUtils.generateShowViewAction(imgAnalyzerCommandsView);
//        
//        IView preProcessingPanelView = dockingManager.getViewFactory().addView("Analyzer Pre Processing", null,
//                getPreProcessingPanel(), "preProcessingPanel", advancedSplitPane);
//        preProcessingPanelViewAction = DockingUtils.generateShowViewAction(preProcessingPanelView);
//
//        IView generalView = dockingManager.getViewFactory().addView("Analyzer General", null,
//                getGeneralPanel(), "generalPanel", advancedSplitPane);
//        generalViewAction = DockingUtils.generateShowViewAction(generalView);
//
//        IView userROIPanelView = dockingManager.getViewFactory().addView("Analyzer User ROI", null,
//                getUserROIPanel(), "userROIPanel", advancedSplitPane);
//        userROIPanelViewAction = DockingUtils.generateShowViewAction(userROIPanelView);
//
//        IView autoRoiPanelView = dockingManager.getViewFactory().addView("Analyzer Auto ROI", null,
//                getAutoROIPanel(), "autoRoiPanel", advancedSplitPane);
//        autoRoiPanelViewAction = DockingUtils.generateShowViewAction(autoRoiPanelView);
//
////        IView globalProfilePanelView = advancedDockingManager.getViewFactory().addView("Analyzer Profiles/Projections",
////                null, getGlobalProfilePanel(), "globalProfilePanel", advancedSplitPane);
////        globalProfilePanelViewAction = DockingUtils.generateShowViewAction(globalProfilePanelView);
//
//        IView gaussian2DPanelView = dockingManager.getViewFactory().addView("Analyzer 2D Gaussian Fit", null,
//                getGaussian2dPanel(), "gaussian2DPanel", advancedSplitPane);
//        gaussian2dPanelViewAction = DockingUtils.generateShowViewAction(gaussian2DPanelView);
//
//        IView attributesPanelView = dockingManager.getViewFactory().addView("Analyzer Moments", null,
//                getMomentsPanel(), "momentsPanel", advancedSplitPane);
//        attributesPanelViewAction = DockingUtils.generateShowViewAction(attributesPanelView);
//
//        IView pixelConversionPanelView = dockingManager.getViewFactory().addView("Analyzer Pixel Conversion",
//                null, getPixelConversionPanel(), "pixelConversionPanel", advancedSplitPane);
//        pixelConversionPanelViewAction = DockingUtils.generateShowViewAction(pixelConversionPanelView);
//
//        IView histogramPanelView = dockingManager.getViewFactory().addView("Analyzer Histogram", null,
//                getHistogramPanel(), "histogramPanel", advancedSplitPane);
//        histogramPanelViewAction = DockingUtils.generateShowViewAction(histogramPanelView);
//
//        IView gammaPanelView = dockingManager.getViewFactory().addView("Analyzer Gamma", null, getGammaPanel(),
//                "gammaPanel", advancedSplitPane);
//        gammaPanelViewAction = DockingUtils.generateShowViewAction(gammaPanelView);
//        // mainPanel.add(new JScrollPane(fillAttributesFields()), "Center");
//        // mainPanel.setBorder(new TitledBorder(ANALYZER_BORDER, "ImgAnalyzer"));
//    }

    @Override
    protected void layoutMainPanel() {

    }

    public JPanel getCommandPanel() {
        if (commandPanel == null) {

            commandPanel = new JPanel(/*new GridLayout(2, 4, 5, 5)*/);
            GridBagLayout gbl = new GridBagLayout();
            commandPanel.setLayout(gbl);

            GridBagConstraints bgc1 = new GridBagConstraints(0, 0, 1, 1, 0.25, 0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
            GridBagConstraints bgc2 = new GridBagConstraints(1, 0, 1, 1, 0.25, 0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
            GridBagConstraints bgc3 = new GridBagConstraints(2, 0, 1, 1, 0.25, 0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
            GridBagConstraints bgc4 = new GridBagConstraints(3, 0, 1, 1, 0.25, 0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
            GridBagConstraints bgc5 = new GridBagConstraints(0, 1, 1, 1, 0.25, 0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
            GridBagConstraints bgc6 = new GridBagConstraints(1, 1, 1, 1, 0.25, 0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
            GridBagConstraints bgc7 = new GridBagConstraints(2, 1, 1, 1, 0.25, 0, GridBagConstraints.CENTER,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);

            commandPanel.add(start, bgc1);
            commandPanel.add(stop, bgc2);
            commandPanel.add(init, bgc3);
            commandPanel.add(process, bgc4);
            commandPanel.add(state, bgc5);
            commandPanel.add(status, bgc6);
            commandPanel.add(versionNumber, bgc7);
            // commandPanel.setBorder(new TitledBorder("Commands"));
        }
        return commandPanel;
    }

    /**
     * Fill global profile tabs
     */
    public void fillProfile(JComponent advancedSplitPane) {
        IView imgAnalyzerCommandsView = dockingManager.getViewFactory().addView("Analyzer Commands", null,
                new JScrollPane(getCommandPanel()), "advImgAnalyzerCommands", advancedSplitPane);
        imgAnalyzerCommandsViewAction = DockingUtils.generateShowViewAction(imgAnalyzerCommandsView);

        IView preProcessingPanelView = dockingManager.getViewFactory().addView("Analyzer Pre Processing", null,
                new JScrollPane(getPreProcessingPanel()), "advPreProcessingPanelView", advancedSplitPane);
        preProcessingPanelViewAction = DockingUtils.generateShowViewAction(preProcessingPanelView);

        IView generalView = dockingManager.getViewFactory().addView("Analyzer General", null,
                new JScrollPane(getGeneralPanel()), "advGeneralPanel", advancedSplitPane);
        generalViewAction = DockingUtils.generateShowViewAction(generalView);

        IView userROIPanelView = dockingManager.getViewFactory().addView("Analyzer User ROI Advanced", null,
                new JScrollPane(getUserROIPanel()), "advUserROIPanelViewAdvanced", advancedSplitPane);
        userROIPanelViewAction = DockingUtils.generateShowViewAction(userROIPanelView);

        IView autoRoiPanelView = dockingManager.getViewFactory().addView("Analyzer Auto ROI", null,
                new JScrollPane(getAutoROIPanel()), "advAutoRoiPanel", advancedSplitPane);
        autoRoiPanelViewAction = DockingUtils.generateShowViewAction(autoRoiPanelView);

        IView gaussian2DPanelView = dockingManager.getViewFactory().addView("Analyzer 2D Gaussian Fit", null,
                new JScrollPane(getGaussian2dPanel()), "advGaussian2DPanel", advancedSplitPane);
        gaussian2dPanelViewAction = DockingUtils.generateShowViewAction(gaussian2DPanelView);

        IView attributesPanelView = dockingManager.getViewFactory().addView("Analyzer Moments", null,
                new JScrollPane(getMomentsPanel()), "advMomentsPanelView", advancedSplitPane);
        attributesPanelViewAction = DockingUtils.generateShowViewAction(attributesPanelView);

        IView pixelConversionPanelView = dockingManager.getViewFactory().addView("Analyzer Pixel Conversion", null,
                new JScrollPane(getPixelConversionPanel()), "advPixelConversionPanel", advancedSplitPane);
        pixelConversionPanelViewAction = DockingUtils.generateShowViewAction(pixelConversionPanelView);

        IView histogramPanelView = dockingManager.getViewFactory().addView("Analyzer Histogram", null,
                new JScrollPane(getHistogramPanel()), "advHistogramPanel", advancedSplitPane);
        histogramPanelViewAction = DockingUtils.generateShowViewAction(histogramPanelView);

        IView gammaPanelView = dockingManager.getViewFactory().addView("Analyzer Gamma", null,
                new JScrollPane(getGammaPanel()), "advGammaPanel", advancedSplitPane);
        gammaPanelViewAction = DockingUtils.generateShowViewAction(gammaPanelView);
        // mainPanel.add(new JScrollPane(fillAttributesFields()), "Center");

        IView commonProfileView = dockingManager.getViewFactory().addView("Analyzer Common profile", null,
                new JScrollPane(getCommonProfilePanel()), "advCommonProfileView", advancedSplitPane);
        commonProfileViewAction = DockingUtils.generateShowViewAction(commonProfileView);

        IView xProfileView = dockingManager.getViewFactory().addView("Analyzer X Projection", null,
                new JScrollPane(getXProfilePanel()), "advXProfileView", advancedSplitPane);
        xProfileViewAction = DockingUtils.generateShowViewAction(xProfileView);

        IView yProfileView = dockingManager.getViewFactory().addView("Analyzer Y Projection", null,
                new JScrollPane(getYProfilePanel()), "advYProfileView", advancedSplitPane);
        yProfileViewAction = DockingUtils.generateShowViewAction(yProfileView);

        IView lineProfileView = dockingManager.getViewFactory().addView("Analyzer Line Profile", null,
                new JScrollPane(getLineProfilePanel()), "advLineProfileView", advancedSplitPane);
        lineProfileViewAction = DockingUtils.generateShowViewAction(lineProfileView);
    }

    /**
     * @return the profileCommonPanel
     */
    public JPanel getCommonProfilePanel() {
        if (commonProfilePanel == null) {
            commonProfilePanel = new JPanel();
            commonProfilePanel.setLayout(new GridBagLayout());

            GridBagConstraints enableLabelConstraints = new GridBagConstraints();
            enableLabelConstraints.fill = GridBagConstraints.NONE;
            enableLabelConstraints.gridx = 0;
            enableLabelConstraints.gridy = 0;
            enableLabelConstraints.weightx = 0;
            enableLabelConstraints.insets = SPACE;
            GridBagConstraints enableViewerConstraints = new GridBagConstraints();
            enableViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            enableViewerConstraints.gridx = 1;
            enableViewerConstraints.gridy = 0;
            enableViewerConstraints.weightx = 0.9;
            enableViewerConstraints.insets = SPACE;
            GridBagConstraints enableSetterConstraints = new GridBagConstraints();
            enableSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
            enableSetterConstraints.gridx = 2;
            enableSetterConstraints.gridy = 0;
            enableSetterConstraints.weightx = 0.1;
            enableSetterConstraints.insets = SPACE;

            GridBagConstraints iterLabelConstraints = new GridBagConstraints();
            iterLabelConstraints.fill = GridBagConstraints.NONE;
            iterLabelConstraints.gridx = 0;
            iterLabelConstraints.gridy = 1;
            iterLabelConstraints.weightx = 0;
            iterLabelConstraints.insets = SPACE;
            GridBagConstraints iterViewerConstraints = new GridBagConstraints();
            iterViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            iterViewerConstraints.gridx = 1;
            iterViewerConstraints.gridy = 1;
            iterViewerConstraints.weightx = 0.9;
            iterViewerConstraints.insets = SPACE;
            GridBagConstraints iterSetterConstraints = new GridBagConstraints();
            iterSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
            iterSetterConstraints.gridx = 2;
            iterSetterConstraints.gridy = 1;
            iterSetterConstraints.weightx = 0.1;
            iterSetterConstraints.insets = SPACE;

            GridBagConstraints changeLabelConstraints = new GridBagConstraints();
            changeLabelConstraints.fill = GridBagConstraints.NONE;
            changeLabelConstraints.gridx = 0;
            changeLabelConstraints.gridy = 2;
            changeLabelConstraints.weightx = 0;
            changeLabelConstraints.insets = SPACE;
            GridBagConstraints changeViewerConstraints = new GridBagConstraints();
            changeViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            changeViewerConstraints.gridx = 1;
            changeViewerConstraints.gridy = 2;
            changeViewerConstraints.weightx = 0.9;
            changeViewerConstraints.insets = SPACE;
            GridBagConstraints changeSetterConstraints = new GridBagConstraints();
            changeSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
            changeSetterConstraints.gridx = 2;
            changeSetterConstraints.gridy = 2;
            changeSetterConstraints.weightx = 0.1;
            changeSetterConstraints.insets = SPACE;

            GridBagConstraints bgLabelConstraints = new GridBagConstraints();
            bgLabelConstraints.fill = GridBagConstraints.NONE;
            bgLabelConstraints.gridx = 0;
            bgLabelConstraints.gridy = 3;
            bgLabelConstraints.weightx = 0;
            bgLabelConstraints.insets = SPACE;
            GridBagConstraints bgViewerConstraints = new GridBagConstraints();
            bgViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            bgViewerConstraints.gridx = 1;
            bgViewerConstraints.gridy = 3;
            bgViewerConstraints.weightx = 0.9;
            bgViewerConstraints.insets = SPACE;
            GridBagConstraints bgSetterConstraints = new GridBagConstraints();
            bgSetterConstraints.fill = GridBagConstraints.HORIZONTAL;
            bgSetterConstraints.gridx = 2;
            bgSetterConstraints.gridy = 3;
            bgSetterConstraints.weightx = 0.1;
            bgSetterConstraints.insets = SPACE;

            commonProfilePanel.add(new JLabel("enable profiles"), enableLabelConstraints);
            commonProfilePanel.add(enableProfiles, enableViewerConstraints);
            commonProfilePanel.add(enableProfiles2, enableSetterConstraints);

            commonProfilePanel.add(new JLabel("nb iterations max"), iterLabelConstraints);
            commonProfilePanel.add(fit1DNbIterMax, iterViewerConstraints);
            commonProfilePanel.add(fit1DNbIterMax2, iterSetterConstraints);

            commonProfilePanel.add(new JLabel("relative change max"), changeLabelConstraints);
            commonProfilePanel.add(fit1DMaxRelChange, changeViewerConstraints);
            commonProfilePanel.add(fit1DMaxRelChange2, changeSetterConstraints);

            commonProfilePanel.add(new JLabel("fixed background"), bgLabelConstraints);
            commonProfilePanel.add(profileFitFixedBg, bgViewerConstraints);
            commonProfilePanel.add(profileFitFixedBg2, bgSetterConstraints);
        }
        return commonProfilePanel;
    }

    /**
     * @return the xprofilePanel
     */
    public JPanel getXProfilePanel() {
        if (xProfilePanel == null) {
            xProfilePanel = new JPanel();
            xProfilePanel.setLayout(new GridBagLayout());

            GridBagConstraints convergedLabelConstraints = new GridBagConstraints();
            convergedLabelConstraints.fill = GridBagConstraints.NONE;
            convergedLabelConstraints.gridx = 0;
            convergedLabelConstraints.gridy = 0;
            convergedLabelConstraints.weightx = 0;
            convergedLabelConstraints.insets = SPACE;
            GridBagConstraints convergedViewerConstraints = new GridBagConstraints();
            convergedViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            convergedViewerConstraints.gridx = 1;
            convergedViewerConstraints.gridy = 0;
            convergedViewerConstraints.weightx = 1;
            convergedViewerConstraints.insets = SPACE;

            GridBagConstraints magLabelConstraints = new GridBagConstraints();
            magLabelConstraints.fill = GridBagConstraints.NONE;
            magLabelConstraints.gridx = 0;
            magLabelConstraints.gridy = 1;
            magLabelConstraints.weightx = 0;
            magLabelConstraints.insets = SPACE;
            GridBagConstraints magViewerConstraints = new GridBagConstraints();
            magViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            magViewerConstraints.gridx = 1;
            magViewerConstraints.gridy = 1;
            magViewerConstraints.weightx = 1;
            magViewerConstraints.insets = SPACE;

            GridBagConstraints centerLabelConstraints = new GridBagConstraints();
            centerLabelConstraints.fill = GridBagConstraints.NONE;
            centerLabelConstraints.gridx = 0;
            centerLabelConstraints.gridy = 2;
            centerLabelConstraints.weightx = 0;
            centerLabelConstraints.insets = SPACE;
            GridBagConstraints centerViewerConstraints = new GridBagConstraints();
            centerViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            centerViewerConstraints.gridx = 1;
            centerViewerConstraints.gridy = 2;
            centerViewerConstraints.weightx = 1;
            centerViewerConstraints.insets = SPACE;

            GridBagConstraints sigmaLabelConstraints = new GridBagConstraints();
            sigmaLabelConstraints.fill = GridBagConstraints.NONE;
            sigmaLabelConstraints.gridx = 0;
            sigmaLabelConstraints.gridy = 3;
            sigmaLabelConstraints.weightx = 0;
            sigmaLabelConstraints.insets = SPACE;
            GridBagConstraints sigmaViewerConstraints = new GridBagConstraints();
            sigmaViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            sigmaViewerConstraints.gridx = 1;
            sigmaViewerConstraints.gridy = 3;
            sigmaViewerConstraints.weightx = 1;
            sigmaViewerConstraints.insets = SPACE;

            GridBagConstraints fwhmLabelConstraints = new GridBagConstraints();
            fwhmLabelConstraints.fill = GridBagConstraints.NONE;
            fwhmLabelConstraints.gridx = 0;
            fwhmLabelConstraints.gridy = 4;
            fwhmLabelConstraints.weightx = 0;
            fwhmLabelConstraints.insets = SPACE;
            GridBagConstraints fwhmViewerConstraints = new GridBagConstraints();
            fwhmViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            fwhmViewerConstraints.gridx = 1;
            fwhmViewerConstraints.gridy = 4;
            fwhmViewerConstraints.weightx = 1;
            fwhmViewerConstraints.insets = SPACE;

            GridBagConstraints bgLabelConstraints = new GridBagConstraints();
            bgLabelConstraints.fill = GridBagConstraints.NONE;
            bgLabelConstraints.gridx = 0;
            bgLabelConstraints.gridy = 5;
            bgLabelConstraints.weightx = 0;
            bgLabelConstraints.insets = SPACE;
            GridBagConstraints bgViewerConstraints = new GridBagConstraints();
            bgViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            bgViewerConstraints.gridx = 1;
            bgViewerConstraints.gridy = 5;
            bgViewerConstraints.weightx = 1;
            bgViewerConstraints.insets = SPACE;

            GridBagConstraints chi2LabelConstraints = new GridBagConstraints();
            chi2LabelConstraints.fill = GridBagConstraints.NONE;
            chi2LabelConstraints.gridx = 0;
            chi2LabelConstraints.gridy = 6;
            chi2LabelConstraints.weightx = 0;
            chi2LabelConstraints.insets = SPACE;
            GridBagConstraints chi2ViewerConstraints = new GridBagConstraints();
            chi2ViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            chi2ViewerConstraints.gridx = 1;
            chi2ViewerConstraints.gridy = 6;
            chi2ViewerConstraints.weightx = 1;
            chi2ViewerConstraints.insets = SPACE;

            GridBagConstraints iterLabelConstraints = new GridBagConstraints();
            iterLabelConstraints.fill = GridBagConstraints.NONE;
            iterLabelConstraints.gridx = 0;
            iterLabelConstraints.gridy = 7;
            iterLabelConstraints.weightx = 0;
            iterLabelConstraints.insets = SPACE;
            GridBagConstraints iterViewerConstraints = new GridBagConstraints();
            iterViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            iterViewerConstraints.gridx = 1;
            iterViewerConstraints.gridy = 7;
            iterViewerConstraints.weightx = 1;
            iterViewerConstraints.insets = SPACE;

            GridBagConstraints changeLabelConstraints = new GridBagConstraints();
            changeLabelConstraints.fill = GridBagConstraints.NONE;
            changeLabelConstraints.gridx = 0;
            changeLabelConstraints.gridy = 8;
            changeLabelConstraints.weightx = 0;
            changeLabelConstraints.insets = SPACE;
            GridBagConstraints changeViewerConstraints = new GridBagConstraints();
            changeViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            changeViewerConstraints.gridx = 1;
            changeViewerConstraints.gridy = 8;
            changeViewerConstraints.weightx = 1;
            changeViewerConstraints.insets = SPACE;

            xProfilePanel.add(new JLabel("fit converged"), convergedLabelConstraints);
            xProfilePanel.add(xProfileFitConverged, convergedViewerConstraints);

            xProfilePanel.add(new JLabel("magnitude"), magLabelConstraints);
            xProfilePanel.add(xProfileMag, magViewerConstraints);

            xProfilePanel.add(new JLabel("center"), centerLabelConstraints);
            xProfilePanel.add(xProfileCenter, centerViewerConstraints);

            xProfilePanel.add(new JLabel("sigma"), sigmaLabelConstraints);
            xProfilePanel.add(xProfileSigma, sigmaViewerConstraints);

            xProfilePanel.add(new JLabel("FWHM"), fwhmLabelConstraints);
            xProfilePanel.add(xProfileFWHM, fwhmViewerConstraints);

            xProfilePanel.add(new JLabel("background"), bgLabelConstraints);
            xProfilePanel.add(xProfileBG, bgViewerConstraints);

            xProfilePanel.add(new JLabel("chi2"), chi2LabelConstraints);
            xProfilePanel.add(xProfileChi2, chi2ViewerConstraints);

            xProfilePanel.add(new JLabel("nb iterations"), iterLabelConstraints);
            xProfilePanel.add(xProfileNbIter, iterViewerConstraints);

            xProfilePanel.add(new JLabel("relative change"), changeLabelConstraints);
            xProfilePanel.add(xProfileFitRelChange, changeViewerConstraints);
        }
        return xProfilePanel;
    }

    /**
     * @return the yProfilePanel
     */
    public JPanel getYProfilePanel() {
        if (yProfilePanel == null) {
            yProfilePanel = new JPanel();
            yProfilePanel.setLayout(new GridBagLayout());

            GridBagConstraints convergedLabelConstraints = new GridBagConstraints();
            convergedLabelConstraints.fill = GridBagConstraints.NONE;
            convergedLabelConstraints.gridx = 0;
            convergedLabelConstraints.gridy = 0;
            convergedLabelConstraints.weightx = 0;
            convergedLabelConstraints.insets = SPACE;
            GridBagConstraints convergedViewerConstraints = new GridBagConstraints();
            convergedViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            convergedViewerConstraints.gridx = 1;
            convergedViewerConstraints.gridy = 0;
            convergedViewerConstraints.weightx = 1;
            convergedViewerConstraints.insets = SPACE;

            GridBagConstraints magLabelConstraints = new GridBagConstraints();
            magLabelConstraints.fill = GridBagConstraints.NONE;
            magLabelConstraints.gridx = 0;
            magLabelConstraints.gridy = 1;
            magLabelConstraints.weightx = 0;
            magLabelConstraints.insets = SPACE;
            GridBagConstraints magViewerConstraints = new GridBagConstraints();
            magViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            magViewerConstraints.gridx = 1;
            magViewerConstraints.gridy = 1;
            magViewerConstraints.weightx = 1;
            magViewerConstraints.insets = SPACE;

            GridBagConstraints centerLabelConstraints = new GridBagConstraints();
            centerLabelConstraints.fill = GridBagConstraints.NONE;
            centerLabelConstraints.gridx = 0;
            centerLabelConstraints.gridy = 2;
            centerLabelConstraints.weightx = 0;
            centerLabelConstraints.insets = SPACE;
            GridBagConstraints centerViewerConstraints = new GridBagConstraints();
            centerViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            centerViewerConstraints.gridx = 1;
            centerViewerConstraints.gridy = 2;
            centerViewerConstraints.weightx = 1;
            centerViewerConstraints.insets = SPACE;

            GridBagConstraints sigmaLabelConstraints = new GridBagConstraints();
            sigmaLabelConstraints.fill = GridBagConstraints.NONE;
            sigmaLabelConstraints.gridx = 0;
            sigmaLabelConstraints.gridy = 3;
            sigmaLabelConstraints.weightx = 0;
            sigmaLabelConstraints.insets = SPACE;
            GridBagConstraints sigmaViewerConstraints = new GridBagConstraints();
            sigmaViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            sigmaViewerConstraints.gridx = 1;
            sigmaViewerConstraints.gridy = 3;
            sigmaViewerConstraints.weightx = 1;
            sigmaViewerConstraints.insets = SPACE;

            GridBagConstraints fwhmLabelConstraints = new GridBagConstraints();
            fwhmLabelConstraints.fill = GridBagConstraints.NONE;
            fwhmLabelConstraints.gridx = 0;
            fwhmLabelConstraints.gridy = 4;
            fwhmLabelConstraints.weightx = 0;
            fwhmLabelConstraints.insets = SPACE;
            GridBagConstraints fwhmViewerConstraints = new GridBagConstraints();
            fwhmViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            fwhmViewerConstraints.gridx = 1;
            fwhmViewerConstraints.gridy = 4;
            fwhmViewerConstraints.weightx = 1;
            fwhmViewerConstraints.insets = SPACE;

            GridBagConstraints bgLabelConstraints = new GridBagConstraints();
            bgLabelConstraints.fill = GridBagConstraints.NONE;
            bgLabelConstraints.gridx = 0;
            bgLabelConstraints.gridy = 5;
            bgLabelConstraints.weightx = 0;
            bgLabelConstraints.insets = SPACE;
            GridBagConstraints bgViewerConstraints = new GridBagConstraints();
            bgViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            bgViewerConstraints.gridx = 1;
            bgViewerConstraints.gridy = 5;
            bgViewerConstraints.weightx = 1;
            bgViewerConstraints.insets = SPACE;

            GridBagConstraints chi2LabelConstraints = new GridBagConstraints();
            chi2LabelConstraints.fill = GridBagConstraints.NONE;
            chi2LabelConstraints.gridx = 0;
            chi2LabelConstraints.gridy = 6;
            chi2LabelConstraints.weightx = 0;
            chi2LabelConstraints.insets = SPACE;
            GridBagConstraints chi2ViewerConstraints = new GridBagConstraints();
            chi2ViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            chi2ViewerConstraints.gridx = 1;
            chi2ViewerConstraints.gridy = 6;
            chi2ViewerConstraints.weightx = 1;
            chi2ViewerConstraints.insets = SPACE;

            GridBagConstraints iterLabelConstraints = new GridBagConstraints();
            iterLabelConstraints.fill = GridBagConstraints.NONE;
            iterLabelConstraints.gridx = 0;
            iterLabelConstraints.gridy = 7;
            iterLabelConstraints.weightx = 0;
            iterLabelConstraints.insets = SPACE;
            GridBagConstraints iterViewerConstraints = new GridBagConstraints();
            iterViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            iterViewerConstraints.gridx = 1;
            iterViewerConstraints.gridy = 7;
            iterViewerConstraints.weightx = 1;
            iterViewerConstraints.insets = SPACE;

            GridBagConstraints changeLabelConstraints = new GridBagConstraints();
            changeLabelConstraints.fill = GridBagConstraints.NONE;
            changeLabelConstraints.gridx = 0;
            changeLabelConstraints.gridy = 8;
            changeLabelConstraints.weightx = 0;
            changeLabelConstraints.insets = SPACE;
            GridBagConstraints changeViewerConstraints = new GridBagConstraints();
            changeViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            changeViewerConstraints.gridx = 1;
            changeViewerConstraints.gridy = 8;
            changeViewerConstraints.weightx = 1;
            changeViewerConstraints.insets = SPACE;

            yProfilePanel.add(new JLabel("fit converged"), convergedLabelConstraints);
            yProfilePanel.add(yProfileFitConverged, convergedViewerConstraints);

            yProfilePanel.add(new JLabel("magnitude"), magLabelConstraints);
            yProfilePanel.add(yProfileMag, magViewerConstraints);

            yProfilePanel.add(new JLabel("center"), centerLabelConstraints);
            yProfilePanel.add(yProfileCenter, centerViewerConstraints);

            yProfilePanel.add(new JLabel("sigma"), sigmaLabelConstraints);
            yProfilePanel.add(yProfileSigma, sigmaViewerConstraints);

            yProfilePanel.add(new JLabel("FWHM"), fwhmLabelConstraints);
            yProfilePanel.add(yProfileFWHM, fwhmViewerConstraints);

            yProfilePanel.add(new JLabel("background"), bgLabelConstraints);
            yProfilePanel.add(yProfileBG, bgViewerConstraints);

            yProfilePanel.add(new JLabel("chi2"), chi2LabelConstraints);
            yProfilePanel.add(yProfileChi2, chi2ViewerConstraints);

            yProfilePanel.add(new JLabel("nb iterations"), iterLabelConstraints);
            yProfilePanel.add(yProfileNbIter, iterViewerConstraints);

            yProfilePanel.add(new JLabel("relative change"), changeLabelConstraints);
            yProfilePanel.add(yProfileFitRelChange, changeViewerConstraints);

        }
        return yProfilePanel;
    }

    @Override
    protected void layoutLineProfilePanel() {
        super.layoutLineProfilePanel();

        GridBagConstraints magLabelConstraints = new GridBagConstraints();
        magLabelConstraints.fill = GridBagConstraints.NONE;
        magLabelConstraints.gridx = 0;
        magLabelConstraints.gridy = 6;
        magLabelConstraints.weightx = 0;
        magLabelConstraints.insets = SPACE;
        GridBagConstraints magViewerConstraints = new GridBagConstraints();
        magViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        magViewerConstraints.gridx = 1;
        magViewerConstraints.gridy = 6;
        magViewerConstraints.weightx = 1;
        magViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        magViewerConstraints.insets = SPACE;

        GridBagConstraints centerLabelConstraints = new GridBagConstraints();
        centerLabelConstraints.fill = GridBagConstraints.NONE;
        centerLabelConstraints.gridx = 0;
        centerLabelConstraints.gridy = 7;
        centerLabelConstraints.weightx = 0;
        centerLabelConstraints.insets = SPACE;
        GridBagConstraints centerViewerConstraints = new GridBagConstraints();
        centerViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        centerViewerConstraints.gridx = 1;
        centerViewerConstraints.gridy = 7;
        centerViewerConstraints.weightx = 1;
        centerViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        centerViewerConstraints.insets = SPACE;

        GridBagConstraints sigmaLabelConstraints = new GridBagConstraints();
        sigmaLabelConstraints.fill = GridBagConstraints.NONE;
        sigmaLabelConstraints.gridx = 0;
        sigmaLabelConstraints.gridy = 8;
        sigmaLabelConstraints.weightx = 0;
        sigmaLabelConstraints.insets = SPACE;
        GridBagConstraints sigmaViewerConstraints = new GridBagConstraints();
        sigmaViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        sigmaViewerConstraints.gridx = 1;
        sigmaViewerConstraints.gridy = 8;
        sigmaViewerConstraints.weightx = 1;
        sigmaViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        sigmaViewerConstraints.insets = SPACE;

        GridBagConstraints fwhmLabelConstraints = new GridBagConstraints();
        fwhmLabelConstraints.fill = GridBagConstraints.NONE;
        fwhmLabelConstraints.gridx = 0;
        fwhmLabelConstraints.gridy = 9;
        fwhmLabelConstraints.weightx = 0;
        fwhmLabelConstraints.insets = SPACE;
        GridBagConstraints fwhmViewerConstraints = new GridBagConstraints();
        fwhmViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        fwhmViewerConstraints.gridx = 1;
        fwhmViewerConstraints.gridy = 9;
        fwhmViewerConstraints.weightx = 1;
        fwhmViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        fwhmViewerConstraints.insets = SPACE;

        GridBagConstraints bgLabelConstraints = new GridBagConstraints();
        bgLabelConstraints.fill = GridBagConstraints.NONE;
        bgLabelConstraints.gridx = 0;
        bgLabelConstraints.gridy = 10;
        bgLabelConstraints.weightx = 0;
        bgLabelConstraints.insets = SPACE;
        GridBagConstraints bgViewerConstraints = new GridBagConstraints();
        bgViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        bgViewerConstraints.gridx = 1;
        bgViewerConstraints.gridy = 10;
        bgViewerConstraints.weightx = 1;
        bgViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        bgViewerConstraints.insets = SPACE;

        GridBagConstraints chi2LabelConstraints = new GridBagConstraints();
        chi2LabelConstraints.fill = GridBagConstraints.NONE;
        chi2LabelConstraints.gridx = 0;
        chi2LabelConstraints.gridy = 11;
        chi2LabelConstraints.weightx = 0;
        chi2LabelConstraints.insets = SPACE;
        GridBagConstraints chi2ViewerConstraints = new GridBagConstraints();
        chi2ViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        chi2ViewerConstraints.gridx = 1;
        chi2ViewerConstraints.gridy = 11;
        chi2ViewerConstraints.weightx = 1;
        chi2ViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        chi2ViewerConstraints.insets = SPACE;

        GridBagConstraints iterLabelConstraints = new GridBagConstraints();
        iterLabelConstraints.fill = GridBagConstraints.NONE;
        iterLabelConstraints.gridx = 0;
        iterLabelConstraints.gridy = 12;
        iterLabelConstraints.weightx = 0;
        iterLabelConstraints.insets = SPACE;
        GridBagConstraints iterViewerConstraints = new GridBagConstraints();
        iterViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        iterViewerConstraints.gridx = 1;
        iterViewerConstraints.gridy = 12;
        iterViewerConstraints.weightx = 1;
        iterViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        iterViewerConstraints.insets = SPACE;

        GridBagConstraints changeLabelConstraints = new GridBagConstraints();
        changeLabelConstraints.fill = GridBagConstraints.NONE;
        changeLabelConstraints.gridx = 0;
        changeLabelConstraints.gridy = 13;
        changeLabelConstraints.weightx = 0;
        changeLabelConstraints.insets = SPACE;
        GridBagConstraints changeViewerConstraints = new GridBagConstraints();
        changeViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        changeViewerConstraints.gridx = 1;
        changeViewerConstraints.gridy = 13;
        changeViewerConstraints.weightx = 1;
        changeViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
        changeViewerConstraints.insets = SPACE;

        lineProfilePanel.add(new JLabel("magnitude"), magLabelConstraints);
        lineProfilePanel.add(lineProfileMag, magViewerConstraints);

        lineProfilePanel.add(new JLabel("center"), centerLabelConstraints);
        lineProfilePanel.add(lineProfileCenter, centerViewerConstraints);

        lineProfilePanel.add(new JLabel("sigma"), sigmaLabelConstraints);
        lineProfilePanel.add(lineProfileSigma, sigmaViewerConstraints);

        lineProfilePanel.add(new JLabel("FWHM"), fwhmLabelConstraints);
        lineProfilePanel.add(lineProfileFWHM, fwhmViewerConstraints);

        lineProfilePanel.add(new JLabel("background"), bgLabelConstraints);
        lineProfilePanel.add(lineProfileBG, bgViewerConstraints);

        lineProfilePanel.add(new JLabel("chi2"), chi2LabelConstraints);
        lineProfilePanel.add(lineProfileChi2, chi2ViewerConstraints);

        lineProfilePanel.add(new JLabel("nb iterations"), iterLabelConstraints);
        lineProfilePanel.add(lineProfileNbIter, iterViewerConstraints);

        lineProfilePanel.add(new JLabel("relative change"), changeLabelConstraints);
        lineProfilePanel.add(lineProfileFitRelChange, changeViewerConstraints);
    }

    public StringButton getVersionNumber() {
        return versionNumber;
    }

    public StringButton getInit() {
        return init;
    }

    public StringButton getProcess() {
        return process;
    }

    public StringButton getStart() {
        return start;
    }

    public StringButton getState() {
        return state;
    }

    public StringButton getStatus() {
        return status;
    }

    public StringButton getStop() {
        return stop;
    }

    public CheckBox getEnableProfiles() {
        return enableProfiles;
    }

    public BooleanComboBox getEnableProfiles2() {
        return enableProfiles2;
    }

    public Label getFit1DNbIterMax() {
        return fit1DNbIterMax;
    }

    public WheelSwitch getFit1DNbIterMax2() {
        return fit1DNbIterMax2;
    }

    public Label getFit1DMaxRelChange() {
        return fit1DMaxRelChange;
    }

    public WheelSwitch getFit1DMaxRelChange2() {
        return fit1DMaxRelChange2;
    }

    public CheckBox getProfileFitFixedBg() {
        return profileFitFixedBg;
    }

    public BooleanComboBox getProfileFitFixedBg2() {
        return profileFitFixedBg2;
    }

    public JPanel getxProfilePanel() {
        return xProfilePanel;
    }

    public CheckBox getXProfileFitConverged() {
        return xProfileFitConverged;
    }

    public Label getXProfileCenter() {
        return xProfileCenter;
    }

    public Label getXProfileMag() {
        return xProfileMag;
    }

    public Label getXProfileSigma() {
        return xProfileSigma;
    }

    public Label getXProfileFWHM() {
        return xProfileFWHM;
    }

    public Label getXProfileBG() {
        return xProfileBG;
    }

    public Label getXProfileChi2() {
        return xProfileChi2;
    }

    public Label getXProfileNbIter() {
        return xProfileNbIter;
    }

    public Label getXProfileFitRelChange() {
        return xProfileFitRelChange;
    }

    public CheckBox getYProfileFitConverged() {
        return yProfileFitConverged;
    }

    public Label getYProfileCenter() {
        return yProfileCenter;
    }

    public Label getYProfileMag() {
        return yProfileMag;
    }

    public Label getYProfileSigma() {
        return yProfileSigma;
    }

    public Label getYProfileFWHM() {
        return yProfileFWHM;
    }

    public Label getYProfileBG() {
        return yProfileBG;
    }

    public Label getYProfileChi2() {
        return yProfileChi2;
    }

    public Label getYProfileNbIter() {
        return yProfileNbIter;
    }

    public Label getYProfileFitRelChange() {
        return yProfileFitRelChange;
    }

    public Label getLineProfileCenter() {
        return lineProfileCenter;
    }

    public Label getLineProfileMag() {
        return lineProfileMag;
    }

    public Label getLineProfileSigma() {
        return lineProfileSigma;
    }

    public Label getLineProfileFWHM() {
        return lineProfileFWHM;
    }

    public Label getLineProfileBG() {
        return lineProfileBG;
    }

    public Label getLineProfileChi2() {
        return lineProfileChi2;
    }

    public Label getLineProfileNbIter() {
        return lineProfileNbIter;
    }

    public Label getLineProfileFitRelChange() {
        return lineProfileFitRelChange;
    }

    public AbstractAction getPreProcessingPanelViewAction() {
        return preProcessingPanelViewAction;
    }

    public AbstractAction getGeneralViewAction() {
        return generalViewAction;
    }

    public AbstractAction getUserROIPanelViewAction() {
        return userROIPanelViewAction;
    }

    public AbstractAction getAutoRoiPanelViewAction() {
        return autoRoiPanelViewAction;
    }

//    public AbstractAction getGlobalProfilePanelViewAction() {
//        return globalProfilePanelViewAction;
//    }

    public AbstractAction getGaussian2dPanelViewAction() {
        return gaussian2dPanelViewAction;
    }

    public AbstractAction getAttributesPanelViewAction() {
        return attributesPanelViewAction;
    }

    public AbstractAction getPixelConversionPanelViewAction() {
        return pixelConversionPanelViewAction;
    }

    public AbstractAction getHistogramPanelViewAction() {
        return histogramPanelViewAction;
    }

    public AbstractAction getGammaPanelViewAction() {
        return gammaPanelViewAction;
    }

    public AbstractAction getImgAnalyzerCommandsViewAction() {
        return imgAnalyzerCommandsViewAction;
    }

//    public ADockingManager getGlobalProfileDockingManager() {
//        return globalProfileDockingManager;
//    }

    public AbstractAction getCommonProfileViewAction() {
        return commonProfileViewAction;
    }

    public AbstractAction getxProfileViewAction() {
        return xProfileViewAction;
    }

    public AbstractAction getyProfileViewAction() {
        return yProfileViewAction;
    }

    public AbstractAction getLineProfileViewAction() {
        return lineProfileViewAction;
    }

}
