package fr.soleil.bean.imgbeamanalyzer.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.action.ActionContainerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.bean.imggrabber.ImgGrabber;
import fr.soleil.bean.utils.CharlestonImageViewer;
import fr.soleil.bean.utils.ImageProfileViewManager;
import fr.soleil.bean.utils.ProfiledImageViewer;
import fr.soleil.charleston.docking.DockingFactory;
import fr.soleil.charleston.docking.perspective.CharlestonPerspectiveFactory;
import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.NumberMatrixTable;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextFieldButton;
import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.exception.DockingException;
import fr.soleil.docking.util.DockingUtils;
import fr.soleil.docking.view.IView;

public class ImgBeamAnalyzerMainView extends ImgBeamAnalyzerAbstractView {

    protected ImgGrabber grabberBean;
    protected StringButton saveCurrentSettingsButton;
    protected JButton writeAllRoiButton;
    protected JComponent mainSplitPane;
    // protected JComponent profiledImagesSplit;
    protected JPanel inputImagePanel;
    protected ImageProfileViewManager roiImageProfilePanel;
    // protected ImageProfilePanel inputImageProfilePanel;
    protected ProfiledImageViewer inputImageViewer;
    protected CharlestonImageViewer imageViewer;
    protected NumberMatrixTable gaussianCovarianceViewer;
    protected JComponent gaussianSplitPane;
    // JIRA CONTROLGUI-15
    protected Chart histogrammViewer;
    protected JPanel histogramGlobalPanel;

    /**
     * Change refreshing period action JIRA CONTROLGUI-12
     */
    private final ChangeRefreshingPeriodAction changeRefreshingPeriodAction = new ByPeriodChangeRefreshingPeriodAction();

//    private final ActionContainerFactory containerFactory;

    private final static Logger LOGGER = LoggerFactory.getLogger(ImgBeamAnalyzerMainView.class.getName());
    private final String propertiesDirectory;
//    private JComponent rightPanel;
    private JPanel grabberPanel;

    private AbstractAction analyzerInputImageViewAction;
    private AbstractAction imageViewerViewAction;
    private AbstractAction profileTabbedPaneViewAction;
    private AbstractAction momentsPanelViewAction;
    private AbstractAction preProcessingComponentViewAction;
    private AbstractAction userROIPanelViewAction;
    private AbstractAction autoROIPanelViewAction;
    private AbstractAction lineProfilePanelViewAction;

    private AbstractAction gaussianImageViewAction;
    private AbstractAction gaussianImagePanelViewAction;
    private AbstractAction grabberRightPanelViewAction;

    public ImgBeamAnalyzerMainView(ImgGrabber grabber, DockingFactory dockingFactory, ADockingManager dockingManager) {
        super(dockingFactory, dockingManager);

        // JIRA CONTROLGUI-15
        histogrammViewer = new Chart();

        propertiesDirectory = ImgBeamAnalyzer.getPropertiesDirectoryName();
        File file = ImgBeamAnalyzer.getPropertiesFile(propertiesDirectory, ImgBeamAnalyzer.HISTOGRAMM_CHART_PROPERTIES);

        if (file.exists()) {
            ChartProperties chartProperties = ImgBeamAnalyzer.getChartProperties(file);

            histogrammViewer.setChartProperties(chartProperties);
        }

        grabberBean = grabber;
        new ActionContainerFactory();
    }

    @Override
    protected void initLineProfileEndX2() {
        lineProfileEndX2 = generateNumberFieldButton();
    }

    @Override
    protected void initLineProfileEndY2() {
        lineProfileEndY2 = generateNumberFieldButton();
    }

    @Override
    protected void initLineProfileOriginX2() {
        lineProfileOriginX2 = generateNumberFieldButton();
    }

    @Override
    protected void initLineProfileOriginY2() {
        lineProfileOriginY2 = generateNumberFieldButton();
    }

    @Override
    protected void initUserROIHeight2() {
        userROIHeight2 = generateNumberFieldButton();
    }

    @Override
    protected void initUserROIWidth2() {
        userROIWidth2 = generateNumberFieldButton();
    }

    @Override
    protected void initUserROIOriginX2() {
        userROIOriginX2 = generateNumberFieldButton();
    }

    @Override
    protected void initUserROIOriginY2() {
        userROIOriginY2 = generateNumberFieldButton();
    }

    @Override
    protected void layoutLineProfilePanel() {
        super.layoutLineProfilePanel();
        lineProfilePanel.remove(lineProfileFitConvergedLabel);
        lineProfilePanel.remove(lineProfileFitConverged);
    }

    @Override
    protected void layoutUserRoiPanel() {
        super.layoutUserRoiPanel();
        GridBagConstraints writeAllConstraints = new GridBagConstraints();
        writeAllConstraints.fill = GridBagConstraints.HORIZONTAL;
        writeAllConstraints.gridx = 2;
        writeAllConstraints.gridy = 5;
        writeAllConstraints.weightx = 0.1;
        writeAllConstraints.insets = SPACE;
        userROIPanel.add(getWriteAllRoiButton(), writeAllConstraints);

        GridBagConstraints saveCurrentSettingsConstraints = new GridBagConstraints();
        saveCurrentSettingsConstraints.fill = GridBagConstraints.HORIZONTAL;
        saveCurrentSettingsConstraints.gridx = 1;
        saveCurrentSettingsConstraints.gridy = 5;
        saveCurrentSettingsConstraints.weightx = 0.1;
        saveCurrentSettingsConstraints.insets = SPACE;
        userROIPanel.add(getSaveCurrentSettingsButton(), saveCurrentSettingsConstraints);
    }

    @Override
    protected void layoutMainPanel() {
//        JPanel momentsComponent = getMomentsPanel();
//        JPanel preProcessingComponent = getPreProcessingPanel();
//        JPanel userROIComponent = getUserROIPanel();
//        JPanel autoROIComponent = getAutoROIPanel();
//        JPanel lineProfileComponent = getLineProfilePanel();
//        dockingManager.getViewFactory().addView("Moments", null, momentsComponent, "momentsPanel", mainPanel);
//        dockingManager.getViewFactory().addView("Pre-Processing", null, preProcessingComponent, "preProcessingPanel",
//                mainPanel);
//        dockingManager.getViewFactory().addView("User ROI", null, userROIComponent, "userROIPanel", mainPanel);
//        dockingManager.getViewFactory().addView("Auto ROI", null, autoROIComponent, "autoROIPanel", mainPanel);
//        dockingManager.getViewFactory().addView("Line Profile", null, lineProfileComponent, "lineProfilePanel",
//                mainPanel);
//
//        // mainPanel.setBorder(new TitledBorder(ANALYZER_BORDER, "ImgAnalyzer"));
//        mainPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));
    }

    public JComponent getMainSplitPane() {
        if (mainSplitPane == null) {

            mainSplitPane = dockingManager.createNewDockingArea(Color.gray);

//            getProfiledImagesSplit();
            getInputImageViewer();
            getRoiImageProfilePanel();
//            getMainPanel();
            grabberPanel = grabberBean.getLightView().getMainPanel();

            IView analyzerInputImageView = dockingManager.getViewFactory().addView("Analyzer Input Image", null,
                    inputImageViewer, "analyzerInputImage", mainSplitPane);
            analyzerInputImageViewAction = DockingUtils.generateShowViewAction(analyzerInputImageView);

            IView imageViewerView = dockingManager.getViewFactory().addView("Analyzer ROI Image Viewer", null,
                    roiImageProfilePanel.getImageViewer(), "imageViewer", mainSplitPane);
            imageViewerViewAction = DockingUtils.generateShowViewAction(imageViewerView);

            IView profileTabbedPaneView = dockingManager.getViewFactory().addView("Analyzer ROI Image Profile", null,
                    roiImageProfilePanel.getProfileTabbedPane(), "profileTabbedPane", mainSplitPane);
            profileTabbedPaneViewAction = DockingUtils.generateShowViewAction(profileTabbedPaneView);

            JPanel momentsComponent = getMomentsPanel();
            JPanel preProcessingComponent = getPreProcessingPanel();
            JPanel userROIComponent = getUserROIPanel();
            JPanel autoROIComponent = getAutoROIPanel();
            JPanel lineProfileComponent = getLineProfilePanel();
            IView momentsPanelView = dockingManager.getViewFactory().addView("Analyzer Moments", null,
                    new JScrollPane(momentsComponent), "momentsPanel", mainSplitPane);
            momentsPanelViewAction = DockingUtils.generateShowViewAction(momentsPanelView);

            IView preProcessingComponentView = dockingManager.getViewFactory().addView("Analyzer Pre-Processing", null,
                    new JScrollPane(preProcessingComponent), "preProcessingPanel", mainSplitPane);
            preProcessingComponentViewAction = DockingUtils.generateShowViewAction(preProcessingComponentView);

            IView userROIPanelViewMgmt = dockingManager.getViewFactory().addView("Analyzer User ROI", null,
                    new JScrollPane(userROIComponent), "userROIPanel", mainSplitPane);
            userROIPanelViewAction = DockingUtils.generateShowViewAction(userROIPanelViewMgmt);

            IView autoROIPanelView = dockingManager.getViewFactory().addView("Analyzer Auto ROI", null,
                    new JScrollPane(autoROIComponent), "autoROIPanel", mainSplitPane);
            autoROIPanelViewAction = DockingUtils.generateShowViewAction(autoROIPanelView);

            IView lineProfilePanelView = dockingManager.getViewFactory().addView("Analyzer Line Profile", null,
                    new JScrollPane(lineProfileComponent), "lineProfilePanel", mainSplitPane);
            lineProfilePanelViewAction = DockingUtils.generateShowViewAction(lineProfilePanelView);

            IView grabberRightPanelView = dockingManager.getViewFactory().addView("Image Grabber", null,
                    new JScrollPane(grabberPanel), "grabberRightPanel", mainSplitPane);
            grabberRightPanelViewAction = DockingUtils.generateShowViewAction(grabberRightPanelView);

        }
        return mainSplitPane;
    }

    @Override
    public JComponent getMainPanel() {
        // if (mainPanel == null) {
//        mainPanel = dockingManager.createNewDockingArea(Color.gray);
//        layoutMainPanel();
        // }
        return null;
        // return mainPanel;
    }

    public JComponent getAnalyzerRightPanel() {
        return mainPanel;
    }

    public void saveSelectedPerspective(ImgBeamAnalyzer imgBeamAnalyzer) {

        Preferences prefs = Preferences.userNodeForPackage(getClass());
        CharlestonPerspectiveFactory charlestonPerspectiveFactory = (CharlestonPerspectiveFactory) dockingManager
                .getPerspectiveFactory();

        try {
            dockingManager.updatePerspective(charlestonPerspectiveFactory.getImageMgmtMainPerspective(),
                    getMainSplitPane());
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }
        try {
            dockingManager.savePreferences(prefs);
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }

        CharlestonPerspectiveFactory perspectiveFactory = (CharlestonPerspectiveFactory) dockingManager
                .getPerspectiveFactory();

        try {
            dockingManager.updatePerspective(perspectiveFactory.getGaussianPerspective(), getGaussianSplitPane());
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }
        try {
            dockingManager.savePreferences(prefs);
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }

        try {

            String name = CharlestonPerspectiveFactory.IMAGE_MGMT_MAIN_PERSPECTIVE;
            File file = imgBeamAnalyzer.getFile(name, false);

            dockingManager.getPerspectiveFactory().savePerspective(file,
                    charlestonPerspectiveFactory.getImageMgmtMainPerspective());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        try {

            String name = CharlestonPerspectiveFactory.GAUSSIAN_PERSPECTIVE;
            File file = imgBeamAnalyzer.getFile(name, false);

            dockingManager.getPerspectiveFactory().savePerspective(file, perspectiveFactory.getGaussianPerspective());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

    }

    public ImageProfileViewManager getRoiImageProfilePanel() {
        if (roiImageProfilePanel == null) {
            roiImageProfilePanel = new ImageProfileViewManager(dockingManager);
            // JIRA CONTROLGUI-14
            File file = ImgBeamAnalyzer.getPropertiesFile(propertiesDirectory, ImgBeamAnalyzer.ROI_IMAGE_PROPERTIES);
            if (file.exists()) {
                ImageProperties imageProperties = ImgBeamAnalyzer.getImageProperties(file);
                roiImageProfilePanel.getImageViewer().setImageProperties(imageProperties);
            }
            // roiImageProfilePanel.setImageBorder(BorderFactory.createTitledBorder("Analyzer ROI Image"));
            roiImageProfilePanel.setProfileX1TextField(getLineProfileOriginX2());
            roiImageProfilePanel.setProfileX2TextField(getLineProfileEndX2());
            roiImageProfilePanel.setProfileY1TextField(getLineProfileOriginY2());
            roiImageProfilePanel.setProfileY2TextField(getLineProfileEndY2());
        }
        return roiImageProfilePanel;
    }

    public ProfiledImageViewer getInputImageViewer() {
        if (inputImageViewer == null) {
            inputImageViewer = new ProfiledImageViewer();
            // JIRA CONTROLGUI-14
            File file = ImgBeamAnalyzer.getPropertiesFile(propertiesDirectory, ImgBeamAnalyzer.INPUT_IMAGE_PROPERTIES);
            if (file.exists()) {
                ImageProperties imageProperties = ImgBeamAnalyzer.getImageProperties(file);
                inputImageViewer.setImageProperties(imageProperties);
            }
            // Add refreshing period action popup menu JIRA CONTROLGUI-12
            inputImageViewer.getPopupMenu().add(changeRefreshingPeriodAction);
            inputImageViewer.setXTextField(getUserROIOriginX2());
            inputImageViewer.setYTextField(getUserROIOriginY2());
            inputImageViewer.setWidthTextField(getUserROIWidth2());
            inputImageViewer.setHeightTextField(getUserROIHeight2());
            inputImageViewer.setToolBarVisible(true);
        }
        return inputImageViewer;
    }

    /**
     * @return the inputImagePanel
     */
    public JPanel getInputImagePanel() {
        if (inputImagePanel == null) {
            inputImagePanel = new JPanel();
            inputImagePanel.setLayout(new GridBagLayout());

            GridBagConstraints viewerConstraints = new GridBagConstraints();
            viewerConstraints.fill = GridBagConstraints.BOTH;
            viewerConstraints.gridx = 0;
            viewerConstraints.gridy = 0;
            viewerConstraints.weightx = 1;
            viewerConstraints.weighty = 1;

            GridBagConstraints attrPanelConstraints = new GridBagConstraints();
            attrPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
            attrPanelConstraints.gridx = 0;
            attrPanelConstraints.gridy = 1;
            attrPanelConstraints.weightx = 1;
            attrPanelConstraints.weighty = 0;

            inputImagePanel.add(getImageViewer(), viewerConstraints);

            inputImagePanel.setBorder(new TitledBorder("Image"));
        }
        return inputImagePanel;
    }

    /**
     * @return the histogramGlobalPanel
     */
    public JPanel getHistogramGlobalPanel() {
        if (histogramGlobalPanel == null) {
            histogramGlobalPanel = new JPanel();
            histogramGlobalPanel.setLayout(new GridBagLayout());

            GridBagConstraints spectrumConstraints = new GridBagConstraints();
            spectrumConstraints.fill = GridBagConstraints.BOTH;
            spectrumConstraints.gridx = 0;
            spectrumConstraints.gridy = 0;
            spectrumConstraints.weightx = 1;
            spectrumConstraints.weighty = 1;

            GridBagConstraints scalarsConstraints = new GridBagConstraints();
            scalarsConstraints.fill = GridBagConstraints.HORIZONTAL;
            scalarsConstraints.gridx = 0;
            scalarsConstraints.gridy = 1;
            scalarsConstraints.weightx = 1;
            scalarsConstraints.weighty = 0;

            histogramGlobalPanel.add(getHistogrammViewer(), spectrumConstraints);
            histogramGlobalPanel.add(getHistogramPanel(), scalarsConstraints);
            histogramGlobalPanel.setBorder(ANALYZER_BORDER);
        }
        return histogramGlobalPanel;
    }

    public StringButton getSaveCurrentSettingsButton() {
        if (saveCurrentSettingsButton == null) {
            saveCurrentSettingsButton = generateStringButton();
            saveCurrentSettingsButton.setText("SaveCurrentSettings");
        }
        return saveCurrentSettingsButton;
    }

    public JButton getWriteAllRoiButton() {
        if (writeAllRoiButton == null) {
            writeAllRoiButton = new JButton(generateWriteAllAction());
            writeAllRoiButton.setText("Write All");
        }
        return writeAllRoiButton;
    }

    private AbstractAction generateWriteAllAction() {
        AbstractAction result = new AbstractAction() {
            private static final long serialVersionUID = 344234665851954332L;

            @Override
            public void actionPerformed(ActionEvent e) {
                TextFieldButton tfbUserRoiHeight = (TextFieldButton) userROIHeight2;
                TextFieldButton tfbUserRoiWidth = (TextFieldButton) userROIWidth2;
                TextFieldButton tfbUserRoiX = (TextFieldButton) userROIOriginX2;
                TextFieldButton tfbUserRoiY = (TextFieldButton) userROIOriginY2;

                if (tfbUserRoiHeight != null && !tfbUserRoiHeight.getText().isEmpty()) {
                    tfbUserRoiHeight.send();
                }
                if (tfbUserRoiWidth != null && !tfbUserRoiWidth.getText().isEmpty()) {
                    tfbUserRoiWidth.send();
                }
                if (tfbUserRoiX != null && !tfbUserRoiX.getText().isEmpty()) {
                    tfbUserRoiX.send();
                }
                if (tfbUserRoiY != null && !tfbUserRoiY.getText().isEmpty()) {
                    tfbUserRoiY.send();
                }
            }
        };
        return result;
    }

    public CharlestonImageViewer getImageViewer() {
        if (imageViewer == null) {
            imageViewer = new CharlestonImageViewer();
            imageViewer.setVisible(true);
            imageViewer.setBorder(new TitledBorder("Input Image (with rotation and flip)"));
        }
        return imageViewer;
    }

    /**
     * @return the gaussianCovarianceViewer
     */
    public NumberMatrixTable getGaussianCovarianceViewer() {
        if (gaussianCovarianceViewer == null) {
            gaussianCovarianceViewer = new NumberMatrixTable();
            gaussianCovarianceViewer.setVisible(true);
            ((JComponent) gaussianCovarianceViewer).setBorder(new TitledBorder("GaussianFitParameterCovariance"));
        }
        return gaussianCovarianceViewer;
    }

    /**
     * @param histogramm the histogramm to set
     */
    public void setHistogramm(TangoKey histogramm) {
        AbstractCometeBox<IDataArrayTarget> box = CometeBoxProvider.getCometeBox(ChartViewerBox.class);
        box.disconnectWidgetFromAll(getHistogrammViewer());
        if (histogramm != null) {

            getHistogrammViewer().setDataViewFillCometeColor(histogramm.getInformationKey(), CometeColor.RED);
            box.connectWidget(getHistogrammViewer(), histogramm);
        }
    }

    /**
     * @param profile the xProfile to set
     */
    public void setXProfile(TangoKey profile) {
        getRoiImageProfilePanel().setProfile1(profile);
    }

    /**
     * @param profileFitted the xProfileFitted to set
     */
    public void setXProfileFitted(TangoKey profileFitted) {
        getRoiImageProfilePanel().setProfileFitted1(profileFitted);
    }

    /**
     * @param profileError the xProfileError to set
     */
    public void setXProfileError(TangoKey profileError) {
        getRoiImageProfilePanel().setProfileError1(profileError);
    }

    /**
     * @param profile the yProfile to set
     */
    public void setYProfile(TangoKey profile) {
        getRoiImageProfilePanel().setProfile2(profile);
    }

    /**
     * @param profileFitted the yProfileFitted to set
     */
    public void setYProfileFitted(TangoKey profileFitted) {
        getRoiImageProfilePanel().setProfileFitted2(profileFitted);
    }

    /**
     * @param profileError the yProfileError to set
     */
    public void setYProfileError(TangoKey profileError) {
        getRoiImageProfilePanel().setProfileError2(profileError);
    }

    /**
     * @param profileError the yProfileError to set
     */
    public void setXProfilePixelSizeX(TangoKey xProfilePixelSizeX) {
        getRoiImageProfilePanel().setPixelSizeX(xProfilePixelSizeX);
    }

    public void setYProfilePixelSizeY(TangoKey yProfilePixelSizeY) {
        getRoiImageProfilePanel().setPixelSizeY(yProfilePixelSizeY);
    }

    /**
     * @param profile the lineProfile to set
     */
    public void setLineProfile(TangoKey profile) {
        getRoiImageProfilePanel().setProfile3(profile);
    }

    /**
     * @param profileFitted the lineProfileFitted to set
     */
    public void setLineProfileFitted(TangoKey profileFitted) {
        getRoiImageProfilePanel().setProfileFitted3(profileFitted);
    }

    /**
     * @param profileError the lineProfileError to set
     */
    public void setLineProfileError(TangoKey profileError) {
        getRoiImageProfilePanel().setProfileError3(profileError);
    }

    /**
     * @param gaussianCovariance the gaussianCovariance to set
     */
    public void setGaussianCovariance(TangoKey gaussianCovariance) {
        AbstractCometeBox<INumberMatrixTarget> box = CometeBoxProvider.getCometeBox(NumberMatrixBox.class);

        box.disconnectWidgetFromAll(getGaussianCovarianceViewer());
        box.connectWidget(getGaussianCovarianceViewer(), gaussianCovariance);

        getGaussianCovarianceViewer().refresh();
    }

    /**
     * @param input the input to set
     */
    public void setInput(TangoKey input) {

        AbstractCometeBox<INumberMatrixTarget> box = CometeBoxProvider.getCometeBox(NumberMatrixBox.class);
        box.disconnectWidgetFromAll(getInputImageViewer());
        box.connectWidget(getInputImageViewer(), input);

        // JIRA CONTROLGUI-12
        changeRefreshingPeriodAction.setKey(input);
    }

    public IBooleanTarget getAutoRoiEnableTarget() {
        return getRoiImageProfilePanel().getAutoRoiEnableTarget();
    }

    public INumberTarget getAutoRoiXNumberTarget() {
        return getRoiImageProfilePanel().getAutoRoiXNumberTarget();
    }

    public INumberTarget getAutoRoiYNumberTarget() {
        return getRoiImageProfilePanel().getAutoRoiYNumberTarget();
    }

    public IBooleanTarget getUserRoiEnableTarget() {
        return getRoiImageProfilePanel().getUserRoiEnableTarget();
    }

    public INumberTarget getUserRoiXNumberTarget() {
        return getRoiImageProfilePanel().getUserRoiXNumberTarget();
    }

    public INumberTarget getUserRoiYNumberTarget() {
        return getRoiImageProfilePanel().getUserRoiYNumberTarget();
    }

    public IRefreshingGroupListener getRefreshingGroupListener() {
        return getRoiImageProfilePanel().getRefreshingGroupListener();
    }

    /**
     * @param roiImage the roi to set
     */
    public void setRoi(TangoKey roiImage) {
        getRoiImageProfilePanel().setImageModel(roiImage);
    }

    public JComponent getGaussianSplitPane() {
        if (gaussianSplitPane == null) {

            gaussianSplitPane = dockingManager.createNewDockingArea(Color.GRAY);
            IView gaussianImageView = dockingManager.getViewFactory().addView("2D Gaussian Fit Image", null,
                    getGaussianCovarianceViewer(), "gaussianCovarianceViewer", gaussianSplitPane);

            gaussianImageViewAction = DockingUtils.generateShowViewAction(gaussianImageView);

            IView gaussianImagePanelView = dockingManager.getViewFactory().addView("2D Gaussian Fit Panel", null,
                    new JScrollPane(getGaussian2dPanel()), "gaussianCovariancePanel", gaussianSplitPane);

            gaussianImagePanelViewAction = DockingUtils.generateShowViewAction(gaussianImagePanelView);

        }
        return gaussianSplitPane;
    }

    /**
     * @return the histogrammViewer
     */
    public Chart getHistogrammViewer() {
        return histogrammViewer;
    }

    public JPanel getGrabberPanel() {
        return grabberPanel;
    }

    public AbstractAction getAnalyzerInputImageViewAction() {
        return analyzerInputImageViewAction;
    }

    public AbstractAction getImageViewerView() {
        return imageViewerViewAction;
    }

    public AbstractAction getProfileTabbedPaneViewAction() {
        return profileTabbedPaneViewAction;
    }

    public AbstractAction getMomentsPanelViewAction() {
        return momentsPanelViewAction;
    }

    public AbstractAction getPreProcessingComponentViewAction() {
        return preProcessingComponentViewAction;
    }

    public AbstractAction getUserROIPanelViewAction() {
        return userROIPanelViewAction;
    }

    public AbstractAction getAutoROIPanelViewAction() {
        return autoROIPanelViewAction;
    }

    public AbstractAction getLineProfilePanelViewAction() {
        return lineProfilePanelViewAction;
    }

    public AbstractAction getGaussianImageViewAction() {
        return gaussianImageViewAction;
    }

    public AbstractAction getGaussianImagePanelViewAction() {
        return gaussianImagePanelViewAction;
    }

    public AbstractAction getGrabberRightPanelViewAction() {
        return grabberRightPanelViewAction;
    }

}
