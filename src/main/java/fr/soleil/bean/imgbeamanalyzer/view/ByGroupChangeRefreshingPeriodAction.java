package fr.soleil.bean.imgbeamanalyzer.view;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.data.service.GroupRefreshingStrategy;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;

public class ByGroupChangeRefreshingPeriodAction extends ChangeRefreshingPeriodAction {

    private static final long serialVersionUID = 4823265430106277279L;

    @Override
    protected PolledRefreshingStrategy buildRefreshingStrategy(IKey key, int period) {
        return new GroupRefreshingStrategy(ImgBeamAnalyzer.ROI_IMAGE_GROUP, period);
    }

}
