package fr.soleil.bean.imgbeamanalyzer;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.imgbeamanalyzer.view.ImgBeamAnalyzerAdvancedView;
import fr.soleil.bean.imgbeamanalyzer.view.ImgBeamAnalyzerMainView;
import fr.soleil.bean.imggrabber.ImgGrabber;
import fr.soleil.charleston.docking.DockingFactory;
import fr.soleil.charleston.docking.infonode.DockingType;
import fr.soleil.charleston.docking.infonode.InfoNodeDockingFactory;
import fr.soleil.charleston.docking.infonode.VlDockingFactory;
import fr.soleil.charleston.docking.perspective.CharlestonPerspectiveFactory;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.xml.ChartPropertiesXmlManager;
import fr.soleil.comete.definition.widget.properties.xml.ImagePropertiesXmlManager;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GroupRefreshingStrategy;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.IRefreshingStrategy;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.exception.DockingException;
import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.Perspective;
import fr.soleil.docking.util.DockingUtils;
import fr.soleil.docking.view.IView;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.JClickableMenu;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

/**
 * This is a bean that groups ImageAnalyzer and VideoGrabber functionalities in a single panel
 */
public class ImgBeamAnalyzer extends AbstractTangoBox {

    private static final long serialVersionUID = 8521063165387565883L;

    // Attributes
    private static final String ESTIM_COMPUT_TIME = "EstimComputTime";
    private static final String AUTO_ROI_FOUND = "AutoROIFound";
    private static final String AUTO_ROI_ORIGIN_X = "AutoROIOriginX";
    private static final String AUTO_ROI_ORIGIN_Y = "AutoROIOriginY";
    private static final String AUTO_ROI_WIDTH = "AutoROIWidth";
    private static final String AUTO_ROI_HEIGHT = "AutoROIHeight";
    private static final String MAX_INTENSITY = "MaxIntensity";
    private static final String MEAN_INTENSITY = "MeanIntensity";
    private static final String CENTROID_X = "CentroidX";
    private static final String CENTROID_Y = "CentroidY";
    private static final String VARIANCE_X = "VarianceX";
    private static final String VARIANCE_Y = "VarianceY";
    private static final String COVARIANCE_XY = "CovarianceXY";
    private static final String CORRELATION_XY = "CorrelationXY";
    private static final String SKEW_X = "SkewX";
    private static final String SKEW_Y = "SkewY";
    private static final String SKEW_X2Y = "SkewX2Y";
    private static final String SKEW_XY2 = "SkewXY2";
    private static final String X_PROJ_FIT_CONVERGED = "XProjFitConverged";
    private static final String X_PROJ_FIT_CENTER = "XProjFitCenter";
    private static final String X_PROJ_FIT_MAG = "XProjFitMag";
    private static final String X_PROJ_FIT_SIGMA = "XProjFitSigma";
    private static final String X_PROJ_FIT_FWHM = "XProjFitFWHM";
    private static final String X_PROJ_FIT_BG = "XProjFitBG";
    private static final String X_PROJ_FIT_CHI2 = "XProjFitChi2";
    private static final String Y_PROJ_FIT_CONVERGED = "YProjFitConverged";
    private static final String Y_PROJ_FIT_CENTER = "YProjFitCenter";
    private static final String Y_PROJ_FIT_MAG = "YProjFitMag";
    private static final String Y_PROJ_FIT_SIGMA = "YProjFitSigma";
    private static final String Y_PROJ_FIT_FWHM = "YProjFitFWHM";
    private static final String Y_PROJ_FIT_BG = "YProjFitBG";
    private static final String Y_PROJ_FIT_CHI2 = "YProjFitChi2";
    private static final String LINE_PROFILE_FIT_CONVERGED = "LineProfileFitConverged";
    private static final String LINE_PROFILE_FIT_CENTER = "LineProfileFitCenter";
    private static final String LINE_PROFILE_FIT_MAG = "LineProfileFitMag";
    private static final String LINE_PROFILE_FIT_SIGMA = "LineProfileFitSigma";
    private static final String LINE_PROFILE_FIT_FWHM = "LineProfileFitFWHM";
    private static final String LINE_PROFILE_FIT_CHI2 = "LineProfileFitChi2";
    private static final String LINE_PROFILE_FIT_BG = "LineProfileFitBG";
    private static final String GAUSSIAN_FIT_CONVERGED = "GaussianFitConverged";
    private static final String GAUSSIAN_FIT_MAGNITUDE = "GaussianFitMagnitude";
    private static final String GAUSSIAN_FIT_CENTER_X = "GaussianFitCenterX";
    private static final String GAUSSIAN_FIT_CENTER_Y = "GaussianFitCenterY";
    private static final String GAUSSIAN_FIT_VARIANCE_X = "GaussianFitVarianceX";
    private static final String GAUSSIAN_FIT_VARIANCE_Y = "GaussianFitVarianceY";
    private static final String GAUSSIAN_FIT_COVARIANCE_XY = "GaussianFitCovarianceXY";
    private static final String GAUSSIAN_FIT_MAJOR_AXIS_FWHM = "GaussianFitMajorAxisFWHM";
    private static final String GAUSSIAN_FIT_MINOR_AXIS_FWHM = "GaussianFitMinorAxisFWHM";
    private static final String GAUSSIAN_FIT_TILT = "GaussianFitTilt";
    private static final String GAUSSIAN_FIT_BG = "GaussianFitBG";
    private static final String GAUSSIAN_FIT_CHI2 = "GaussianFitChi2";
    private static final String X_PROJ_FIT_NB_ITER = "XProjFitNbIter";
    private static final String X_PROJ_FIT_REL_CHANGE = "XProjFitRelChange";
    private static final String Y_PROJ_FIT_NB_ITER = "YProjFitNbIter";
    private static final String Y_PROJ_FIT_REL_CHANGE = "YProjFitRelChange";
    private static final String LINE_PROFILE_FIT_NB_ITER = "LineProfileFitNbIter";
    private static final String LINE_PROFILE_FIT_REL_CHANGE = "LineProfileFitRelChange";
    private static final String GAUSSIAN_FIT_NB_ITER = "GaussianFitNbIter";
    private static final String GAUSSIAN_FIT_REL_CHANGE = "GaussianFitRelChange";
    private static final String ENABLE_IMAGE_STATS = "EnableImageStats";
    private static final String ENABLE_PROFILES = "EnableProfiles";
    private static final String ENABLE_HISTOGRAM = "EnableHistogram";
    private static final String ENABLE_USER_ROI = "EnableUserROI";
    private static final String ENABLE_AUTO_ROI = "EnableAutoROI";
    private static final String ENABLE2D_GAUSSIAN_FIT = "Enable2DGaussianFit";
    private static final String COMPUTATION_PERIOD = "ComputationPeriod";
    private static final String ROTATION = "Rotation";
    private static final String HORIZONTAL_FLIP = "HorizontalFlip";
    private static final String BITS_PER_PIXEL = "BitsPerPixel";
    private static final String HISTOGRAM_NB_BINS = "HistogramNbBins";
    private static final String HISTOGRAM_RANGE_MIN = "HistogramRangeMin";
    private static final String HISTOGRAM_RANGE_MAX = "HistogramRangeMax";
    private static final String GAMMA_CORRECTION = "GammaCorrection";
    private static final String AUTO_ROI_THRESHOLD = "AutoROIThreshold";
    private static final String AUTO_ROI_MAG_FACTOR_X = "AutoROIMagFactorX";
    private static final String AUTO_ROI_MAG_FACTOR_Y = "AutoROIMagFactorY";
    private static final String ALARM_ZONE = "AlarmZone";
    private static final String PIXEL_SIZE_X = "PixelSizeX";
    private static final String PIXEL_SIZE_Y = "PixelSizeY";
    private static final String OPTICAL_MAGNIFICATION = "OpticalMagnification";
    private static final String PROFILE_FIT_FIXED_BG = "ProfileFitFixedBg";
    private static final String USER_ROI_ORIGIN_X = "UserROIOriginX";
    private static final String USER_ROI_ORIGIN_Y = "UserROIOriginY";
    private static final String USER_ROI_WIDTH = "UserROIWidth";
    private static final String USER_ROI_HEIGHT = "UserROIHeight";
    private static final String FIT1D_NB_ITER_MAX = "Fit1DNbIterMax";
    private static final String FIT1D_MAX_REL_CHANGE = "Fit1DMaxRelChange";
    private static final String FIT2D_NB_ITER_MAX = "Fit2DNbIterMax";
    private static final String FIT2D_MAX_REL_CHANGE = "Fit2DMaxRelChange";
    private static final String X_PROJ = "XProj";
    private static final String X_PROJ_FITTED = "XProjFitted";
    private static final String X_PROJ_ERROR = "XProjError";
    private static final String Y_PROJ = "YProj";
    private static final String Y_PROJ_FITTED = "YProjFitted";
    private static final String Y_PROJ_ERROR = "YProjError";
    private static final String LINE_PROFILE = "LineProfile";
    private static final String LINE_PROFILE_FITTED = "LineProfileFitted";
    private static final String LINE_PROFILE_ERROR = "LineProfileError";
    private static final String LINE_PROFILE_ORIGIN_X = "LineProfileOriginX";
    private static final String LINE_PROFILE_END_X = "LineProfileEndX";
    private static final String LINE_PROFILE_ORIGIN_Y = "LineProfileOriginY";
    private static final String LINE_PROFILE_END_Y = "LineProfileEndY";
    private static final String LINE_PROFILE_THICKNESS = "LineProfileThickness";
    private static final String HISTOGRAM = "Histogram";
    private static final String ROI_IMAGE = "ROIImage";
    private static final String INPUT_IMAGE = "InputImage";
    private static final String GAUSSIAN_FIT_PARAMETER_COVARIANCE = "GaussianFitParameterCovariance";
    protected static final String[] EXPECTED_ATTRIBUTES = { ESTIM_COMPUT_TIME, AUTO_ROI_FOUND, AUTO_ROI_ORIGIN_X,
            AUTO_ROI_ORIGIN_Y, AUTO_ROI_WIDTH, AUTO_ROI_HEIGHT, MAX_INTENSITY, MEAN_INTENSITY, CENTROID_X, CENTROID_Y,
            VARIANCE_X, VARIANCE_Y, COVARIANCE_XY, CORRELATION_XY, SKEW_X, SKEW_Y, SKEW_X2Y, SKEW_XY2,
            X_PROJ_FIT_CONVERGED, X_PROJ_FIT_CENTER, X_PROJ_FIT_MAG, X_PROJ_FIT_SIGMA, X_PROJ_FIT_FWHM, X_PROJ_FIT_BG,
            X_PROJ_FIT_CHI2, Y_PROJ_FIT_CONVERGED, Y_PROJ_FIT_CENTER, Y_PROJ_FIT_MAG, Y_PROJ_FIT_SIGMA, Y_PROJ_FIT_FWHM,
            Y_PROJ_FIT_BG, Y_PROJ_FIT_CHI2, LINE_PROFILE_FIT_CONVERGED, LINE_PROFILE_FIT_CENTER, LINE_PROFILE_FIT_MAG,
            LINE_PROFILE_FIT_SIGMA, LINE_PROFILE_FIT_FWHM, LINE_PROFILE_FIT_CHI2, LINE_PROFILE_FIT_BG,
            GAUSSIAN_FIT_CONVERGED, GAUSSIAN_FIT_MAGNITUDE, GAUSSIAN_FIT_CENTER_X, GAUSSIAN_FIT_CENTER_Y,
            GAUSSIAN_FIT_VARIANCE_X, GAUSSIAN_FIT_VARIANCE_Y, GAUSSIAN_FIT_COVARIANCE_XY, GAUSSIAN_FIT_MAJOR_AXIS_FWHM,
            GAUSSIAN_FIT_MINOR_AXIS_FWHM, GAUSSIAN_FIT_TILT, GAUSSIAN_FIT_BG, GAUSSIAN_FIT_CHI2, X_PROJ_FIT_NB_ITER,
            X_PROJ_FIT_REL_CHANGE, Y_PROJ_FIT_NB_ITER, Y_PROJ_FIT_REL_CHANGE, LINE_PROFILE_FIT_NB_ITER,
            LINE_PROFILE_FIT_REL_CHANGE, GAUSSIAN_FIT_NB_ITER, GAUSSIAN_FIT_REL_CHANGE, ENABLE_IMAGE_STATS,
            ENABLE_PROFILES, ENABLE_HISTOGRAM, ENABLE_USER_ROI, ENABLE_AUTO_ROI, ENABLE2D_GAUSSIAN_FIT,
            COMPUTATION_PERIOD, ROTATION, HORIZONTAL_FLIP, BITS_PER_PIXEL, HISTOGRAM_NB_BINS, HISTOGRAM_RANGE_MIN,
            HISTOGRAM_RANGE_MAX, GAMMA_CORRECTION, AUTO_ROI_THRESHOLD, AUTO_ROI_MAG_FACTOR_X, AUTO_ROI_MAG_FACTOR_Y,
            ALARM_ZONE, PIXEL_SIZE_X, PIXEL_SIZE_Y, OPTICAL_MAGNIFICATION, PROFILE_FIT_FIXED_BG, USER_ROI_ORIGIN_X,
            USER_ROI_ORIGIN_Y, USER_ROI_WIDTH, USER_ROI_HEIGHT, FIT1D_NB_ITER_MAX, FIT1D_MAX_REL_CHANGE,
            FIT2D_NB_ITER_MAX, FIT2D_MAX_REL_CHANGE, X_PROJ, X_PROJ_FITTED, X_PROJ_ERROR, Y_PROJ, Y_PROJ_FITTED,
            Y_PROJ_ERROR, LINE_PROFILE, LINE_PROFILE_FITTED, LINE_PROFILE_ERROR, LINE_PROFILE_ORIGIN_X,
            LINE_PROFILE_END_X, LINE_PROFILE_ORIGIN_Y, LINE_PROFILE_END_Y, LINE_PROFILE_THICKNESS, HISTOGRAM, ROI_IMAGE,
            INPUT_IMAGE, GAUSSIAN_FIT_PARAMETER_COVARIANCE };

    public static final String ROI_IMAGE_GROUP = "Roi Image Group";

    // Commands
    private static final String GET_VERSION_NUMBER = "GetVersionNumber";
    private static final String INIT = "Init";
    private static final String PROCESS = "Process";
    private static final String START = "Start";
    private static final String STATE = "State";
    private static final String STATUS = "Status";
    private static final String STOP = "Stop";
    private static final String SAVE_CURRENT_SETTINGS = "SaveCurrentSettings";
    protected static final String[] EXPECTED_COMMANDS = { GET_VERSION_NUMBER, INIT, PROCESS, START, STATE, STATUS, STOP,
            SAVE_CURRENT_SETTINGS };

    private static final String SHOW_VIEW = "Show View";

    private static final String IMAGE_2D_FITTING = "Image 2D Fitting";
    private static final String IMAGE_MANAGEMENT = "Image Management";

    public static final int LIGHT_MODE = 0;
    public static final int GRABBER_CONFIGURATION_MODE = 1;
    public static final int FULL_MODE = 2;

    protected static final String IMG_GRABBER_PROPERTY = "ImageDevice";
    protected static final String GENERAL_TITLE = "General";
    protected static final String GRABBER_TITLE = "Grabber";
    protected static final String ADVANCED_TITLE = "Advanced";

    protected static final String INIT_ATTRIBUTES_INFO = "Attributes initialization...";
    protected static final String INIT_COMMANDS_INFO = "Commands initialization...";
    private static final String DEFAULT_DOCKING_DIRECTORY = System.getProperty("user.home") + File.separator
            + "Charleston";
    private static final String DEFAULT = "default";

    private static final String PROPERTIES_DIRECTORY = System.getProperty("user.home") + File.separator + "Charleston"
            + File.separator + "Properties";
    public static final String HISTOGRAMM_CHART_PROPERTIES = "histogramm-chart.xml";
    public static final String PROFILE1_CHART_PROPERTIES = "profile1-chart1.xml";
    public static final String PROFILE2_CHART_PROPERTIES = "profile2-chart1.xml";
    public static final String PROFILE3_CHART_PROPERTIES = "profile3-chart1.xml";
    public static final String INPUT_IMAGE_PROPERTIES = "input_image.xml";
    public static final String ROI_IMAGE_PROPERTIES = "roi_image.xml";
    public static final String GRABBER_IMAGE_PROPERTIES = "grabber_image.xml";

    private static final String SETTING_MODELS = "Setting models...";

    private static final Logger LOGGER = LoggerFactory.getLogger(ImgBeamAnalyzer.class.getName());

    protected int mode;

    protected ImgGrabber grabberBean;
    protected GrabberReader grabberReader;

    protected ImgBeamAnalyzerAdvancedView imgBeamAnalyzerAdvancedView;
    protected ImgBeamAnalyzerMainView imgBeamAnalyzerMainView;
    protected JComponent mainTabbedPane;
    protected JComponent imageSpectrumTabbedPane;
    protected JComponent advancedSplitPane;
    protected boolean analyzerInitialization;
    protected boolean analyzerConnected;
    protected boolean grabberConnected;
    protected JPanel globalStatusPanel;
    protected String missingProperty;

    private final DockingFactory dockingFactory;

    // Generic docking manager
    private final ADockingManager dockingManager;

    // Default perspective at higher level
    private Perspective defaultPerspective;
    private final String defaultDockingDirectory;
    private static String defaultPropertiesDirectory;

    private final DockingType dockingType;
    private final JMenu resetViewMenu;
    private IView imgMgmtView;
    private IView imgFitView;
    private IView histogramView;
    // private AbstractAction imgAnalyzerViewAction;
    private AbstractAction grabberAdvancedViewAction;

    /**
     * Default constructor
     */
    public ImgBeamAnalyzer() {
        this(null, null);
    }

    /**
     * Constructor
     * 
     * @param dockingDirectory String
     * @param propertiesDirectory String
     */
    public ImgBeamAnalyzer(String dockingDirectory, String propertiesDirectory) {
        super();
        resetViewMenu = createResetViewMenu();
        defaultPropertiesDirectory = getDirectory(propertiesDirectory, PROPERTIES_DIRECTORY);

        this.defaultDockingDirectory = getDirectory(dockingDirectory, DEFAULT_DOCKING_DIRECTORY);

        String dockingProperty = System.getProperty("dockingtype", "INFONODE");
        dockingType = DockingType.valueOf(dockingProperty.trim().toUpperCase());
        switch (dockingType) {
            case INFONODE: {
                dockingFactory = new InfoNodeDockingFactory();
            }
                break;
            case VL: {
                dockingFactory = new VlDockingFactory();
            }
                break;
            default: {
                dockingFactory = new InfoNodeDockingFactory();
            }
        }
        dockingManager = dockingFactory.createDockingManager(dockingFactory.createViewFactory(),
                dockingFactory.createPerspectiveFactory());

        grabberBean = new AdvancedImgGrabber();
        grabberReader = new GrabberReader();
        stringBox.setErrorText(grabberReader, "");
        badAttributes = new ArrayList<String>();
        badCommands = new ArrayList<String>();
        imgBeamAnalyzerAdvancedView = new ImgBeamAnalyzerAdvancedView(dockingFactory, dockingManager);
        imgBeamAnalyzerMainView = new ImgBeamAnalyzerMainView(grabberBean, dockingFactory, dockingManager);
    }

    protected JMenu createResetViewMenu() {
        return new JMenu(SHOW_VIEW);
    }

    public JMenu getResetViewMenu() {
        return resetViewMenu;
    }

    /**
     * Sets a directory's value according to args value or, in
     * default case, according to a default value
     * 
     * @param argDirectory String
     * @param defaultDirectory String
     */
    private String getDirectory(String argDirectory, String defaultDirectory) {
        String effectiveDirectory = null;
        if ((argDirectory == null) || argDirectory.trim().isEmpty()) {
            effectiveDirectory = defaultDirectory;
        } else {
            try {
                File tmp = new File(argDirectory);
                if (tmp.exists()) {
                    if (tmp.isDirectory()) {
                        effectiveDirectory = argDirectory;
                    } else {
                        effectiveDirectory = tmp.getParent();
                    }
                } else {
                    tmp.mkdirs();
                    effectiveDirectory = argDirectory;
                }
            } catch (Exception e) {
                LOGGER.warn("Invalid directory. Using default directory for saving + (" + defaultDirectory + ")");
                effectiveDirectory = defaultDirectory;
                try {
                    File tmp = new File(defaultDirectory);
                    if (!tmp.exists()) {
                        tmp.mkdirs();
                    }
                } catch (Exception e2) {
                    LOGGER.error("Failed to create default directory for saving + (" + defaultDirectory + ")", e2);
                }
            }
        }

        return effectiveDirectory;
    }

    @Override
    protected void initMaxProgress() {
        currentProgression = 0;
        maxProgress = 343;
    }

    public void initGUI() {
        this.setLayout(new GridBagLayout());
        GridBagConstraints tabConstraints = new GridBagConstraints();
        tabConstraints.fill = GridBagConstraints.BOTH;
        tabConstraints.gridx = 0;
        tabConstraints.gridy = 0;
        tabConstraints.weightx = 1;
        tabConstraints.weighty = 1;
        this.add(getMainTabbedPane(), tabConstraints);
        GridBagConstraints statusConstraints = new GridBagConstraints();
        statusConstraints.fill = GridBagConstraints.HORIZONTAL;
        statusConstraints.gridx = 0;
        statusConstraints.gridy = 1;
        statusConstraints.weightx = 1;
        statusConstraints.weighty = 0;
        this.add(getGlobalStatusPanel(), statusConstraints);

    }

    protected void initAttributes() {
        // // little hack to have all attributes connected
        // DAOConnectionThread.connect(getModel());
        // try {
        // Thread.sleep(5000);
        // }
        // catch (InterruptedException e1) {
        // // TODO Auto-generated catch block
        // e1.printStackTrace();
        // }
        fireProgressionChanged(currentProgression, INIT_ATTRIBUTES_INFO);
        checkAttributes(EXPECTED_ATTRIBUTES);
        fireProgressionChanged(currentProgression, INIT_ATTRIBUTES_INFO + " done");
        currentProgression = currentProgression % (maxProgress + 1);
    }

    @Override
    protected boolean checkAttribute(String attributeShortName) {
        boolean result = super.checkAttribute(attributeShortName);
        fireProgressionChanged(++currentProgression, INIT_ATTRIBUTES_INFO);
        return result;
    }

    protected void initCommands() {
        fireProgressionChanged(currentProgression, INIT_COMMANDS_INFO);
        checkCommands(EXPECTED_COMMANDS);
        fireProgressionChanged(currentProgression, INIT_COMMANDS_INFO + " done");
        currentProgression = currentProgression % (maxProgress + 1);
    }

    @Override
    protected boolean checkCommand(String commandShortName) {
        boolean result = super.checkCommand(commandShortName);
        fireProgressionChanged(++currentProgression, INIT_COMMANDS_INFO);
        return result;
    }

    public JComponent getMainTabbedPane() {
        if (mainTabbedPane == null) {

            mainTabbedPane = dockingManager.createNewDockingArea(Color.gray);
            IView genView = dockingManager.getViewFactory().addView(GENERAL_TITLE, null, getImageSpectrumTabbedPane(),
                    GENERAL_TITLE, mainTabbedPane);

            AbstractAction genViewAction = DockingUtils.generateShowViewAction(genView);
            JMenu generalDockingAreaMenu = new JClickableMenu(genViewAction);

            AbstractAction imgMgmtDockingAreaAction = DockingUtils.generateShowViewAction(imgMgmtView);

            JMenu imgMgmtDockingAreaMenu = new JClickableMenu(imgMgmtDockingAreaAction);
            generalDockingAreaMenu.add(imgMgmtDockingAreaMenu);

            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getAnalyzerInputImageViewAction());
            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getImageViewerView());

            JMenu imageProfileMenu = new JClickableMenu(imgBeamAnalyzerMainView.getProfileTabbedPaneViewAction());

            imageProfileMenu.add(imgBeamAnalyzerMainView.getRoiImageProfilePanel().getxProjectionViewAction());
            imageProfileMenu.add(imgBeamAnalyzerMainView.getRoiImageProfilePanel().getyProjectionViewAction());
            imageProfileMenu.add(imgBeamAnalyzerMainView.getRoiImageProfilePanel().getLineProfileViewAction());
            imgMgmtDockingAreaMenu.add(imageProfileMenu);

            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getMomentsPanelViewAction());
            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getPreProcessingComponentViewAction());
            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getUserROIPanelViewAction());
            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getAutoROIPanelViewAction());
            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getLineProfilePanelViewAction());
            imgMgmtDockingAreaMenu.add(imgBeamAnalyzerMainView.getGrabberRightPanelViewAction());

            AbstractAction imgFitViewAction = DockingUtils.generateShowViewAction(imgFitView);
            JMenu imgFitDockingAreaMenu = new JClickableMenu(imgFitViewAction);
            generalDockingAreaMenu.add(imgFitDockingAreaMenu);

            imgFitDockingAreaMenu.add(imgBeamAnalyzerMainView.getGaussianImageViewAction());
            imgFitDockingAreaMenu.add(imgBeamAnalyzerMainView.getGaussianImagePanelViewAction());

            JMenuItem histogramDockingAreaMenu = new JMenuItem(DockingUtils.generateShowViewAction(histogramView));
            generalDockingAreaMenu.add(histogramDockingAreaMenu);

            resetViewMenu.add(generalDockingAreaMenu);

        }
        return mainTabbedPane;

    }

    public JComponent getImageSpectrumTabbedPane() {
        if (imageSpectrumTabbedPane == null) {
            imageSpectrumTabbedPane = dockingManager.createNewDockingArea(Color.gray);

            imgMgmtView = dockingManager.getViewFactory().addView(IMAGE_MANAGEMENT, null,
                    imgBeamAnalyzerMainView.getMainSplitPane(), IMAGE_MANAGEMENT, imageSpectrumTabbedPane);

            imgFitView = dockingManager.getViewFactory().addView(IMAGE_2D_FITTING, null,
                    imgBeamAnalyzerMainView.getGaussianSplitPane(), IMAGE_2D_FITTING, imageSpectrumTabbedPane);

            histogramView = dockingManager.getViewFactory().addView(HISTOGRAM, null,
                    new JScrollPane(imgBeamAnalyzerMainView.getHistogramGlobalPanel()), HISTOGRAM,
                    imageSpectrumTabbedPane);

        }
        return imageSpectrumTabbedPane;
    }

    protected JPanel getGrabberImagePanel() {
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints fullConstraints = new GridBagConstraints();
        fullConstraints.fill = GridBagConstraints.BOTH;
        fullConstraints.gridx = 0;
        fullConstraints.gridy = 0;
        fullConstraints.weightx = 1;
        fullConstraints.weighty = 1;
        panel.add(grabberBean.getMainView().getMainSplitPane(), fullConstraints);
        return panel;
    }

    /**
     * @return the advancedSplitPane
     */
    public JComponent getAdvancedSplitPane() {
        if (advancedSplitPane == null) {

            advancedSplitPane = dockingManager.createNewDockingArea(Color.GRAY);

            imgBeamAnalyzerAdvancedView.fillProfile(advancedSplitPane);

            grabberBean.getAdvancedView().fillAdvancedPanel(dockingManager, advancedSplitPane);
        }
        return advancedSplitPane;
    }

    @Override
    public void start() {
        super.start();
        grabberBean.start();
    }

    @Override
    public void stop() {
        grabberBean.stop();
        super.stop();
    }

    @Override
    protected void clearGUI() {
        clearModels(true, true);
        stringBox.disconnectWidgetFromAll(grabberReader);
    }

    @Override
    protected void onConnectionError() {
        String errorMessage = "Failed to connect to ImgBeamAnalyzer device";
        setModel(null);
        System.err.println(getClass().getSimpleName() + ":\n" + errorMessage);
        fireProgressionChanged(getMaxProgression(), errorMessage);
        grabberBean.fireProgression(grabberBean.getMaxProgression(), "No grabber defined");
    }

    @Override
    protected void refreshGUI() {
        analyzerInitialization = true;
        initAttributes();
        initCommands();
        displayBadAttributesAndCommands();
        manageGlobalModels(true);
        analyzerConnected = true;
    }

    protected void fireNograbber() {
        grabberBean.fireProgression(grabberBean.getMaxProgression(), "No grabber defined");
    }

    public void setAdvancedModels(boolean manageGrabber, boolean warnProgress) {

        String info = SETTING_MODELS;
        if (manageGrabber) {
            setGrabberAdvancedModels(warnProgress);
        }

        /* commands */
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        setCommandModel(GET_VERSION_NUMBER, warnProgress, info, imgBeamAnalyzerAdvancedView.getVersionNumber());
        setCommandModel(INIT, warnProgress, info, imgBeamAnalyzerAdvancedView.getInit());
        setCommandModel(PROCESS, warnProgress, info, imgBeamAnalyzerAdvancedView.getProcess());
        setCommandModel(START, warnProgress, info, imgBeamAnalyzerAdvancedView.getStart());
        setCommandModel(STATE, warnProgress, info, imgBeamAnalyzerAdvancedView.getState());
        setCommandModel(STATUS, warnProgress, info, imgBeamAnalyzerAdvancedView.getStatus());
        setCommandModel(STOP, warnProgress, info, imgBeamAnalyzerAdvancedView.getStop());

        /* attributes */
        setAttributeModel(ESTIM_COMPUT_TIME, warnProgress, info, imgBeamAnalyzerAdvancedView.getEstimComputTime());
        setAttributeModel(AUTO_ROI_FOUND, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIFound());
        setAttributeModel(AUTO_ROI_ORIGIN_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIOriginX());
        setAttributeModel(AUTO_ROI_ORIGIN_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIOriginY());
        setAttributeModel(AUTO_ROI_WIDTH, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIWidth());
        setAttributeModel(AUTO_ROI_HEIGHT, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIHeight());
        setAttributeModel(MAX_INTENSITY, warnProgress, info, imgBeamAnalyzerAdvancedView.getMaxIntensity());
        // MODIFIED 25/02
        setAttributeModel(MEAN_INTENSITY, warnProgress, info, imgBeamAnalyzerAdvancedView.getMeanIntensity());
        setAttributeModel(CENTROID_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getCentroidX());
        setAttributeModel(CENTROID_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getCentroidY());
        setAttributeModel(VARIANCE_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getVarianceX());
        setAttributeModel(VARIANCE_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getVarianceY());
        setAttributeModel(COVARIANCE_XY, warnProgress, info, imgBeamAnalyzerAdvancedView.getCovarianceXY());
        setAttributeModel(CORRELATION_XY, warnProgress, info, imgBeamAnalyzerAdvancedView.getCorrelationXY());
        setAttributeModel(SKEW_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getSkewX());
        setAttributeModel(SKEW_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getSkewY());
        setAttributeModel(SKEW_X2Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getSkewX2Y());
        setAttributeModel(SKEW_XY2, warnProgress, info, imgBeamAnalyzerAdvancedView.getSkewXY2());
        setAttributeModel(X_PROJ_FIT_CONVERGED, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getXProfileFitConverged());
        setAttributeModel(X_PROJ_FIT_CENTER, warnProgress, info, imgBeamAnalyzerAdvancedView.getXProfileCenter());
        setAttributeModel(X_PROJ_FIT_MAG, warnProgress, info, imgBeamAnalyzerAdvancedView.getXProfileMag());
        setAttributeModel(X_PROJ_FIT_SIGMA, warnProgress, info, imgBeamAnalyzerAdvancedView.getXProfileSigma());
        setAttributeModel(X_PROJ_FIT_FWHM, warnProgress, info, imgBeamAnalyzerAdvancedView.getXProfileFWHM());
        setAttributeModel(X_PROJ_FIT_BG, warnProgress, info, imgBeamAnalyzerAdvancedView.getXProfileBG());
        setAttributeModel(X_PROJ_FIT_CHI2, warnProgress, info, imgBeamAnalyzerAdvancedView.getXProfileChi2());
        setAttributeModel(Y_PROJ_FIT_CONVERGED, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getYProfileFitConverged());
        setAttributeModel(Y_PROJ_FIT_CENTER, warnProgress, info, imgBeamAnalyzerAdvancedView.getYProfileCenter());
        setAttributeModel(Y_PROJ_FIT_MAG, warnProgress, info, imgBeamAnalyzerAdvancedView.getYProfileMag());
        setAttributeModel(Y_PROJ_FIT_SIGMA, warnProgress, info, imgBeamAnalyzerAdvancedView.getYProfileSigma());
        setAttributeModel(Y_PROJ_FIT_FWHM, warnProgress, info, imgBeamAnalyzerAdvancedView.getYProfileFWHM());
        setAttributeModel(Y_PROJ_FIT_BG, warnProgress, info, imgBeamAnalyzerAdvancedView.getYProfileBG());
        setAttributeModel(Y_PROJ_FIT_CHI2, warnProgress, info, imgBeamAnalyzerAdvancedView.getYProfileChi2());
        setAttributeModel(LINE_PROFILE_FIT_CONVERGED, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileFitConverged());
        setAttributeModel(LINE_PROFILE_FIT_CENTER, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileCenter());
        setAttributeModel(LINE_PROFILE_FIT_MAG, warnProgress, info, imgBeamAnalyzerAdvancedView.getLineProfileMag());
        setAttributeModel(LINE_PROFILE_FIT_SIGMA, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileSigma());
        setAttributeModel(LINE_PROFILE_FIT_FWHM, warnProgress, info, imgBeamAnalyzerAdvancedView.getLineProfileFWHM());
        setAttributeModel(LINE_PROFILE_FIT_BG, warnProgress, info, imgBeamAnalyzerAdvancedView.getLineProfileBG());
        setAttributeModel(LINE_PROFILE_FIT_CHI2, warnProgress, info, imgBeamAnalyzerAdvancedView.getLineProfileChi2());
        setAttributeModel(GAUSSIAN_FIT_CONVERGED, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitConverged());
        setAttributeModel(GAUSSIAN_FIT_MAGNITUDE, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitMagnitude());
        setAttributeModel(GAUSSIAN_FIT_CENTER_X, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitCenterX());
        setAttributeModel(GAUSSIAN_FIT_CENTER_Y, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitCenterY());
        setAttributeModel(GAUSSIAN_FIT_VARIANCE_X, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitVarianceX());
        setAttributeModel(GAUSSIAN_FIT_VARIANCE_Y, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitVarianceY());
        setAttributeModel(GAUSSIAN_FIT_COVARIANCE_XY, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitCovarianceXY());
        setAttributeModel(GAUSSIAN_FIT_MAJOR_AXIS_FWHM, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitMajorAxisFWHM());
        setAttributeModel(GAUSSIAN_FIT_MINOR_AXIS_FWHM, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitMinorAxisFWHM());
        setAttributeModel(GAUSSIAN_FIT_TILT, warnProgress, info, imgBeamAnalyzerAdvancedView.getGaussianFitTilt());
        setAttributeModel(GAUSSIAN_FIT_BG, warnProgress, info, imgBeamAnalyzerAdvancedView.getGaussianFitBG());
        setAttributeModel(GAUSSIAN_FIT_CHI2, warnProgress, info, imgBeamAnalyzerAdvancedView.getGaussianFitChi2());
        setAttributeModel(X_PROJ_FIT_NB_ITER, warnProgress, info, imgBeamAnalyzerAdvancedView.getXProfileNbIter());
        setAttributeModel(X_PROJ_FIT_REL_CHANGE, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getXProfileFitRelChange());
        setAttributeModel(Y_PROJ_FIT_NB_ITER, warnProgress, info, imgBeamAnalyzerAdvancedView.getYProfileNbIter());
        setAttributeModel(Y_PROJ_FIT_REL_CHANGE, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getYProfileFitRelChange());
        setAttributeModel(LINE_PROFILE_FIT_NB_ITER, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileNbIter());
        setAttributeModel(LINE_PROFILE_FIT_REL_CHANGE, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileFitRelChange());
        setAttributeModel(LINE_PROFILE_ORIGIN_X, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileOriginX(),
                imgBeamAnalyzerAdvancedView.getLineProfileOriginX2());
        setAttributeModel(LINE_PROFILE_END_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getLineProfileEndX(),
                imgBeamAnalyzerAdvancedView.getLineProfileEndX2());
        setAttributeModel(LINE_PROFILE_ORIGIN_Y, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileOriginY(),
                imgBeamAnalyzerAdvancedView.getLineProfileOriginY2());
        setAttributeModel(LINE_PROFILE_END_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getLineProfileEndY(),
                imgBeamAnalyzerAdvancedView.getLineProfileEndY2());
        setAttributeModel(LINE_PROFILE_THICKNESS, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getLineProfileThickness(),
                imgBeamAnalyzerAdvancedView.getLineProfileThickness2());
        setAttributeModel(GAUSSIAN_FIT_NB_ITER, warnProgress, info, imgBeamAnalyzerAdvancedView.getGaussianFitNbIter());
        setAttributeModel(GAUSSIAN_FIT_REL_CHANGE, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getGaussianFitRelChange());
        setAttributeModel(ENABLE_IMAGE_STATS, warnProgress, info, imgBeamAnalyzerAdvancedView.getEnableImageMoments(),
                imgBeamAnalyzerAdvancedView.getEnableImageMoments2());
        setAttributeModel(ENABLE_PROFILES, warnProgress, info, imgBeamAnalyzerAdvancedView.getEnableProfiles(),
                imgBeamAnalyzerAdvancedView.getEnableProfiles2());
        setAttributeModel(ENABLE_HISTOGRAM, warnProgress, info, imgBeamAnalyzerAdvancedView.getEnableHistogram(),
                imgBeamAnalyzerAdvancedView.getEnableHistogram2());
        setAttributeModel(ENABLE_USER_ROI, warnProgress, info, imgBeamAnalyzerAdvancedView.getEnableUserROI(),
                imgBeamAnalyzerAdvancedView.getEnableUserROI2());
        setAttributeModel("imgBeamAnalyzerAdvancedView", warnProgress, info,
                imgBeamAnalyzerAdvancedView.getEnableAutoROI(), imgBeamAnalyzerAdvancedView.getEnableAutoROI2());
        setAttributeModel(ENABLE2D_GAUSSIAN_FIT, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getEnable2DGaussianFit(),
                imgBeamAnalyzerAdvancedView.getEnable2DGaussianFit2());
        setAttributeModel(COMPUTATION_PERIOD, warnProgress, info, imgBeamAnalyzerAdvancedView.getComputationPeriod(),
                imgBeamAnalyzerAdvancedView.getComputationPeriod2());
        setAttributeModel(ROTATION, warnProgress, info, imgBeamAnalyzerAdvancedView.getRotation(),
                imgBeamAnalyzerAdvancedView.getRotation2());
        setAttributeModel(HORIZONTAL_FLIP, warnProgress, info, imgBeamAnalyzerAdvancedView.getHorizontalFlip(),
                imgBeamAnalyzerAdvancedView.getHorizontalFlip2());
        setAttributeModel(BITS_PER_PIXEL, warnProgress, info, imgBeamAnalyzerAdvancedView.getBitsPerPixel(),
                imgBeamAnalyzerAdvancedView.getBitsPerPixel2());
        setAttributeModel(HISTOGRAM_NB_BINS, warnProgress, info, imgBeamAnalyzerAdvancedView.getHistogramNbBins(),
                imgBeamAnalyzerAdvancedView.getHistogramNbBins2());
        setAttributeModel(HISTOGRAM_RANGE_MIN, warnProgress, info, imgBeamAnalyzerAdvancedView.getHistogramRangeMin(),
                imgBeamAnalyzerAdvancedView.getHistogramRangeMin2());
        setAttributeModel(HISTOGRAM_RANGE_MAX, warnProgress, info, imgBeamAnalyzerAdvancedView.getHistogramRangeMax(),
                imgBeamAnalyzerAdvancedView.getHistogramRangeMax2());
        setAttributeModel(GAMMA_CORRECTION, warnProgress, info, imgBeamAnalyzerAdvancedView.getGammaCorrection(),
                imgBeamAnalyzerAdvancedView.getGammaCorrection2());
        setAttributeModel(AUTO_ROI_THRESHOLD, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIThreshold(),
                imgBeamAnalyzerAdvancedView.getAutoROIThreshold2());
        setAttributeModel(AUTO_ROI_MAG_FACTOR_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIMagFactorX(),
                imgBeamAnalyzerAdvancedView.getAutoROIMagFactorX2());
        setAttributeModel(AUTO_ROI_MAG_FACTOR_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getAutoROIMagFactorY(),
                imgBeamAnalyzerAdvancedView.getAutoROIMagFactorY2());
        setAttributeModel(ALARM_ZONE, warnProgress, info, imgBeamAnalyzerAdvancedView.getAlarmZone(),
                imgBeamAnalyzerAdvancedView.getAlarmZone2());
        setAttributeModel(PIXEL_SIZE_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getPixelSizeX(),
                imgBeamAnalyzerAdvancedView.getPixelSizeX2());
        setAttributeModel(PIXEL_SIZE_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getPixelSizeY(),
                imgBeamAnalyzerAdvancedView.getPixelSizeY2());
        setAttributeModel(OPTICAL_MAGNIFICATION, warnProgress, info,
                imgBeamAnalyzerAdvancedView.getOpticalMagnification(),
                imgBeamAnalyzerAdvancedView.getOpticalMagnification2());
        setAttributeModel(PROFILE_FIT_FIXED_BG, warnProgress, info, imgBeamAnalyzerAdvancedView.getProfileFitFixedBg(),
                imgBeamAnalyzerAdvancedView.getProfileFitFixedBg2());
        setAttributeModel(USER_ROI_ORIGIN_X, warnProgress, info, imgBeamAnalyzerAdvancedView.getUserROIOriginX(),
                imgBeamAnalyzerAdvancedView.getUserROIOriginX2());
        setAttributeModel(USER_ROI_ORIGIN_Y, warnProgress, info, imgBeamAnalyzerAdvancedView.getUserROIOriginY(),
                imgBeamAnalyzerAdvancedView.getUserROIOriginY2());
        setAttributeModel(USER_ROI_WIDTH, warnProgress, info, imgBeamAnalyzerAdvancedView.getUserROIWidth(),
                imgBeamAnalyzerAdvancedView.getUserROIWidth2());
        setAttributeModel(USER_ROI_HEIGHT, warnProgress, info, imgBeamAnalyzerAdvancedView.getUserROIHeight(),
                imgBeamAnalyzerAdvancedView.getUserROIHeight2());
        setAttributeModel(FIT1D_NB_ITER_MAX, warnProgress, info, imgBeamAnalyzerAdvancedView.getFit1DNbIterMax(),
                imgBeamAnalyzerAdvancedView.getFit1DNbIterMax2());
        setAttributeModel(FIT1D_MAX_REL_CHANGE, warnProgress, info, imgBeamAnalyzerAdvancedView.getFit1DMaxRelChange(),
                imgBeamAnalyzerAdvancedView.getFit1DMaxRelChange2());
        setAttributeModel(FIT2D_NB_ITER_MAX, warnProgress, info, imgBeamAnalyzerAdvancedView.getFit2DNbIterMax(),
                imgBeamAnalyzerAdvancedView.getFit2DNbIterMax2());
        setAttributeModel(FIT2D_MAX_REL_CHANGE, warnProgress, info, imgBeamAnalyzerAdvancedView.getFit2DMaxRelChange(),
                imgBeamAnalyzerAdvancedView.getFit2DMaxRelChange2());
    }

    public void clearAdvancedModels(boolean manageGrabber, boolean warnProgress) {

        String info = SETTING_MODELS;
        if (manageGrabber) {
            clearGrabberAdvancedModels(warnProgress);
        }

        /* commands */
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getVersionNumber());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getInit());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getProcess());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getStart());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getState());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getStatus());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getStop());

        /* attributes */
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEstimComputTime());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIFound());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIOriginX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIOriginY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIWidth());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIHeight());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getMaxIntensity());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getMeanIntensity());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getCentroidX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getCentroidY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getVarianceX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getVarianceY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getCovarianceXY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getCorrelationXY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getSkewX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getSkewY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getSkewX2Y());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getSkewXY2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileFitConverged());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileCenter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileMag());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileSigma());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileFWHM());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileBG());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileChi2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileFitConverged());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileCenter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileMag());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileSigma());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileFWHM());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileBG());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileChi2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitConverged());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitMagnitude());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitCenterX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitCenterY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitVarianceX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitVarianceY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitCovarianceXY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitMajorAxisFWHM());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitMinorAxisFWHM());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitTilt());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitBG());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitChi2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileNbIter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getXProfileFitRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileNbIter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getYProfileFitRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitNbIter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGaussianFitRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableImageMoments());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableImageMoments2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableProfiles());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableProfiles2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableHistogram());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableHistogram2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableUserROI());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableUserROI2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableAutoROI());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnableAutoROI2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnable2DGaussianFit());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getEnable2DGaussianFit2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getComputationPeriod());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getComputationPeriod2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getRotation());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getRotation2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHorizontalFlip());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHorizontalFlip2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getBitsPerPixel());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getBitsPerPixel2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHistogramNbBins());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHistogramNbBins2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHistogramRangeMin());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHistogramRangeMin2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHistogramRangeMax());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getHistogramRangeMax2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGammaCorrection());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getGammaCorrection2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIThreshold());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIThreshold2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIMagFactorX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIMagFactorX2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIMagFactorY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAutoROIMagFactorY2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAlarmZone());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getAlarmZone2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getPixelSizeX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getPixelSizeX2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getPixelSizeY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getPixelSizeY2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getOpticalMagnification());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getOpticalMagnification2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getProfileFitFixedBg());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getProfileFitFixedBg2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIOriginX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIOriginX2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIOriginY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIOriginY2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIWidth());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIWidth2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIHeight());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getUserROIHeight2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit1DNbIterMax());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit1DNbIterMax2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit1DMaxRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit1DMaxRelChange2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit2DNbIterMax());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit2DNbIterMax2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit2DMaxRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getFit2DMaxRelChange2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileBG());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileCenter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileChi2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileFitConverged());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileFWHM());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileMag());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileSigma());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileNbIter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileFitRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileOriginX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileOriginX2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileEndX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileEndX2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileOriginY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileOriginY2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileEndY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileEndY2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileThickness());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerAdvancedView.getLineProfileThickness2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        currentProgression = currentProgression % (maxProgress + 1);
    }

    public void setMainModels(boolean manageGrabber, boolean warnProgress) {
        setImageGeneralModels(manageGrabber, warnProgress);
        setImageFittingModels(warnProgress);
        setHistogramModels(warnProgress);
    }

    public void clearMainModels(boolean manageGrabber, boolean warnProgress) {
        clearImageGeneralModels(manageGrabber, warnProgress);
        clearImageFittingModels(warnProgress);
        clearHistogramModels(warnProgress);
    }

    protected void applyGroupRefreshingStrategy(IKey key) {
        IRefreshingStrategy oldStrategy = DataSourceProducerProvider.getRefreshingStrategy(key);
        int period = 1000;
        if (oldStrategy instanceof PolledRefreshingStrategy) {
            period = ((PolledRefreshingStrategy) oldStrategy).getRefreshingPeriod();
        }
        DataSourceProducerProvider.setRefreshingStrategy(key, new GroupRefreshingStrategy(ROI_IMAGE_GROUP, period));
    }

    protected void setImageGeneralModels(boolean manageGrabber, boolean warnProgress) {
        if (manageGrabber) {
            setGrabberLightModels(warnProgress);
        }

        String info = SETTING_MODELS;

        // Image
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        if (!badAttributes.contains(INPUT_IMAGE)) {
            imgBeamAnalyzerMainView.setInput(generateAttributeKey(INPUT_IMAGE));
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        // TODO REFRESHING STRATEGY
        setAttributeModel(ROTATION, warnProgress, info, imgBeamAnalyzerMainView.getRotation(),
                imgBeamAnalyzerMainView.getRotation2());
        setAttributeModel(HORIZONTAL_FLIP, warnProgress, info, imgBeamAnalyzerMainView.getHorizontalFlip(),
                imgBeamAnalyzerMainView.getHorizontalFlip2());
        if (!badAttributes.contains(ROI_IMAGE)) {
            TangoKey key = generateAttributeKey(ROI_IMAGE);
            imgBeamAnalyzerMainView.setRoi(key);
            applyGroupRefreshingStrategy(key);
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        if (!badAttributes.contains(X_PROJ)) {
            imgBeamAnalyzerMainView.setXProfile(generateAttributeKey(X_PROJ));
            if (!badAttributes.contains(X_PROJ_FITTED)) {
                imgBeamAnalyzerMainView.setXProfileFitted(generateAttributeKey(X_PROJ_FITTED));
            }
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
            currentProgression++;
        }
        if (!badAttributes.contains(X_PROJ_ERROR)) {
            imgBeamAnalyzerMainView.setXProfileError(generateAttributeKey(X_PROJ_ERROR));
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }

        if (!badAttributes.contains(Y_PROJ)) {
            imgBeamAnalyzerMainView.setYProfile(generateAttributeKey(Y_PROJ));
            if (!badAttributes.contains(Y_PROJ_FITTED)) {
                imgBeamAnalyzerMainView.setYProfileFitted(generateAttributeKey(Y_PROJ_FITTED));
            }
        }
        if (!badAttributes.contains(PIXEL_SIZE_X)) {
            imgBeamAnalyzerMainView.setXProfilePixelSizeX(generateAttributeKey(PIXEL_SIZE_X));
        }

        if (!badAttributes.contains(PIXEL_SIZE_Y)) {
            imgBeamAnalyzerMainView.setYProfilePixelSizeY(generateAttributeKey(PIXEL_SIZE_Y));
        }

        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
            currentProgression++;
        }
        if (!badAttributes.contains(Y_PROJ_ERROR)) {
            imgBeamAnalyzerMainView.setYProfileError(generateAttributeKey(Y_PROJ_ERROR));
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }

        if (!badAttributes.contains(LINE_PROFILE)) {
            imgBeamAnalyzerMainView.setLineProfile(generateAttributeKey(LINE_PROFILE));
            if (!badAttributes.contains(LINE_PROFILE_FITTED)) {
                imgBeamAnalyzerMainView.setLineProfileFitted(generateAttributeKey(LINE_PROFILE_FITTED));
            }
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
            currentProgression++;
        }
        if (!badAttributes.contains(LINE_PROFILE_ERROR)) {
            imgBeamAnalyzerMainView.setLineProfileError(generateAttributeKey(LINE_PROFILE_ERROR));
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }

        setAttributeModel(LINE_PROFILE_ORIGIN_X, warnProgress, info, imgBeamAnalyzerMainView.getLineProfileOriginX(),
                imgBeamAnalyzerMainView.getLineProfileOriginX2());
        setAttributeModel(LINE_PROFILE_ORIGIN_Y, warnProgress, info, imgBeamAnalyzerMainView.getLineProfileOriginY(),
                imgBeamAnalyzerMainView.getLineProfileOriginY2());
        setAttributeModel(LINE_PROFILE_END_X, warnProgress, info, imgBeamAnalyzerMainView.getLineProfileEndX(),
                imgBeamAnalyzerMainView.getLineProfileEndX2());
        setAttributeModel(LINE_PROFILE_END_Y, warnProgress, info, imgBeamAnalyzerMainView.getLineProfileEndY(),
                imgBeamAnalyzerMainView.getLineProfileEndY2());
        setAttributeModel(LINE_PROFILE_THICKNESS, warnProgress, info, imgBeamAnalyzerMainView.getLineProfileThickness(),
                imgBeamAnalyzerMainView.getLineProfileThickness2());

        // User ROI
        TangoKey key = setAttributeModel(ENABLE_USER_ROI, warnProgress, info,
                imgBeamAnalyzerMainView.getEnableUserROI(), imgBeamAnalyzerMainView.getEnableUserROI2(),
                imgBeamAnalyzerMainView.getUserRoiEnableTarget());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(USER_ROI_ORIGIN_X, warnProgress, info, imgBeamAnalyzerMainView.getUserROIOriginX(),
                imgBeamAnalyzerMainView.getUserROIOriginX2(), imgBeamAnalyzerMainView.getUserRoiXNumberTarget());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(USER_ROI_ORIGIN_Y, warnProgress, info, imgBeamAnalyzerMainView.getUserROIOriginY(),
                imgBeamAnalyzerMainView.getUserROIOriginY2(), imgBeamAnalyzerMainView.getUserRoiYNumberTarget());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(USER_ROI_WIDTH, warnProgress, info, imgBeamAnalyzerMainView.getUserROIWidth(),
                imgBeamAnalyzerMainView.getUserROIWidth2());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(USER_ROI_HEIGHT, warnProgress, info, imgBeamAnalyzerMainView.getUserROIHeight(),
                imgBeamAnalyzerMainView.getUserROIHeight2());
        applyGroupRefreshingStrategy(key);
        setCommandModel(SAVE_CURRENT_SETTINGS, warnProgress, info,
                imgBeamAnalyzerMainView.getSaveCurrentSettingsButton());

        // Auto ROI
        key = setAttributeModel(ENABLE_AUTO_ROI, warnProgress, info, imgBeamAnalyzerMainView.getEnableAutoROI(),
                imgBeamAnalyzerMainView.getEnableAutoROI2(), imgBeamAnalyzerMainView.getAutoRoiEnableTarget());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_ORIGIN_X, warnProgress, info, imgBeamAnalyzerMainView.getAutoROIOriginX(),
                imgBeamAnalyzerMainView.getAutoRoiXNumberTarget());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_ORIGIN_Y, warnProgress, info, imgBeamAnalyzerMainView.getAutoROIOriginY(),
                imgBeamAnalyzerMainView.getAutoRoiYNumberTarget());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_WIDTH, warnProgress, info, imgBeamAnalyzerMainView.getAutoROIWidth());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_HEIGHT, warnProgress, info, imgBeamAnalyzerMainView.getAutoROIHeight());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_FOUND, warnProgress, info, imgBeamAnalyzerMainView.getAutoROIFound());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(ALARM_ZONE, warnProgress, info, imgBeamAnalyzerMainView.getAlarmZone(),
                imgBeamAnalyzerMainView.getAlarmZone2());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_THRESHOLD, warnProgress, info, imgBeamAnalyzerMainView.getAutoROIThreshold(),
                imgBeamAnalyzerMainView.getAutoROIThreshold2());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_MAG_FACTOR_X, warnProgress, info,
                imgBeamAnalyzerMainView.getAutoROIMagFactorX(), imgBeamAnalyzerMainView.getAutoROIMagFactorX2());
        applyGroupRefreshingStrategy(key);
        key = setAttributeModel(AUTO_ROI_MAG_FACTOR_Y, warnProgress, info,
                imgBeamAnalyzerMainView.getAutoROIMagFactorY(), imgBeamAnalyzerMainView.getAutoROIMagFactorY2());
        applyGroupRefreshingStrategy(key);
        ((TangoDataSourceFactory) DataSourceProducerProvider.getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID))
                .addRefreshingGroupListener(imgBeamAnalyzerMainView.getRefreshingGroupListener());

        // Moments
        setAttributeModel(ENABLE_IMAGE_STATS, warnProgress, info, imgBeamAnalyzerMainView.getEnableImageMoments(),
                imgBeamAnalyzerMainView.getEnableImageMoments2());
        setAttributeModel(MAX_INTENSITY, warnProgress, info, imgBeamAnalyzerMainView.getMaxIntensity());
        setAttributeModel(MEAN_INTENSITY, warnProgress, info, imgBeamAnalyzerMainView.getMeanIntensity());
        setAttributeModel(CENTROID_X, warnProgress, info, imgBeamAnalyzerMainView.getCentroidX());
        setAttributeModel(CENTROID_Y, warnProgress, info, imgBeamAnalyzerMainView.getCentroidY());
        setAttributeModel(VARIANCE_X, warnProgress, info, imgBeamAnalyzerMainView.getVarianceX());
        setAttributeModel(VARIANCE_Y, warnProgress, info, imgBeamAnalyzerMainView.getVarianceY());
        setAttributeModel(COVARIANCE_XY, warnProgress, info, imgBeamAnalyzerMainView.getCovarianceXY());
        setAttributeModel(CORRELATION_XY, warnProgress, info, imgBeamAnalyzerMainView.getCorrelationXY());
        setAttributeModel(SKEW_X, warnProgress, info, imgBeamAnalyzerMainView.getSkewX());
        setAttributeModel(SKEW_Y, warnProgress, info, imgBeamAnalyzerMainView.getSkewY());
        setAttributeModel(SKEW_X2Y, warnProgress, info, imgBeamAnalyzerMainView.getSkewX2Y());
        setAttributeModel(SKEW_XY2, warnProgress, info, imgBeamAnalyzerMainView.getSkewXY2());
    }

    protected void clearImageGeneralModels(boolean manageGrabber, boolean warnProgress) {
        ((TangoDataSourceFactory) DataSourceProducerProvider.getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID))
                .removeRefreshingGroupListener(imgBeamAnalyzerMainView.getRefreshingGroupListener());
        String info = SETTING_MODELS;
        if (manageGrabber) {
            clearGrabberLightModels(warnProgress);
        }

        // Image
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        imgBeamAnalyzerMainView.setInput(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getRotation());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getRotation2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHorizontalFlip());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHorizontalFlip2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setRoi(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setXProfile(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setXProfileFitted(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setXProfileError(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setYProfile(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setYProfileFitted(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        // ADDED 25/02
        imgBeamAnalyzerMainView.setXProfilePixelSizeX(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setYProfilePixelSizeY(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }

        imgBeamAnalyzerMainView.setYProfileError(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setLineProfile(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setLineProfileFitted(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        imgBeamAnalyzerMainView.setLineProfileError(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }

        // User ROI
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getEnableUserROI(),
                imgBeamAnalyzerMainView.getUserRoiEnableTarget());

        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getEnableUserROI2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIOriginX(),
                imgBeamAnalyzerMainView.getUserRoiXNumberTarget());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIOriginX2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIOriginY(),
                imgBeamAnalyzerMainView.getUserRoiYNumberTarget());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIOriginY2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIWidth());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIWidth2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIHeight());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getUserROIHeight2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getSaveCurrentSettingsButton());

        // Auto ROI
        // ADDED 25/02
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileOriginX());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileOriginX2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileOriginY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileOriginY2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileEndX());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileEndX2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileEndY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileEndY2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileThickness());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getLineProfileThickness2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getEnableAutoROI(),
                imgBeamAnalyzerMainView.getAutoRoiEnableTarget());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getEnableAutoROI2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIOriginX(),
                imgBeamAnalyzerMainView.getAutoRoiXNumberTarget());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIOriginY(),
                imgBeamAnalyzerMainView.getAutoRoiYNumberTarget());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIWidth());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIHeight());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIFound());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAlarmZone());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAlarmZone2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIThreshold());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIThreshold2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIMagFactorX());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIMagFactorX2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIMagFactorY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getAutoROIMagFactorY2());

        // Moments
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getEnableImageMoments());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getEnableImageMoments2());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getMaxIntensity());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getMeanIntensity());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getCentroidX());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getCentroidY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getVarianceX());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getVarianceY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getCovarianceXY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getCorrelationXY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getSkewX());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getSkewY());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getSkewX2Y());
        cleanWidgets(warnProgress, info, imgBeamAnalyzerMainView.getSkewXY2());
        currentProgression = currentProgression % (maxProgress + 1);
    }

    protected void setImageFittingModels(boolean warnProgress) {
        String info = SETTING_MODELS;
        String entityShortName;

        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        // Gaussian
        entityShortName = GAUSSIAN_FIT_PARAMETER_COVARIANCE;
        if (!badAttributes.contains(entityShortName)) {
            imgBeamAnalyzerMainView.setGaussianCovariance(generateAttributeKey(entityShortName));
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        entityShortName = ENABLE2D_GAUSSIAN_FIT;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getEnable2DGaussianFit(),
                imgBeamAnalyzerMainView.getEnable2DGaussianFit2());
        entityShortName = FIT2D_NB_ITER_MAX;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getFit2DNbIterMax(),
                imgBeamAnalyzerMainView.getFit2DNbIterMax2());
        entityShortName = FIT2D_MAX_REL_CHANGE;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getFit2DMaxRelChange(),
                imgBeamAnalyzerMainView.getFit2DMaxRelChange2());
        entityShortName = GAUSSIAN_FIT_CONVERGED;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitConverged());
        entityShortName = GAUSSIAN_FIT_MAGNITUDE;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitMagnitude());
        entityShortName = GAUSSIAN_FIT_CENTER_X;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitCenterX());
        entityShortName = GAUSSIAN_FIT_CENTER_Y;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitCenterY());
        entityShortName = GAUSSIAN_FIT_VARIANCE_X;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitVarianceX());
        entityShortName = GAUSSIAN_FIT_VARIANCE_Y;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitVarianceY());
        entityShortName = GAUSSIAN_FIT_COVARIANCE_XY;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitCovarianceXY());
        entityShortName = GAUSSIAN_FIT_MAJOR_AXIS_FWHM;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitMajorAxisFWHM());
        entityShortName = GAUSSIAN_FIT_MINOR_AXIS_FWHM;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitMinorAxisFWHM());
        entityShortName = GAUSSIAN_FIT_TILT;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitTilt());
        entityShortName = GAUSSIAN_FIT_BG;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitBG());
        entityShortName = GAUSSIAN_FIT_CHI2;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitChi2());
        entityShortName = GAUSSIAN_FIT_NB_ITER;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitNbIter());
        entityShortName = GAUSSIAN_FIT_REL_CHANGE;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getGaussianFitRelChange());
    }

    protected void clearImageFittingModels(boolean warnProgress) {
        String info = SETTING_MODELS;
        // Gaussian
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        imgBeamAnalyzerMainView.setGaussianCovariance(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getEnable2DGaussianFit());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getEnable2DGaussianFit2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getFit2DNbIterMax());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getFit2DNbIterMax2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getFit2DMaxRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getFit2DMaxRelChange2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitConverged());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitMagnitude());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitCenterX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitCenterY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitVarianceX());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitVarianceY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitCovarianceXY());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitMajorAxisFWHM());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitMinorAxisFWHM());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitTilt());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitBG());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitChi2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitNbIter());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getGaussianFitRelChange());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        currentProgression = currentProgression % (maxProgress + 1);
    }

    protected void setHistogramModels(boolean warnProgress) {
        String info = SETTING_MODELS;
        String entityShortName;

        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        entityShortName = HISTOGRAM;
        if (!badAttributes.contains(entityShortName)) {
            imgBeamAnalyzerMainView.setHistogramm(generateAttributeKey(entityShortName));
        }
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        entityShortName = ENABLE_HISTOGRAM;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getEnableHistogram(),
                imgBeamAnalyzerMainView.getEnableHistogram2());
        entityShortName = HISTOGRAM_NB_BINS;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getHistogramNbBins(),
                imgBeamAnalyzerMainView.getHistogramNbBins2());
        entityShortName = HISTOGRAM_RANGE_MIN;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getHistogramRangeMin(),
                imgBeamAnalyzerMainView.getHistogramRangeMin2());
        entityShortName = HISTOGRAM_RANGE_MAX;
        setAttributeModel(entityShortName, warnProgress, info, imgBeamAnalyzerMainView.getHistogramRangeMax(),
                imgBeamAnalyzerMainView.getHistogramRangeMax2());
    }

    protected void clearHistogramModels(boolean warnProgress) {
        String info = SETTING_MODELS;
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        imgBeamAnalyzerMainView.setHistogramm(null);
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getEnableHistogram());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getEnableHistogram2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHistogramNbBins());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHistogramNbBins2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHistogramRangeMin());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHistogramRangeMin2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHistogramRangeMax());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        cleanWidget(imgBeamAnalyzerMainView.getHistogramRangeMax2());
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
        currentProgression = currentProgression % (maxProgress + 1);
    }

    protected void setGrabberMainModels(boolean warnProgress) {
        grabberBean.setMainModels(warnProgress);
    }

    protected void clearGrabberMainModels(boolean warnProgress) {
        grabberBean.clearMainModels(warnProgress);
    }

    protected void setGrabberAdvancedModels(boolean warnProgress) {
        grabberBean.setAdvancedModels(warnProgress);
    }

    protected void clearGrabberAdvancedModels(boolean warnProgress) {
        grabberBean.clearAdvancedModels(warnProgress);
    }

    protected void setGrabberLightModels(boolean warnProgress) {
        grabberBean.setLightModels(warnProgress);
    }

    protected void clearGrabberLightModels(boolean warnProgress) {
        grabberBean.clearLightModels(warnProgress);
    }

    protected void manageGlobalModels(boolean warnProgress) {
        // stringBox.connectWidget(grabberReader, generatePropertyKey("ImageDevice"));
        setWidgetModel(grabberReader, stringBox, generatePropertyKey("ImageDevice"));
        if (getMainTabbedPane() == null) {
            clearModels(false, warnProgress);
        } else {
            setStatusModel();
            switch (getMode()) {
                case LIGHT_MODE:
                    manageMainModels(false, warnProgress);
                    break;
                case GRABBER_CONFIGURATION_MODE:
                    manageMainModels(false, warnProgress);
                    break;
                case FULL_MODE:
                    manageMainModels(false, warnProgress);
                    setAdvancedModels(false, warnProgress);
                    break;
                default:
                    break;
            }
        }
        fireProgressionChanged(getMaxProgression(), "Setting models... done");
    }

    protected void manageMainModels(boolean manageGrabber, boolean warnProgress) {
        if (getImageSpectrumTabbedPane() == null) {
            // clearMainModels(manageGrabber, warnProgress);
        } else if (getMainTabbedPane() == null) {
            // clearMainModels(manageGrabber, warnProgress);
        } else {
            // int index = getImageSpectrumTabbedPane().getSelectedIndex();
            // String title = getSelectedViewTitle(imageSpectrumViewFactory);

            // if (title.equalsIgnoreCase(IMAGE_MANAGEMENT)) {
            setImageGeneralModels(manageGrabber, warnProgress);
            // } else if (title.equalsIgnoreCase(IMAGE_2D_FITTING)) {
            setImageFittingModels(warnProgress);
            // } else if (title.equalsIgnoreCase(HISTOGRAM)) {
            setHistogramModels(warnProgress);
            // } else {
            // clearMainModels(manageGrabber, warnProgress);
            // }
            // switch (index) {
            // case 0:
            // // Image General
            // // clearImageFittingModels(warnProgress);
            // // clearHistogramModels(warnProgress);
            // setImageGeneralModels(manageGrabber, warnProgress);
            // break;
            // case 1:
            // // Image Fitting
            // // clearImageGeneralModels(manageGrabber, warnProgress);
            // // clearHistogramModels(warnProgress);
            // setImageFittingModels(warnProgress);
            // break;
            // case 2:
            // // Histogram
            // // clearImageGeneralModels(manageGrabber, warnProgress);
            // // clearImageFittingModels(warnProgress);
            // setHistogramModels(warnProgress);
            // break;
            // default:
            // clearMainModels(manageGrabber, warnProgress);
            // }
        }
    }

    public void clearModels(boolean manageGrabber, boolean warnProgress) {
        clearAdvancedModels(manageGrabber, warnProgress);
        if (manageGrabber) {
            clearGrabberMainModels(warnProgress);
        }
        clearMainModels(manageGrabber, warnProgress);
        cleanStatusModel();
    }

    // private void stateChanged() {
    // SwingWorker<Void, Void> worker = null;
    //
    // worker = new SwingWorker<Void, Void>() {
    // @Override
    // protected Void doInBackground() throws Exception {
    // manageGlobalModels(true, false);
    // return null;
    // }
    //
    // };
    // if (worker != null) {
    // worker.execute();
    // }
    // }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode, boolean manageGrabber) {
        this.mode = mode;
        int index = -1;
        initGUI();
        switch (mode) {
            case LIGHT_MODE:
                dockingManager.getPerspectiveFactory()
                        .setSelectedPerspective(CharlestonPerspectiveFactory.LIGHT_MODE_PERSPECTIVE);
                if (dockingManager.getViewFactory().getView(GRABBER_TITLE) != null) {
                    dockingManager.getDockingArea().remove(index);
                    if (manageGrabber) {
                        clearGrabberMainModels(false);
                    }
                }
                if (dockingManager.getViewFactory().getView(ADVANCED_TITLE) != null) {
                    dockingManager.getDockingArea().remove(index);
                    clearAdvancedModels(manageGrabber, false);
                }
                break;
            case GRABBER_CONFIGURATION_MODE:
                dockingManager.getPerspectiveFactory()
                        .setSelectedPerspective(CharlestonPerspectiveFactory.GRABBER_CONF_MODE_PERSPECTIVE);

                if (dockingManager.getViewFactory().getView(ADVANCED_TITLE) != null) {
                    dockingManager.getDockingArea().remove(index);
                    clearAdvancedModels(manageGrabber, false);
                }
                addGrabberTab();
                break;
            case FULL_MODE:
                dockingManager.getPerspectiveFactory()
                        .setSelectedPerspective(CharlestonPerspectiveFactory.FULL_MODE_PERSPECTIVE);

                addGrabberTab();
                addAdvancedTab();
                break;
        }
        loadSelectedPerspective();
    }

    private void addAdvancedTab() {
        if (dockingManager.getViewFactory().getView(ADVANCED_TITLE) == null) {
            IView advancedView = dockingManager.getViewFactory().addView(ADVANCED_TITLE, null, getAdvancedSplitPane(),
                    ADVANCED_TITLE, getMainTabbedPane());

            AbstractAction advancedViewAction = DockingUtils.generateShowViewAction(advancedView);

            JMenu advancedViewMenu = new JClickableMenu(advancedViewAction);

//            advancedViewMenu.add(advancedViewAction);

//            JMenu imgAnalyzerViewMenu = new JMenu(imgAnalyzerViewAction);
//
//            imgAnalyzerViewMenu.add(imgAnalyzerViewAction);
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getImgAnalyzerCommandsViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getPreProcessingPanelViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getGeneralViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getUserROIPanelViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getAutoRoiPanelViewAction());
//            AbstractAction globalProfilePanelViewAction = imgBeamAnalyzerAdvancedView.getGlobalProfilePanelViewAction();
//            JMenu globalProfilePanelViewActionMenu = new JClickableMenu(globalProfilePanelViewAction);
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getCommonProfileViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getxProfileViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getyProfileViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getLineProfileViewAction());
//            advancedViewMenu.add(globalProfilePanelViewActionMenu);
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getGaussian2dPanelViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getAttributesPanelViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getPixelConversionPanelViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getHistogramPanelViewAction());
            advancedViewMenu.add(imgBeamAnalyzerAdvancedView.getGammaPanelViewAction());
            advancedViewMenu.add(grabberBean.getAdvancedView().getAttributesViewAction());
            advancedViewMenu.add(grabberBean.getAdvancedView().getCommandsViewAction());

            // advancedViewMenu.add(imgAnalyzerViewMenu);

            advancedViewMenu.add(grabberAdvancedViewAction);

            resetViewMenu.add(advancedViewMenu);
        }
    }

    private void addGrabberTab() {
        if (dockingManager.getViewFactory().getView(GRABBER_TITLE) == null) {
            IView grabberView = dockingManager.getViewFactory().addView(GRABBER_TITLE, null, getGrabberImagePanel(),
                    GRABBER_TITLE, getMainTabbedPane());

            AbstractAction grabberViewAction = DockingUtils.generateShowViewAction(grabberView);

            JMenu grabberDockingAreaMenu = new JClickableMenu(grabberViewAction);
//            grabberDockingAreaMenu.add(grabberViewAction);

            grabberDockingAreaMenu.add(grabberBean.getMainView().getGrabberImageViewerViewAction());
            grabberDockingAreaMenu.add(grabberBean.getMainView().getMainPanelViewAction());

            resetViewMenu.add(grabberDockingAreaMenu);

        }
    }

    /**
     * Load selected perspective (full, grabber conf or light) and its inner perspectives
     */
    public void loadSelectedPerspective() {
        CharlestonPerspectiveFactory charlestonPerspectiveFactory = (CharlestonPerspectiveFactory) dockingManager
                .getPerspectiveFactory();
        final IPerspective perspective = dockingManager.getPerspectiveFactory().getSelectedPerspective();
        final IPerspective innerPerspective = ((CharlestonPerspectiveFactory) dockingManager.getPerspectiveFactory())
                .getInnerPerspective();
        final IPerspective imgMgmtMainPerspective = charlestonPerspectiveFactory.getImageMgmtMainPerspective();

        final IPerspective imageProfilePerspective = charlestonPerspectiveFactory.getImageProfilePerspective();

        final IPerspective grabberPerspective = charlestonPerspectiveFactory.getGrabberPerspective();

        final IPerspective advancedPerspective = charlestonPerspectiveFactory.getAdvancedPerspective();

        final IPerspective gaussianPerspective = charlestonPerspectiveFactory.getGaussianPerspective();

        File filePerspective = getFile(perspective.getName(), false);
        File fileInnerPerspective = getFile(innerPerspective.getName(), false);
        File imgMgmtMainFilePerspective = getFile(imgMgmtMainPerspective.getName(), false);
        File gaussianFilePerspective = getFile(gaussianPerspective.getName(), false);
        File grabberFilePerspective = getFile(grabberPerspective.getName(), false);
        File advancedFilePerspective = getFile(advancedPerspective.getName(), false);
        File imageProfileFilePerspective = getFile(imageProfilePerspective.getName(), false);

        try {
            dockingManager.getPerspectiveFactory().loadFileInPerspective(filePerspective, perspective);
            dockingManager.getPerspectiveFactory().loadFileInPerspective(fileInnerPerspective, innerPerspective);
            dockingManager.getPerspectiveFactory().loadFileInPerspective(imgMgmtMainFilePerspective,
                    imgMgmtMainPerspective);
            dockingManager.getPerspectiveFactory().loadFileInPerspective(gaussianFilePerspective, gaussianPerspective);
            dockingManager.getPerspectiveFactory().loadFileInPerspective(grabberFilePerspective, grabberPerspective);
            dockingManager.getPerspectiveFactory().loadFileInPerspective(advancedFilePerspective, advancedPerspective);
            dockingManager.getPerspectiveFactory().loadFileInPerspective(imageProfileFilePerspective,
                    imageProfilePerspective);

        } catch (DockingException e1) {
            LOGGER.error(e1.getMessage());
        }
        applyPerspectives(perspective, innerPerspective, imgMgmtMainPerspective, gaussianPerspective,
                grabberPerspective, advancedPerspective, imageProfilePerspective);

    }

    protected void applyPerspectives(final IPerspective perspective, final IPerspective innerPerspective,
            final IPerspective imgMgmtMainPerspective, final IPerspective gaussianPerspective,
            final IPerspective grabberPerspective, final IPerspective advancedPerspective,
            final IPerspective imageProfilePerspective) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                doApplyPerspectives(perspective, innerPerspective, imgMgmtMainPerspective, gaussianPerspective,
                        grabberPerspective, advancedPerspective, imageProfilePerspective);
            }
        });
    }

    protected void doApplyPerspectives(final IPerspective perspective, final IPerspective innerPerspective,
            final IPerspective imgMgmtMainPerspective, IPerspective gaussianPerspective,
            IPerspective grabberPerspective, IPerspective advancedPerspective, IPerspective imageProfilePerspective) {
        try {
            dockingManager.applyPerspective(perspective, mainTabbedPane);
            dockingManager.applyPerspective(innerPerspective, imageSpectrumTabbedPane);
            dockingManager.applyPerspective(imgMgmtMainPerspective, imgBeamAnalyzerMainView.getMainSplitPane());
            dockingManager.applyPerspective(gaussianPerspective, imgBeamAnalyzerMainView.getGaussianSplitPane());
            dockingManager.applyPerspective(grabberPerspective, grabberBean.getMainView().getMainSplitPane());
            dockingManager.applyPerspective(advancedPerspective, getAdvancedSplitPane());
            dockingManager.applyPerspective(imageProfilePerspective,
                    imgBeamAnalyzerMainView.getRoiImageProfilePanel().getProfileTabbedPane());

        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Load default perspective
     * 
     * @throws IOException
     */
    public void loadDefaultPerspective() throws IOException {
        if ((dockingManager.getPerspectiveFactory() instanceof CharlestonPerspectiveFactory)) {
            try {
                switch (getMode()) {
                    case LIGHT_MODE:
                        defaultPerspective = new Perspective(CharlestonPerspectiveFactory.LIGHT_MODE_PERSPECTIVE);
                        dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                                CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                        .getFileName(CharlestonPerspectiveFactory.LIGHT_MODE_PERSPECTIVE),
                                defaultPerspective);
                        break;
                    case GRABBER_CONFIGURATION_MODE:
                        defaultPerspective = new Perspective(
                                CharlestonPerspectiveFactory.GRABBER_CONF_MODE_PERSPECTIVE);
                        dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                                CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                        .getFileName(CharlestonPerspectiveFactory.GRABBER_CONF_MODE_PERSPECTIVE),
                                defaultPerspective);
                        break;
                    case FULL_MODE:
                        defaultPerspective = new Perspective(CharlestonPerspectiveFactory.FULL_MODE_PERSPECTIVE);
                        dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                                CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                        .getFileName(CharlestonPerspectiveFactory.FULL_MODE_PERSPECTIVE),
                                defaultPerspective);
                        break;
                    default:
                        LOGGER.error("Wrong perspective!");
                }
                if (defaultPerspective != null) {
                    final Perspective defaultInnerPerspective = new Perspective(
                            CharlestonPerspectiveFactory.GENERAL_TAB_PERSPECTIVE);

                    dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                            CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                    .getFileName(CharlestonPerspectiveFactory.GENERAL_TAB_PERSPECTIVE),
                            defaultInnerPerspective);

                    final Perspective defaultGrabberPerspective = new Perspective(
                            CharlestonPerspectiveFactory.GRABBER_PERSPECTIVE);

                    dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                            CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                    .getFileName(CharlestonPerspectiveFactory.GRABBER_PERSPECTIVE),
                            defaultGrabberPerspective);

                    final Perspective defaultAdvancedPerspective = new Perspective(
                            CharlestonPerspectiveFactory.ADVANCED_PERSPECTIVE);

                    dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                            CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                    .getFileName(CharlestonPerspectiveFactory.ADVANCED_PERSPECTIVE),
                            defaultAdvancedPerspective);

                    final Perspective defaultImageMgmtMainPerspective = new Perspective(
                            CharlestonPerspectiveFactory.IMAGE_MGMT_MAIN_PERSPECTIVE);
                    dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                            CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                    .getFileName(CharlestonPerspectiveFactory.IMAGE_MGMT_MAIN_PERSPECTIVE),
                            defaultImageMgmtMainPerspective);

                    final Perspective defaultGaussianPerspective = new Perspective(
                            CharlestonPerspectiveFactory.GAUSSIAN_PERSPECTIVE);

                    dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                            CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                    .getFileName(CharlestonPerspectiveFactory.GAUSSIAN_PERSPECTIVE),
                            defaultGaussianPerspective);

                    final Perspective defaultImageProfilePerspective = new Perspective(
                            CharlestonPerspectiveFactory.IMAGE_PROFILE_PERSPECTIVE);

                    dockingManager.getPerspectiveFactory().loadResourceInPerspective(
                            CharlestonPerspectiveFactory.getPerspectivePackage() + CharlestonPerspectiveFactory
                                    .getFileName(CharlestonPerspectiveFactory.IMAGE_PROFILE_PERSPECTIVE),
                            defaultImageProfilePerspective);

                    applyPerspectives(defaultPerspective, defaultInnerPerspective, defaultImageMgmtMainPerspective,
                            defaultGaussianPerspective, defaultGrabberPerspective, defaultAdvancedPerspective,
                            defaultImageProfilePerspective);

                } else {
                    LOGGER.error("No default perspective defined");
                }
            } catch (DockingException e1) {
                LOGGER.error(e1.getMessage());
            }
        }
    }

    /**
     * Save selected perspective
     */
    public void saveSelectedPerspective() {

        Preferences prefs = Preferences.userNodeForPackage(getClass());
        CharlestonPerspectiveFactory charlestonPerspectiveFactory = (CharlestonPerspectiveFactory) dockingManager
                .getPerspectiveFactory();

        try {
            dockingManager.savePreferences(prefs);
            String name = dockingManager.getPerspectiveFactory().getSelectedPerspective().getName();
            IPerspective perspective = charlestonPerspectiveFactory.getPerspective(name);
            File file = getFile(name, false);
            dockingManager.updatePerspective(perspective, mainTabbedPane);
            dockingManager.getPerspectiveFactory().saveSelected(file);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        try {
            String name = CharlestonPerspectiveFactory.GENERAL_TAB_PERSPECTIVE;
            File file = getFile(name, false);
            dockingManager.updatePerspective(charlestonPerspectiveFactory.getInnerPerspective(),
                    imageSpectrumTabbedPane);
            dockingManager.getPerspectiveFactory().savePerspective(file,
                    charlestonPerspectiveFactory.getInnerPerspective());
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }

        try {
            String name = CharlestonPerspectiveFactory.GRABBER_PERSPECTIVE;
            File file = getFile(name, false);
            dockingManager.updatePerspective(charlestonPerspectiveFactory.getGrabberPerspective(),
                    grabberBean.getMainView().getMainSplitPane());
            charlestonPerspectiveFactory.savePerspective(file, charlestonPerspectiveFactory.getGrabberPerspective());
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }

        try {
            String name = CharlestonPerspectiveFactory.ADVANCED_PERSPECTIVE;
            File file = getFile(name, false);
            dockingManager.updatePerspective(charlestonPerspectiveFactory.getAdvancedPerspective(),
                    getAdvancedSplitPane());
            dockingManager.getPerspectiveFactory().savePerspective(file,
                    charlestonPerspectiveFactory.getAdvancedPerspective());
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }

        try {
            String name = CharlestonPerspectiveFactory.IMAGE_PROFILE_PERSPECTIVE;
            File file = getFile(name, false);
            dockingManager.updatePerspective(charlestonPerspectiveFactory.getImageProfilePerspective(),
                    imgBeamAnalyzerMainView.getRoiImageProfilePanel().getProfileTabbedPane());
            dockingManager.getPerspectiveFactory().savePerspective(file,
                    charlestonPerspectiveFactory.getImageProfilePerspective());
        } catch (DockingException e) {
            LOGGER.error(e.getMessage());
        }

        if (imgBeamAnalyzerMainView != null) {
            imgBeamAnalyzerMainView.saveSelectedPerspective(this);
        }

    }

    /**
     * Returns docking files directory
     * 
     * @param isDefault boolean
     * @return String
     */
    public String getDockingDirectory(boolean isDefault) {
        String dockingDirectory = defaultDockingDirectory;
        if (isDefault) {
            dockingDirectory = dockingDirectory + File.separator + DEFAULT;
        }
        File dir = new File(dockingDirectory);
        if (dir.exists() && (!dir.isDirectory())) {
            if (new File(defaultDockingDirectory).getAbsolutePath().equals(dir.getAbsolutePath())) {
                dir = new File(System.getProperty("user.home"));
            } else {
                dir = new File(defaultDockingDirectory);
            }
        }
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dockingDirectory;
    }

    /**
     * Returns the file associated with a given perspective
     * 
     * @param name String
     * @param isDefault boolean
     * @return File
     */
    public File getFile(String name, boolean isDefault) {
        File file = null;
        try {
            String fileName = CharlestonPerspectiveFactory.getFileName(name);
            if (dockingType == DockingType.VL) {
                fileName = fileName.replaceAll("dock", "vldock");
            }
            file = new File(getDockingDirectory(isDefault) + File.separator + fileName);
            if (!file.exists()) {
                FileUtils.duplicateFile(
                        getClass().getResourceAsStream(CharlestonPerspectiveFactory.getPerspectivePackage() + fileName),
                        file, false);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return file;
    }

    /**
     * Retrieve ChartProperties object from chart properties xml file
     * 
     * @param file
     * @return ChartProperties
     */
    public static ChartProperties getChartProperties(File file) {
        StringBuilder fileData = new StringBuilder();
        ChartProperties chartProperties = null;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);

            }
            reader.close();
            String xmlString = fileData.toString();

            chartProperties = ChartPropertiesXmlManager
                    .loadChartProperties(XMLUtils.getRootNodeFromFileContent(xmlString));

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (XMLWarning e) {
            LOGGER.error(e.getMessage());
        }
        return chartProperties;
    }

    /**
     * Retrieve ImageProperties object from image properties xml file
     * 
     * @param file
     * @return ImageProperties
     */
    public static ImageProperties getImageProperties(File file) {
        StringBuilder fileData = new StringBuilder();
        ImageProperties imageProperties = null;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);

            }
            reader.close();
            String xmlString = fileData.toString();

            imageProperties = ImagePropertiesXmlManager
                    .loadImageProperties(XMLUtils.getRootNodeFromFileContent(xmlString));

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (XMLWarning e) {
            LOGGER.error(e.getMessage());
        }
        return imageProperties;
    }

    /**
     * Returns property file
     * 
     * @param properties directory
     * @param propertiesFileName
     * @return File
     */
    public static File getPropertiesFile(String directory, String propertiesFileName) {
        File file = null;
        try {
            file = new File(directory + File.separator + propertiesFileName);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return file;
    }

    /**
     * Returns properties directory name
     * 
     * @return String
     */
    public static String getPropertiesDirectoryName() {
        String directory = defaultPropertiesDirectory;
        File dir = new File(defaultPropertiesDirectory);
        if (dir.exists() && (!dir.isDirectory())) {
            if (new File(defaultPropertiesDirectory).getAbsolutePath().equals(dir.getAbsolutePath())) {
                dir = new File(System.getProperty("user.home"));
            } else {
                dir = new File(defaultPropertiesDirectory);
            }
        }
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return directory;
    }

    public JPanel getGlobalStatusPanel() {
        if (globalStatusPanel == null) {
            globalStatusPanel = new JPanel(new GridBagLayout());
            getStatusPanel().setBorder(new LineBorder(Color.BLUE, 2));
            GridBagConstraints status1Constraints = new GridBagConstraints();
            status1Constraints.fill = GridBagConstraints.BOTH;
            status1Constraints.gridx = 0;
            status1Constraints.gridy = 0;
            status1Constraints.weightx = 0.5;
            status1Constraints.weighty = 1;
            globalStatusPanel.add(getStatusPanel(), status1Constraints);
            grabberBean.getStatusPanel().setBorder(new LineBorder(Color.RED, 2));
            GridBagConstraints status2Constraints = new GridBagConstraints();
            status2Constraints.fill = GridBagConstraints.BOTH;
            status2Constraints.gridx = 1;
            status2Constraints.gridy = 0;
            status2Constraints.weightx = 0.5;
            status2Constraints.weighty = 1;
            globalStatusPanel.add(grabberBean.getStatusPanel(), status2Constraints);
        }
        return globalStatusPanel;
    }

    public ImgGrabber getGrabber() {
        return grabberBean;
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    public ImgBeamAnalyzerMainView getImgBeamAnalyzerMainView() {
        return imgBeamAnalyzerMainView;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * ImgGrabber compatible with tab selection dependant model management
     * 
     * @author GIRARDOT
     */
    protected class AdvancedImgGrabber extends ImgGrabber {

        private static final long serialVersionUID = 6117473285997015149L;

        public AdvancedImgGrabber() {
            super(dockingManager);
        }

        @Override
        protected void refreshGUI() {
            initAttributes();
            initCommands();
            displayBadAttributesAndCommands();
//            if (ImgBeamAnalyzer.this.getMainTabbedPane() == null) {
            clearModels(true);
//            } else {
            switch (getMode()) {
                case LIGHT_MODE: {
                    setLightModels(true);
                }
                    break;
                case GRABBER_CONFIGURATION_MODE: {
                    setLightModels(true);
                    setMainModels(true);
                }
                    break;
                case FULL_MODE: {
                    setLightModels(true);
                    setMainModels(true);
                    setAdvancedModels(true);
                }
                    break;
                default: {

                }
            }
            // if (generalTitle.equals(title)) {
            // setLightModels(true);
            // clearMainModels(true);
            // clearAdvancedModels(true);
            // } else if (grabberTitle.equals(title)) {
            // setMainModels(true);
            // clearLightModels(true);
            // clearAdvancedModels(true);
            // } else if (advancedTitle.equals(title)) {
            // setAdvancedModels(true);
            // clearLightModels(true);
            // clearMainModels(true);
            // } else {
            // clearModels(true);
            // }
            // }
            setStatusModel();
        }

        @Override
        public void setModel(String model) {
            super.setModel(model);
            if (getModel().isEmpty()) {
                fireProgressionChanged(getMaxProgression(), "No grabber device");
            }
        }
    }

    protected class GrabberReader implements ITextTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public String getText() {
            return grabberBean == null ? null : grabberBean.getModel();
        }

        @Override
        public void setText(String text) {
            if (grabberBean != null) {
//                grabberBean.clearModels(true);
//
                grabberBean.setModel(text);
            }
        }

    }

    public AbstractAction getGrabberAdvancedViewAction() {
        return grabberAdvancedViewAction;
    }

}
