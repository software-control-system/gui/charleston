package fr.soleil.bean.imggrabber.view;

public class ImgGrabberLightView extends ImgGrabberAbstractView {

    public ImgGrabberLightView() {
        super();
    }

    @Override
    protected void initSetROI() {
        // nothing to do: no command in light view
    }

}
