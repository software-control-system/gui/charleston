package fr.soleil.bean.imggrabber.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.bean.view.AbstractView;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.WheelSwitch;

public abstract class ImgGrabberAbstractView extends AbstractView {

    protected final static Border GRABBER_BORDER = new LineBorder(Color.RED, 2);

    // Viewers and labels
    protected JLabel activeChannelLabel;
    protected JLabel exposureTimeLabel;
    protected Label activeChannel;
    protected Label exposureTime;

    // Editors
    protected WheelSwitch activeChannel2;
    protected WheelSwitch exposureTime2;

    // Command buttons
    protected StringButton setROI;
    protected StringButton resetROI;

    // Panels
    protected JPanel mainPanel;
    protected JPanel commandsPanel;
    protected JPanel attributesPanel;

    // Look and Feel
    protected Dimension viewerPreferredSize = new Dimension(40, 40);

    public ImgGrabberAbstractView() {
        super();
    }

    public JPanel getMainPanel() {
        if (mainPanel == null) {
            mainPanel = new JPanel();
            layoutMainPanel();
        }
        return mainPanel;
    }

    protected void layoutMainPanel() {
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(getAttributesPanel(), BorderLayout.CENTER);
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.RED));
    }

    public JPanel getAttributesPanel() {
        if (attributesPanel == null) {
            attributesPanel = new JPanel();
            layoutAttributesPanel();
        }
        return attributesPanel;
    }

    protected void layoutAttributesPanel() {
        // Constraints
        GridBagConstraints activeChanelLabelConstraints = new GridBagConstraints();
        activeChanelLabelConstraints.fill = GridBagConstraints.NONE;
        activeChanelLabelConstraints.gridx = 0;
        activeChanelLabelConstraints.gridy = 0;
        activeChanelLabelConstraints.weightx = 0;
        activeChanelLabelConstraints.weighty = 0;
        activeChanelLabelConstraints.insets = SPACE;
        GridBagConstraints activeChannelConstraints = new GridBagConstraints();
        activeChannelConstraints.fill = GridBagConstraints.HORIZONTAL;
        activeChannelConstraints.gridx = 1;
        activeChannelConstraints.gridy = 0;
        activeChannelConstraints.weightx = 1;
        activeChannelConstraints.weighty = 0;
        activeChannelConstraints.insets = SPACE;
        activeChannelConstraints.anchor = GridBagConstraints.WEST;
        GridBagConstraints activeChannel2Constraints = new GridBagConstraints();
        activeChannel2Constraints.fill = GridBagConstraints.NONE;
        activeChannel2Constraints.gridx = 2;
        activeChannel2Constraints.gridy = 0;
        activeChannel2Constraints.weightx = 0;
        activeChannel2Constraints.weighty = 0;
        activeChannel2Constraints.insets = SPACE;
        activeChannel2Constraints.anchor = GridBagConstraints.WEST;

        GridBagConstraints exposureTimeLabelConstraints = new GridBagConstraints();
        exposureTimeLabelConstraints.fill = GridBagConstraints.NONE;
        exposureTimeLabelConstraints.gridx = 0;
        exposureTimeLabelConstraints.gridy = 1;
        exposureTimeLabelConstraints.weightx = 0;
        exposureTimeLabelConstraints.weighty = 0;
        exposureTimeLabelConstraints.insets = SPACE;
        GridBagConstraints exposureTimeConstraints = new GridBagConstraints();
        exposureTimeConstraints.fill = GridBagConstraints.HORIZONTAL;
        exposureTimeConstraints.gridx = 1;
        exposureTimeConstraints.gridy = 1;
        exposureTimeConstraints.weightx = 1;
        exposureTimeConstraints.weighty = 0;
        exposureTimeConstraints.insets = SPACE;
        GridBagConstraints exposureTime2Constraints = new GridBagConstraints();
        exposureTime2Constraints.fill = GridBagConstraints.NONE;
        exposureTime2Constraints.gridx = 2;
        exposureTime2Constraints.gridy = 1;
        exposureTime2Constraints.weightx = 0;
        exposureTime2Constraints.weighty = 0;
        exposureTime2Constraints.insets = SPACE;

        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = 2;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        glueConstraints.gridwidth = GridBagConstraints.REMAINDER;
        glueConstraints.gridheight = GridBagConstraints.REMAINDER;
        glueConstraints.insets = SPACE;
        // END Constraints

        attributesPanel.setLayout(new GridBagLayout());

        attributesPanel.add(getActiveChannelLabel(), activeChanelLabelConstraints);
        attributesPanel.add(getActiveChannel(), activeChannelConstraints);
        attributesPanel.add(getActiveChannel2(), activeChannel2Constraints);

        attributesPanel.add(getExposureTimeLabel(), exposureTimeLabelConstraints);
        attributesPanel.add(getExposureTime(), exposureTimeConstraints);
        attributesPanel.add(getExposureTime2(), exposureTime2Constraints);

        attributesPanel.add(Box.createGlue(), glueConstraints);

        attributesPanel.setBorder(new TitledBorder("Scalar Attributes"));
    }

    public JPanel getCommandsPanel() {
        if (commandsPanel == null) {
            commandsPanel = new JPanel();
            layoutCommandsPanel();
        }
        return commandsPanel;
    }

    protected void layoutCommandsPanel() {
        commandsPanel.setBorder(new TitledBorder("Commands"));
    }

    public Label getActiveChannel() {
        if (activeChannel == null) {
            activeChannel = generateLabel();
        }
        return activeChannel;
    }

    public WheelSwitch getActiveChannel2() {
        if (activeChannel2 == null) {
            activeChannel2 = generateWheelSwitch();
        }
        return activeChannel2;
    }

    public Label getExposureTime() {
        if (exposureTime == null) {
            exposureTime = generateLabel();
        }
        return exposureTime;
    }

    public WheelSwitch getExposureTime2() {
        if (exposureTime2 == null) {
            exposureTime2 = generateWheelSwitch();
        }
        return exposureTime2;
    }

    public JLabel getActiveChannelLabel() {
        if (activeChannelLabel == null) {
            activeChannelLabel = new JLabel("activeChannel", JLabel.LEFT);
        }
        return activeChannelLabel;
    }

    public JLabel getExposureTimeLabel() {
        if (exposureTimeLabel == null) {
            exposureTimeLabel = new JLabel("exposureTime", JLabel.LEFT);
        }
        return exposureTimeLabel;
    }

    public StringButton getSetROI() {
        if (setROI == null) {
            initSetROI();
        }
        return setROI;
    }

    protected abstract void initSetROI();

    public StringButton getResetROI() {
        if (resetROI == null) {
            resetROI = generateStringButton();
            resetROI.setText("Reset ROI");
        }
        return resetROI;
    }

}
