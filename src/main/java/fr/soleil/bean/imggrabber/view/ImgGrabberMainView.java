package fr.soleil.bean.imggrabber.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.action.ActionContainerFactory;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.bean.imgbeamanalyzer.view.ByPeriodChangeRefreshingPeriodAction;
import fr.soleil.bean.imggrabber.utils.SetRoiButton;
import fr.soleil.bean.utils.CharlestonImageViewer;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.util.DockingUtils;
import fr.soleil.docking.view.IView;
import fr.soleil.lib.project.swing.text.DocumentNumber;

public class ImgGrabberMainView extends ImgGrabberAbstractView {

    // Commands
    // TODO use getSetROI() as soon as a solution is found in Comete for the double commands use
    protected SetRoiButton setROI2;
    protected StringButton saveSettings;
    protected JLabel xLabel;
    protected JTextField xTextField;
    protected JLabel yLabel;
    protected JTextField yTextField;
    protected JLabel widthLabel;
    protected JTextField widthTextField;
    protected JLabel heightLabel;
    protected JTextField heightTextField;

    // Initialisation de l'attribut image
    protected CharlestonImageViewer imageViewer;
    protected JComponent mainSplitPane;
    protected NumberMatrixBox imageBox;
    /**
     * Change refreshing period action JIRA CONTROLGUI-12
     */
    private final ChangeRefreshingPeriodAction changeRefreshingPeriodAction = new ByPeriodChangeRefreshingPeriodAction();
    private final ActionContainerFactory containerFactory;
    private AbstractAction grabberImageViewerViewAction;
    private AbstractAction mainPanelViewAction;

    private final ADockingManager dockingManager;

    public ImgGrabberMainView(ADockingManager dockingManager) {
        super();
        this.dockingManager = dockingManager;
        imageBox = new NumberMatrixBox();
        containerFactory = new ActionContainerFactory();
    }

    @Override
    protected void layoutMainPanel() {
        super.layoutMainPanel();
        mainPanel.add(getCommandsPanel(), BorderLayout.NORTH);
        mainPanel.setBorder(null);
    }

    public JComponent getMainSplitPane() {
        if (mainSplitPane == null) {

            mainSplitPane = dockingManager.createNewDockingArea(Color.GRAY);

            JComponent inputImagePanel = new JPanel();
            inputImagePanel.setLayout(new GridBagLayout());

            GridBagConstraints viewerConstraints = new GridBagConstraints();
            viewerConstraints.fill = GridBagConstraints.BOTH;
            viewerConstraints.gridx = 0;
            viewerConstraints.gridy = 0;
            viewerConstraints.weightx = 1;
            viewerConstraints.weighty = 1;

            inputImagePanel.add(getImageViewer(), viewerConstraints);

            IView grabberImageViewerView = dockingManager.getViewFactory().addView("Grabber Image", null,
                    inputImagePanel, "grabberImageViewer", mainSplitPane);
            grabberImageViewerViewAction = DockingUtils.generateShowViewAction(grabberImageViewerView);

            IView mainPanelView = dockingManager.getViewFactory().addView("Grabber Panel", null,
                    new JScrollPane(getMainPanel()), "mainPanelView", mainSplitPane);
            mainPanelViewAction = DockingUtils.generateShowViewAction(mainPanelView);

            mainSplitPane.setBorder(GRABBER_BORDER);
        }
        return mainSplitPane;
    }

    @Override
    public void layoutCommandsPanel() {
        super.layoutCommandsPanel();

        Insets gap1 = new Insets(5, 5, 5, 5);
        Insets gap2 = new Insets(0, 5, 5, 0);
        Insets gap3 = new Insets(0, 0, 5, 5);
        GridBagLayout commandLayout = new GridBagLayout();
        commandsPanel.setLayout(commandLayout);

        GridBagConstraints fullROIConstraints = new GridBagConstraints();
        fullROIConstraints.fill = GridBagConstraints.HORIZONTAL;
        fullROIConstraints.gridx = 0;
        fullROIConstraints.gridy = 0;
        fullROIConstraints.weightx = 1;
        fullROIConstraints.insets = gap1;
        fullROIConstraints.gridwidth = GridBagConstraints.REMAINDER;

        GridBagConstraints setROIConstraints = new GridBagConstraints();
        setROIConstraints.fill = GridBagConstraints.HORIZONTAL;
        setROIConstraints.gridx = 0;
        setROIConstraints.gridy = 1;
        setROIConstraints.weightx = 1;
        setROIConstraints.insets = gap1;
        setROIConstraints.gridwidth = GridBagConstraints.REMAINDER;

        GridBagConstraints saveSettingsConstraints = new GridBagConstraints();
        saveSettingsConstraints.fill = GridBagConstraints.HORIZONTAL;
        saveSettingsConstraints.gridx = 0;
        saveSettingsConstraints.gridy = 2;
        saveSettingsConstraints.weightx = 1;
        saveSettingsConstraints.insets = gap1;
        saveSettingsConstraints.gridwidth = GridBagConstraints.REMAINDER;

        GridBagConstraints xLabelConstraints = new GridBagConstraints();
        xLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        xLabelConstraints.gridx = 0;
        xLabelConstraints.gridy = 3;
        xLabelConstraints.weightx = 0;
        xLabelConstraints.insets = gap2;
        GridBagConstraints xTextFieldConstraints = new GridBagConstraints();
        xTextFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        xTextFieldConstraints.gridx = 1;
        xTextFieldConstraints.gridy = 3;
        xTextFieldConstraints.weightx = 0.5;
        xTextFieldConstraints.insets = gap3;

        GridBagConstraints yLabelConstraints = new GridBagConstraints();
        yLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        yLabelConstraints.gridx = 2;
        yLabelConstraints.gridy = 3;
        yLabelConstraints.weightx = 0;
        yLabelConstraints.insets = gap2;
        GridBagConstraints yTextFieldConstraints = new GridBagConstraints();
        yTextFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        yTextFieldConstraints.gridx = 3;
        yTextFieldConstraints.gridy = 3;
        yTextFieldConstraints.weightx = 0.5;
        yTextFieldConstraints.insets = gap3;

        GridBagConstraints widthLabelConstraints = new GridBagConstraints();
        widthLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        widthLabelConstraints.gridx = 0;
        widthLabelConstraints.gridy = 4;
        widthLabelConstraints.weightx = 0;
        widthLabelConstraints.insets = gap2;
        GridBagConstraints widthTextFieldConstraints = new GridBagConstraints();
        widthTextFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        widthTextFieldConstraints.gridx = 1;
        widthTextFieldConstraints.gridy = 4;
        widthTextFieldConstraints.weightx = 0.5;
        widthTextFieldConstraints.insets = gap3;

        GridBagConstraints heightLabelConstraints = new GridBagConstraints();
        heightLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        heightLabelConstraints.gridx = 2;
        heightLabelConstraints.gridy = 4;
        heightLabelConstraints.weightx = 0;
        heightLabelConstraints.insets = gap2;
        GridBagConstraints heightTextFieldConstraints = new GridBagConstraints();
        heightTextFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        heightTextFieldConstraints.gridx = 3;
        heightTextFieldConstraints.gridy = 4;
        heightTextFieldConstraints.weightx = 0.5;
        heightTextFieldConstraints.insets = gap3;

        commandsPanel.add(getResetROI(), fullROIConstraints);
        // TODO use getSetROI() as soon as a solution is found in Comete for the double commands use
        commandsPanel.add(getSetROI2(), setROIConstraints);
        commandsPanel.add(getSaveSettings(), saveSettingsConstraints);
        commandsPanel.add(getXLabel(), xLabelConstraints);
        commandsPanel.add(getXTextField(), xTextFieldConstraints);
        commandsPanel.add(getYLabel(), yLabelConstraints);
        commandsPanel.add(getYTextField(), yTextFieldConstraints);

        commandsPanel.add(getWidthLabel(), widthLabelConstraints);
        commandsPanel.add(getWidthTextField(), widthTextFieldConstraints);
        commandsPanel.add(getHeightLabel(), heightLabelConstraints);
        commandsPanel.add(getHeightTextField(), heightTextFieldConstraints);
    }

    @Override
    protected void initSetROI() {
        setROI = generateStringButton();
    }

    // TODO use getSetROI() as soon as a solution is found in Comete for the double commands use
    public SetRoiButton getSetROI2() {
        if (setROI2 == null) {
            setROI2 = new SetRoiButton();
            setROI2.setText("SetROI");
            setROI2.setXTextField(getXTextField());
            setROI2.setYTextField(getYTextField());
            setROI2.setWidthTextField(getWidthTextField());
            setROI2.setHeightTextField(getHeightTextField());
        }
        return setROI2;
    }

    public StringMatrixComboBoxViewer getGetRoi() {
        return getSetROI2().getCurrentRoiViewer();
    }

    public StringButton getSaveSettings() {
        if (saveSettings == null) {
            saveSettings = generateStringButton();
            saveSettings.setText("Save Settings");
        }
        return saveSettings;
    }

    @Override
    public StringButton getResetROI() {
        if (resetROI == null) {
            resetROI = generateStringButton();
            resetROI.setText("Reset ROI");
        }
        return resetROI;
    }

    /**
     * @return the xTextField
     */
    public JTextField getXTextField() {
        if (xTextField == null) {
            DocumentNumber xFormater = new DocumentNumber();
            xTextField = new JTextField(xFormater, "", 0);
        }
        return xTextField;
    }

    /**
     * @return the yTextField
     */
    public JTextField getYTextField() {
        if (yTextField == null) {
            DocumentNumber yFormater = new DocumentNumber();
            yTextField = new JTextField(yFormater, "", 0);
        }
        return yTextField;
    }

    /**
     * @return the widthTextField
     */
    public JTextField getWidthTextField() {
        if (widthTextField == null) {
            DocumentNumber widthFormater = new DocumentNumber();
            widthTextField = new JTextField(widthFormater, "", 0);
        }
        return widthTextField;
    }

    /**
     * @return the heightTextField
     */
    public JTextField getHeightTextField() {
        if (heightTextField == null) {
            DocumentNumber heightFormater = new DocumentNumber();
            heightTextField = new JTextField(heightFormater, "", 0);
        }
        return heightTextField;
    }

    public JLabel getXLabel() {
        if (xLabel == null) {
            xLabel = new JLabel("ROI x", JLabel.CENTER);
        }
        return xLabel;
    }

    public JLabel getYLabel() {
        if (yLabel == null) {
            yLabel = new JLabel("ROI y", JLabel.CENTER);
        }
        return yLabel;
    }

    public JLabel getWidthLabel() {
        if (widthLabel == null) {
            widthLabel = new JLabel("ROI width", JLabel.CENTER);
        }
        return widthLabel;
    }

    public JLabel getHeightLabel() {
        if (heightLabel == null) {
            heightLabel = new JLabel("ROI height", JLabel.CENTER);
        }
        return heightLabel;
    }

    public CharlestonImageViewer getImageViewer() {
        if (imageViewer == null) {
            imageViewer = new CharlestonImageViewer();

            // JIRA CONTROLGUI-14
            String propertiesDirectory = ImgBeamAnalyzer.getPropertiesDirectoryName();
            File file = ImgBeamAnalyzer
                    .getPropertiesFile(propertiesDirectory, ImgBeamAnalyzer.GRABBER_IMAGE_PROPERTIES);
            if (file.exists()) {
                ImageProperties imageProperties = ImgBeamAnalyzer.getImageProperties(file);
                imageViewer.setImageProperties(imageProperties);
            }
            imageViewer.setVisible(true);
            ((JComponent) imageViewer).setBorder(new TitledBorder("Image"));
            // Add refreshing period action popup menu JIRA CONTROLGUI-12
            JMenuItem menuItem = containerFactory.createMenuItem(changeRefreshingPeriodAction);
            imageViewer.getPopupMenu().add(menuItem);
        }
        return imageViewer;
    }

    public void setImage(TangoKey image) {
        imageBox.disconnectWidgetFromAll(imageViewer);
        if (image != null) {
            // JIRA CONTROLGUI-12
            changeRefreshingPeriodAction.setKey(image);
            imageBox.connectWidget(imageViewer, image);
            imageViewer.setAlwaysFitMaxSize(true);
        }
    }

    @Override
    public JLabel getActiveChannelLabel() {
        if (activeChannelLabel == null) {
            activeChannelLabel = new JLabel("activeChannel", JLabel.LEFT);
            activeChannelLabel.setMaximumSize(activeChannelLabel.getPreferredSize());
        }
        return activeChannelLabel;
    }

    @Override
    public JLabel getExposureTimeLabel() {
        if (exposureTimeLabel == null) {
            exposureTimeLabel = new JLabel("exposureTime", JLabel.LEFT);
            exposureTimeLabel.setMaximumSize(exposureTimeLabel.getPreferredSize());
        }
        return exposureTimeLabel;
    }

    public AbstractAction getGrabberImageViewerViewAction() {
        return grabberImageViewerViewAction;
    }

    public AbstractAction getMainPanelViewAction() {
        return mainPanelViewAction;
    }

}
