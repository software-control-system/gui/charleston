package fr.soleil.bean.imggrabber.view;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.util.DockingUtils;
import fr.soleil.docking.view.IView;

public class ImgGrabberAdvancedView extends ImgGrabberAbstractView {

    // Attributes
    protected JLabel modeLabel;
    protected Label mode;
    protected TextField mode2;
    protected ComboBox mode3;
    GridBagConstraints mode2Constraints;
    protected JLabel blackLevelLabel;
    protected Label blackLevel;
    protected WheelSwitch blackLevel2;
    protected JLabel whiteLevelLabel;
    protected Label whiteLevel;
    protected WheelSwitch whiteLevel2;
    protected JLabel gainLabel;
    protected Label gain;
    protected WheelSwitch gain2;
    protected JLabel triggerModeLabel;
    protected Label triggerMode;
    protected TextField triggerMode2;
    protected ComboBox triggerMode3;
    GridBagConstraints triggerMode2Constraints;
    protected JLabel triggerLineLabel;
    protected Label triggerLine;
    protected WheelSwitch triggerLine2;
    protected JLabel triggerActivationLabel;
    protected Label triggerActivation;
    protected WheelSwitch triggerActivation2;

    // Commands
    protected StringButton init;
    protected StringButton state;
    protected StringButton status;
    protected StringButton open;
    protected StringButton close;
    protected StringButton start;
    protected StringButton stop;
    protected StringButton setBinning;
    protected StringButton snap;
    private AbstractAction attributesViewAction;
    private AbstractAction commandsViewAction;

    public ImgGrabberAdvancedView() {
        super();
    }

    @Override
    protected void layoutAttributesPanel() {
        super.layoutAttributesPanel();
        attributesPanel.removeAll();

        // Constraints
        int line = 0;

        GridBagConstraints modeLabelConstraints = new GridBagConstraints();
        modeLabelConstraints.fill = GridBagConstraints.NONE;
        modeLabelConstraints.gridx = 0;
        modeLabelConstraints.gridy = line;
        modeLabelConstraints.weightx = 0;
        modeLabelConstraints.weighty = 0;
        modeLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints modeConstraints = new GridBagConstraints();
        modeConstraints.fill = GridBagConstraints.HORIZONTAL;
        modeConstraints.gridx = 1;
        modeConstraints.gridy = line;
        modeConstraints.weightx = 1;
        modeConstraints.weighty = 0;
        modeConstraints.insets = CometeUtils.getzInset();
        mode2Constraints = new GridBagConstraints();
        mode2Constraints.fill = GridBagConstraints.NONE;
        mode2Constraints.gridx = 2;
        mode2Constraints.gridy = line++;
        mode2Constraints.weightx = 0;
        mode2Constraints.weighty = 0;
        mode2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints activeChanelLabelConstraints = new GridBagConstraints();
        activeChanelLabelConstraints.fill = GridBagConstraints.NONE;
        activeChanelLabelConstraints.gridx = 0;
        activeChanelLabelConstraints.gridy = line;
        activeChanelLabelConstraints.weightx = 0;
        activeChanelLabelConstraints.weighty = 0;
        activeChanelLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints activeChannelConstraints = new GridBagConstraints();
        activeChannelConstraints.fill = GridBagConstraints.HORIZONTAL;
        activeChannelConstraints.gridx = 1;
        activeChannelConstraints.gridy = line;
        activeChannelConstraints.weightx = 1;
        activeChannelConstraints.weighty = 0;
        activeChannelConstraints.insets = CometeUtils.getzInset();
        activeChannelConstraints.anchor = GridBagConstraints.WEST;
        GridBagConstraints activeChannel2Constraints = new GridBagConstraints();
        activeChannel2Constraints.fill = GridBagConstraints.NONE;
        activeChannel2Constraints.gridx = 2;
        activeChannel2Constraints.gridy = line++;
        activeChannel2Constraints.weightx = 0;
        activeChannel2Constraints.weighty = 0;
        activeChannel2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints exposureTimeLabelConstraints = new GridBagConstraints();
        exposureTimeLabelConstraints.fill = GridBagConstraints.NONE;
        exposureTimeLabelConstraints.gridx = 0;
        exposureTimeLabelConstraints.gridy = line;
        exposureTimeLabelConstraints.weightx = 0;
        exposureTimeLabelConstraints.weighty = 0;
        exposureTimeLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints exposureTimeConstraints = new GridBagConstraints();
        exposureTimeConstraints.fill = GridBagConstraints.HORIZONTAL;
        exposureTimeConstraints.gridx = 1;
        exposureTimeConstraints.gridy = line;
        exposureTimeConstraints.weightx = 1;
        exposureTimeConstraints.weighty = 0;
        exposureTimeConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints exposureTime2Constraints = new GridBagConstraints();
        exposureTime2Constraints.fill = GridBagConstraints.NONE;
        exposureTime2Constraints.gridx = 2;
        exposureTime2Constraints.gridy = line++;
        exposureTime2Constraints.weightx = 0;
        exposureTime2Constraints.weighty = 0;
        exposureTime2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints blackLevelLabelConstraints = new GridBagConstraints();
        blackLevelLabelConstraints.fill = GridBagConstraints.NONE;
        blackLevelLabelConstraints.gridx = 0;
        blackLevelLabelConstraints.gridy = line;
        blackLevelLabelConstraints.weightx = 0;
        blackLevelLabelConstraints.weighty = 0;
        blackLevelLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints blackLevelConstraints = new GridBagConstraints();
        blackLevelConstraints.fill = GridBagConstraints.HORIZONTAL;
        blackLevelConstraints.gridx = 1;
        blackLevelConstraints.gridy = line;
        blackLevelConstraints.weightx = 1;
        blackLevelConstraints.weighty = 0;
        blackLevelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints blackLevel2Constraints = new GridBagConstraints();
        blackLevel2Constraints.fill = GridBagConstraints.NONE;
        blackLevel2Constraints.gridx = 2;
        blackLevel2Constraints.gridy = line++;
        blackLevel2Constraints.weightx = 0;
        blackLevel2Constraints.weighty = 0;
        blackLevel2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints whiteLevelLabelConstraints = new GridBagConstraints();
        whiteLevelLabelConstraints.fill = GridBagConstraints.NONE;
        whiteLevelLabelConstraints.gridx = 0;
        whiteLevelLabelConstraints.gridy = line;
        whiteLevelLabelConstraints.weightx = 0;
        whiteLevelLabelConstraints.weighty = 0;
        whiteLevelLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints whiteLevelConstraints = new GridBagConstraints();
        whiteLevelConstraints.fill = GridBagConstraints.HORIZONTAL;
        whiteLevelConstraints.gridx = 1;
        whiteLevelConstraints.gridy = line;
        whiteLevelConstraints.weightx = 1;
        whiteLevelConstraints.weighty = 0;
        whiteLevelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints whiteLevel2Constraints = new GridBagConstraints();
        whiteLevel2Constraints.fill = GridBagConstraints.NONE;
        whiteLevel2Constraints.gridx = 2;
        whiteLevel2Constraints.gridy = line++;
        whiteLevel2Constraints.weightx = 0;
        whiteLevel2Constraints.weighty = 0;
        whiteLevel2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints gainLabelConstraints = new GridBagConstraints();
        gainLabelConstraints.fill = GridBagConstraints.NONE;
        gainLabelConstraints.gridx = 0;
        gainLabelConstraints.gridy = line;
        gainLabelConstraints.weightx = 0;
        gainLabelConstraints.weighty = 0;
        gainLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints gainConstraints = new GridBagConstraints();
        gainConstraints.fill = GridBagConstraints.HORIZONTAL;
        gainConstraints.gridx = 1;
        gainConstraints.gridy = line;
        gainConstraints.weightx = 1;
        gainConstraints.weighty = 0;
        gainConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints gain2Constraints = new GridBagConstraints();
        gain2Constraints.fill = GridBagConstraints.NONE;
        gain2Constraints.gridx = 2;
        gain2Constraints.gridy = line++;
        gain2Constraints.weightx = 0;
        gain2Constraints.weighty = 0;
        gain2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints triggerModeLabelConstraints = new GridBagConstraints();
        triggerModeLabelConstraints.fill = GridBagConstraints.NONE;
        triggerModeLabelConstraints.gridx = 0;
        triggerModeLabelConstraints.gridy = line;
        triggerModeLabelConstraints.weightx = 0;
        triggerModeLabelConstraints.weighty = 0;
        triggerModeLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints triggerModeConstraints = new GridBagConstraints();
        triggerModeConstraints.fill = GridBagConstraints.HORIZONTAL;
        triggerModeConstraints.gridx = 1;
        triggerModeConstraints.gridy = line;
        triggerModeConstraints.weightx = 1;
        triggerModeConstraints.weighty = 0;
        triggerModeConstraints.insets = CometeUtils.getzInset();
        triggerMode2Constraints = new GridBagConstraints();
        triggerMode2Constraints.fill = GridBagConstraints.NONE;
        triggerMode2Constraints.gridx = 2;
        triggerMode2Constraints.gridy = line++;
        triggerMode2Constraints.weightx = 0;
        triggerMode2Constraints.weighty = 0;
        triggerMode2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints triggerLineLabelConstraints = new GridBagConstraints();
        triggerLineLabelConstraints.fill = GridBagConstraints.NONE;
        triggerLineLabelConstraints.gridx = 0;
        triggerLineLabelConstraints.gridy = line;
        triggerLineLabelConstraints.weightx = 0;
        triggerLineLabelConstraints.weighty = 0;
        triggerLineLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints triggerLineConstraints = new GridBagConstraints();
        triggerLineConstraints.fill = GridBagConstraints.HORIZONTAL;
        triggerLineConstraints.gridx = 1;
        triggerLineConstraints.gridy = line;
        triggerLineConstraints.weightx = 1;
        triggerLineConstraints.weighty = 0;
        triggerLineConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints triggerLine2Constraints = new GridBagConstraints();
        triggerLine2Constraints.fill = GridBagConstraints.NONE;
        triggerLine2Constraints.gridx = 2;
        triggerLine2Constraints.gridy = line++;
        triggerLine2Constraints.weightx = 0;
        triggerLine2Constraints.weighty = 0;
        triggerLine2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints triggerActivationLabelConstraints = new GridBagConstraints();
        triggerActivationLabelConstraints.fill = GridBagConstraints.NONE;
        triggerActivationLabelConstraints.gridx = 0;
        triggerActivationLabelConstraints.gridy = line;
        triggerActivationLabelConstraints.weightx = 0;
        triggerActivationLabelConstraints.weighty = 0;
        triggerActivationLabelConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints triggerActivationConstraints = new GridBagConstraints();
        triggerActivationConstraints.fill = GridBagConstraints.HORIZONTAL;
        triggerActivationConstraints.gridx = 1;
        triggerActivationConstraints.gridy = line;
        triggerActivationConstraints.weightx = 1;
        triggerActivationConstraints.weighty = 0;
        triggerActivationConstraints.insets = CometeUtils.getzInset();
        GridBagConstraints triggerActivation2Constraints = new GridBagConstraints();
        triggerActivation2Constraints.fill = GridBagConstraints.NONE;
        triggerActivation2Constraints.gridx = 2;
        triggerActivation2Constraints.gridy = line++;
        triggerActivation2Constraints.weightx = 0;
        triggerActivation2Constraints.weighty = 0;
        triggerActivation2Constraints.insets = CometeUtils.getzInset();

        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.BOTH;
        glueConstraints.gridx = 0;
        glueConstraints.gridy = line++;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 1;
        glueConstraints.gridwidth = GridBagConstraints.REMAINDER;
        glueConstraints.gridheight = GridBagConstraints.REMAINDER;
        glueConstraints.insets = CometeUtils.getzInset();
        // END Constraints

        attributesPanel.add(getModeLabel(), modeLabelConstraints);
        attributesPanel.add(getMode(), modeConstraints);
        attributesPanel.add(getMode2(), mode2Constraints);

        attributesPanel.add(getActiveChannelLabel(), activeChanelLabelConstraints);
        attributesPanel.add(getActiveChannel(), activeChannelConstraints);
        attributesPanel.add(getActiveChannel2(), activeChannel2Constraints);

        attributesPanel.add(getExposureTimeLabel(), exposureTimeLabelConstraints);
        attributesPanel.add(getExposureTime(), exposureTimeConstraints);
        attributesPanel.add(getExposureTime2(), exposureTime2Constraints);

        attributesPanel.add(getBlackLevelLabel(), blackLevelLabelConstraints);
        attributesPanel.add(getBlackLevel(), blackLevelConstraints);
        attributesPanel.add(getBlackLevel2(), blackLevel2Constraints);

        attributesPanel.add(getWhiteLevelLabel(), whiteLevelLabelConstraints);
        attributesPanel.add(getWhiteLevel(), whiteLevelConstraints);
        attributesPanel.add(getWhiteLevel2(), whiteLevel2Constraints);

        attributesPanel.add(getGainLabel(), gainLabelConstraints);
        attributesPanel.add(getGain(), gainConstraints);
        attributesPanel.add(getGain2(), gain2Constraints);

        attributesPanel.add(getTriggerModeLabel(), triggerModeLabelConstraints);
        attributesPanel.add(getTriggerMode(), triggerModeConstraints);
        attributesPanel.add(getTriggerMode2(), triggerMode2Constraints);

        attributesPanel.add(getTriggerLineLabel(), triggerLineLabelConstraints);
        attributesPanel.add(getTriggerLine(), triggerLineConstraints);
        attributesPanel.add(getTriggerLine2(), triggerLine2Constraints);

        attributesPanel.add(getTriggerActivationLabel(), triggerActivationLabelConstraints);
        attributesPanel.add(getTriggerActivation(), triggerActivationConstraints);
        attributesPanel.add(getTriggerActivation2(), triggerActivation2Constraints);
        attributesPanel.add(Box.createGlue(), glueConstraints);
    }

    public void displayMode2(boolean display) {
        display(display, getMode2(), getMode3(), mode2Constraints);
    }

    public void displayTriggerMode2(boolean display) {
        display(display, getTriggerMode2(), getTriggerMode3(), triggerMode2Constraints);
    }

    protected void display(boolean display, JComponent firstComponent, JComponent secondComponent,
            GridBagConstraints constraints) {
        attributesPanel.remove(firstComponent);
        attributesPanel.remove(secondComponent);
        JComponent comp = (display ? firstComponent : secondComponent);
        attributesPanel.add(comp, constraints);
    }

    public void fillAdvancedPanel(ADockingManager advancedDockingManager, JComponent advancedSplitPane) {
        IView commandsView = advancedDockingManager.getViewFactory().addView("Grabber commands", null,
                new JScrollPane(getCommandsPanel()), "commandsGrabber", advancedSplitPane);
        commandsViewAction = DockingUtils.generateShowViewAction(commandsView);
        IView attributesView = advancedDockingManager.getViewFactory().addView("Grabber scalar attributes", null,
                new JScrollPane(getAttributesPanel()), "attributesGrabber", advancedSplitPane);
        attributesViewAction = DockingUtils.generateShowViewAction(attributesView);
    }

    @Override
    protected void layoutCommandsPanel() {
        super.layoutCommandsPanel();
        GridBagLayout gbl = new GridBagLayout();
        commandsPanel = new JPanel(/*new GridLayout(6, 2, 5, 5)*/);
        commandsPanel.setLayout(gbl);

        GridBagConstraints bgc1 = new GridBagConstraints(0, 0, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc2 = new GridBagConstraints(1, 0, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc3 = new GridBagConstraints(0, 1, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc4 = new GridBagConstraints(1, 1, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc5 = new GridBagConstraints(0, 2, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc6 = new GridBagConstraints(1, 2, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc7 = new GridBagConstraints(0, 3, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc8 = new GridBagConstraints(1, 3, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc9 = new GridBagConstraints(0, 4, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);
        GridBagConstraints bgc10 = new GridBagConstraints(1, 4, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);

        GridBagConstraints bgc11 = new GridBagConstraints(0, 5, 1, 1, 0.5, 0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0);

        commandsPanel.add(getState(), bgc1);
        commandsPanel.add(getStatus(), bgc2);
        commandsPanel.add(getStart(), bgc3);
        commandsPanel.add(getStop(), bgc4);
        commandsPanel.add(getOpen(), bgc5);
        commandsPanel.add(getClose(), bgc6);
        commandsPanel.add(getSetROI(), bgc7);
        commandsPanel.add(getResetROI(), bgc8);
        commandsPanel.add(getSetBinning(), bgc9);
        commandsPanel.add(getSnap(), bgc10);
        commandsPanel.add(getInit(), bgc11);
    }

    @Override
    protected void layoutMainPanel() {
        super.layoutMainPanel();
        mainPanel.add(getCommandsPanel(), BorderLayout.NORTH);
    }

    @Override
    protected void initSetROI() {
        setROI = generateStringButton();
        setROI.setText("SetROI");
    }

    @Override
    protected TextField generateNumberField() {
        TextField field = super.generateNumberField();
        field.setColumns(20);
        return field;
    }

    public JLabel getModeLabel() {
        if (modeLabel == null) {
            modeLabel = new JLabel("acquisitionMode", JLabel.LEFT);
        }
        return modeLabel;
    }

    public JLabel getBlackLevelLabel() {
        if (blackLevelLabel == null) {
            blackLevelLabel = new JLabel("blackLevel", JLabel.LEFT);
        }
        return blackLevelLabel;
    }

    public JLabel getWhiteLevelLabel() {
        if (whiteLevelLabel == null) {
            whiteLevelLabel = new JLabel("whiteLevel", JLabel.LEFT);
        }
        return whiteLevelLabel;
    }

    public JLabel getGainLabel() {
        if (gainLabel == null) {
            gainLabel = new JLabel("gain", JLabel.LEFT);
        }
        return gainLabel;
    }

    public JLabel getTriggerModeLabel() {
        if (triggerModeLabel == null) {
            triggerModeLabel = new JLabel("triggerMode", JLabel.LEFT);
        }
        return triggerModeLabel;
    }

    public JLabel getTriggerLineLabel() {
        if (triggerLineLabel == null) {
            triggerLineLabel = new JLabel("triggerLine", JLabel.LEFT);
        }
        return triggerLineLabel;
    }

    public JLabel getTriggerActivationLabel() {
        if (triggerActivationLabel == null) {
            triggerActivationLabel = new JLabel("triggerActivation", JLabel.LEFT);
        }
        return triggerActivationLabel;
    }

    public Label getMode() {
        if (mode == null) {
            mode = generateLabel();
        }
        return mode;
    }

    public Label getBlackLevel() {
        if (blackLevel == null) {
            blackLevel = generateLabel();
        }
        return blackLevel;
    }

    public Label getWhiteLevel() {
        if (whiteLevel == null) {
            whiteLevel = generateLabel();
        }
        return whiteLevel;
    }

    public Label getGain() {
        if (gain == null) {
            gain = generateLabel();
        }
        return gain;
    }

    public Label getTriggerMode() {
        if (triggerMode == null) {
            triggerMode = generateLabel();
        }
        return triggerMode;
    }

    public Label getTriggerLine() {
        if (triggerLine == null) {
            triggerLine = generateLabel();
        }
        return triggerLine;
    }

    public Label getTriggerActivation() {
        if (triggerActivation == null) {
            triggerActivation = generateLabel();
        }
        return triggerActivation;
    }

    public TextField getMode2() {
        if (mode2 == null) {
            mode2 = generateNumberField();
        }
        return mode2;
    }

    public ComboBox getMode3() {
        if (mode3 == null) {
            mode3 = new ComboBox();
        }
        return mode3;
    }

    public WheelSwitch getBlackLevel2() {
        if (blackLevel2 == null) {
            blackLevel2 = generateWheelSwitch();
        }
        return blackLevel2;
    }

    public WheelSwitch getWhiteLevel2() {
        if (whiteLevel2 == null) {
            whiteLevel2 = generateWheelSwitch();
        }
        return whiteLevel2;
    }

    public WheelSwitch getGain2() {
        if (gain2 == null) {
            gain2 = generateWheelSwitch();
        }
        return gain2;
    }

    public TextField getTriggerMode2() {
        if (triggerMode2 == null) {
            triggerMode2 = generateNumberField();
        }
        return triggerMode2;
    }

    public ComboBox getTriggerMode3() {
        if (triggerMode3 == null) {
            triggerMode3 = new ComboBox();
        }
        return triggerMode3;
    }

    public WheelSwitch getTriggerLine2() {
        if (triggerLine2 == null) {
            triggerLine2 = generateWheelSwitch();
        }
        return triggerLine2;
    }

    public WheelSwitch getTriggerActivation2() {
        if (triggerActivation2 == null) {
            triggerActivation2 = generateWheelSwitch();
        }
        return triggerActivation2;
    }

    public StringButton getInit() {
        if (init == null) {
            init = generateStringButton();
            init.setText("Init");
        }
        return init;
    }

    public StringButton getState() {
        if (state == null) {
            state = generateStringButton();
            state.setText("State");
        }
        return state;
    }

    public StringButton getStatus() {
        if (status == null) {
            status = generateStringButton();
            status.setText("Status");
        }
        return status;
    }

    public StringButton getOpen() {
        if (open == null) {
            open = generateStringButton();
            open.setText("Open");
        }
        return open;
    }

    public StringButton getClose() {
        if (close == null) {
            close = generateStringButton();
            close.setText("Close");
        }
        return close;
    }

    public StringButton getStart() {
        if (start == null) {
            start = generateStringButton();
            start.setText("Start");
        }
        return start;
    }

    public StringButton getStop() {
        if (stop == null) {
            stop = generateStringButton();
            stop.setText("Stop");
        }
        return stop;
    }

    public StringButton getSetBinning() {
        if (setBinning == null) {
            setBinning = generateStringButton();
            setBinning.setText("SetBinning");
        }
        return setBinning;
    }

    public StringButton getSnap() {
        if (snap == null) {
            snap = generateStringButton();
            snap.setText("Snap");
        }
        return snap;
    }

    public AbstractAction getAttributesViewAction() {
        return attributesViewAction;
    }

    public AbstractAction getCommandsViewAction() {
        return commandsViewAction;
    }

}
