package fr.soleil.bean.imggrabber.utils;

import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;

import javax.swing.JTextField;

import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;

public class SetRoiButton extends StringButton {

    private static final long serialVersionUID = -7290009129571987234L;

    protected WeakReference<JTextField> xTextField;
    protected WeakReference<JTextField> yTextField;
    protected WeakReference<JTextField> widthTextField;
    protected WeakReference<JTextField> heightTextField;
    protected StringMatrixComboBoxViewer currentRoiViewer;

    public SetRoiButton() {
        super();
        currentRoiViewer = new StringMatrixComboBoxViewer();
        setButtonLook(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        String[] roiValues = currentRoiViewer.getFlatStringMatrix();
        int roiX = 0, roiY = 0, roiWidth = 0, roiHeight = 0;
        if ((roiValues != null) && (roiValues.length > 2)) {
            try {
                roiX = Integer.parseInt(roiValues[0]);
            } catch (NumberFormatException nfe) {
                roiX = 0;
            }
            try {
                roiY = Integer.parseInt(roiValues[1]);
            } catch (NumberFormatException nfe) {
                roiY = 0;
            }
            JTextField xTextField = getXTextField();
            JTextField yTextField = getYTextField();
            JTextField widthTextField = getWidthTextField();
            JTextField heightTextField = getHeightTextField();
            try {
                if ((xTextField != null) && (yTextField != null) && (widthTextField != null)
                        && (heightTextField != null) && (xTextField.getText() != null)
                        && (!xTextField.getText().trim().isEmpty()) && (yTextField.getText() != null)
                        && (!yTextField.getText().trim().isEmpty()) && (widthTextField.getText() != null)
                        && (!widthTextField.getText().trim().isEmpty()) && (heightTextField.getText() != null)
                        && (!heightTextField.getText().trim().isEmpty())) {
                    roiX += Integer.parseInt(xTextField.getText());
                    roiY += Integer.parseInt(yTextField.getText());
                    roiWidth = Integer.parseInt(widthTextField.getText());
                    roiHeight = Integer.parseInt(heightTextField.getText());
                    StringBuilder parameterBuffer = new StringBuilder();
                    parameterBuffer.append(roiX).append(",");
                    parameterBuffer.append(roiY).append(",");
                    parameterBuffer.append(roiWidth).append(",");
                    parameterBuffer.append(roiHeight);
                    setParameter(parameterBuffer.toString());
                    super.actionPerformed(event);
                }
            } catch (NumberFormatException nfe) {
                // simply refuse to execute dao
            }
        }
    }

    public JTextField getXTextField() {
        if (xTextField == null) {
            return null;
        } else {
            return xTextField.get();
        }
    }

    public void setXTextField(JTextField xTextField) {
        if (this.xTextField != null) {
            this.xTextField.clear();
        }
        if (xTextField == null) {
            this.xTextField = null;
        } else {
            this.xTextField = new WeakReference<JTextField>(xTextField);
        }
    }

    public JTextField getYTextField() {
        if (yTextField == null) {
            return null;
        } else {
            return yTextField.get();
        }
    }

    public void setYTextField(JTextField yTextField) {
        if (this.yTextField != null) {
            this.yTextField.clear();
        }
        if (yTextField == null) {
            this.yTextField = null;
        } else {
            this.yTextField = new WeakReference<JTextField>(yTextField);
        }
    }

    public JTextField getWidthTextField() {
        if (widthTextField == null) {
            return null;
        } else {
            return widthTextField.get();
        }
    }

    public void setWidthTextField(JTextField widthTextField) {
        if (this.widthTextField != null) {
            this.widthTextField.clear();
        }
        if (widthTextField == null) {
            this.widthTextField = null;
        } else {
            this.widthTextField = new WeakReference<JTextField>(widthTextField);
        }
    }

    public JTextField getHeightTextField() {
        if (heightTextField == null) {
            return null;
        } else {
            return heightTextField.get();
        }
    }

    public void setHeightTextField(JTextField heightTextField) {
        if (this.heightTextField != null) {
            this.heightTextField.clear();
        }
        if (heightTextField == null) {
            this.heightTextField = null;
        } else {
            this.heightTextField = new WeakReference<JTextField>(heightTextField);
        }
    }

    public StringMatrixComboBoxViewer getCurrentRoiViewer() {
        return currentRoiViewer;
    }

}
