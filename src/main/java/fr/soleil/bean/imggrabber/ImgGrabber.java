package fr.soleil.bean.imggrabber;

import java.awt.BorderLayout;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.bean.imggrabber.view.ImgGrabberAdvancedView;
import fr.soleil.bean.imggrabber.view.ImgGrabberLightView;
import fr.soleil.bean.imggrabber.view.ImgGrabberMainView;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.tango.data.service.helper.TangoCommandHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.target.scalar.IScalarTarget;
import fr.soleil.docking.ADockingManager;

public class ImgGrabber extends AbstractTangoBox {

    private static final long serialVersionUID = -7223616441409012827L;

    protected ImgGrabberAdvancedView advancedImgGrabberView = null;
    protected ImgGrabberMainView mainImgGrabberView = null;
    protected ImgGrabberLightView lightImgGrabberView = null;
    protected JTabbedPane mainTabbedPane = null;
    protected JPanel mainPanel = null;

    // COMMANDS //
    protected static final String RESET_ROI = "ResetROI";
    protected static final String INIT = "Init";
    protected static final String OPEN = "Open";
    protected static final String CLOSE = "Close";
    protected static final String SET_ROI = "SetROI";
    protected static final String GET_ROI = "GetROI";
    protected static final String START = "Start";
    protected static final String STOP = "Stop";
    protected static final String STATE = "State";
    protected static final String STATUS = "Status";
    protected static final String SAVE_SETTINGS = "SaveSettings";
    protected static final String SET_BINNING = "SetBinning";
    protected static final String SNAP = "Snap";

    // special command that does not need checking
    protected static final String ATTRIBUTE_VALUES = "GetAttributeAvailableValues";

    protected static final String[] EXPECTED_COMMANDS = { INIT, SET_ROI, START, STATE, STATUS, STOP };
    protected static final String[] POTENTIAL_COMMANDS = { RESET_ROI, GET_ROI, SAVE_SETTINGS, OPEN, CLOSE, SET_BINNING,
            SNAP };
    protected static final List<String> POTENTIAL_COMMAND_LIST = Arrays.asList(POTENTIAL_COMMANDS);

    // ATTRIBUTES //
    protected static final String ACQUISITION_MODE = "acquisitionMode";
    protected static final String EXPOSURE_TIME = "ExposureTime";
    protected static final String IMAGE = "Image";
    protected static final String ACTIVE_CHANNEL = "ActiveChannel";
    protected static final String BLACK_LEVEL = "BlackLevel";
    protected static final String WHITE_LEVEL = "WhiteLevel";
    protected static final String GAIN = "Gain";
    protected static final String TRIGGER_MODE = "TriggerMode";
    protected static final String TRIGGER_LINE = "TriggerLine";
    protected static final String TRIGGER_ACTIVATION = "TriggerActivation";

    protected static final String[] EXPECTED_ATTRIBUTES = { EXPOSURE_TIME, IMAGE };
    protected static final String[] POTENTIAL_ATTRIBUTES = { ACQUISITION_MODE, ACTIVE_CHANNEL, BLACK_LEVEL, WHITE_LEVEL,
            GAIN, TRIGGER_MODE, TRIGGER_LINE, TRIGGER_ACTIVATION };
    protected static final List<String> POTENTIAL_ATTRIBUTE_LIST = Arrays.asList(POTENTIAL_ATTRIBUTES);

    // DISPLAYED MESSAGES //
    protected final static String INIT_ATTRIBUTES_INFO = "Attributes initialization...";
    protected final static String INIT_COMMANDS_INFO = "Commands initialization...";

    private final ADockingManager dockingManager;

    public ImgGrabber(ADockingManager dockingManager) {
        super();
        this.dockingManager = dockingManager;
        setAllowedBadAttributes(POTENTIAL_ATTRIBUTE_LIST);
        setAllowedBadCommands(POTENTIAL_COMMAND_LIST);
        initGUI();
    }

    @Override
    protected void initMaxProgress() {
        currentProgression = 0;
        // 2 light model + 6 main model + 20 advanced model + all entity checks
        maxProgress = 28 + EXPECTED_ATTRIBUTES.length + EXPECTED_COMMANDS.length + POTENTIAL_ATTRIBUTES.length
                + POTENTIAL_COMMANDS.length;
    }

    public void initGUI() {
        this.setLayout(new BorderLayout());
        this.add(getMainPanel());
    }

    public void fireProgression(final int progression, final String info) {
        super.fireProgressionChanged(progression, info);
    }

    @Override
    protected void progress(boolean warnProgress, String info) {
        if (warnProgress) {
            fireProgressionChanged(++currentProgression, info);
        }
    }

    public void setMainModels(boolean warnProgress) {
        String info = "Setting models...";

        /* Commands */
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        connectCommand(RESET_ROI, warnProgress, info, getMainView().getResetROI());
        connectCommand(SET_ROI, warnProgress, info, getMainView().getSetROI2());
        connectCommand(GET_ROI, warnProgress, info, getMainView().getGetRoi());
        connectCommand(SAVE_SETTINGS, warnProgress, info, getMainView().getSaveSettings());

        /* Attributes read/write */
        connectAttribute(ACTIVE_CHANNEL, warnProgress, info, getMainView().getActiveChannelLabel(),
                getMainView().getActiveChannel(), getMainView().getActiveChannel2());
        connectAttribute(EXPOSURE_TIME, warnProgress, info, getMainView().getExposureTimeLabel(),
                getMainView().getExposureTime(), getMainView().getExposureTime2());
        if (!badAttributes.contains(IMAGE)) {
            getMainView().setImage(generateAttributeKey(IMAGE));
        }
        progress(warnProgress, info);
    }

    public void setLightModels(boolean warnProgress) {
        String info = "Setting models...";
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        /* Attributes read/write */
        connectAttribute(ACTIVE_CHANNEL, warnProgress, info, getLightView().getActiveChannelLabel(),
                getLightView().getActiveChannel(), getLightView().getActiveChannel2());
        connectAttribute(EXPOSURE_TIME, warnProgress, info, getLightView().getExposureTimeLabel(),
                getLightView().getExposureTime(), getLightView().getExposureTime2());
    }

    public void setAdvancedModels(boolean warnProgress) {
        String info = "Setting models...";

        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        /* Commands */
        connectCommand(RESET_ROI, warnProgress, info, getAdvancedView().getResetROI());
        connectCommand(INIT, warnProgress, info, getAdvancedView().getInit());
        connectCommand(SET_ROI, warnProgress, info, getAdvancedView().getSetROI());
        connectCommand(START, warnProgress, info, getAdvancedView().getStart());
        connectCommand(STATE, warnProgress, info, getAdvancedView().getState());
        connectCommand(STATUS, warnProgress, info, getAdvancedView().getStatus());
        connectCommand(STOP, warnProgress, info, getAdvancedView().getStop());
        connectCommand(OPEN, warnProgress, info, getAdvancedView().getOpen());
        connectCommand(CLOSE, warnProgress, info, getAdvancedView().getClose());
        connectCommand(SNAP, warnProgress, info, getAdvancedView().getSnap());
        connectCommand(SET_BINNING, warnProgress, info, getAdvancedView().getSetBinning());

//        TangoCommand command = null;
//        try {
//            command = new TangoCommand(getModel(), ATTRIBUTE_VALUES);
//        } catch (Exception e) {
//            command = null;
//        }

        /* Attributes read/write */
        // extract possible acquisition modes in case of lima
        IScalarTarget setter;
//        String[] possibleValues = extractCommandValue(command, ACQUISITION_MODE);
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
        String[] possibleValues = extractCommandValue(proxy, ATTRIBUTE_VALUES, ACQUISITION_MODE);
        boolean display;
        if ((possibleValues == null) || (possibleValues.length == 0)) {
            display = true;
            setter = getAdvancedView().getMode2();
        } else {
            display = false;
            setter = getAdvancedView().getMode3();
            getAdvancedView().getMode3().setValueList((Object[]) possibleValues);
        }
        getAdvancedView().displayMode2(display);
        connectAttribute(ACQUISITION_MODE, warnProgress, info, getAdvancedView().getModeLabel(),
                getAdvancedView().getMode(), setter);
        connectAttribute(EXPOSURE_TIME, warnProgress, info, getAdvancedView().getExposureTimeLabel(),
                getAdvancedView().getExposureTime(), getAdvancedView().getExposureTime2());
        connectAttribute(ACTIVE_CHANNEL, warnProgress, info, getAdvancedView().getActiveChannelLabel(),
                getAdvancedView().getActiveChannel(), getAdvancedView().getActiveChannel2());
        connectAttribute(BLACK_LEVEL, warnProgress, info, getAdvancedView().getBlackLevelLabel(),
                getAdvancedView().getBlackLevel(), getAdvancedView().getBlackLevel2());
        connectAttribute(WHITE_LEVEL, warnProgress, info, getAdvancedView().getWhiteLevelLabel(),
                getAdvancedView().getWhiteLevel(), getAdvancedView().getWhiteLevel2());
        connectAttribute(GAIN, warnProgress, info, getAdvancedView().getGainLabel(), getAdvancedView().getGain(),
                getAdvancedView().getGain2());
        // extract possible trigger modes in case of lima
//        possibleValues = extractCommandValue(command, TRIGGER_MODE);
        possibleValues = extractCommandValue(proxy, ATTRIBUTE_VALUES, TRIGGER_MODE);
        if ((possibleValues == null) || (possibleValues.length == 0)) {
            display = true;
            setter = getAdvancedView().getTriggerMode2();
        } else {
            display = false;
            setter = getAdvancedView().getTriggerMode3();
            getAdvancedView().getTriggerMode3().setValueList((Object[]) possibleValues);
        }
        getAdvancedView().displayTriggerMode2(display);
        connectAttribute(TRIGGER_MODE, warnProgress, info, getAdvancedView().getTriggerModeLabel(),
                getAdvancedView().getTriggerMode(), setter);
        connectAttribute(TRIGGER_LINE, warnProgress, info, getAdvancedView().getTriggerLineLabel(),
                getAdvancedView().getTriggerLine(), getAdvancedView().getTriggerLine2());
        connectAttribute(TRIGGER_ACTIVATION, warnProgress, info, getAdvancedView().getTriggerActivationLabel(),
                getAdvancedView().getTriggerActivation(), getAdvancedView().getTriggerActivation2());
        currentProgression = currentProgression % (maxProgress + 1);
    }

//    protected String[] extractCommandValue(TangoCommand command, String parameter) {
//        String[] result = null;
//        try {
//            result = command.execute(String[].class, parameter);
//        } catch (Exception e) {
//            result = null;
//        }
//        return result;
//    }

    protected String[] extractCommandValue(DeviceProxy proxy, String commandName, String parameter) {
        String[] result = null;
        try {
            result = TangoCommandHelper.executeCommand(proxy, commandName, String[].class, parameter);
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    @Override
    protected void refreshGUI() {
        setStatusModel();
        initAttributes();
        initCommands();
        displayBadAttributesAndCommands();
        clearLightModels(true);
        setMainModels(true);
        setAdvancedModels(true);
    }

    @Override
    protected void clearGUI() {
        try {
            clearModels(true);
        } finally {
            fireProgressionChanged(getMaxProgression(), "Successfully disconnected from ImgGrabber device");
        }
    }

    @Override
    protected void onConnectionError() {
        String errorMessage = "Failed to connect to ImgGrabber device";
        System.err.println(getClass().getSimpleName() + ":\n" + errorMessage);
        fireProgressionChanged(getMaxProgression(), errorMessage);
    }

    public void clearLightModels(boolean warnProgress) {
        String info = "Setting models...";
        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        cleanWidgets(warnProgress, info, getLightView().getActiveChannel(), getLightView().getActiveChannel2());
        cleanWidgets(warnProgress, info, getLightView().getExposureTime(), getLightView().getExposureTime2());
        currentProgression = currentProgression % (maxProgress + 1);
    }

    public void clearMainModels(boolean warnProgress) {
        String info = "Setting models...";

        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }

        cleanWidgets(warnProgress, info, getMainView().getResetROI());
        // TODO use getSetROI()
        cleanWidgets(warnProgress, info, getMainView().getSetROI2(), getMainView().getGetRoi());
        cleanWidgets(warnProgress, info, getMainView().getSaveSettings());
        cleanWidgets(warnProgress, info, getMainView().getActiveChannel(), getMainView().getActiveChannel2());
        cleanWidgets(warnProgress, info, getMainView().getExposureTime(), getMainView().getExposureTime2());
        // stringBox.disconnectWidgetFromAll(getMainView().getExposureTime2());
        getMainView().setImage(null);
        progress(warnProgress, info);
        currentProgression = currentProgression % (maxProgress + 1);
    }

    public void clearAdvancedModels(boolean warnProgress) {
        String info = "Setting models...";

        if (warnProgress) {
            fireProgressionChanged(currentProgression, info);
        }
        /* Commands */
        cleanWidgets(warnProgress, info, getAdvancedView().getResetROI());
        cleanWidgets(warnProgress, info, getAdvancedView().getInit());
        cleanWidgets(warnProgress, info, getAdvancedView().getOpen());
        cleanWidgets(warnProgress, info, getAdvancedView().getClose());
        cleanWidgets(warnProgress, info, getAdvancedView().getSetROI());
        cleanWidgets(warnProgress, info, getAdvancedView().getStart());
        cleanWidgets(warnProgress, info, getAdvancedView().getState());
        cleanWidgets(warnProgress, info, getAdvancedView().getStatus());
        cleanWidgets(warnProgress, info, getAdvancedView().getStop());
        cleanWidgets(warnProgress, info, getAdvancedView().getSnap());
        cleanWidgets(warnProgress, info, getAdvancedView().getSetBinning());
        /* Attributes read/write */
        cleanWidgets(warnProgress, info, getAdvancedView().getMode(), getAdvancedView().getMode2(),
                getAdvancedView().getMode3());
        getAdvancedView().displayMode2(true);
        cleanWidgets(warnProgress, info, getAdvancedView().getActiveChannel(), getAdvancedView().getActiveChannel2());
        cleanWidgets(warnProgress, info, getAdvancedView().getExposureTime(), getAdvancedView().getExposureTime2());
        cleanWidgets(warnProgress, info, getAdvancedView().getBlackLevel(), getAdvancedView().getBlackLevel2());
        // MODIFIED 25/02
        cleanWidgets(warnProgress, info, getAdvancedView().getWhiteLevel(), getAdvancedView().getWhiteLevel2());

        cleanWidgets(warnProgress, info, getAdvancedView().getGain(), getAdvancedView().getGain2());
        cleanWidgets(warnProgress, info, getAdvancedView().getTriggerMode(), getAdvancedView().getTriggerMode2(),
                getAdvancedView().getTriggerMode3());
        getAdvancedView().displayTriggerMode2(true);
        cleanWidgets(warnProgress, info, getAdvancedView().getTriggerLine(), getAdvancedView().getTriggerLine2());
        cleanWidgets(warnProgress, info, getAdvancedView().getTriggerActivation(),
                getAdvancedView().getTriggerActivation2());
        currentProgression = currentProgression % (maxProgress + 1);
    }

    public void clearModels(boolean warnProgress) {
        cleanStatusModel();
        clearMainModels(warnProgress);
        clearAdvancedModels(warnProgress);
    }

    protected void initAttributes() {
        // little hack to have all attributes connected
        // DAOConnectionThread.connect(getModel());
        // try {
        // Thread.sleep(5000);
        // }
        // catch (InterruptedException e1) {
        // // TODO Auto-generated catch block
        // e1.printStackTrace();
        // }
        fireProgressionChanged(currentProgression, INIT_ATTRIBUTES_INFO);
        String[] toCheck = new String[EXPECTED_ATTRIBUTES.length + POTENTIAL_ATTRIBUTES.length];
        System.arraycopy(EXPECTED_ATTRIBUTES, 0, toCheck, 0, EXPECTED_ATTRIBUTES.length);
        System.arraycopy(POTENTIAL_ATTRIBUTES, 0, toCheck, EXPECTED_ATTRIBUTES.length, POTENTIAL_ATTRIBUTES.length);
        checkAttributes(toCheck);
        toCheck = null;
        fireProgressionChanged(currentProgression, INIT_ATTRIBUTES_INFO + " done");
        currentProgression = currentProgression % (maxProgress + 1);
    }

    @Override
    protected boolean checkAttribute(String attributeShortName) {
        boolean result = super.checkAttribute(attributeShortName);
        progress(true, INIT_ATTRIBUTES_INFO);
        return result;
    }

    protected void initCommands() {
        fireProgressionChanged(currentProgression, INIT_COMMANDS_INFO);
        String[] toCheck = new String[EXPECTED_COMMANDS.length + POTENTIAL_COMMANDS.length];
        System.arraycopy(EXPECTED_COMMANDS, 0, toCheck, 0, EXPECTED_COMMANDS.length);
        System.arraycopy(POTENTIAL_COMMANDS, 0, toCheck, EXPECTED_COMMANDS.length, POTENTIAL_COMMANDS.length);
        checkCommands(toCheck);
        fireProgressionChanged(currentProgression, INIT_COMMANDS_INFO + " done");
        currentProgression = currentProgression % (maxProgress + 1);
    }

    @Override
    protected boolean checkCommand(String commandShortName) {
        boolean result = super.checkCommand(commandShortName);
        progress(true, INIT_COMMANDS_INFO);
        return result;
    }

    public ImgGrabberAdvancedView getAdvancedView() {
        if (advancedImgGrabberView == null) {
            advancedImgGrabberView = new ImgGrabberAdvancedView();
        }
        return advancedImgGrabberView;
    }

    public ImgGrabberMainView getMainView() {
        if (mainImgGrabberView == null) {
            mainImgGrabberView = new ImgGrabberMainView(dockingManager);
        }
        return mainImgGrabberView;
    }

    public ImgGrabberLightView getLightView() {
        if (lightImgGrabberView == null) {
            lightImgGrabberView = new ImgGrabberLightView();
        }
        return lightImgGrabberView;
    }

    /**
     * @return the mainTabbedPane
     */
    public JTabbedPane getMainTabbedPane() {
        if (mainTabbedPane == null) {
            mainTabbedPane = new JTabbedPane();
            mainTabbedPane.add("General", getMainView().getMainSplitPane());
            mainTabbedPane.add("Advanced", getAdvancedView().getMainPanel());
        }
        return mainTabbedPane;
    }

    public JPanel getMainPanel() {
        if (mainPanel == null) {
            mainPanel = new JPanel(new BorderLayout());
            mainPanel.add(getMainTabbedPane(), BorderLayout.CENTER);
            mainPanel.add(getStatusPanel(), BorderLayout.SOUTH);
        }
        return mainPanel;
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

}
