/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.controler;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.preferences.PreferencesFrame;

public class PreferencesDisplayingAction extends AbstractAction {

    private static final long serialVersionUID = 4904203503368866673L;

    private final ImgBeamAnalyzer analyzer;
    private final static Logger LOGGER = LoggerFactory.getLogger(PreferencesDisplayingAction.class.getName());

    /**
     * 
     */
    public PreferencesDisplayingAction(ImgBeamAnalyzer analyzer) {
        super();

        this.analyzer = analyzer;
        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(Action.NAME, "Preferences");
        // Set tool tip text
        putValue(Action.SHORT_DESCRIPTION, "Show Preferences");

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(Action.LONG_DESCRIPTION, "Show Preferences");

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        // putValue(Action.SMALL_ICON, icon);

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the component
        // using this action has the focus and In some look and feels, this causes
        // the specified character in the label to be underlined and
        putValue(Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_P));

        // Set an accelerator key; this value is used by menu items
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt enter"));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            // new PreferencesFrame(analyzer).setVisible(true);
            PreferencesFrame.getInstance(analyzer).setVisible(true);
        } catch (Exception e1) {
            // e1.printStackTrace();
            LOGGER.error(e1.getMessage());
            JOptionPane.showMessageDialog(analyzer, e1, e1.getMessage(), JOptionPane.WARNING_MESSAGE);
        }
    }
}