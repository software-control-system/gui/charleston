package fr.soleil.charleston;

import java.awt.Color;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.bean.imggrabber.ImgGrabber;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.xml.ChartPropertiesXmlManager;
import fr.soleil.comete.definition.widget.properties.xml.ImagePropertiesXmlManager;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.controler.PreferencesDisplayingAction;
import fr.soleil.lib.project.application.Application;
import fr.soleil.lib.project.progress.IProgressable;
import fr.soleil.lib.project.progress.IProgressionListener;
import fr.soleil.lib.project.swing.DoubleProgressSplash;
import fr.soleil.lib.project.swing.icons.Icons;

public class Charleston extends Application implements IProgressionListener {

    private static final long serialVersionUID = 4596776045216599112L;

    private static final ResourceBundle APPLICATION_BUNDLE = ResourceBundle
            .getBundle("fr.soleil.charleston.application");
    private static final Icons ICONS = new Icons("fr.soleil.charleston.icons");
    private static final String RESET_LAYOUT = "Reset layout";
    private static final String LAYOUT = "Layout";
    public static final String ANALYZER_DEVICE = "AnalyzerDevice";
    private static final String COPYRIGHT = "SOLEIL Synchrotron";
    private static final ImageIcon SMALL_ICON = ICONS.getIcon("Charleston.icon.small");
    private static final ImageIcon SPLASH_ICON = ICONS.getIcon("Charleston.splash");
    public static final ImageIcon PROFILE_ICON = ICONS.getIcon("Charleston.profile");
    private static final Logger LOGGER = LoggerFactory.getLogger(Charleston.class.getName());

    protected ImgBeamAnalyzer analyzer;
    protected DoubleProgressSplash splash;
    private JMenu layoutMenu;
    private String dockingDirectory;
    private String propertiesDirectory;
    private String version;

    public Charleston() {
        this(new String[0]);
    }

    public Charleston(String... args) {
        super();

        if (args != null) {
            if (args.length > 2) {
                dockingDirectory = args[2];
            }
            if (args.length > 3) {
                propertiesDirectory = args[3];
            }
        }

        // Get the global logger to configure it
        // Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

        getJMenuBar().add(getLayoutMenu());

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(getAnalyzer());

        getPreferenceMenuItem().setAction(new PreferencesDisplayingAction(analyzer));

        getAnalyzer().addProgressionListener(this);
        getAnalyzer().getGrabber().addProgressionListener(this);
//        getAnalyzer().getMainTabbedPane().setEnabled(true);
//        getAnalyzer().getImageSpectrumTabbedPane().setEnabled(true);
        loadPreferences();
    }

    public JMenu getLayoutMenu() {
        if (layoutMenu == null) {
            layoutMenu = new JMenu();
            layoutMenu.setText(LAYOUT);
            JMenuItem resetLayoutMenuItem = new javax.swing.JMenuItem();
            resetLayoutMenuItem.setText(RESET_LAYOUT);

            layoutMenu.add(getAnalyzer().getResetViewMenu());

            resetLayoutMenuItem.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    try {
                        getAnalyzer().loadDefaultPerspective();
                    } catch (IOException e1) {
                        // e1.printStackTrace();
                        LOGGER.error(e1.getMessage());
                    }
                }
            });
            layoutMenu.add(resetLayoutMenuItem);
        }
        return layoutMenu;
    }

    public void closeSplash() {
        splash.setVisible(false);
//        getAnalyzer().getMainTabbedPane().setEnabled(true);
//        getAnalyzer().getImageSpectrumTabbedPane().setEnabled(true);
    }

    @Override
    public void progressionChanged(IProgressable source, int progression, String info) {
        if (source instanceof ImgBeamAnalyzer) {
            splash.setFirstMessage("ImgBeamAnalyzer: " + info);
            splash.setFGProgress(progression);
        } else if (source instanceof ImgGrabber) {
            splash.setSecondMessage("ImgGrabber: " + info);
            splash.setBGProgress(progression);
        }
        if (splash.getDoubleProgressBar().getBGValue() == splash.getDoubleProgressBar().getBGMaximum()
                && splash.getDoubleProgressBar().getFGValue() == splash.getDoubleProgressBar().getFGMaximum()) {
            closeSplash();
        }
        if (splash.isVisible()) {
            splash.repaint();
        }
    }

    public ImgBeamAnalyzer getAnalyzer() {
        if (analyzer == null) {
            analyzer = new ImgBeamAnalyzer(dockingDirectory, propertiesDirectory);
        }
        return analyzer;
    }

    public DoubleProgressSplash getDoubleProgressSplash() {
        if (splash == null) {
            splash = new DoubleProgressSplash();
            splash.setTitle(getApplicationTitle());
            splash.setCopyright("(c) SOLEIL Synchrotron");
            splash.setFirstMessage("Connecting to ImgBeamAnalyzer...");
            splash.setSecondMessage("Connecting to ImgGrabber...");
            splash.getSplashPanel().getDoubleProgressBar().setFGName("ImgBeamAnalyzer");
            splash.getSplashPanel().getDoubleProgressBar().setBGName("ImgGrabber");
            splash.setFGMaxProgress(getAnalyzer().getMaxProgression());
            splash.setBGMaxProgress(getAnalyzer().getGrabber().getMaxProgression());
            splash.getSplashPanel().getDoubleProgressBar().setFGValue(100);
            splash.pack();
            splash.getSplashPanel().getDoubleProgressBar()
                    .setMinimumSize(splash.getSplashPanel().getDoubleProgressBar().getPreferredSize());
            splash.getSplashPanel().getDoubleProgressBar().setFGValue(0);
            splash.setLocationRelativeTo(this);
        }
        return splash;
    }

    @Override
    protected Preferences recoverPreferences() {
        return Preferences.userNodeForPackage(Charleston.class);
    }

    @Override
    protected void savePreferences() {
        super.savePreferences();
        if (prefs != null) {
            prefs.put(ANALYZER_DEVICE, analyzer.getModel());
        }
    }

    @Override
    protected void loadPreferences() {
        super.loadPreferences();
        if ((prefs != null) && (analyzer != null)) {
            String analyzerDevice = prefs.get(ANALYZER_DEVICE, null);
            analyzer.setModel(analyzerDevice);
        }
    }

    @Override
    public boolean quit() {
        getAnalyzer().saveSelectedPerspective();

        // JIRA CONTROLGUI-14
        saveProperties();

        return super.quit();
    }

    /**
     * JIRA CONTROLGUI-14
     * Save properties in files
     */
    private void saveProperties() {
        Chart histogrammViewer = getAnalyzer().getImgBeamAnalyzerMainView().getHistogrammViewer();
        ChartProperties histogrammChartProperties = histogrammViewer.getChartProperties();
        String histogrammChartPropertiesXml = ChartPropertiesXmlManager.toXmlString(histogrammChartProperties);

        Chart profileViewer1 = getAnalyzer().getImgBeamAnalyzerMainView().getRoiImageProfilePanel().getProfileViewer1()
                .getProfileViewer();
        ChartProperties profileViewer1ChartProperties = profileViewer1.getChartProperties();
        String profileViewer1ChartPropertiesXml = ChartPropertiesXmlManager.toXmlString(profileViewer1ChartProperties);

        Chart profileViewer2 = getAnalyzer().getImgBeamAnalyzerMainView().getRoiImageProfilePanel().getProfileViewer2()
                .getProfileViewer();
        ChartProperties profileViewer2ChartProperties = profileViewer2.getChartProperties();
        String profileViewer2ChartPropertiesXml = ChartPropertiesXmlManager.toXmlString(profileViewer2ChartProperties);

        Chart profileViewer3 = getAnalyzer().getImgBeamAnalyzerMainView().getRoiImageProfilePanel().getProfileViewer3()
                .getProfileViewer();
        ChartProperties profileViewer3ChartProperties = profileViewer3.getChartProperties();
        String profileViewer3ChartPropertiesXml = ChartPropertiesXmlManager.toXmlString(profileViewer3ChartProperties);

        ImageViewer analyzerInputImage = getAnalyzer().getImgBeamAnalyzerMainView().getInputImageViewer();
        ImageProperties analyzerInputImageProperties = analyzerInputImage.getImageProperties();
        String analyzerInputImagePropertiesXml = ImagePropertiesXmlManager.toXmlString(analyzerInputImageProperties);

        ImageViewer analyzerRoiImage = getAnalyzer().getImgBeamAnalyzerMainView().getRoiImageProfilePanel()
                .getImageViewer();
        ImageProperties analyzerRoiImageProperties = analyzerRoiImage.getImageProperties();
        String analyzerRoiImagePropertiesXml = ImagePropertiesXmlManager.toXmlString(analyzerRoiImageProperties);

        ImageViewer grabberImage = getAnalyzer().getGrabber().getMainView().getImageViewer();
        ImageProperties grabberImageProperties = grabberImage.getImageProperties();
        String grabberImagePropertiesXml = ImagePropertiesXmlManager.toXmlString(grabberImageProperties);

        String directory = ImgBeamAnalyzer.getPropertiesDirectoryName();
        File histogrammChartPropertiesFile = ImgBeamAnalyzer.getPropertiesFile(directory,
                ImgBeamAnalyzer.HISTOGRAMM_CHART_PROPERTIES);

        File profileViewer1ChartPropertiesFile = ImgBeamAnalyzer.getPropertiesFile(directory,
                ImgBeamAnalyzer.PROFILE1_CHART_PROPERTIES);

        File profileViewer2ChartPropertiesFile = ImgBeamAnalyzer.getPropertiesFile(directory,
                ImgBeamAnalyzer.PROFILE2_CHART_PROPERTIES);

        File profileViewer3ChartPropertiesFile = ImgBeamAnalyzer.getPropertiesFile(directory,
                ImgBeamAnalyzer.PROFILE3_CHART_PROPERTIES);

        File inputImagePropertiesFile = ImgBeamAnalyzer.getPropertiesFile(directory,
                ImgBeamAnalyzer.INPUT_IMAGE_PROPERTIES);

        File roiImagePropertiesFile = ImgBeamAnalyzer.getPropertiesFile(directory,
                ImgBeamAnalyzer.ROI_IMAGE_PROPERTIES);

        File grabberImagePropertiesFile = ImgBeamAnalyzer.getPropertiesFile(directory,
                ImgBeamAnalyzer.GRABBER_IMAGE_PROPERTIES);

        writePropertiesInFile(histogrammChartPropertiesXml, histogrammChartPropertiesFile);
        writePropertiesInFile(profileViewer1ChartPropertiesXml, profileViewer1ChartPropertiesFile);
        writePropertiesInFile(profileViewer2ChartPropertiesXml, profileViewer2ChartPropertiesFile);
        writePropertiesInFile(profileViewer3ChartPropertiesXml, profileViewer3ChartPropertiesFile);
        writePropertiesInFile(analyzerInputImagePropertiesXml, inputImagePropertiesFile);
        writePropertiesInFile(analyzerRoiImagePropertiesXml, roiImagePropertiesFile);
        writePropertiesInFile(grabberImagePropertiesXml, grabberImagePropertiesFile);
    }

    /**
     * Write String properties unto file
     * 
     * @param propertiesString XML String properties
     * @param file
     */
    private void writePropertiesInFile(String propertiesString, File file) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(propertiesString);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    public static void main(String[] args) {
        final Charleston charleston = new Charleston(args);
        // Reactivate following code if you need to debug threads
        // AbstractAction startAction = new AbstractAction("Start") {
        //
        // private static final long serialVersionUID = -6093010254772883331L;
        //
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // charleston.getAnalyzer().start();
        // }
        // };
        // AbstractAction stopAction = new AbstractAction("Stop") {
        //
        // private static final long serialVersionUID = 281560338686175777L;
        //
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // charleston.getAnalyzer().stop();
        // }
        // };
        // AbstractAction printAction = new AbstractAction("print buffer") {
        //
        // private static final long serialVersionUID = 7403963318782309108L;
        //
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // TangoDataSourceFactory factory = (TangoDataSourceFactory) DataSourceProducerProvider
        // .getProducerByClassName(TangoDataSourceFactory.class.getName());
        // factory.printRefreshedSources();
        // }
        // };
        // charleston.getJJMenuBar().add(new JMenuItem(startAction));
        // charleston.getJJMenuBar().add(new JMenuItem(stopAction));
        // charleston.getJJMenuBar().add(new JMenuItem(printAction));

        // String path_analyzer = null;
        String option = null;
        int mode = ImgBeamAnalyzer.LIGHT_MODE;

        // if (args == null || args.length <= 0) {
        // String data = null;
        // data = (String) JOptionPane
        // .showInputDialog(
        // charleston,
        // "Please enter a device path. Optional mode grabber or advanced",
        // "Missing information", JOptionPane.QUESTION_MESSAGE);
        //
        // if (data == null || data.length() <= 0) {
        // System.exit(EXIT_ON_CLOSE);
        // } else {
        // String[] dataTab = data.split(" ");
        // charleston.setPath_analyzer(dataTab[0]);
        // // path_analyzer = dataTab[0];
        // if (dataTab.length > 1) {
        // option = dataTab[1];
        // }
        // }
        // }
        final String analyzerName;
        if (args.length > 0) {
            analyzerName = args[0];
            if (args.length > 1) {
                option = args[1];
            }

        } else {
            // analyzer may be stored in preferences
            String tmp = charleston.getAnalyzer().getModel();
            if ((tmp == null) || (tmp.trim().isEmpty())) {
                String data = JOptionPane.showInputDialog(charleston,
                        "Please enter a device path. Optional mode grabber or advanced", "Missing information",
                        JOptionPane.QUESTION_MESSAGE);

                if (data == null || data.trim().isEmpty()) {
                    analyzerName = null;
                } else {
                    String[] dataTab = data.split(" ");
                    analyzerName = dataTab[0];
                    if (dataTab.length > 1) {
                        option = dataTab[1];
                    }
                }
            } else {
                analyzerName = tmp;
            }
        }
        if (analyzerName == null || analyzerName.trim().isEmpty()) {
            System.exit(EXIT_ON_CLOSE);
        } else {
            if (option != null) {
                option = option.trim().toLowerCase();
                if ("grabber".equals(option)) {
                    mode = ImgBeamAnalyzer.GRABBER_CONFIGURATION_MODE;
                } else if ("advanced".equals(option)) {
                    mode = ImgBeamAnalyzer.FULL_MODE;
                }
            }
            charleston.getAnalyzer().setMode(mode, false);

            charleston.getAnalyzer().getMainTabbedPane().setEnabled(true);
            charleston.getAnalyzer().getImageSpectrumTabbedPane().setEnabled(true);

            charleston.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
            charleston.setVisible(true);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    charleston.getDoubleProgressSplash().setVisible(true);
                    charleston.getDoubleProgressSplash().setAlwaysOnTop(true);
                    charleston.getAnalyzer().setModel(analyzerName);
                    charleston.getAnalyzer().start();
                }
            });
        }
    }

    @Override
    protected Image getDefaultIconImage() {
        return SMALL_ICON.getImage();
    }

    @Override
    protected String getApplicationTitle() {
        if (version == null) {
            version = new StringBuilder(APPLICATION_BUNDLE.getString("project.name")).append(" ")
                    .append(APPLICATION_BUNDLE.getString("project.version")).append(" - ")
                    .append(APPLICATION_BUNDLE.getString("build.date")).toString().trim();
        }
        return version;
    }

    @Override
    protected String getCopyright() {
        return COPYRIGHT;
    }

    @Override
    protected Color getSplashForeground() {
        return Color.BLACK;
    }

    @Override
    protected ImageIcon getSplashImage() {
        return SPLASH_ICON;
    }
}
