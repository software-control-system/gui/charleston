package fr.soleil.charleston.docking.perspective;

import fr.soleil.charleston.docking.infonode.DockingType;
import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.Perspective;
import fr.soleil.docking.perspective.PerspectiveFactory;

/**
 * Charleston specific perspective factory
 * 
 * @author guerre-giordano
 * 
 */
public class CharlestonPerspectiveFactory extends PerspectiveFactory {

    public static final String FULL_MODE_PERSPECTIVE = "FULL_MODE";
    public static final String GENERAL_TAB_PERSPECTIVE = "INNER";
    public static final String GRABBER_CONF_MODE_PERSPECTIVE = "GRABBER_CONF_MODE";
    public static final String IMAGE_MGMT_MAIN_PERSPECTIVE = "IMAGE_MGMT_MAIN";
    public static final String LIGHT_MODE_PERSPECTIVE = "LIGHT_MODE";
    public static final String GAUSSIAN_PERSPECTIVE = "GAUSSIAN";
    public static final String GRABBER_PERSPECTIVE = "GRABBER";
    public static final String ADVANCED_PERSPECTIVE = "ADVANCED";
    public static final String IMAGE_PROFILE_PERSPECTIVE = "IMAGE_PROFILE_ATTRIBUTES";
//    public static final String ADVANCED_GLOBAL_PROFILE_PERSPECTIVE = "ADVANCED_GLOBAL_PROFILE";

    private static final String FULL_MODE_FILE_NAME = "full_mode.dock";
    private static final String GENERAL_TAB_FILE_NAME = "inner.dock";
    private static final String GRABBER_MODE_FILE_NAME = "grabber_mode.dock";
    private static final String IMG_MGMT_MAIN_FILE_NAME = "image_mgmt_main.dock";
    private static final String LIGHT_MODE_FILE_NAME = "light_mode.dock";
    public static final String GAUSSIAN_FILE_NAME = "gaussian.dock";
    public static final String GRABBER_FILE_NAME = "grabber.dock";
    public static final String ADVANCED_FILE_NAME = "advanced.dock";
    public static final String IMAGE_PROFILE_FILE_NAME = "image_profile.dock";
//    public static final String ADVANCED_GLOBAL_PROFILE_FILE_NAME = "advanced_global_profile.dock";

    private final IPerspective lightModePerspective;
    private final IPerspective grabberConfPerspective;
    private final IPerspective fullModePerspective;
    private final IPerspective innerPerspective;
    private final IPerspective imageMgmtMainPerspective;
    private final IPerspective gaussianPerspective;
    private final IPerspective grabberPerspective;
    private final IPerspective advancedPerspective;
    private final IPerspective imageProfilePerspective;

//    private final IPerspective advancedGlobalProfilePerspective;

    /**
     * Constructor
     * 
     * @param dockingType INFONODE or VL
     */
    public CharlestonPerspectiveFactory(DockingType dockingType) {
        super(new Perspective(LIGHT_MODE_PERSPECTIVE));
        lightModePerspective = getSelectedPerspective();
        grabberConfPerspective = new Perspective(GRABBER_CONF_MODE_PERSPECTIVE);
        fullModePerspective = new Perspective(FULL_MODE_PERSPECTIVE);
        innerPerspective = new Perspective(GENERAL_TAB_PERSPECTIVE);
        imageMgmtMainPerspective = new Perspective(IMAGE_MGMT_MAIN_PERSPECTIVE);
        gaussianPerspective = new Perspective(GAUSSIAN_PERSPECTIVE);
        grabberPerspective = new Perspective(GRABBER_PERSPECTIVE);
        advancedPerspective = new Perspective(ADVANCED_PERSPECTIVE);
        imageProfilePerspective = new Perspective(IMAGE_PROFILE_PERSPECTIVE);
//        advancedGlobalProfilePerspective = new Perspective(ADVANCED_GLOBAL_PROFILE_PERSPECTIVE);
        this.add(lightModePerspective);
        this.add(grabberConfPerspective);
        this.add(fullModePerspective);
        this.add(innerPerspective);
        this.add(imageMgmtMainPerspective);
        this.add(gaussianPerspective);
        this.add(grabberPerspective);
        this.add(advancedPerspective);
        this.add(imageProfilePerspective);
//        this.add(advancedGlobalProfilePerspective);
    }

    @Override
    public void setSelectedPerspective(String perspective) {
        IPerspective selectedPerspective = null;
        if (LIGHT_MODE_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = lightModePerspective;
        } else if (GRABBER_CONF_MODE_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = grabberConfPerspective;
        } else if (FULL_MODE_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = fullModePerspective;
        } else if (GENERAL_TAB_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = innerPerspective;
        } else if (IMAGE_MGMT_MAIN_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = imageMgmtMainPerspective;
        } else if (GAUSSIAN_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = gaussianPerspective;
        } else if (GRABBER_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = grabberPerspective;
        } else if (ADVANCED_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = advancedPerspective;
        } else if (IMAGE_PROFILE_PERSPECTIVE.equalsIgnoreCase(perspective)) {
            selectedPerspective = imageProfilePerspective;
//        } else if (ADVANCED_GLOBAL_PROFILE_PERSPECTIVE.equalsIgnoreCase(perspective)) {
//            selectedPerspective = advancedGlobalProfilePerspective;
        } else {
            selectedPerspective = getSelectedPerspective();
        }
        setSelectedPerspective(selectedPerspective);
    }

    @Override
    public IPerspective getDefault() {
        return new Perspective(LIGHT_MODE_PERSPECTIVE);
    }

    @Override
    public IPerspective createPerspective(String name) {
        IPerspective perspective;
        if (name.equalsIgnoreCase(LIGHT_MODE_PERSPECTIVE)) {
            perspective = lightModePerspective;
        } else if (GRABBER_CONF_MODE_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = grabberConfPerspective;
        } else if (FULL_MODE_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = fullModePerspective;
        } else if (GENERAL_TAB_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = innerPerspective;
        } else if (IMAGE_MGMT_MAIN_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = imageMgmtMainPerspective;
        } else if (GAUSSIAN_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = gaussianPerspective;
        } else if (GRABBER_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = grabberPerspective;
        } else if (ADVANCED_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = advancedPerspective;
        } else if (IMAGE_PROFILE_PERSPECTIVE.equalsIgnoreCase(name)) {
            perspective = imageProfilePerspective;
//        } else if (ADVANCED_GLOBAL_PROFILE_PERSPECTIVE.equalsIgnoreCase(name)) {
//            perspective = advancedGlobalProfilePerspective;
        } else {
            perspective = lightModePerspective;
        }
        return perspective;
    }

    /**
     * Returns the docking file name according to the perspective name
     * 
     * @param name
     * @return String
     */
    public static String getFileName(String name) {
        String fileName;
        if (name.equalsIgnoreCase(LIGHT_MODE_PERSPECTIVE)) {
            fileName = LIGHT_MODE_FILE_NAME;
        } else if (GRABBER_CONF_MODE_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = GRABBER_MODE_FILE_NAME;
        } else if (FULL_MODE_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = FULL_MODE_FILE_NAME;
        } else if (GENERAL_TAB_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = GENERAL_TAB_FILE_NAME;
        } else if (IMAGE_MGMT_MAIN_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = IMG_MGMT_MAIN_FILE_NAME;
        } else if (GAUSSIAN_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = GAUSSIAN_FILE_NAME;
        } else if (GRABBER_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = GRABBER_FILE_NAME;
        } else if (ADVANCED_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = ADVANCED_FILE_NAME;
        } else if (IMAGE_PROFILE_PERSPECTIVE.equalsIgnoreCase(name)) {
            fileName = IMAGE_PROFILE_FILE_NAME;
//        } else if (ADVANCED_GLOBAL_PROFILE_PERSPECTIVE.equalsIgnoreCase(name)) {
//            fileName = ADVANCED_GLOBAL_PROFILE_FILE_NAME;
        } else {
            fileName = null;
        }
        return fileName;
    }

    /**
     * Returns the resource package name where default docking files are stored
     * 
     * @return String
     */
    public static String getPerspectivePackage() {
        String path = "/fr/soleil/charleston/resources/";
        return path;
    }

    public IPerspective getInnerPerspective() {
        return innerPerspective;
    }

    public IPerspective getImageMgmtMainPerspective() {
        return imageMgmtMainPerspective;
    }

    public IPerspective getFullModePerspective() {
        return fullModePerspective;
    }

    public IPerspective getGaussianPerspective() {
        return gaussianPerspective;
    }

    public IPerspective getGrabberPerspective() {
        return grabberPerspective;
    }

    public IPerspective getAdvancedPerspective() {
        return advancedPerspective;
    }

    public IPerspective getImageProfilePerspective() {
        return imageProfilePerspective;
    }

//    public IPerspective getAdvancedGlobalProfilePerspective() {
//        return advancedGlobalProfilePerspective;
//    }

}
