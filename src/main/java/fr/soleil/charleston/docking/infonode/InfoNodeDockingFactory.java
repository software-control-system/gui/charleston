package fr.soleil.charleston.docking.infonode;

import fr.soleil.charleston.docking.DockingFactory;
import fr.soleil.charleston.docking.perspective.CharlestonPerspectiveFactory;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.infonode.InfoNodeDockingManager;
import fr.soleil.docking.infonode.view.InfoNodeViewFactory;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.docking.view.IViewFactory;

/**
 * Infonode docking objects factory
 * 
 * @author guerre-giordano
 * 
 */
public class InfoNodeDockingFactory implements DockingFactory {

    @Override
    public IViewFactory createViewFactory() {
        return new InfoNodeViewFactory();
    }

    @Override
    public CharlestonPerspectiveFactory createPerspectiveFactory() {
        return new CharlestonPerspectiveFactory(DockingType.INFONODE);
    }

    @Override
    public ADockingManager createDockingManager(IViewFactory viewFactory, IPerspectiveFactory perspectiveFactory) {
        return new InfoNodeDockingManager(viewFactory, perspectiveFactory);
    }

    @Override
    public ADockingManager createDockingManager(IViewFactory viewFactory) {
        return new InfoNodeDockingManager(viewFactory);
    }

}
