package fr.soleil.charleston.docking.infonode;

/**
 * Indicates whether VL or Infonode library is used to setup docking functionalities
 * 
 * @author guerre-giordano
 * 
 */
public enum DockingType {
    INFONODE("INFONODE"), VL("VL");

    private String name = "";

    /**
     * Constructor
     * 
     * @param name String
     */
    DockingType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
