package fr.soleil.charleston.docking.infonode;

import fr.soleil.charleston.docking.DockingFactory;
import fr.soleil.charleston.docking.perspective.CharlestonPerspectiveFactory;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.docking.view.IViewFactory;
import fr.soleil.docking.vl.VlDockDockingManager;
import fr.soleil.docking.vl.view.VlDockViewFactory;

/**
 * Vl docking objects factory
 * 
 * @author guerre-giordano
 * 
 */
public class VlDockingFactory implements DockingFactory {

    @Override
    public ADockingManager createDockingManager(IViewFactory viewFactory, IPerspectiveFactory perspectiveFactory) {
        return new VlDockDockingManager(viewFactory, perspectiveFactory);
    }

    @Override
    public IViewFactory createViewFactory() {
        return new VlDockViewFactory();
    }

    @Override
    public CharlestonPerspectiveFactory createPerspectiveFactory() {
        return new CharlestonPerspectiveFactory(DockingType.VL);
    }

    @Override
    public ADockingManager createDockingManager(IViewFactory viewFactory) {
        return new VlDockDockingManager(viewFactory);
    }

}
