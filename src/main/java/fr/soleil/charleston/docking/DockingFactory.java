package fr.soleil.charleston.docking;

import fr.soleil.charleston.docking.perspective.CharlestonPerspectiveFactory;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.perspective.IPerspectiveFactory;
import fr.soleil.docking.view.IViewFactory;

public interface DockingFactory {

    public ADockingManager createDockingManager(IViewFactory viewFactory, IPerspectiveFactory perspectiveFactory);

    public IViewFactory createViewFactory();

    public CharlestonPerspectiveFactory createPerspectiveFactory();

    public ADockingManager createDockingManager(IViewFactory viewFactory);

}
