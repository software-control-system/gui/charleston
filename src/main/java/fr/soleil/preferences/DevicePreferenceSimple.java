package fr.soleil.preferences;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Hashtable;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

/**
 * 
 * @author HARDION
 * 
 */
// TODO preference migration to Comete
public class DevicePreferenceSimple extends JPanel {

    private static final long serialVersionUID = -5691884220710312757L;

    private String model;
    protected JTableX table;
    private final TableModel tableModel;
    private ImgBeamAnalyzer analyzer;
    private String deviceName = "";
    private final static Logger LOGGER = LoggerFactory.getLogger(DevicePreferenceSimple.class.getName());
    private final String[] analyzersList;

    public DevicePreferenceSimple(ImgBeamAnalyzer analyzer) {
        super();
        this.analyzer = analyzer;
        this.model = analyzer.getModel();

        // CONTROLGUI-17
        // Get analyzer devices list
        Database database = TangoDeviceHelper.getDatabase();
        String[] tmpList = null;
        if (database != null) {
            try {
                tmpList = database.get_device_exported_for_class("ImgBeamAnalyzer");
            } catch (DevFailed e) {
                LOGGER.error(DevFailedUtils.toString(e));
            }
        }
        analyzersList = tmpList;

        setLayout(new BorderLayout());
        tableModel = new TableModel(model);
        table = new JTableX(tableModel, analyzersList);
        table.setRowSelectionAllowed(false);
        table.setColumnSelectionAllowed(false);
        table.setValueAt(model, 0, 1);

        tableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                TableModel tempModel = (TableModel) e.getSource();
                String name = (String) (tempModel).getValueAt(e.getFirstRow(), TableModel.ANALYZER_MODEL);

                if ("Analyzer".equalsIgnoreCase(name)) {
                    getAnalyzer().setModel((String) tempModel.getValueAt(e.getFirstRow(), TableModel.CHOOSE_DEVICE));

//                    String oldDeviceName = "";
//                    try {
//                    oldDeviceName = getAnalyzer().getModel();
                    deviceName = (String) tempModel.getValueAt(e.getFirstRow(), TableModel.CHOOSE_DEVICE);

//                        new DeviceProxy(deviceName);
                    getAnalyzer().setModel(deviceName);
                    // CONTROLGUI-18 Lorsqu'on modifie dans les préférences
                    // le nom du device Analyzer cela ne s'applique pas à l'imageur
//                        getAnalyzer().getGrabber().setModel(deviceName);
//                        getAnalyzer().setMainModels(true, true);

//                    } catch (DevFailed e1) {
//                        JOptionPane.showMessageDialog(null, "Device " + deviceName + " not defined in the database "
//                                + System.getProperty("TANGO_HOST"));
//
//                        tempModel.setValueAt(oldDeviceName, e.getFirstRow(), TableModel.CHOOSE_DEVICE);
//                        // getanalyzer().setModel(oldDeviceName);
//                    }
                    // getanalyzer().setModel(deviceName);
                }
            }
        });

        RowEditorModel rowModel = new RowEditorModel();
        table.setRowEditorModel(rowModel);
        JScrollPane jsp = new JScrollPane(table);
        Dimension preferredSize = new Dimension(350, 200);
        jsp.setPreferredSize(preferredSize);
        add(jsp, BorderLayout.CENTER);
    }

    public void validateTable() {
        if (table != null && table.getCellEditor() != null)
            table.getCellEditor().stopCellEditing();
    }

    /**
     * 
     * @return analyzer
     */
    public ImgBeamAnalyzer getAnalyzer() {
        return analyzer;
    }

    /**
     * 
     * @param bean
     */
    public void setAnalyzer(ImgBeamAnalyzer analyzer) {
        this.analyzer = analyzer;
        model = analyzer.getModel();
    }

    /**
     * @return Returns the model.
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model The model to set.
     */
    public void setModel(String model) {
        this.model = model;
        tableModel.setSalsaModel(model);
    }

    public Component getComponent() {
        return this;
    }

    public String getIconTitle() {
        return "Devices";
    }

    public String getIconUrl() {
        return "icons/devices32x32.png";
    }

    public String getTitle() {
        return "Devices Preferences";
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class TableModel extends AbstractTableModel {

        private static final long serialVersionUID = -341059867158893571L;

        public static final int ANALYZER_MODEL = 0;
        public static final int CHOOSE_DEVICE = 1;

        public final static String[] COLUMN_NAMES = { "Analyzer Model", "Choose Device" };

        public Object[][] values = { { "Analyzer", "" } };

        // FIN ALIKE GROUP EBO
        public TableModel(String model) {
            super();
        }

        public void setSalsaModel(String model) {
        }

        @Override
        public int getColumnCount() {
            return COLUMN_NAMES.length;
        }

        @Override
        public int getRowCount() {
            return values.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return values[rowIndex][columnIndex];
        }

        @Override
        public void setValueAt(Object aValue, int r, int c) {
            values[r][c] = aValue;
            fireTableCellUpdated(r, c);
        }

        @Override
        public String getColumnName(int column) {
            return COLUMN_NAMES[column];
        }

        @Override
        public boolean isCellEditable(int r, int c) {
            return c == CHOOSE_DEVICE;
        }

    }

    protected static class RowEditorModel {
        private final Hashtable<Integer, TableCellEditor> data;

        public RowEditorModel() {
            data = new Hashtable<Integer, TableCellEditor>();
        }

        public void addEditorForRow(int row, TableCellEditor e) {
            data.put(new Integer(row), e);
        }

        public void removeEditorForRow(int row) {
            data.remove(new Integer(row));
        }

        public TableCellEditor getEditor(int row) {
            return data.get(new Integer(row));
        }
    }

    protected static class JTableX extends JTable {

        private static final long serialVersionUID = 7435853663386916199L;

        protected RowEditorModel rowModel;
        private final String[] analyzersList;

        public JTableX(TableModel tableModel, String[] analyzersList) {
            super(tableModel);
            this.analyzersList = analyzersList;
        }

        public void setRowEditorModel(RowEditorModel rowModel) {
            this.rowModel = rowModel;
        }

        public RowEditorModel getRowEditorModel() {
            return rowModel;
        }

        @Override
        public TableCellEditor getCellEditor(int row, int col) {
            TableCellEditor editor = null;
            if (col == 1) {
                JComboBox<String> comboBox;
                if (analyzersList == null) {
                    comboBox = new JComboBox<>();
                } else {
                    comboBox = new JComboBox<>(analyzersList);
                }
                editor = new DefaultCellEditor(comboBox);
            } else if (rowModel != null) {
                editor = rowModel.getEditor(row);
            }
            if (editor == null) {
                editor = super.getCellEditor(row, col);
            }
            return editor;
        }
    }

}
