package fr.soleil.preferences;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UnsupportedLookAndFeelException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.imgbeamanalyzer.ImgBeamAnalyzer;

/**
 * 
 * @author HARDION
 * 
 */
public class PreferencesFrame extends JFrame {

    private static final long serialVersionUID = -3584695646813354599L;

    private JPanel jContentPane;
    private ImgBeamAnalyzer analyzer;
    private JPanel prefsPanel;
    private DevicePreferenceSimple dp;

    private static PreferencesFrame instance;
    private final static Logger LOGGER = LoggerFactory.getLogger(PreferencesFrame.class.getName());

    /**
     * This is the default constructor
     */
    public PreferencesFrame() throws Exception {
        super();
        initialize();
    }

    public static PreferencesFrame getInstance(ImgBeamAnalyzer analyzer) {
        if (instance == null) {
            try {
                instance = new PreferencesFrame(analyzer);
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                // e.printStackTrace();
            }
        }
        return instance;
    }

    /**
     * 
     * @param bean
     * @throws Exception
     */
    public PreferencesFrame(ImgBeamAnalyzer analyzer) throws Exception {
        super();
        initialize();
        setBean(analyzer);
    }

    /**
     * This method initializes this
     * 
     * @return void
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    private void initialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
            UnsupportedLookAndFeelException {
        // Dimension preferredSize = new Dimension(300, 300);
        // this.setPreferredSize(preferredSize);
        this.setContentPane(getJContentPane());
        this.setTitle("Preferences");

        getContentPane().setLayout(new BorderLayout());
        prefsPanel = new JPanel();
        getContentPane().add("Center", prefsPanel);
    }

    /**
     * This method initializes jContentPane
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getJContentPane() {
        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BorderLayout());
        }
        return jContentPane;
    }

    public void setBean(ImgBeamAnalyzer analyzer) {
        this.analyzer = analyzer;
        dp = new DevicePreferenceSimple(analyzer);
        // dp.setModel(analyzer.getLPSModel());
        dp.setModel(analyzer.getModel());
        prefsPanel.add(dp);
        pack();
    }

    /**
     * @return Returns the model.
     */
    public ImgBeamAnalyzer getControlBean() {
        return analyzer;
    }

    /**
     * @param analyzer The model to set.
     */
    public void setControlBean(ImgBeamAnalyzer analyzer) {
        this.analyzer = analyzer;
    }

    // /**
    // *
    // * @return Diff4cJPanel
    // */
    // public Diff4cJPanel getDiff4cJPanel() {
    // return diff4cJPanel;
    // }
    //
    // /**
    // *
    // * @param bean
    // */
    // public void setDiff4cJPanel(Diff4cJPanel bean) {
    // diff4cJPanel = bean;
    // }
}
