package fr.soleil.charleston;

import java.awt.GraphicsEnvironment;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.junit.Ignore;

import fr.soleil.bean.imggrabber.ImgGrabber;
import fr.soleil.lib.project.progress.IProgressable;
import fr.soleil.lib.project.progress.IProgressionListener;
import fr.soleil.lib.project.swing.DoubleProgressSplash;

/**
 * Class to display grabber bean
 * 
 * @author GIRARDOT
 */
@Ignore
public class GrabberTester extends JFrame implements IProgressionListener {

    private static final long serialVersionUID = 4426358446308186516L;

    protected ImgGrabber grabber;
    protected DoubleProgressSplash splash;

    public GrabberTester() {
        super();
        grabber = null;
        splash = null;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        String version = "Grabber Tester";
        setTitle(version);
        setContentPane(getGrabber());

        getGrabber().addProgressionListener(this);
    }

    @Override
    public void progressionChanged(IProgressable source, int progression, String info) {
        if (source instanceof ImgGrabber) {
            splash.setSecondMessage("ImgGrabber: " + info);
            splash.setFGProgress(progression);
        }
        if (splash.getDoubleProgressBar().getFGValue() == splash.getDoubleProgressBar().getFGMaximum()) {
            splash.setVisible(false);
        }
        if (splash.isVisible()) {
            splash.repaint();
        }
    }

    public ImgGrabber getGrabber() {
        if (grabber == null) {
            grabber = new ImgGrabber(null);
        }
        return grabber;
    }

    public DoubleProgressSplash getSplash() {
        if (splash == null) {
            splash = new DoubleProgressSplash();
            splash.setTitle("Charleston");
            splash.getSplashPanel().getDoubleProgressBar().setFGName("ImgBeamAnalyzer");
            splash.getSplashPanel().getDoubleProgressBar().setBGName("ImgGrabber");
            splash.setFirstMessage("Connecting to ImgGrabber...");
            splash.setFGMaxProgress(getGrabber().getMaxProgression());
            splash.setBGMaxProgress(0);
        }
        return splash;
    }

    public static void main(String[] args) {

        final GrabberTester grabberTester = new GrabberTester();
        String path_grabber = null;
        if (args != null && args.length > 0) {
            path_grabber = args[0];
        }
        final String grabberName = path_grabber;
        grabberTester.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
        grabberTester.setVisible(true);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            // ignore;
        }
        grabberTester.getSplash().setVisible(true);
        grabberTester.getSplash().setAlwaysOnTop(true);
        grabberTester.getGrabber().setModel(grabberName);
    }
}
